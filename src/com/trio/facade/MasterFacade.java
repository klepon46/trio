package com.trio.facade;

import com.trio.dao.bsc.AhmdsbscDtlsettingDao;
import com.trio.dao.bsc.AhmdsbscHdrsettingDao;
import com.trio.dao.bsc.DbaRolePrivsDao;
import com.trio.dao.h000.HondaH000DealersDao;
import com.trio.dao.h000.HondaH000GatesDao;
import com.trio.dao.h000.HondaH000ItemsDao;
import com.trio.dao.h000.HondaH000PropinsDao;
import com.trio.dao.h000.HondaH000SetdlrsDao;
import com.trio.dao.h000.HondaH000SetupsDao;
import com.trio.dao.h000.MasterRunnumDao;
import com.trio.dao.h000.TrioH000MstmenuDao;
import com.trio.dao.h000.TrioH000MstroleaccessDao;
import com.trio.dao.h100.HondaH100ArsDao;
import com.trio.dao.h100.HondaH100CustomersDao;
import com.trio.dao.h100.HondaH100DtlfakdosDao;
import com.trio.dao.h100.HondaH100DtlinvsDao;
import com.trio.dao.h100.HondaH100DtlpicklistsDao;
import com.trio.dao.h100.HondaH100DtlpodlrfixesDao;
import com.trio.dao.h100.HondaH100DtlsublokasiDao;
import com.trio.dao.h100.HondaH100FakdosDao;
import com.trio.dao.h100.HondaH100GudangsDao;
import com.trio.dao.h100.HondaH100HargasDao;
import com.trio.dao.h100.HondaH100HdrpicklistsDao;
import com.trio.dao.h100.HondaH100HdrsublokasiDao;
import com.trio.dao.h100.HondaH100ItemcustsDao;
import com.trio.dao.h100.HondaH100MstpodlrsDao;
import com.trio.dao.h100.HondaH100StdetailsDao;
import com.trio.dao.h100.HondaH100StglobalsDao;
import com.trio.dao.h100.TrioH100ArsDao;
import com.trio.dao.h100.TrioH100BundlingaksDao;
import com.trio.dao.h100.TrioH100DlrposDao;
import com.trio.dao.h100.TrioH100DologsDao;
import com.trio.dao.h100.TrioH100DtlAreaDealersDao;
import com.trio.dao.h100.TrioH100DtlHargasDao;
import com.trio.dao.h100.TrioH100DtlarsDao;
import com.trio.dao.h100.TrioH100DtlbundlingaksDao;
import com.trio.dao.h100.TrioH100DtldologsDao;
import com.trio.dao.h100.TrioH100DtlpktaksItemDao;
import com.trio.dao.h100.TrioH100DtlpktaksPartDao;
import com.trio.dao.h100.TrioH100KecamatansDao;
import com.trio.dao.h100.TrioH100KelurahansDao;
import com.trio.dao.h100.TrioH100PelangWilsDao;
import com.trio.dao.h100.TrioH100PktaksDao;
import com.trio.dao.h100.TrioH100StdetailsDao;
import com.trio.dao.h300.HondaH300AhmFaktursDao;
import com.trio.dao.h300.HondaH300AhmpsesDao;
import com.trio.dao.h300.HondaH300DetInoutManualsDao;
import com.trio.dao.h300.HondaH300InoutManualsDao;
import com.trio.dao.h300.HondaH300MasterPartsDao;
import com.trio.dao.h300.HondaH300PartLokasesDao;
import com.trio.dao.h300.HondaH300SalesmanDao;
import com.trio.dao.h300.TrioH300DtlProgramsDao;
import com.trio.dao.h300.TrioH300HdrHadiahsDao;
import com.trio.dao.h300.TrioH300HdrProgramsDao;
import com.trio.spring.util.TrioGlobalTransactionDao;

public class MasterFacade {
	
	private TrioH100DlrposDao trioH100DlrposDao;
	
	private HondaH000PropinsDao hondaH000PropinsDao;
	
	private HondaH000GatesDao hondaH000GatesDao;
	
	private HondaH000DealersDao hondaH000DealersDao;
	
	private TrioH100DtlAreaDealersDao trioH100DtlAreaDealersDao;
	
	private TrioH100KecamatansDao trioH100KecamatansDao;
	
	private TrioH100KelurahansDao trioH100KelurahansDao;
	
	private HondaH100CustomersDao hondaH100CustomersDao;
	
	private TrioH100PelangWilsDao trioH100PelangWilsDao;
	
	private TrioH100StdetailsDao trioH100StdetailsDao;
	
	private HondaH000SetupsDao hondaH000SetupsDao;
	
	private MasterRunnumDao masterRunnumDao;
	
	private HondaH100HdrpicklistsDao hondaH100HdrpicklistsDao;
	
	private HondaH100DtlpicklistsDao hondaH100DtlpicklistsDao;
	
	private HondaH000ItemsDao hondaH000ItemsDao;
	
	private HondaH100FakdosDao hondaH100FakdosDao;
	
	private HondaH100DtlfakdosDao hondaH100DtlfakdosDao;
	
	private HondaH100GudangsDao hondaH100GudangsDao;
	
	private HondaH100HdrsublokasiDao hondaH100HdrsublokasiDao;
	
	private HondaH100DtlsublokasiDao hondaH100DtlsublokasiDao;
	
	private HondaH100StdetailsDao hondaH100StdetailsDao;
	
	private DbaRolePrivsDao dbaRolePrivsDao;
	
	private HondaH100MstpodlrsDao hondaH100MstpodlrsDao;
	
	private HondaH100DtlpodlrfixesDao hondaH100DtlpodlrfixesDao;
	
	private TrioH100DologsDao trioH100DologsDao;
	
	private TrioH100DtldologsDao trioH100DtldologsDao;
	
	private HondaH100StglobalsDao hondaH100StglobalsDao;
	
	private HondaH100ArsDao hondaH100ArsDao;
	
	private HondaH000SetdlrsDao hondaH000SetdlrsDao;
	
	private HondaH300SalesmanDao hondaH300SalesmanDao;
	
	private HondaH100HargasDao hondaH100HargasDao;
	
	private AhmdsbscHdrsettingDao ahmdsbscHdrsettingDao;
	
	private AhmdsbscDtlsettingDao ahmdsbscDtlsettingDao;
	
	private TrioH000MstmenuDao trioH000MstmenuDao;
	
	private TrioH000MstroleaccessDao trioH000MstroleaccessDao;
	
	private TrioH100ArsDao trioH100ArsDao;
	
	private TrioH100DtlarsDao trioH100DtlarsDao;
	
	private HondaH100DtlinvsDao hondaH100DtlinvsDao;
	
	private HondaH100ItemcustsDao hondaH100ItemcustsDao;
	
	private HondaH300MasterPartsDao hondaH300MasterPartsDao;
	
	private TrioH300HdrProgramsDao trioH300HdrProgramsDao;
	
	private TrioH300DtlProgramsDao trioH300DtlProgramsDao;
	
	private TrioH300HdrHadiahsDao  trioH300HdrHadiahsDao;
	
	private TrioH100DtlHargasDao trioH100DtlHargasDao;
	
	private TrioH100PktaksDao trioH100PktaksDao;
	
	private TrioH100DtlpktaksPartDao trioH100DtlpktaksPartDao;
	
	private TrioH100DtlpktaksItemDao trioH100DtlpktaksItemDao;
	
	private TrioH100BundlingaksDao trioH100BundlingaksDao;
	
	private TrioH100DtlbundlingaksDao trioH100DtlbundlingaksDao;
	
	private HondaH300PartLokasesDao hondaH300PartLokasesDao;
	
	private HondaH300InoutManualsDao hondaH300InoutManualsDao;
	
	private HondaH300DetInoutManualsDao hondaH300DetInoutManualsDao;
	
	private HondaH300AhmFaktursDao hondaH300AhmFaktursDao;
	
	private HondaH300AhmpsesDao hondaH300AhmpsesDao;
	
	private TrioGlobalTransactionDao trioGlobalTransactionDao;
	
	public TrioH100DlrposDao getTrioH100DlrposDao() {
		return trioH100DlrposDao;
	}

	public void setTrioH100DlrposDao(TrioH100DlrposDao trioH100DlrposDao) {
		this.trioH100DlrposDao = trioH100DlrposDao;
	}

	public HondaH000PropinsDao getHondaH000PropinsDao() {
		return hondaH000PropinsDao;
	}

	public void setHondaH000PropinsDao(HondaH000PropinsDao hondaH000PropinsDao) {
		this.hondaH000PropinsDao = hondaH000PropinsDao;
	}

	public TrioH000MstroleaccessDao getTrioH000MstroleaccessDao() {
		return trioH000MstroleaccessDao;
	}

	public void setTrioH000MstroleaccessDao(
			TrioH000MstroleaccessDao trioH000MstroleaccessDao) {
		this.trioH000MstroleaccessDao = trioH000MstroleaccessDao;
	}

	public TrioH000MstmenuDao getTrioH000MstmenuDao() {
		return trioH000MstmenuDao;
	}

	public void setTrioH000MstmenuDao(TrioH000MstmenuDao trioH000MstmenuDao) {
		this.trioH000MstmenuDao = trioH000MstmenuDao;
	}

	public AhmdsbscHdrsettingDao getAhmdsbscHdrsettingDao() {
		return ahmdsbscHdrsettingDao;
	}

	public void setAhmdsbscHdrsettingDao(AhmdsbscHdrsettingDao ahmdsbscHdrsettingDao) {
		this.ahmdsbscHdrsettingDao = ahmdsbscHdrsettingDao;
	}

	public AhmdsbscDtlsettingDao getAhmdsbscDtlsettingDao() {
		return ahmdsbscDtlsettingDao;
	}

	public void setAhmdsbscDtlsettingDao(AhmdsbscDtlsettingDao ahmdsbscDtlsettingDao) {
		this.ahmdsbscDtlsettingDao = ahmdsbscDtlsettingDao;
	}

	public HondaH000GatesDao getHondaH000GatesDao() {
		return hondaH000GatesDao;
	}

	public void setHondaH000GatesDao(HondaH000GatesDao hondaH000GatesDao) {
		this.hondaH000GatesDao = hondaH000GatesDao;
	}

	public HondaH000DealersDao getHondaH000DealersDao() {
		return hondaH000DealersDao;
	}

	public void setHondaH000DealersDao(HondaH000DealersDao hondaH000DealersDao) {
		this.hondaH000DealersDao = hondaH000DealersDao;
	}

	public TrioH100DtlAreaDealersDao getTrioH100DtlAreaDealersDao() {
		return trioH100DtlAreaDealersDao;
	}

	public void setTrioH100DtlAreaDealersDao(
			TrioH100DtlAreaDealersDao trioH100DtlAreaDealersDao) {
		this.trioH100DtlAreaDealersDao = trioH100DtlAreaDealersDao;
	}

	public TrioH100KecamatansDao getTrioH100KecamatansDao() {
		return trioH100KecamatansDao;
	}

	public void setTrioH100KecamatansDao(TrioH100KecamatansDao trioH100KecamatansDao) {
		this.trioH100KecamatansDao = trioH100KecamatansDao;
	}

	public TrioH100KelurahansDao getTrioH100KelurahansDao() {
		return trioH100KelurahansDao;
	}

	public void setTrioH100KelurahansDao(TrioH100KelurahansDao trioH100KelurahansDao) {
		this.trioH100KelurahansDao = trioH100KelurahansDao;
	}

	public HondaH100CustomersDao getHondaH100CustomersDao() {
		return hondaH100CustomersDao;
	}

	public void setHondaH100CustomersDao(HondaH100CustomersDao hondaH100CustomersDao) {
		this.hondaH100CustomersDao = hondaH100CustomersDao;
	}

	public TrioH100PelangWilsDao getTrioH100PelangWilsDao() {
		return trioH100PelangWilsDao;
	}

	public void setTrioH100PelangWilsDao(TrioH100PelangWilsDao trioH100PelangWilsDao) {
		this.trioH100PelangWilsDao = trioH100PelangWilsDao;
	}

	public TrioH100StdetailsDao getTrioH100StdetailsDao() {
		return trioH100StdetailsDao;
	}

	public void setTrioH100StdetailsDao(TrioH100StdetailsDao trioH100StdetailsDao) {
		this.trioH100StdetailsDao = trioH100StdetailsDao;
	}

	public HondaH000SetupsDao getHondaH000SetupsDao() {
		return hondaH000SetupsDao;
	}

	public void setHondaH000SetupsDao(HondaH000SetupsDao hondaH000SetupsDao) {
		this.hondaH000SetupsDao = hondaH000SetupsDao;
	}

	public MasterRunnumDao getMasterRunnumDao() {
		return masterRunnumDao;
	}

	public void setMasterRunnumDao(MasterRunnumDao masterRunnumDao) {
		this.masterRunnumDao = masterRunnumDao;
	}

	public HondaH100HdrpicklistsDao getHondaH100HdrpicklistsDao() {
		return hondaH100HdrpicklistsDao;
	}

	public void setHondaH100HdrpicklistsDao(
			HondaH100HdrpicklistsDao hondaH100HdrpicklistsDao) {
		this.hondaH100HdrpicklistsDao = hondaH100HdrpicklistsDao;
	}

	public HondaH100DtlpicklistsDao getHondaH100DtlpicklistsDao() {
		return hondaH100DtlpicklistsDao;
	}

	public void setHondaH100DtlpicklistsDao(
			HondaH100DtlpicklistsDao hondaH100DtlpicklistsDao) {
		this.hondaH100DtlpicklistsDao = hondaH100DtlpicklistsDao;
	}

	public HondaH100FakdosDao getHondaH100FakdosDao() {
		return hondaH100FakdosDao;
	}

	public void setHondaH100FakdosDao(HondaH100FakdosDao hondaH100FakdosDao) {
		this.hondaH100FakdosDao = hondaH100FakdosDao;
	}

	public HondaH100DtlfakdosDao getHondaH100DtlfakdosDao() {
		return hondaH100DtlfakdosDao;
	}

	public void setHondaH100DtlfakdosDao(HondaH100DtlfakdosDao hondaH100DtlfakdosDao) {
		this.hondaH100DtlfakdosDao = hondaH100DtlfakdosDao;
	}

	public HondaH100GudangsDao getHondaH100GudangsDao() {
		return hondaH100GudangsDao;
	}

	public void setHondaH100GudangsDao(HondaH100GudangsDao hondaH100GudangsDao) {
		this.hondaH100GudangsDao = hondaH100GudangsDao;
	}

	public HondaH100HdrsublokasiDao getHondaH100HdrsublokasiDao() {
		return hondaH100HdrsublokasiDao;
	}

	public void setHondaH100HdrsublokasiDao(
			HondaH100HdrsublokasiDao hondaH100HdrsublokasiDao) {
		this.hondaH100HdrsublokasiDao = hondaH100HdrsublokasiDao;
	}

	public HondaH100DtlsublokasiDao getHondaH100DtlsublokasiDao() {
		return hondaH100DtlsublokasiDao;
	}

	public void setHondaH100DtlsublokasiDao(
			HondaH100DtlsublokasiDao hondaH100DtlsublokasiDao) {
		this.hondaH100DtlsublokasiDao = hondaH100DtlsublokasiDao;
	}

	public HondaH000ItemsDao getHondaH000ItemsDao() {
		return hondaH000ItemsDao;
	}

	public void setHondaH000ItemsDao(HondaH000ItemsDao hondaH000ItemsDao) {
		this.hondaH000ItemsDao = hondaH000ItemsDao;
	}

	public HondaH100StdetailsDao getHondaH100StdetailsDao() {
		return hondaH100StdetailsDao;
	}

	public void setHondaH100StdetailsDao(HondaH100StdetailsDao hondaH100StdetailsDao) {
		this.hondaH100StdetailsDao = hondaH100StdetailsDao;
	}

	public DbaRolePrivsDao getDbaRolePrivsDao() {
		return dbaRolePrivsDao;
	}

	public void setDbaRolePrivsDao(DbaRolePrivsDao dbaRolePrivsDao) {
		this.dbaRolePrivsDao = dbaRolePrivsDao;
	}

	public HondaH100MstpodlrsDao getHondaH100MstpodlrsDao() {
		return hondaH100MstpodlrsDao;
	}

	public void setHondaH100MstpodlrsDao(HondaH100MstpodlrsDao hondaH100MstpodlrsDao) {
		this.hondaH100MstpodlrsDao = hondaH100MstpodlrsDao;
	}

	public HondaH100DtlpodlrfixesDao getHondaH100DtlpodlrfixesDao() {
		return hondaH100DtlpodlrfixesDao;
	}

	public void setHondaH100DtlpodlrfixesDao(
			HondaH100DtlpodlrfixesDao hondaH100DtlpodlrfixesDao) {
		this.hondaH100DtlpodlrfixesDao = hondaH100DtlpodlrfixesDao;
	}

	public TrioH100DologsDao getTrioH100DologsDao() {
		return trioH100DologsDao;
	}

	public void setTrioH100DologsDao(TrioH100DologsDao trioH100DologsDao) {
		this.trioH100DologsDao = trioH100DologsDao;
	}

	public TrioH100DtldologsDao getTrioH100DtldologsDao() {
		return trioH100DtldologsDao;
	}

	public void setTrioH100DtldologsDao(TrioH100DtldologsDao trioH100DtldologsDao) {
		this.trioH100DtldologsDao = trioH100DtldologsDao;
	}

	public HondaH100StglobalsDao getHondaH100StglobalsDao() {
		return hondaH100StglobalsDao;
	}

	public void setHondaH100StglobalsDao(HondaH100StglobalsDao hondaH100StglobalsDao) {
		this.hondaH100StglobalsDao = hondaH100StglobalsDao;
	}

	public HondaH100ArsDao getHondaH100ArsDao() {
		return hondaH100ArsDao;
	}

	public void setHondaH100ArsDao(HondaH100ArsDao hondaH100ArsDao) {
		this.hondaH100ArsDao = hondaH100ArsDao;
	}

	public HondaH000SetdlrsDao getHondaH000SetdlrsDao() {
		return hondaH000SetdlrsDao;
	}

	public void setHondaH000SetdlrsDao(HondaH000SetdlrsDao hondaH000SetdlrsDao) {
		this.hondaH000SetdlrsDao = hondaH000SetdlrsDao;
	}

	public HondaH300SalesmanDao getHondaH300SalesmanDao() {
		return hondaH300SalesmanDao;
	}

	public void setHondaH300SalesmanDao(HondaH300SalesmanDao hondaH300SalesmanDao) {
		this.hondaH300SalesmanDao = hondaH300SalesmanDao;
	}

	public HondaH100HargasDao getHondaH100HargasDao() {
		return hondaH100HargasDao;
	}

	public void setHondaH100HargasDao(HondaH100HargasDao hondaH100HargasDao) {
		this.hondaH100HargasDao = hondaH100HargasDao;
	}

	public TrioH100ArsDao getTrioH100ArsDao() {
		return trioH100ArsDao;
	}

	public void setTrioH100ArsDao(TrioH100ArsDao trioH100ArsDao) {
		this.trioH100ArsDao = trioH100ArsDao;
	}

	public TrioH100DtlarsDao getTrioH100DtlarsDao() {
		return trioH100DtlarsDao;
	}

	public void setTrioH100DtlarsDao(TrioH100DtlarsDao trioH100DtlarsDao) {
		this.trioH100DtlarsDao = trioH100DtlarsDao;
	}

	public HondaH100DtlinvsDao getHondaH100DtlinvsDao() {
		return hondaH100DtlinvsDao;
	}

	public void setHondaH100DtlinvsDao(HondaH100DtlinvsDao hondaH100DtlinvsDao) {
		this.hondaH100DtlinvsDao = hondaH100DtlinvsDao;
	}

	public HondaH100ItemcustsDao getHondaH100ItemcustsDao() {
		return hondaH100ItemcustsDao;
	}

	public void setHondaH100ItemcustsDao(HondaH100ItemcustsDao hondaH100ItemcustsDao) {
		this.hondaH100ItemcustsDao = hondaH100ItemcustsDao;
	}

	public TrioH300HdrProgramsDao getTrioH300HdrProgramsDao() {
		return trioH300HdrProgramsDao;
	}

	public void setTrioH300HdrProgramsDao(
			TrioH300HdrProgramsDao trioH300HdrProgramsDao) {
		this.trioH300HdrProgramsDao = trioH300HdrProgramsDao;
	}

	public TrioH300DtlProgramsDao getTrioH300DtlProgramsDao() {
		return trioH300DtlProgramsDao;
	}

	public void setTrioH300DtlProgramsDao(
			TrioH300DtlProgramsDao trioH300DtlProgramsDao) {
		this.trioH300DtlProgramsDao = trioH300DtlProgramsDao;
	}

	public TrioH300HdrHadiahsDao getTrioH300HdrHadiahsDao() {
		return trioH300HdrHadiahsDao;
	}

	public void setTrioH300HdrHadiahsDao(TrioH300HdrHadiahsDao trioH300HdrHadiahsDao) {
		this.trioH300HdrHadiahsDao = trioH300HdrHadiahsDao;
	}

	public TrioH100DtlHargasDao getTrioH100DtlHargasDao() {
		return trioH100DtlHargasDao;
	}

	public void setTrioH100DtlHargasDao(TrioH100DtlHargasDao trioH100DtlHargasDao) {
		this.trioH100DtlHargasDao = trioH100DtlHargasDao;
	}

	public TrioH100PktaksDao getTrioH100PktaksDao() {
		return trioH100PktaksDao;
	}

	public void setTrioH100PktaksDao(TrioH100PktaksDao trioH100PktaksDao) {
		this.trioH100PktaksDao = trioH100PktaksDao;
	}

	public TrioH100DtlpktaksPartDao getTrioH100DtlpktaksPartDao() {
		return trioH100DtlpktaksPartDao;
	}

	public void setTrioH100DtlpktaksPartDao(
			TrioH100DtlpktaksPartDao trioH100DtlpktaksPartDao) {
		this.trioH100DtlpktaksPartDao = trioH100DtlpktaksPartDao;
	}

	public TrioH100DtlpktaksItemDao getTrioH100DtlpktaksItemDao() {
		return trioH100DtlpktaksItemDao;
	}

	public void setTrioH100DtlpktaksItemDao(
			TrioH100DtlpktaksItemDao trioH100DtlpktaksItemDao) {
		this.trioH100DtlpktaksItemDao = trioH100DtlpktaksItemDao;
	}

	public HondaH300MasterPartsDao getHondaH300MasterPartsDao() {
		return hondaH300MasterPartsDao;
	}

	public void setHondaH300MasterPartsDao(
			HondaH300MasterPartsDao hondaH300MasterPartsDao) {
		this.hondaH300MasterPartsDao = hondaH300MasterPartsDao;
	}

	public TrioH100BundlingaksDao getTrioH100BundlingaksDao() {
		return trioH100BundlingaksDao;
	}

	public void setTrioH100BundlingaksDao(
			TrioH100BundlingaksDao trioH100BundlingaksDao) {
		this.trioH100BundlingaksDao = trioH100BundlingaksDao;
	}

	public TrioH100DtlbundlingaksDao getTrioH100DtlbundlingaksDao() {
		return trioH100DtlbundlingaksDao;
	}

	public void setTrioH100DtlbundlingaksDao(
			TrioH100DtlbundlingaksDao trioH100DtlbundlingaksDao) {
		this.trioH100DtlbundlingaksDao = trioH100DtlbundlingaksDao;
	}

	public HondaH300PartLokasesDao getHondaH300PartLokasesDao() {
		return hondaH300PartLokasesDao;
	}

	public void setHondaH300PartLokasesDao(
			HondaH300PartLokasesDao hondaH300PartLokasesDao) {
		this.hondaH300PartLokasesDao = hondaH300PartLokasesDao;
	}

	public HondaH300InoutManualsDao getHondaH300InoutManualsDao() {
		return hondaH300InoutManualsDao;
	}

	public void setHondaH300InoutManualsDao(
			HondaH300InoutManualsDao hondaH300InoutManualsDao) {
		this.hondaH300InoutManualsDao = hondaH300InoutManualsDao;
	}

	public HondaH300DetInoutManualsDao getHondaH300DetInoutManualsDao() {
		return hondaH300DetInoutManualsDao;
	}

	public void setHondaH300DetInoutManualsDao(
			HondaH300DetInoutManualsDao hondaH300DetInoutManualsDao) {
		this.hondaH300DetInoutManualsDao = hondaH300DetInoutManualsDao;
	}

	public HondaH300AhmFaktursDao getHondaH300AhmFaktursDao() {
		return hondaH300AhmFaktursDao;
	}

	public void setHondaH300AhmFaktursDao(
			HondaH300AhmFaktursDao hondaH300AhmFaktursDao) {
		this.hondaH300AhmFaktursDao = hondaH300AhmFaktursDao;
	}

	public HondaH300AhmpsesDao getHondaH300AhmpsesDao() {
		return hondaH300AhmpsesDao;
	}

	public void setHondaH300AhmpsesDao(HondaH300AhmpsesDao hondaH300AhmpsesDao) {
		this.hondaH300AhmpsesDao = hondaH300AhmpsesDao;
	}

	public TrioGlobalTransactionDao getTrioGlobalTransactionDao() {
		return trioGlobalTransactionDao;
	}

	public void setTrioGlobalTransactionDao(
			TrioGlobalTransactionDao trioGlobalTransactionDao) {
		this.trioGlobalTransactionDao = trioGlobalTransactionDao;
	}
	
}
