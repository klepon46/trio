package com.trio.component;

import java.util.List;

import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Popup;


/**
 * @author glassfish | Saipi Ramli
 *
 * Nov 2, 2012
 */
public class TrioPopup extends Popup{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Listbox listbox;
	private Listheader listheader;
	private Listhead listhead;
	
	public TrioPopup(){
		  
	}
	  
	@SuppressWarnings("rawtypes")
	public TrioPopup(List list, ListitemRenderer renderer, Listbox lb, String height, String width){
		
		super();
		
		lb.setHeight(height);
		lb.setWidth(width);
		lb.setMold("paging");
		lb.setPageSize(10);
		lb.setStyle("width: auto;");
		lb.setModel(new ListModelList(list));
		lb.setItemRenderer(renderer);
		
	}
}
