package com.trio.test;

import java.io.Serializable;

public class MenuTrio implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String parent;
	private String title;
	private String path;
	private String image;
	
	public MenuTrio() {
		// TODO Auto-generated constructor stub
	}
	
	public MenuTrio(String parent, String title, String path, String image) {
		this.parent = parent;
		this.title = title;
		this.path = path;
		this.image = image;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	

}
