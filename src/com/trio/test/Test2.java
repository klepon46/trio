package com.trio.test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.trio.base.TrioBasePage;
import com.trio.bean.h300.HondaH300DetInoutManuals;

public class Test2 extends TrioBasePage {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) throws IOException, SQLException {

		Map<String, Integer> cekMap = new HashMap<String, Integer>();
		Set<HondaH300DetInoutManuals> setdtl = new HashSet<HondaH300DetInoutManuals>();
		
		int seq = 1;
		
		for(int x = 1;  x <= 2; x++){
			HondaH300DetInoutManuals dtlInoutManuals = new HondaH300DetInoutManuals();
			dtlInoutManuals.getHondaH300DetInoutManualsPK().setInoutTransNo("001");
			dtlInoutManuals.getHondaH300DetInoutManualsPK().setSeq(seq);
			dtlInoutManuals.setPartNo("part1");
			dtlInoutManuals.setQty(1);
			
			if(x > 1){
				dtlInoutManuals.setPartNo("part1");
				dtlInoutManuals.setQty(2);
			}
			setdtl.add(dtlInoutManuals);
		}
		
		for(HondaH300DetInoutManuals current : setdtl){
			System.out.println(current.getPartNo());
			System.out.println(current.getQty());
		}
	}
}