package com.trio.test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.trio.bean.h100.TrioH100DtlHargas;
import com.trio.facade.MasterFacade;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;

public class Test {

	public static void main (String [] args){
		
//		System.out.println(faktorial(4));
		System.out.println(faktorialLoop(4));
	}
	
	static int faktorial(int n){
		if(n <= 1){
			return 1;
		}else{
			return n * faktorial(n-1);
		}
	}
	
	
	static int faktorialLoop(int n){
		int tot = 1;
		for(int i = n; i >= 1; i--){
			tot = tot * i;
		}
		
		return tot;
	}
}
