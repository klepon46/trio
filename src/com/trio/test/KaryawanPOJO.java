package com.trio.test;

import java.util.Date;

/**
 * @author Gusti Arya 10:36:10 AM Jun 22, 2013
 */

public class KaryawanPOJO {

	private String name;
	private String nip;
	private Date date;
	private String email;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
