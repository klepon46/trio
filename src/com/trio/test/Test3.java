package com.trio.test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.trio.base.TrioBasePage;

public class Test3 extends TrioBasePage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static void main(String[] args) {
//		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
//		ApplicationContext ap = new ClassPathXmlApplicationContext("applicationContext.xml");
//		MasterFacade mf = (MasterFacade) ap.getBean("masterFacade");
		
//		List<HondaH100Dtlinvs> lists = mf.getHondaH100DtlinvsDao().getHargaFakTerbaru("FC1");

		Calendar cal = Calendar.getInstance();
		cal.set(2013, 00, 01);
		
		List<Date> dateLists = new ArrayList<Date>();
		dateLists.add(cal.getTime());
		dateLists.add(new Date());
		
		Date maxDate = Collections.max(dateLists);
		System.out.println(maxDate);
		
		List<String> riderLists = new ArrayList<String>();
		riderLists.add("Dovi");
		riderLists.add("Simoncelli");
		riderLists.add("Rossi");
		riderLists.add("Rossi");
		riderLists.add("Marc");
		riderLists.add("Zulnain");
		
		
		int i = Collections.frequency(riderLists, "Rossi");
		System.out.println(i);
		System.out.println(riderLists.get(0));
		
		
	}
}