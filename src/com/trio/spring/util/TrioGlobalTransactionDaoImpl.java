package com.trio.spring.util;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.bean.h300.HondaH300InoutManuals;
import com.trio.dao.h100.TrioH100BundlingaksDao;
import com.trio.dao.h300.HondaH300InoutManualsDao;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 5:13:01 PM Aug 5, 2014
 */

public class TrioGlobalTransactionDaoImpl extends TrioHibernateDaoSupport implements TrioGlobalTransactionDao {
	
	private static final Logger logger = Logger.getLogger(TrioGlobalTransactionDaoImpl.class);

	private TrioH100BundlingaksDao daoUnit;
	private HondaH300InoutManualsDao daoPart;


	@Override
	@Transactional(readOnly=false, propagation=Propagation.REQUIRES_NEW, rollbackFor=Exception.class)
	public String save(TrioH100Bundlingaks domain,
			HondaH300InoutManuals domain2, String gudang, String rak,
			String user) {
		
		logger.info("mulai simpan");
		String kdBUndling = "";
		
		try {
			logger.info("daoUnit.processAll");
			kdBUndling = daoUnit.processAll(domain, user);
			logger.info("daoPart.saveInOut");
			daoPart.saveInOut(domain2, domain, gudang, rak, user);
		} catch (Exception e) {
			logger.info("error daoUnit.processAll && daoPart.saveInOut");
			kdBUndling = "";
		}

		return kdBUndling;
	}


	public TrioH100BundlingaksDao getDaoUnit() {
		return daoUnit;
	}


	public void setDaoUnit(TrioH100BundlingaksDao daoUnit) {
		this.daoUnit = daoUnit;
	}


	public HondaH300InoutManualsDao getDaoPart() {
		return daoPart;
	}


	public void setDaoPart(HondaH300InoutManualsDao daoPart) {
		this.daoPart = daoPart;
	}

}
