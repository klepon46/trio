package com.trio.spring.util;

import org.springframework.util.Assert;

public class DatabaseContextHolder {

	private static final  ThreadLocal<ConnectionType> contextHolder = new ThreadLocal<ConnectionType>();
	
	public static void setConnectionType(ConnectionType connectionType){
		Assert.notNull("Kada boleh null wal ae");
		contextHolder.set(connectionType);
	}
	
	public static ConnectionType getConnectionType(){
		return contextHolder.get();
	}
	
	public static void clearConnectionType(){
		contextHolder.remove();
	}
	
}
