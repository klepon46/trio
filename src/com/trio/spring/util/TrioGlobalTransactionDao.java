package com.trio.spring.util;

import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.bean.h300.HondaH300InoutManuals;

/**
 * @author Gusti Arya 5:11:54 PM Aug 5, 2014
 */

public interface TrioGlobalTransactionDao {

	String save(TrioH100Bundlingaks domain, HondaH300InoutManuals domain2, String gudang, String rak, String user);
	
}
