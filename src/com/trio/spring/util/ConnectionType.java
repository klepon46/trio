package com.trio.spring.util;

public enum ConnectionType {

	UNIT("UNIT"), PART("PART");
	
	private String connection;
	
	private ConnectionType(String connection){
		this.connection = connection;
	}
	
	public static ConnectionType setType(String connection){
		for(ConnectionType conn : values()){
			if(conn.connection.equals(connection)) return conn;
		}
		return null;
	}
}
