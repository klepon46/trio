package com.trio.usertrail;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class TrioPartEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Embedded
	protected TrioPartUserTrailing partUserTrailing;

	public TrioPartUserTrailing getPartUserTrailing() {
		if(this.partUserTrailing==null)
			this.partUserTrailing = new TrioPartUserTrailing();
		return partUserTrailing;
	}

	public void setPartUserTrailing(TrioPartUserTrailing partUserTrailing) {
		this.partUserTrailing = partUserTrailing;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((partUserTrailing == null) ? 0 : partUserTrailing.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioPartEntityUserTrail other = (TrioPartEntityUserTrail) obj;
		if (partUserTrailing == null) {
			if (other.partUserTrailing != null)
				return false;
		} else if (!partUserTrailing.equals(other.partUserTrailing))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioPartEntityUserTrail [partUserTrailing=" + partUserTrailing
				+ "]";
	}
}
