package com.trio.base;

import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zkplus.spring.SpringUtil;

import com.trio.facade.MasterFacade;

/*
 * Created by Saifi Ramli $
 * 
 * Tue Sep 25 21:03:40 PM @ kos Bandaneira
 */

public class TrioBasePage extends GenericForwardComposer {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String userSession;
	private static String connSession;
	
	private MasterFacade mf;
	
	public MasterFacade getMasterFacade() {
		MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");
		return mf;
	}

	public static String getUserSession() {
		String username = (String) Sessions.getCurrent().getAttribute("username");
		System.out.println("username = "+username);
		userSession = username;
		return userSession;
	}
	
	public static String getConnectionSession(){
		String connName = (String) Sessions.getCurrent().getAttribute("connection");
		System.out.println("connection = " + connName);
		connSession = connName;
		return connSession;
	}

	public static void setUserSession(String userSession) {
		TrioBasePage.userSession = userSession;
	}
	
	public static void setConnectionSession(String connSession){
		TrioBasePage.connSession = connSession;
	}
	
	public Connection getReportConnection(){
		//DataSource ds = (DataSource) applicationContext.getBean("dataSource");
		DataSource ds = (DataSource) SpringUtil.getBean("dataSource");
		Connection con = DataSourceUtils.getConnection(ds);
		return con;
	}

}
