package com.trio.dao.h100;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Dologs;
import com.trio.bean.h100.TrioH100Dtldologs;
import com.trio.hibernate.TrioHibernateDaoSupport;
import com.trio.util.TrioDateConv;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class TrioH100DologsDaoImpl extends TrioHibernateDaoSupport implements TrioH100DologsDao {

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioH100Dologs domain, String user) {
		// TODO Auto-generated method stub

		//		Set<TrioH100Dtldologs> dtlSet =	domain.getTrioH100DtldologsSet();

		////		for (TrioH100Dtldologs dtl : dtlSet){
		//			dtl.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		//			dtl.getUserTrailing().setVcreaby(user);
		//			getHibernateTemplate().save(dtl);
		//		}

		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		domain.getUserTrailing().setVcreaby(user);

		getHibernateTemplate().save(domain);

	}

	@Override
	public void delete(TrioH100Dologs domain) {
		getHibernateTemplate().delete(domain);

	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioH100Dologs> findAll() {

		String str = "select a.noDolog from TrioH100Dologs a";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery(str);
		List<TrioH100Dologs> lists = query.list();
		return lists;
	}

	@Override

	public List<TrioH100Dologs> findByExample(TrioH100Dologs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100Dologs> findByCriteria(TrioH100Dologs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(TrioH100Dologs domain, String user) {

	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioH100Dologs domain, String user) {

		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain);
	}

	@Override
	public TrioH100Dologs findByPrimaryKey(TrioH100Dologs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = false)
	public String saveTransaction(TrioH100Dologs dologs, String user){


		String str = getMasterFacade().getHondaH000SetupsDao()
				.getRunningNumber("TRIO_DOLOG_T10", TrioDateConv.format(new Date(), "yyyy"), user);
		String noDolog = TrioStringUtil.getFormattedRunno(Integer.valueOf(str), "DOLOG-T10");

		dologs.setNoDolog(noDolog);

		Set<TrioH100Dtldologs> setDtlDologs = new HashSet<TrioH100Dtldologs>();
		setDtlDologs = dologs.getTrioH100DtldologsSet();

		dologs.setTglDolog(TrioDateUtil.getLongSysDateOracle());
		dologs.getUserTrailing().setVcreaby(user);
		dologs.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		getHibernateTemplate().getSessionFactory().getCurrentSession().saveOrUpdate(dologs);


		for (TrioH100Dtldologs current : setDtlDologs ){

			current.setDologNoDolog(dologs.getNoDolog());
			current.getUserTrailing().setVcreaby(user);
			current.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
			getHibernateTemplate().persist(current);

		}

		return noDolog;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<TrioH100Dologs> findDologsByStatus(String status){
		String str = "from TrioH100Dologs where status = :status";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("status", status);
		return q.list();
	}

	@Override
	@Transactional(readOnly=false)
	public String batalDo(String noDolog) {
		String str = "update TrioH100Dologs set status = :status where noDolog = :noDolog";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("status", "X");
		q.setString("noDolog", noDolog);
		q.executeUpdate();

		return noDolog;
	}

	@Override
	@Transactional(readOnly=true)
	public BigInteger getSumHarga() {
		String str = "select sum(harga) from TrioH100Dologs where status = 'N'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		BigInteger sumHarga = (BigInteger) q.uniqueResult();
		return sumHarga;
	}

	
	
	
	@Override
	@Transactional(readOnly=true)
	public BigInteger getSumHarga(String noDolog) {
		String str = "select sum(harga) from TrioH100Dologs where status = 'N' and noDolog <> :noDolog";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noDolog", noDolog);
		BigInteger sumHarga = (BigInteger) q.uniqueResult();
		
		return sumHarga;
	}
	
	
}
