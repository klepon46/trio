package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h000.HondaH000Dealers;
import com.trio.bean.h100.HondaH100Mstpodlrs;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class HondaH100MstpodlrsDaoImpl extends TrioHibernateDaoSupport implements HondaH100MstpodlrsDao {

	@Override
	public void saveOrUpdate(HondaH100Mstpodlrs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH100Mstpodlrs domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Mstpodlrs> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Mstpodlrs> findByExample(HondaH100Mstpodlrs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Mstpodlrs> findByCriteria(HondaH100Mstpodlrs domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true) 
	public List<HondaH000Dealers> findByStatus(String status, String dlrNama){
		System.out.println("masuk findBystatus");
		String str = "select distinct a.dealerKdDlr from HondaH100Mstpodlrs a where a.status = :status";
		if (dlrNama != null){
			str += " and a.dealerKdDlr.dlrNama like :dlrNama";
		}
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("status",status);
		if (dlrNama != null){
			q.setString("dlrNama", "%"+dlrNama+"%"); 
		}
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true) 
	public List<HondaH100Mstpodlrs> findByStatusAndKdDlr(String status, String kdDlr, String jenisPo, String bulan, String tahun){
		String str;
		Query q;
		if(jenisPo.equalsIgnoreCase("W")){
			str = "select a from HondaH100Mstpodlrs a where a.status = :status and a.dealerKdDlr = :kdDlr and a.jenisPo = :jenisPo";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
			q.setString("status",status);
			q.setString("kdDlr",kdDlr);
			q.setString("jenisPo", jenisPo);
			return q.list();
		}else {
			str = "select a from HondaH100Mstpodlrs a where a.status = :status and a.dealerKdDlr = :kdDlr and a.jenisPo <> :jenisPo " +
					"and a.bulan= :bulan and a.tahun= :tahun";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
			q.setString("status",status);
			q.setString("kdDlr",kdDlr);
			q.setString("jenisPo", "W");
			q.setString("bulan", bulan);
			q.setString("tahun", tahun);
			return q.list();
		}
	}

	@Override
	public void save(HondaH100Mstpodlrs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Mstpodlrs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH100Mstpodlrs findByPrimaryKey(HondaH100Mstpodlrs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Mstpodlrs> findWaitingListHeader(String kdDlr) {
		String hql = "from HondaH100Mstpodlrs where status <> 'C' "
				+ "and status <> 'X' "
				+ "and jenisPo = 'W' "
				+ "and kdDlr = :kdDlr";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdDlr", kdDlr);
		
		List<HondaH100Mstpodlrs> lists = q.list();
		
		return lists;
	}
}

