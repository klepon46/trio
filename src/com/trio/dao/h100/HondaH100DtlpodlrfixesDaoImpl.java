package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Dtlpodlrfixes;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class HondaH100DtlpodlrfixesDaoImpl extends TrioHibernateDaoSupport implements HondaH100DtlpodlrfixesDao {

	@Override
	public void saveOrUpdate(HondaH100Dtlpodlrfixes domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH100Dtlpodlrfixes domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Dtlpodlrfixes> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Dtlpodlrfixes> findByExample(
			HondaH100Dtlpodlrfixes domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Dtlpodlrfixes> findByCriteria(
			HondaH100Dtlpodlrfixes domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional(readOnly= true) 
	public Long getQtyOrderByPoIntAndKdItem(String noPoInt, String kdItem){
		String str = "select a.qtyOrder from HondaH100Dtlpodlrfixes a where a.podlrNoPoint = :noPoInt and a.kdItem = :kdItem";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noPoInt", noPoInt);
		q.setString("kdItem", kdItem);
		Object obj = q.uniqueResult();
		return obj != null ? (Long) q.uniqueResult() : 0L;
	}

	@Override
	public void save(HondaH100Dtlpodlrfixes domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Dtlpodlrfixes domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH100Dtlpodlrfixes findByPrimaryKey(HondaH100Dtlpodlrfixes domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Dtlpodlrfixes> getKodeItemByNoPoint(
			List<String> lists) {
		String hql = "from HondaH100Dtlpodlrfixes where podlrNoPoint in (:lists)";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setParameterList("lists", lists);
		
		return q.list();
	}
}
