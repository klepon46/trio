package com.trio.dao.h100;

import com.trio.bean.h100.TrioH100Kecamatans;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH100KecamatansDao extends TrioGenericDao<TrioH100Kecamatans> { 
	
	public TrioH100Kecamatans findByKdKec(String kdKec);
	
}
