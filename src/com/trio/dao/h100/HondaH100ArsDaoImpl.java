package com.trio.dao.h100;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Ars;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class HondaH100ArsDaoImpl extends TrioHibernateDaoSupport implements HondaH100ArsDao {

	@Override
	public void saveOrUpdate(HondaH100Ars domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH100Ars domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Ars> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Ars> findByExample(HondaH100Ars domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Ars> findByCriteria(HondaH100Ars domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional(readOnly=true)
	public BigInteger getARByKdDlr(String kdDlr){
		//select sum(a.amt_pokok) from HONDA_H100_ARS a, HONDA_H100_FAKDOS b 
		//where a.NO_DO=b.NO_DO and b.STATUS in ('B','C') and a.STATUS_POKOK = 'N' and b.KD_DLR = 'S8B'
//		String str = "select sum(a.amtPokok) from HondaH100Ars a where a.statusPokok = 'N' and hondaH100Fakdos.status in ('B','C') and hondaH100Fakdos.hondaH000Dealers.kdDlr = :kdDlr";
		String str = "select sum(a.amtPokok) from HondaH100Ars a where a.statusPokok = 'N' and hondaH100Fakdos.status in ('B','C') and hondaH100Fakdos.hondaH000Dealers.kdDlr = :kdDlr ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdDlr", kdDlr);
		BigInteger sumAmtPokok = null;
		Object obj = q.uniqueResult();
		if (obj == null){
			sumAmtPokok = new BigInteger("0");
		}else{
			sumAmtPokok = (BigInteger) obj;
		}
		return sumAmtPokok;
	}

	@Override
	public void save(HondaH100Ars domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Ars domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH100Ars findByPrimaryKey(HondaH100Ars domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Transactional(readOnly=false)
	@Override
	public void updateAmtPkokAndAmtPpn(BigDecimal amtPkok, BigDecimal amtPpn, String noDo){
		
		String str = "update HondaH100Ars set amtPokok = :amtPokok ,amtPPN = :amtPPn where noDo = :noDo";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setBigDecimal("amtPokok", amtPkok);
		q.setBigDecimal("amtPPn", amtPpn);
		q.setString("noDo", noDo);
		q.executeUpdate();
		
	}

}
