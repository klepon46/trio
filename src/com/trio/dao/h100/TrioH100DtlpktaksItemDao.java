package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.TrioH100DtlpktaksItem;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 2:57:06 PM Dec 20, 2013
 */

public interface TrioH100DtlpktaksItemDao extends TrioGenericDao<TrioH100DtlpktaksItem>{
	
	public List<TrioH100DtlpktaksItem> findBykdItem(String kdItem);
	public TrioH100DtlpktaksItem getKdItemBaru(String kdItem, String pktBundling);

}
