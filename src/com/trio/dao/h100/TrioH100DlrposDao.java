package com.trio.dao.h100;

import com.trio.bean.h100.TrioH100Dlrpos;
import com.trio.hibernate.TrioGenericDao;

/** @author Saifi Ahmada Jan 22, 2013 3:40:02 PM  **/

public interface TrioH100DlrposDao extends TrioGenericDao<TrioH100Dlrpos> {

}

