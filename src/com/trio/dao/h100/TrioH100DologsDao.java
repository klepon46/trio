package com.trio.dao.h100;

import java.math.BigInteger;
import java.util.List;

import com.trio.bean.h100.TrioH100Dologs;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface TrioH100DologsDao extends TrioGenericDao<TrioH100Dologs> { 
	
	public String saveTransaction(TrioH100Dologs dologs, String user);
	
	public List<TrioH100Dologs> findDologsByStatus(String status);
	
	public String batalDo(String noDO);
	
	
	/** Mengembalikan Total Harga yang ada pada TrioH100Dologs
	 * @return total harga
	 */
	public BigInteger getSumHarga();
	
	
	/** Mengembalikan Total harga yang ada pada TrioH100Dologs kecuali noDolog yang ada di parameter
	 * @param noDolog
	 * @return total harga
	 */
	public BigInteger getSumHarga(String noDolog);
}
