package com.trio.dao.h100;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.trio.bean.h100.HondaH100Itemcusts;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH100ItemcustsDao extends TrioGenericDao<HondaH100Itemcusts> {
	
	public List<HondaH100Itemcusts> getJaminan();
	
	public Date getTglMohonStnk(String noMesin);
	
	public BigDecimal getJumlahPenjualan(String bulan, String kdDlr);

}
