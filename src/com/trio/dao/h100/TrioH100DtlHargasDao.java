package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.TrioH100DtlHargas;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:24:35 AM Dec 10, 2013
 */

public interface TrioH100DtlHargasDao extends TrioGenericDao<TrioH100DtlHargas>{

	public List<TrioH100DtlHargas> findByKdItem(String kdItem, String areaDealer);
	
}
