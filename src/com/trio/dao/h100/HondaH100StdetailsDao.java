package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.HondaH100Stdetails;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH100StdetailsDao extends TrioGenericDao<HondaH100Stdetails> {
	
	public HondaH100Stdetails getNomesinPengganti(String noMesin, String kdItem);
	
	public HondaH100Stdetails findByNomesin(String noMesin);
	
	public String getKdItemByNoMesin(String NoMesin);
	
	public String getVselByNoMesin(String noMesin);
	
	public List<HondaH100Stdetails> findNoMesinStatusA();
	
	public HondaH100Stdetails findByNoMesinStatusA(String noMesin);
	
}
