package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.HondaH100Stglobals;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface HondaH100StglobalsDao extends TrioGenericDao<HondaH100Stglobals> { 
	public List<String> findByQtyOnHandMoreZero();
	
	public HondaH100Stglobals getQtyOnHandAndBookByLokasiAndKdItem(String lokasi, String kdItem);
	public List<HondaH100Stglobals> getKdItemByLokasi(String lokasi); 
	public List<HondaH100Stglobals> getKdItemByLokasiandPoDlr(String lokasi, String poDlr);
	public List<HondaH100Stglobals> findKdItemBynameAndLokasi(String kdItem, String lokasi);
}
