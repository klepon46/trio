package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100DtlpktaksItem;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 2:59:47 PM Dec 20, 2013
 */

public class TrioH100DtlpktaksItemDaoImpl extends TrioHibernateDaoSupport implements TrioH100DtlpktaksItemDao {

	@Override
	public void saveOrUpdate(TrioH100DtlpktaksItem domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioH100DtlpktaksItem domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH100DtlpktaksItem domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioH100DtlpktaksItem domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100DtlpktaksItem> findAll() {
		String hql = "from TrioH100DtlpktaksItem";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<TrioH100DtlpktaksItem> findByExample(
			TrioH100DtlpktaksItem domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100DtlpktaksItem> findByCriteria(
			TrioH100DtlpktaksItem domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH100DtlpktaksItem findByPrimaryKey(TrioH100DtlpktaksItem domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100DtlpktaksItem> findBykdItem(String kdItem) {
		String hql = "from TrioH100DtlpktaksItem a where a.trioH100Pktaks.kdPkt = a.kdPkt "
					+ "and a.kdItem = :kdItem";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdItem", kdItem);
		List<TrioH100DtlpktaksItem> iList = q.list();
		
		return iList;
	}

	@Override
	@Transactional(readOnly=true)
	public TrioH100DtlpktaksItem getKdItemBaru(String kdItem, String pktBundling) {
		Criteria c = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(TrioH100DtlpktaksItem.class);
		c.add(Restrictions.eq("kdItem", kdItem));
		c.add(Restrictions.eq("kdPkt", pktBundling));
		TrioH100DtlpktaksItem item= (TrioH100DtlpktaksItem) c.uniqueResult();
		
		return item;
	}


}
