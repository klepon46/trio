package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100DtlpktaksPart;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 2:55:59 PM Dec 20, 2013
 */

public class TrioH100DtlpktaksPartDaoImpl extends TrioHibernateDaoSupport implements TrioH100DtlpktaksPartDao {

	@Override
	public void saveOrUpdate(TrioH100DtlpktaksPart domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioH100DtlpktaksPart domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH100DtlpktaksPart domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioH100DtlpktaksPart domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100DtlpktaksPart> findAll() {
		String hql = "from TrioH100DtlpktaksPart";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	public List<TrioH100DtlpktaksPart> findByExample(
			TrioH100DtlpktaksPart domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100DtlpktaksPart> findByCriteria(
			TrioH100DtlpktaksPart domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH100DtlpktaksPart findByPrimaryKey(TrioH100DtlpktaksPart domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100DtlpktaksPart> findByKdPkt(String kdPkt) {
		String hql = "from TrioH100DtlpktaksPart a where a.trioH100DtlpktaksPartPK.kdPkt = :kdPkt";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdPkt", kdPkt);
		
		return q.list();
	}

}
