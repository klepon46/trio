package com.trio.dao.h100;

import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface TrioH100DtldologsDao extends TrioGenericDao<TrioH100DtldologsDao> {
	
	public Long sumQtyByNoDolog(String noDolog);
	

}
