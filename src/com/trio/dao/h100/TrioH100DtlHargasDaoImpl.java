package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100DtlHargas;

/**
 * @author Gusti Arya 11:26:08 AM Dec 10, 2013
 */

public class TrioH100DtlHargasDaoImpl extends HibernateDaoSupport implements TrioH100DtlHargasDao {

	@Override
	public void saveOrUpdate(TrioH100DtlHargas domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioH100DtlHargas domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH100DtlHargas domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioH100DtlHargas domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100DtlHargas> findAll() {
		String hql = "from TrioH100DtlHargas";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	public List<TrioH100DtlHargas> findByExample(TrioH100DtlHargas domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100DtlHargas> findByCriteria(TrioH100DtlHargas domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH100DtlHargas findByPrimaryKey(TrioH100DtlHargas domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100DtlHargas> findByKdItem(String kdItem, String areaDealer) {
		String hql = "from TrioH100DtlHargas a where a.trioH100DtlHargasPK.kdTipe= :kdItem and a.trioH100DtlHargasPK.area= :area";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdItem", kdItem);
		q.setString("area", areaDealer);
		
		return q.list();
	}

}
