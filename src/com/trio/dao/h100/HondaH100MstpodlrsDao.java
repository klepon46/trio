package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h000.HondaH000Dealers;
import com.trio.bean.h100.HondaH100Mstpodlrs;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface HondaH100MstpodlrsDao extends TrioGenericDao<HondaH100Mstpodlrs> { 
	
	public List<HondaH000Dealers> findByStatus(String status,  String kdDlr);
	
	public List<HondaH100Mstpodlrs> findByStatusAndKdDlr(String status, String kdDlr, String jenisPo, String bulan, String tahun);
	
	public List<HondaH100Mstpodlrs> findWaitingListHeader(String kdDlr);

	
}
