package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100DtlAreaDealers;
import com.trio.hibernate.TrioHibernateDaoSupport;
import com.trio.util.TrioDateUtil;

public class TrioH100DtlAreaDealersDaoImpl extends TrioHibernateDaoSupport implements TrioH100DtlAreaDealersDao {
	
	@Transactional(readOnly = false) 
	public void saveOrUpdate(TrioH100DtlAreaDealers dtl, String user){
		
		TrioH100DtlAreaDealers newDtl = findByKdDlrAndKdKec(dtl.getKdDlr(), dtl.getKdKec());
		if (newDtl == null){
			System.out.println("method simpan");
			save(dtl, user);
		}else{
			System.out.println("method update");
			update(dtl, user);
		}
	}
	
	@Transactional(readOnly = false)
	public void save(TrioH100DtlAreaDealers dtl, String user){
		dtl.getUserTrailing().setVcreaby(user);
		dtl.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		getHibernateTemplate().persist(dtl);
		getHibernateTemplate().flush();
		getHibernateTemplate().clear();
	}
	
	@Transactional(readOnly = false)
	public void update(TrioH100DtlAreaDealers dtl, String user){
		dtl.getUserTrailing().setVmodiby(user);
		dtl.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDateOracle());
		getHibernateTemplate().merge(dtl);
		getHibernateTemplate().flush();
		getHibernateTemplate().clear();
	}
	
	@Transactional
	public void delete(TrioH100DtlAreaDealers dtl) throws DataAccessException {
		getHibernateTemplate().delete(dtl);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100DtlAreaDealers> findAll() throws DataAccessException {
		List<TrioH100DtlAreaDealers> list = getHibernateTemplate().find("from TrioH100DtlAreaDealers");
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100DtlAreaDealers> findByCriteria(TrioH100DtlAreaDealers dtl) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(TrioH100DtlAreaDealers.class);
		if (dtl.getKdDlr() != null){
			criteria = criteria.add(Restrictions.eq("kdDlr", dtl.getKdDlr().toUpperCase())); 
		}
		if (dtl.getKdKec() != null){
			criteria = criteria.add(Restrictions.eq("kdKec", dtl.getKdKec().toUpperCase()));
		}
		if (dtl.getStatus() != null){
			criteria = criteria.add(Restrictions.eq("status", dtl.getStatus().toUpperCase()));
		}
		if (dtl.getKdDlrpos() != null){
			criteria = criteria.add(Restrictions.eq("kdDlrpos", dtl.getKdDlrpos().toUpperCase()));
		}
		List<TrioH100DtlAreaDealers> list = getHibernateTemplate().findByCriteria(criteria);
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly= true) 
	public List<TrioH100DtlAreaDealers> findByExample(TrioH100DtlAreaDealers domain) {
		List<TrioH100DtlAreaDealers> list = getHibernateTemplate().findByExample(domain);
		return list;
	}
	
	@Transactional(readOnly = true) 
	public TrioH100DtlAreaDealers findByKdDlrAndKdKec(String kdDlr, String kdKec){
		String strQ = "select d from TrioH100DtlAreaDealers d where d.kdDlr = :kdDlr and d.kdKec = :kdKec";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(strQ);
		q.setString("kdDlr", kdDlr);
		q.setString("kdKec", kdKec);
		return (TrioH100DtlAreaDealers) q.uniqueResult();
	}

	@Override
	public TrioH100DtlAreaDealers findByPrimaryKey(TrioH100DtlAreaDealers domain) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
