package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Stdetails;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH100StdetailsDaoImpl extends TrioHibernateDaoSupport implements HondaH100StdetailsDao {

	@Override
	@Transactional(readOnly = false) 
	public void saveOrUpdate(HondaH100Stdetails domain, String user) {
		getHibernateTemplate().saveOrUpdate(domain);
	}

	@Override
	public void delete(HondaH100Stdetails domain) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<HondaH100Stdetails> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Stdetails> findByExample(HondaH100Stdetails domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Stdetails> findByCriteria(HondaH100Stdetails domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly = true) 
	public HondaH100Stdetails getNomesinPengganti(String noMesin, String kdItem) {
		//select * from HONDA_H100_STDETAILS where STATUS = 'A' 
		//and VSEL is not NULL and VSTATPICK = 'N'  
		//and NO_MESIN = 'HB61E1614814' and KD_ITEM = 'CE1-SV'
		String str = "from HondaH100Stdetails where status = 'A' and vsel is not null" +
				" and vstatpick = 'N' and no_mesin = :noMesin and kd_item = :kdItem ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noMesin", noMesin);
		q.setString("kdItem", kdItem);
		return (HondaH100Stdetails) q.uniqueResult();
	}

	@Override
	@Transactional(readOnly= true)
	public HondaH100Stdetails findByNomesin(String noMesin) {
		String str = "from HondaH100Stdetails where no_mesin = :noMesin"; 
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noMesin", noMesin);
		return (HondaH100Stdetails) q.uniqueResult();
	}

	@Override
	public void save(HondaH100Stdetails domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(HondaH100Stdetails domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public HondaH100Stdetails findByPrimaryKey(HondaH100Stdetails domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public String getKdItemByNoMesin(String NoMesin) {
		String str = "select kdItem from HondaH100Stdetails where noMesin = :noMesin";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noMesin", NoMesin);
		String result = (String) q.uniqueResult();
		return result;
	}

	@Override
	@Transactional(readOnly = true)
	public String getVselByNoMesin(String noMesin) {
		String str = "select vsel from HondaH100Stdetails where noMesin = :noMesin";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noMesin", noMesin);
		String result = (String) q.uniqueResult();
		return result.substring(0,1);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Stdetails> findNoMesinStatusA() {
		String hql = "select new HondaH100Stdetails(h.noMesin, h.noRangka, h.lokasi, h.kdItem) " 
				+ "from HondaH100Stdetails h where h.status='A' "
				+ "and substr(h.kdItem,1,3) in (select s.valChar from HondaH000Setups s where s.valId = 'H1_VARIASI_UNIT')";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=true)
	public HondaH100Stdetails findByNoMesinStatusA(String noMesin) {
		String hql = "from HondaH100Stdetails h where h.status='A' "
				+ "and h.noMesin = :noMesin "
				+ "and substr(h.kdItem,1,3) in (select s.valChar from HondaH000Setups s where s.valId = 'H1_VARIASI_UNIT')";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("noMesin", noMesin);
		
		return (HondaH100Stdetails) q.uniqueResult(); 
	}
}
