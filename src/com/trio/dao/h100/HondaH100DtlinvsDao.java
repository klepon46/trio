package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.HondaH100Dtlinvs;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH100DtlinvsDao extends TrioGenericDao<HondaH100Dtlinvs>{
	
	public List<HondaH100Dtlinvs>getHargaFakTerbaru(String kdItem);

}
