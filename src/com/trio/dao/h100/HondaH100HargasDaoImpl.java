package com.trio.dao.h100;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Hargas;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class HondaH100HargasDaoImpl extends TrioHibernateDaoSupport implements HondaH100HargasDao {

	@Override
	public void saveOrUpdate(HondaH100Hargas domain, String user) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(HondaH100Hargas domain) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Hargas> findAll() {

		@SuppressWarnings("unchecked")
		List<HondaH100Hargas> lists = getHibernateTemplate().find("from HondaH100Hargas");

		return lists;
	}

	@Override
	public List<HondaH100Hargas> findByExample(HondaH100Hargas domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Hargas> findByCriteria(HondaH100Hargas domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(HondaH100Hargas domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(HondaH100Hargas domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public HondaH100Hargas findByPrimaryKey(HondaH100Hargas domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Transactional(readOnly=true) 
	public BigDecimal findHargaStdByKdItemAndKdDlr(String kdItem, String kdDlr){
		
		String str = "select hargaStd from HondaH100Hargas where kdItem = :kdItem " +
					 " and area = (select dlrArea from HondaH000Dealers where kdDlr = :kdDlr ) " ;
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdItem", kdItem);
		q.setString("kdDlr", kdDlr);
		
		return (BigDecimal) q.uniqueResult();
	}


	@Override
	@Transactional(readOnly=true)
	public BigDecimal getHargasByKdItemAndKdDlr(String kdItem,
			String kdDLr) {
		
		String query = "select a from HondaH100Hargas a where a.kdItem = :kdItem" +
				" and a.area = (select dlrArea from HondaH000Dealers where kdDlr = :kdDlr)";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(query);
		q.setString("kdItem", kdItem);
		q.setString("kdDlr", kdDLr);
		
		HondaH100Hargas hargas = (HondaH100Hargas) q.uniqueResult();
		return hargas.getHargaStd();
	}

	
//	@Override
//	@Transactional(readOnly=true)
//	public Integer getNilaiApprove(String kdItem, String kdDlr) {
//		
//		String str = "select a.approve from HondaH100Hargas a where a.kdItem = :kdItem " +
//				"and a.area = (select dlrArea from HondaH000Dealers where kdDlr = :kdDlr)";
//		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
//		q.setString("kdItem", kdItem);
//		q.setString("kdDlr", kdDlr);
//		Integer result = (Integer) q.uniqueResult();
//		
//		return result;
//	}
	
	@Override
	@Transactional(readOnly=true)
	public HondaH100Hargas getNilaiApprove(String kdItem, String kdDlr) {
		
		String str = "select a from HondaH100Hargas a where a.kdItem = :kdItem " +
				"and a.area = (select dlrArea from HondaH000Dealers where kdDlr = :kdDlr)";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdItem", kdItem);
		q.setString("kdDlr", kdDlr);
		HondaH100Hargas result = (HondaH100Hargas) q.uniqueResult();
		
		return result;
	}

	@Override
	@Transactional(readOnly=true)
	public BigDecimal getHrgBeli(String kdItem, String kdDlr) {
		String str = "select a.hrgBeli from HondaH100Hargas a where a.kdItem = :kdItem " +
				"and a.area = (select dlrArea from HondaH000Dealers where kdDlr = :kdDlr)";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdItem", kdItem);
		q.setString("kdDlr", kdDlr);
		BigDecimal result = (BigDecimal) q.uniqueResult();
		
		return result;
	}
}
