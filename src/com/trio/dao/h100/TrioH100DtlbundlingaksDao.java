package com.trio.dao.h100;

import com.trio.bean.h100.TrioH100Dtlbundlingaks;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:15:54 AM Dec 26, 2013
 */

public interface TrioH100DtlbundlingaksDao extends TrioGenericDao<TrioH100Dtlbundlingaks> {

}
