package com.trio.dao.h100;

import java.math.BigDecimal;
import java.util.List;

import com.trio.bean.h100.TrioH100Ars;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH100ArsDao extends TrioGenericDao<TrioH100Ars> {

	public List<TrioH100Ars> findNoDoNoFakturNamaDealer();
	public void updateSisaUtang(BigDecimal nilaiAr, String noDo);
	
}
