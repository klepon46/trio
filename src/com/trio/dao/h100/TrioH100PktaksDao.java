package com.trio.dao.h100;

import com.trio.bean.h100.TrioH100Pktaks;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 2:37:05 PM Dec 20, 2013
 */

public interface TrioH100PktaksDao extends TrioGenericDao<TrioH100Pktaks> {

	public String savePaketAks(TrioH100Pktaks domain, String user);
	
}
