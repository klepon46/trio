package com.trio.dao.h100;

import java.math.BigDecimal;
import java.util.List;

import com.trio.bean.h100.HondaH100Fakdos;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH100FakdosDao extends TrioGenericDao<HondaH100Fakdos> { 
	
	@SuppressWarnings("rawtypes")
	public List getDataStnk(String noMesin);
	public BigDecimal getSumHargaDOLogistisByKdDlr(String kdDlr);
	public List<HondaH100Fakdos> getNoDoByStatus();
	public void updateHargaByNoDo(BigDecimal harga, String noDo);
	
	public String saveTransaction(HondaH100Fakdos domain,String noDOLogistik, String user);
	
}
