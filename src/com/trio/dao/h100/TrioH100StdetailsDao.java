package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH100StdetailsDao extends TrioGenericDao<TrioH100Stdetails> { 
	
//	public void saveOrUpdate(Stdetail stdetail);
//	
//	public void delete(Stdetail stdetail);
//	
//	public List<Stdetail> findAll(Class<Stdetail> clazz);
//	
//	public List<Stdetail> getStdetailByCriteria(Stdetail clazz, final int firstResult, final int maxResults);
//	
//	public List<Stdetail> getStdetailByCriteria(Stdetail stdetail);
//	
//	public List<Stdetail> findByExample(Stdetail stdetail);
	
	public TrioH100Stdetails findByNomesin(String noMesin);
	
	public TrioH100Stdetails findByNomesinAndStatusPW(String noMesin, String statusPw);
	
	public List<TrioH100Stdetails> findByDistinct();
	
	public List<TrioH100Stdetails> findByQuery(String statusPw);
	
	public List<TrioH100Stdetails> findByQuery(String kdDlr ,String statusPw);
}
