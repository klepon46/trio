package com.trio.dao.h100;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Pktaks;
import com.trio.hibernate.TrioHibernateDaoSupport;
import com.trio.util.TrioDateConv;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;

/**
 * @author Gusti Arya 2:37:31 PM Dec 20, 2013
 */

public class TrioH100PktaksDaoImpl extends TrioHibernateDaoSupport implements TrioH100PktaksDao{

	@Override
	public void saveOrUpdate(TrioH100Pktaks domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly=false)
	public void save(TrioH100Pktaks domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
	}

	@Override
	public void update(TrioH100Pktaks domain, String user) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(TrioH100Pktaks domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH100Pktaks> findAll() {
		String hql = "from TrioH100Pktaks";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);

		return q.list();
	}

	@Override
	public List<TrioH100Pktaks> findByExample(TrioH100Pktaks domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100Pktaks> findByCriteria(TrioH100Pktaks domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH100Pktaks findByPrimaryKey(TrioH100Pktaks domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public String savePaketAks(TrioH100Pktaks domain, String user) {
		String str = getMasterFacade().getHondaH000SetupsDao().getRunningNumber("BDLAKS", TrioDateConv.format(new Date(),"yyyy"), user);
		String kdPktAks = TrioStringUtil.getFormattedRunno2("BDLAKS", Integer.valueOf(str));
		
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);

		return kdPktAks;
	}

}
