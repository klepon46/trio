package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.TrioH100DtlpktaksPart;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 2:53:06 PM Dec 20, 2013
 */

public interface TrioH100DtlpktaksPartDao extends TrioGenericDao<TrioH100DtlpktaksPart>{
	
	public List<TrioH100DtlpktaksPart> findByKdPkt(String kdPkt);

}
