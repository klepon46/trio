package com.trio.dao.h100;

import com.trio.bean.h100.TrioH100Dtlars;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH100DtlarsDao extends TrioGenericDao<TrioH100Dtlars>{

}
