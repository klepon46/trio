package com.trio.dao.h100;

import java.util.List;

import com.trio.bean.h100.HondaH100Hdrpicklists;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH100HdrpicklistsDao extends TrioGenericDao<HondaH100Hdrpicklists> {
	
	public List<HondaH100Hdrpicklists> findByVstatusAndVjenispick(String vstatus, String vjenispcik);

}
