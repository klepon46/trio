package com.trio.dao.h100;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Itemcusts;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH100ItemcustsDaoImpl extends TrioHibernateDaoSupport implements HondaH100ItemcustsDao{

	@Override
	public void saveOrUpdate(HondaH100Itemcusts domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(HondaH100Itemcusts domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Itemcusts domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH100Itemcusts domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Itemcusts> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Itemcusts> findByExample(HondaH100Itemcusts domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Itemcusts> findByCriteria(HondaH100Itemcusts domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH100Itemcusts findByPrimaryKey(HondaH100Itemcusts domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Itemcusts> getJaminan() {
		String str = "from HondaH100Itemcusts where jaminan is not null";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		List<HondaH100Itemcusts> lists = q.list();
		return lists;
	}

	@Override
	@Transactional(readOnly=false)
	public Date getTglMohonStnk(String noMesin) {
		String hql = "select tglMohonStnk from HondaH100Itemcusts where noMesin = :noMesin";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("noMesin", noMesin);
		return (Date) q.uniqueResult();
	}

	@Override
	@Transactional (readOnly=true)
	public BigDecimal getJumlahPenjualan(String bulan, String kdDlr) {
		String str = "select count(*) from HondaH100Itemcusts a, TrioH100Stdetails b " +
				"where a.noMesin = b.noMesin " +
				"and b.kdDlr = :kdDlr " +
				"and to_char(a.tglMohonStnk,'MM-YYYY') = :bulan";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdDlr", kdDlr);
		q.setString("bulan", bulan);
		Long tole = (Long) q.uniqueResult();
		
		return new BigDecimal(tole);
	}
}
