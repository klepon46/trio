package com.trio.dao.h100;

import java.math.BigDecimal;

import com.trio.bean.h100.HondaH100Hargas;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface HondaH100HargasDao extends TrioGenericDao<HondaH100Hargas> {
	
	public BigDecimal findHargaStdByKdItemAndKdDlr(String kdItem, String kdDlr);

	public BigDecimal getHargasByKdItemAndKdDlr(String kdItem,
			String kdDLr);
	public BigDecimal getHrgBeli(String kdItem, String kdDlr);
	public HondaH100Hargas getNilaiApprove(String kdItem, String kdDlr);
//	public Integer getNilaiApprove(String kdItem, String kdDlr);

}
