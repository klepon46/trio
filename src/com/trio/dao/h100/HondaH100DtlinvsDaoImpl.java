package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Dtlinvs;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH100DtlinvsDaoImpl extends TrioHibernateDaoSupport implements HondaH100DtlinvsDao{

	@Override
	public void saveOrUpdate(HondaH100Dtlinvs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(HondaH100Dtlinvs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Dtlinvs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH100Dtlinvs domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Dtlinvs> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Dtlinvs> findByExample(HondaH100Dtlinvs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Dtlinvs> findByCriteria(HondaH100Dtlinvs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH100Dtlinvs findByPrimaryKey(HondaH100Dtlinvs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Dtlinvs> getHargaFakTerbaru(String kdItem) {

		String str = "select (a.amtPokok+a.discO)/a.qty from HondaH100Dtlinvs a where substr(a.hondaH100DtlinvsPK.kdItem,1,3) = :kdItem order by a.creaDate desc";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdItem", kdItem);
		List<HondaH100Dtlinvs> lists = q.list();
		
		return lists;
	}
}
