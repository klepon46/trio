package com.trio.dao.h100;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.trio.bean.h100.HondaH100Ars;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface HondaH100ArsDao extends  TrioGenericDao<HondaH100Ars> {
	
	public BigInteger getARByKdDlr(String kdDlr);
	public void updateAmtPkokAndAmtPpn(BigDecimal amtPkok, BigDecimal amtPpn, String noDo);

}
