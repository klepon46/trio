package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Hdrpicklists;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH100HdrpicklistsDaoImpl extends TrioHibernateDaoSupport implements HondaH100HdrpicklistsDao {

	@Override
	public void saveOrUpdate(HondaH100Hdrpicklists domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH100Hdrpicklists domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Hdrpicklists> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Hdrpicklists> findByExample(
			HondaH100Hdrpicklists domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<HondaH100Hdrpicklists> findByCriteria(
			HondaH100Hdrpicklists domain) {
		
		DetachedCriteria dc = DetachedCriteria.forClass(HondaH100Hdrpicklists.class);
		if (domain.getVnopicklist() != null){
			dc = dc.add(Restrictions.eq("vnopicklist", domain.getVnopicklist()));
		}if (domain.getHondaH100Fakdos().getNoDo() != null){
			dc = dc.add(Restrictions.eq("vnodo", domain.getHondaH100Fakdos().getNoDo()));
		}
		return getHibernateTemplate().findByCriteria(dc);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true) 
	public List<HondaH100Hdrpicklists> findByVstatusAndVjenispick(String vstatus, String vjenispcik){
		String str = null;
		Query q = null;
			str = "select o from HondaH100Hdrpicklists o where o.vstatus = :vstatus and o.vjenispick = :vjenispick";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
			q.setString("vstatus", vstatus);
			q.setString("vjenispick", vjenispcik);
		return q.list();
	}

	@Override
	public void save(HondaH100Hdrpicklists domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Hdrpicklists domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH100Hdrpicklists findByPrimaryKey(HondaH100Hdrpicklists domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
