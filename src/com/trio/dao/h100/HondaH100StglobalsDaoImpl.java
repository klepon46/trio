package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Stglobals;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class HondaH100StglobalsDaoImpl extends TrioHibernateDaoSupport implements HondaH100StglobalsDao {

	@Override
	public void saveOrUpdate(HondaH100Stglobals domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(HondaH100Stglobals domain) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<HondaH100Stglobals> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Stglobals> findByExample(HondaH100Stglobals domain) {
		return getHibernateTemplate().findByExample(domain); 
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<HondaH100Stglobals> findByCriteria(HondaH100Stglobals domain) {
		DetachedCriteria criteria = DetachedCriteria.forClass(HondaH100Stglobals.class);
		if (domain.getKdItem() != null){
			criteria = criteria.add(Restrictions.eq("kdItem", domain.getKdItem()));
		}

		if (domain.getLokasi() != null){
			criteria = criteria.add(Restrictions.eq("lokasi", domain.getLokasi()));
		}

		criteria = criteria.add(Restrictions.gt("qtyOnhand", 0L));

		List<HondaH100Stglobals> list = getHibernateTemplate().findByCriteria(criteria); 
		return list;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<String> findByQtyOnHandMoreZero(){
		String str = "select distinct a.lokasi from HondaH100Stglobals a where a.qtyOnhand > 0 ";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		return q.list();
	}

	@Transactional(readOnly=true)
	public HondaH100Stglobals getQtyOnHandAndBookByLokasiAndKdItem(String lokasi, String kdItem){
		String str = "select a from HondaH100Stglobals a where a.lokasi = :lokasi and a.kdItem = :kdItem";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("lokasi", lokasi);
		q.setString("kdItem", kdItem);
		return (HondaH100Stglobals) q.uniqueResult() ;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public List<HondaH100Stglobals> getKdItemByLokasi(String lokasi){
		/*
		 * Select kd_item,QTY_ONHAND from HONDA_H100_STGLOBALS where lokasi='G13' and Qty_Onhand > 0
		 */
		String str = "select a from HondaH100Stglobals a where a.lokasi = :lokasi and a.qtyOnhand > 0";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("lokasi", lokasi);
		return q.list();
	}

	@Override
	public void save(HondaH100Stglobals domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(HondaH100Stglobals domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public HondaH100Stglobals findByPrimaryKey(HondaH100Stglobals domain) {
		// TODO Auto-generated method stub
		return null;
	}



	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Stglobals> getKdItemByLokasiandPoDlr(String lokasi,
			String poDlr) {

		String str = "select b from HondaH100Dtlpodlrfixes a , HondaH100Stglobals b where a.kdItem = b.kdItem and a.podlrNoPoint = :poDlr and b.lokasi = :lokasi and b.qtyOnhand > 0";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("lokasi", lokasi);
		q.setString("poDlr", poDlr);

		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH100Stglobals> findKdItemBynameAndLokasi(String kdItem,
			String lokasi) {
		String hql = "from HondaH100Stglobals where kdItem= :kdItem and lokasi= :lokasi";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdItem", kdItem);
		q.setString("lokasi", lokasi);
		List<HondaH100Stglobals> list = q.list();
		
		return list;
	}

}
