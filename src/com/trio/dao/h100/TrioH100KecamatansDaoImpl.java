package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Kecamatans;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class TrioH100KecamatansDaoImpl extends TrioHibernateDaoSupport implements TrioH100KecamatansDao {
	
	@Transactional(readOnly = false)
	public void saveOrUpdate(TrioH100Kecamatans kecamatan, String user){
		getHibernateTemplate().saveOrUpdate(kecamatan);
	}
	
	@Transactional(readOnly = false)
	public void delete(TrioH100Kecamatans kecamatan) throws DataAccessException {
		getHibernateTemplate().delete(kecamatan);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100Kecamatans> findAll() throws DataAccessException {
		List<TrioH100Kecamatans> list = getHibernateTemplate().find("from TrioH100Kecamatans");
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100Kecamatans> findByCriteria(TrioH100Kecamatans kecamatan) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(TrioH100Kecamatans.class);
		if (kecamatan.getKdKec() != null){
			criteria = criteria.add(Restrictions.eq("kdKec", kecamatan.getKdKec()));
		}
		if (kecamatan.getKecamatan() != null){
			criteria = criteria.add(Restrictions.like("kecamatan","%"+kecamatan.getKecamatan().toUpperCase()+"%"));
		}
		if (kecamatan.getKota() != null){
			criteria = criteria.add(Restrictions.eq("kota", kecamatan.getKota()));
		}
		if (kecamatan.getKdKota() != null){
			criteria = criteria.add(Restrictions.eq("kdKota", kecamatan.getKdKota()));
		}
		
		List<TrioH100Kecamatans> list = getHibernateTemplate().findByCriteria(criteria);
		
		return list;
	}

	@Override
	@Transactional(readOnly = true)
	public List<TrioH100Kecamatans> findByExample(TrioH100Kecamatans kecamatan) {
		return getHibernateTemplate().findByExample(kecamatan);
	}

//	@Override
//	public List<TrioH100Kecamatans> findByCriteria(TrioH100Kecamatans domain) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	
	@Transactional(readOnly=true)
	public TrioH100Kecamatans findByKdKec(String kdKec){
		String str = "from TrioH100Kecamatans where kdKec = :kdKec";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("kdKec", kdKec);
		return (TrioH100Kecamatans) q.uniqueResult();
	}

	@Override
	public void save(TrioH100Kecamatans domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH100Kecamatans domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TrioH100Kecamatans findByPrimaryKey(TrioH100Kecamatans domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
