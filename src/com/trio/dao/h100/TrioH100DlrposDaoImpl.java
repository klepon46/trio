package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Dlrpos;
import com.trio.hibernate.TrioHibernateDaoSupport;

/** @author Saifi Ahmada Jan 22, 2013 3:41:04 PM  **/

public class TrioH100DlrposDaoImpl extends TrioHibernateDaoSupport implements TrioH100DlrposDao {

	@Override
	public void saveOrUpdate(TrioH100Dlrpos domain, String user) {
		// TODO , masbro
		
		
	}

	@Override
	public void save(TrioH100Dlrpos domain, String user) {
		// TODO , masbro
		
		
	}

	@Override
	public void update(TrioH100Dlrpos domain, String user) {
		// TODO , masbro
		
		
	}

	@Override
	public void delete(TrioH100Dlrpos domain) {
		// TODO , masbro
		
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	@Override
	public List<TrioH100Dlrpos> findAll() {
		String str = "select d from TrioH100Dlrpos d where d.status = 'A' and d.kdDlr <> d.kdDlrpos";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		
		List<TrioH100Dlrpos> list = q.list();
		return list;
	}

	@Override
	public List<TrioH100Dlrpos> findByExample(TrioH100Dlrpos domain) {
		// TODO , masbro
		
		return null;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100Dlrpos> findByCriteria(TrioH100Dlrpos pos) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(TrioH100Dlrpos.class);
		
		if (pos.getNamaDlrpos() != null){
			criteria = criteria.add(Restrictions.like("namaDlrpos", "%"+pos.getNamaDlrpos().toUpperCase()+"%"));
		}
		criteria = criteria.add(Restrictions.eq("status", "A"));
		criteria = criteria.add(Restrictions.neProperty("kdDlr", "kdDlrpos"));
		
		List<TrioH100Dlrpos> list = getHibernateTemplate().findByCriteria(criteria);
		return list;
	}

	@Override
	public TrioH100Dlrpos findByPrimaryKey(TrioH100Dlrpos domain) {
		// TODO , masbro
		
		return null;
	}

}

