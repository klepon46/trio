package com.trio.dao.h100;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Kelurahans;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class TrioH100KelurahansDaoImpl extends TrioHibernateDaoSupport implements TrioH100KelurahansDao {
	
	@Transactional
	public void saveOrUpdate(TrioH100Kelurahans kelurahan, String user){
		getHibernateTemplate().saveOrUpdate(kelurahan);
	}
	
	@Transactional
	public void delete(TrioH100Kelurahans kelurahan) throws DataAccessException {
		getHibernateTemplate().delete(kelurahan);
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100Kelurahans> findAll() throws DataAccessException {
		List<TrioH100Kelurahans> list = getHibernateTemplate().find("from TrioH100Kelurahans");
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<TrioH100Kelurahans> getKelurahanByCriteria(TrioH100Kelurahans kelurahan) throws DataAccessException {
		DetachedCriteria criteria = DetachedCriteria.forClass(TrioH100Kelurahans.class);
		if (kelurahan.getKdKel() != null){
			criteria = criteria.add(Restrictions.eq("kdKel", kelurahan.getKdKel()));
		}
		if (kelurahan.getKdKec() != null){
			criteria = criteria.add(Restrictions.eq("kdKec", kelurahan.getKdKec()));
		}
		if (kelurahan.getKelurahan() != null){
			criteria = criteria.add(Restrictions.eq("kelurahan", kelurahan.getKelurahan()));
		}
		if (kelurahan.getKota() != null){
			criteria = criteria.add(Restrictions.eq("kota", kelurahan.getKota()));
		}
		List<TrioH100Kelurahans> list = getHibernateTemplate().findByCriteria(criteria);
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TrioH100Kelurahans> findByExample(TrioH100Kelurahans domain) {
		List<TrioH100Kelurahans> list = getHibernateTemplate().findByExample(domain);
		return list;
	}

	@Override
	public List<TrioH100Kelurahans> findByCriteria(TrioH100Kelurahans domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(TrioH100Kelurahans domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH100Kelurahans domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TrioH100Kelurahans findByPrimaryKey(TrioH100Kelurahans domain) {
		// TODO Auto-generated method stub
		return null;
	}


}
