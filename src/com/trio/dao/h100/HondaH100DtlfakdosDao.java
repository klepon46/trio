package com.trio.dao.h100;

import java.math.BigDecimal;
import java.util.List;

import com.trio.bean.h100.HondaH100Dtlfakdos;
import com.trio.hibernate.TrioGenericDao;



public interface HondaH100DtlfakdosDao extends TrioGenericDao<HondaH100Dtlfakdos>  {
	public Long getQtyPoOpenByPoIntAndKdItem(String noPoInt, String kdItem);
	public List<HondaH100Dtlfakdos> findBynoDo(String noDo);
	public void updateTransaction(HondaH100Dtlfakdos dtlFakdos, String user);
	public BigDecimal getNilaiDppByNoDo(String fakdoNoDo);
}
