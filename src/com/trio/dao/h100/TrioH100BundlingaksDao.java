package com.trio.dao.h100;

import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:14:48 AM Dec 26, 2013
 */

public interface TrioH100BundlingaksDao extends TrioGenericDao<TrioH100Bundlingaks>{
	
	public String processAll(TrioH100Bundlingaks domain, String user);
	public Long getCount(String kdItem, String lokasi);

}
