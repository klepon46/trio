package com.trio.dao.h100;


import com.trio.bean.h100.TrioH100PelangWils;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH100PelangWilsDao extends TrioGenericDao<TrioH100PelangWils> {

	public String saveTransaction(TrioH100PelangWils pelangWils, String user);
	
	public void updateTransaction (TrioH100PelangWils pelangWils, String user);
	
	public long getCountApprove(String kdDlr,String bulan);	
}
