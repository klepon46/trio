package com.trio.dao.h100;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Stglobals;
import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.bean.h100.TrioH100Dtlbundlingaks;
import com.trio.hibernate.TrioHibernateDaoSupport;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.TrioDateConv;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;

/**
 * @author Gusti Arya 11:15:12 AM Dec 26, 2013
 */

public class TrioH100BundlingaksDaoImpl extends TrioHibernateDaoSupport implements TrioH100BundlingaksDao {

	@Override
	public void saveOrUpdate(TrioH100Bundlingaks domain, String user) {
		// TODO Auto-generated method stub
	}

	@Override
	@Transactional(readOnly=false)
	public void save(TrioH100Bundlingaks domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
	}

	@Override
	@Transactional(readOnly=true)
	public void update(TrioH100Bundlingaks domain, String user) {
		
	}

	@Override
	public void delete(TrioH100Bundlingaks domain) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<TrioH100Bundlingaks> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100Bundlingaks> findByExample(TrioH100Bundlingaks domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100Bundlingaks> findByCriteria(TrioH100Bundlingaks domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH100Bundlingaks findByPrimaryKey(TrioH100Bundlingaks domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false)
	public String processAll(TrioH100Bundlingaks domain, String user) {
		
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
		
		Set<TrioH100Dtlbundlingaks> setBundle = domain.getTrioH100DtlbundlingaksSet();
		
		for(TrioH100Dtlbundlingaks current : setBundle){
			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
			
			String updHondaH100StDetails = "update HondaH100Stdetails set kdItem = :kdItemBaru where noMesin = :noMesin "
					+"and noRangka = :noRangka and kdItem = :kdItem";
			Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH100StDetails);
			q.setString("kdItemBaru", current.getKdItemBaru());
			q.setString("noMesin", current.getTrioH100DtlbundlingaksPK().getNoMesin());
			q.setString("noRangka", current.getNoRangka());
			q.setString("kdItem", current.getKdItemAsal());
			q.executeUpdate();

			String updTrioH100StDetails = "update TrioH100Stdetails set kdItem = :kdItemBaru where noMesin = :noMesin "
					+"and noRangka = :noRangka and kdItem = :kdItem";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updTrioH100StDetails);
			q.setString("kdItemBaru", current.getKdItemBaru());
			q.setString("noMesin", current.getTrioH100DtlbundlingaksPK().getNoMesin());
			q.setString("noRangka", current.getNoRangka());
			q.setString("kdItem", current.getKdItemAsal());
			q.executeUpdate();


			String updHondaH100Dtlsublokasi = "update HondaH100Dtlsublokasi set vkodeitem = :kdItemBaru where vnomesin = :noMesin "
					+ "and vkodeitem = :kdItem ";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH100Dtlsublokasi);
			q.setString("kdItemBaru", current.getKdItemBaru());
			q.setString("noMesin", current.getTrioH100DtlbundlingaksPK().getNoMesin());
			q.setString("kdItem", current.getKdItemAsal());
			q.executeUpdate();

			String updHondaH100Stglobals = "update HondaH100Stglobals s set s.qtyOnhand = " 
					+ "(select count(*) from HondaH100Stdetails where kdItem= :kdItem and status='A' and lokasi= :lokasi) "
					+ "where s.lokasi = :lokasi and s.kdItem = :kdItem";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH100Stglobals);
			q.setString("kdItem", current.getKdItemAsal());
			q.setString("lokasi", current.getLokasi());
			q.executeUpdate();

			List<HondaH100Stglobals> listCek = getMasterFacade().getHondaH100StglobalsDao()
					.findKdItemBynameAndLokasi(current.getKdItemBaru(), current.getLokasi());
			
			if(listCek.size() == 0){
				System.out.println("Masuk tidak ada");
				
				HondaH100Stglobals globals = new HondaH100Stglobals();
				Long qtyOnhand = getCount(current.getKdItemBaru(), current.getLokasi());
				
				globals.setLokasi(current.getLokasi());
				globals.setQtyOnhand(qtyOnhand);
				globals.setKdItem(current.getKdItemBaru());
				getHibernateTemplate().getSessionFactory().getCurrentSession().save(globals);
				
				q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH100Stglobals);
				q.setString("kdItem", current.getKdItemAsal());
				q.setString("lokasi", current.getLokasi());
				q.executeUpdate();
			}else{
				System.out.println("Masuk Ada");
				
				String updHondaH100Stglobals2 = "update HondaH100Stglobals s set s.qtyOnhand = " 
						+ "(select count(*) from HondaH100Stdetails where kdItem= :kdItem and status='A' and lokasi= :lokasi) "
						+ "where s.lokasi = :lokasi and s.kdItem = :kdItem";
				q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH100Stglobals2);
				q.setString("kdItem", current.getKdItemBaru());
				q.setString("lokasi", current.getLokasi());
				q.executeUpdate();
				
				q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH100Stglobals);
				q.setString("kdItem", current.getKdItemAsal());
				q.setString("lokasi", current.getLokasi());
				q.executeUpdate();
			}
		}
		
		String str = getMasterFacade().getHondaH000SetupsDao().getRunningNumber("BDL", TrioDateConv.format(new Date(),"yyyy"), user);
		String kdBundling = TrioStringUtil.getFormattedRunno4("BDL", Integer.valueOf(str));
		
		return kdBundling;
		
	}

	@Override
	@Transactional(readOnly=true)
	public Long getCount(String kdItem, String lokasi) {
		String hql = "select count(*) from HondaH100Stdetails where kdItem= :kdItem and status='A' and lokasi= :lokasi";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdItem", kdItem);
		q.setString("lokasi", lokasi);
		
		return (Long) q.uniqueResult();
	}

}
