package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class TrioH100DtldologsDaoImpl extends TrioHibernateDaoSupport implements TrioH100DtldologsDao {

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioH100DtldologsDao domain, String user) {
		// TODO Auto-generated method stub
		getHibernateTemplate().save(domain);
	}

	@Override
	public void delete(TrioH100DtldologsDao domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<TrioH100DtldologsDao> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100DtldologsDao> findByExample(TrioH100DtldologsDao domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH100DtldologsDao> findByCriteria(TrioH100DtldologsDao domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(TrioH100DtldologsDao domain, String user) {
		
		
	}

	@Override
	public void update(TrioH100DtldologsDao domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TrioH100DtldologsDao findByPrimaryKey(TrioH100DtldologsDao domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public Long sumQtyByNoDolog(String noDolog) {
		String str = "select sum(qty) from TrioH100Dtldologs where dologNoDolog = :noDolog";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noDolog", noDolog);
		
		return (Long) q.uniqueResult();
	}
	
}
