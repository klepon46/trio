package com.trio.dao.h100;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.HondaH100Dtlsublokasi;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH100DtlsublokasiDaoImpl extends TrioHibernateDaoSupport implements HondaH100DtlsublokasiDao {

	@Override
	@Transactional(readOnly = false) 
	public void saveOrUpdate(HondaH100Dtlsublokasi domain, String user) {
		getHibernateTemplate().saveOrUpdate(domain);
	}

	@Override
	public void delete(HondaH100Dtlsublokasi domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH100Dtlsublokasi> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Dtlsublokasi> findByExample(
			HondaH100Dtlsublokasi domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH100Dtlsublokasi> findByCriteria(
			HondaH100Dtlsublokasi domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(HondaH100Dtlsublokasi domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH100Dtlsublokasi domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH100Dtlsublokasi findByPrimaryKey(HondaH100Dtlsublokasi domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public String getVkodeItem(String noMesin) {
		String str = "select vkodeitem from HondaH100Dtlsublokasi where vnomesin = :noMesin";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noMesin", noMesin);
		String result = (String) q.uniqueResult();
		
		return result;
	}

	@Override
	@Transactional(readOnly=true)
	public String getVselByNoMesin(String noMesin) {
		String str = "select vsel from HondaH100Dtlsublokasi where vnomesin = :noMesin";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("noMesin", noMesin);
		String result = (String) q.uniqueResult();
		
		return result.substring(0,1);
	}
	
	

}
