package com.trio.dao.h100;

import com.trio.bean.h100.HondaH100Dtlsublokasi;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH100DtlsublokasiDao extends TrioGenericDao<HondaH100Dtlsublokasi> { 
	
	public String getVkodeItem(String noMesin);
	public String getVselByNoMesin(String noMesin);
	
}
