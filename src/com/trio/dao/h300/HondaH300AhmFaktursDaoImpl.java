package com.trio.dao.h300;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.HondaH300AhmFakturs;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 9:21:08 AM Jan 17, 2014
 */

public class HondaH300AhmFaktursDaoImpl extends TrioHibernateDaoSupport implements HondaH300AhmFaktursDao{

	@Override
	public void saveOrUpdate(HondaH300AhmFakturs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(HondaH300AhmFakturs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH300AhmFakturs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH300AhmFakturs domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<HondaH300AhmFakturs> findAll() {
		Criteria cr = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(HondaH300AhmFakturs.class);
		cr.add(Restrictions.eq("invDate", ""));
		cr.setProjection(Projections.projectionList().add(Projections.sum("qty"))
				.add(Projections.groupProperty("hondaH300AhmFaktursPK.psNo")));
		
		
		return cr.list();
	}

	@Override
	public List<HondaH300AhmFakturs> findByExample(HondaH300AhmFakturs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300AhmFakturs> findByCriteria(HondaH300AhmFakturs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH300AhmFakturs findByPrimaryKey(HondaH300AhmFakturs domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
