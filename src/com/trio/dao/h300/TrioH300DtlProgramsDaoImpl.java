package com.trio.dao.h300;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.TrioH300DtlPrograms;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class TrioH300DtlProgramsDaoImpl extends TrioHibernateDaoSupport implements TrioH300DtlProgramsDao {

	private static final long serialVersionUID = 1L;

	@Override
	public void saveOrUpdate(TrioH300DtlPrograms domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioH300DtlPrograms domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH300DtlPrograms domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioH300DtlPrograms domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioH300DtlPrograms> findAll() {
		List<TrioH300DtlPrograms> lists = getHibernateTemplate().find("from TrioH300DtlPrograms");
		return lists;
	}

	@Override
	public List<TrioH300DtlPrograms> findByExample(TrioH300DtlPrograms domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH300DtlPrograms> findByCriteria(TrioH300DtlPrograms domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH300DtlPrograms findByPrimaryKey(TrioH300DtlPrograms domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioH300DtlPrograms> findItemById(String idProgram) {
		String str = "from TrioH300DtlPrograms a where  a.trioH300DtlProgramsPK.idProgram= :idProgram";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("idProgram", idProgram);
		List<TrioH300DtlPrograms> lists = q.list();
		
		return lists;
		
	}
}
