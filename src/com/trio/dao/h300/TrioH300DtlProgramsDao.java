package com.trio.dao.h300;

import java.util.List;

import com.trio.bean.h300.TrioH300DtlPrograms;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH300DtlProgramsDao extends TrioGenericDao<TrioH300DtlPrograms>{
	
	public List<TrioH300DtlPrograms> findItemById(String idProgram);

}
