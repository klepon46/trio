package com.trio.dao.h300;

import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.bean.h100.TrioH100Dtlbundlingaks;
import com.trio.bean.h300.HondaH300InoutManuals;
import com.trio.bean.h300.HondaH300PartLokases;
import com.trio.bean.h300.HondaH300PartLokasesPK;
import com.trio.hibernate.TrioHibernateDaoSupport;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.TrioDateUtil;

/**
 * @author Gusti Arya 11:37:45 AM Dec 30, 2013
 */

public class HondaH300InoutManualsDaoImpl extends TrioHibernateDaoSupport implements HondaH300InoutManualsDao {

	@Override
	public void saveOrUpdate(HondaH300InoutManuals domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly=false)
	public void save(HondaH300InoutManuals domain, String user) {
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);

	}

	@Override
	public void update(HondaH300InoutManuals domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(HondaH300InoutManuals domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH300InoutManuals> findAll() {
		String hql = "from HondaH300InoutManuals";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		return q.list();
	}

	@Override
	public List<HondaH300InoutManuals> findByExample(
			HondaH300InoutManuals domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300InoutManuals> findByCriteria(
			HondaH300InoutManuals domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH300InoutManuals findByPrimaryKey(HondaH300InoutManuals domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=false, value="transactionManager2")
	public void saveInOut(HondaH300InoutManuals domain, TrioH100Bundlingaks domain2, String gudang,String rak,String user) {
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		Set<TrioH100Dtlbundlingaks> setBundling = domain2.getTrioH100DtlbundlingaksSet();
		
		for(TrioH100Dtlbundlingaks current : setBundling){
			String updHondaH300PartLokases = "update HondaH300PartLokases h set h.qtyOnh = h.qtyOnh - :vqty "
					+"where h.hondaH300PartLokasesPK.rakLokLokasiKodeLokasi = :gudang "
					+"and h.hondaH300PartLokasesPK.rakLokRakNoRak = :rak "
					+"and h.hondaH300PartLokasesPK.partpartNo = :partNo ";
			Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(updHondaH300PartLokases);
			q.setInteger("vqty", current.getQty());
			q.setString("gudang", gudang);
			q.setString("rak", rak);
			q.setString("partNo", current.getTrioH100DtlbundlingaksPK().getPartNo());
			q.executeUpdate();
		}
		
		domain.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		domain.getUserTrailing().setVcreaby(user);
		getHibernateTemplate().getSessionFactory().getCurrentSession().save(domain);
	}
}
