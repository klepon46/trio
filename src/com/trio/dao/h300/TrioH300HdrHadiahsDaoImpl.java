package com.trio.dao.h300;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.TrioH300HdrHadiahs;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class TrioH300HdrHadiahsDaoImpl extends TrioHibernateDaoSupport implements TrioH300HdrHadiahsDao {

	@Override
	public void saveOrUpdate(TrioH300HdrHadiahs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioH300HdrHadiahs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(TrioH300HdrHadiahs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(TrioH300HdrHadiahs domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioH300HdrHadiahs> findAll() {
		List<TrioH300HdrHadiahs> lists = getHibernateTemplate().find("from TrioH300HdrHadiahs");
		return lists;
	}

	@Override
	public List<TrioH300HdrHadiahs> findByExample(TrioH300HdrHadiahs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH300HdrHadiahs> findByCriteria(TrioH300HdrHadiahs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH300HdrHadiahs findByPrimaryKey(TrioH300HdrHadiahs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioH300HdrHadiahs> findHadiahById(String id) {
		
		String str = "from TrioH300HdrHadiahs a where a.trioH300HdrHadiahsPK.idProgram = :id";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("id", id);
		List<TrioH300HdrHadiahs> lists = q.list();
		
		return lists;
	}
}
