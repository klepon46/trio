package com.trio.dao.h300;

import java.util.List;

import com.trio.bean.h300.TrioH300HdrHadiahs;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH300HdrHadiahsDao extends TrioGenericDao<TrioH300HdrHadiahs> {
	
	public List<TrioH300HdrHadiahs> findHadiahById(String id);

}
