package com.trio.dao.h300;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.TrioH300HdrPrograms;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class TrioH300HdrProgramsDaoImpl extends TrioHibernateDaoSupport implements TrioH300HdrProgramsDao {

	@Override
	public void saveOrUpdate(TrioH300HdrPrograms domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(TrioH300HdrPrograms domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioH300HdrPrograms domain, String user) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().update(domain);
		
	}

	@Override
	@Transactional(readOnly=false)
	public void delete(TrioH300HdrPrograms domain) {
		getHibernateTemplate().getSessionFactory().getCurrentSession().delete(domain);
	}

	@Override
	@Transactional(readOnly=true)
	public List<TrioH300HdrPrograms> findAll() {
		List<TrioH300HdrPrograms> lists = getHibernateTemplate().find("from TrioH300HdrPrograms");
		return lists;
	}

	@Override
	public List<TrioH300HdrPrograms> findByExample(TrioH300HdrPrograms domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<TrioH300HdrPrograms> findByCriteria(TrioH300HdrPrograms domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrioH300HdrPrograms findByPrimaryKey(TrioH300HdrPrograms domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
