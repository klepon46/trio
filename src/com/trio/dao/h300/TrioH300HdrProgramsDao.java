package com.trio.dao.h300;

import com.trio.bean.h300.TrioH300HdrPrograms;
import com.trio.hibernate.TrioGenericDao;

public interface TrioH300HdrProgramsDao extends TrioGenericDao<TrioH300HdrPrograms>{

}
