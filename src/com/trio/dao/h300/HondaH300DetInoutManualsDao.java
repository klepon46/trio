package com.trio.dao.h300;

import com.trio.bean.h300.HondaH300DetInoutManuals;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:38:30 AM Dec 30, 2013
 */

public interface HondaH300DetInoutManualsDao extends TrioGenericDao<HondaH300DetInoutManuals>{

}
