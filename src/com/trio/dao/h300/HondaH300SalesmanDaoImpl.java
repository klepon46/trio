package com.trio.dao.h300;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.HondaH300Salesman;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public class HondaH300SalesmanDaoImpl extends TrioHibernateDaoSupport implements HondaH300SalesmanDao {

	@Override
	public void saveOrUpdate(HondaH300Salesman domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH300Salesman domain) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HondaH300Salesman> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300Salesman> findByExample(HondaH300Salesman domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300Salesman> findByCriteria(HondaH300Salesman domain) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<HondaH300Salesman> findByStatusAndBussiness(String status, String bussiness){
		String str = "from HondaH300Salesman where status = :status and bussiness = :bussiness";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("status", status);
		q.setString("bussiness", bussiness);
		return q.list();
	}

	@Override
	public void save(HondaH300Salesman domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH300Salesman domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH300Salesman findByPrimaryKey(HondaH300Salesman domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
