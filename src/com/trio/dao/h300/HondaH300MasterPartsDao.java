package com.trio.dao.h300;

import java.math.BigDecimal;
import java.util.List;

import com.trio.bean.h300.HondaH300MasterParts;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 10:43:02 AM Dec 21, 2013
 */

public interface HondaH300MasterPartsDao extends TrioGenericDao<HondaH300MasterParts>{

	public List<HondaH300MasterParts> findByPartNo(String partNo);
	public HondaH300MasterParts findByPartNoManual(String partNo);
	public BigDecimal getHetByPartNO(String partNo);
	public BigDecimal getHargaPokokByPartNo(String partNo);
	
}
