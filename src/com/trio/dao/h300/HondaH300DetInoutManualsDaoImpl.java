package com.trio.dao.h300;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.HondaH300DetInoutManuals;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 11:38:53 AM Dec 30, 2013
 */

public class HondaH300DetInoutManualsDaoImpl extends TrioHibernateDaoSupport implements HondaH300DetInoutManualsDao{

	@Override
	public void saveOrUpdate(HondaH300DetInoutManuals domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(HondaH300DetInoutManuals domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH300DetInoutManuals domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(readOnly=false)
	public void delete(HondaH300DetInoutManuals domain) {
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH300DetInoutManuals> findAll() {
		String hql = "from HondaH300DetInoutManuals";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	public List<HondaH300DetInoutManuals> findByExample(
			HondaH300DetInoutManuals domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300DetInoutManuals> findByCriteria(
			HondaH300DetInoutManuals domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH300DetInoutManuals findByPrimaryKey(
			HondaH300DetInoutManuals domain) {
		// TODO Auto-generated method stub
		return null;
	}

}
