package com.trio.dao.h300;

import com.trio.bean.h300.HondaH300AhmFakturs;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 9:20:38 AM Jan 17, 2014
 */

public interface HondaH300AhmFaktursDao extends TrioGenericDao<HondaH300AhmFakturs>{

}
