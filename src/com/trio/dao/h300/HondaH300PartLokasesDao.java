package com.trio.dao.h300;

import java.util.List;

import com.trio.bean.h300.HondaH300PartLokases;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 9:09:21 AM Dec 27, 2013
 */

public interface HondaH300PartLokasesDao extends TrioGenericDao<HondaH300PartLokases> {
	
	public List<HondaH300PartLokases> findByGudRakNo(String gudang, String rak, String partNo);
	public HondaH300PartLokases findByGudRakNoObj(String gudang, String rak, String partNo);;

}
