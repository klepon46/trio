package com.trio.dao.h300;

import java.math.BigDecimal;
import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.HondaH300MasterParts;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 10:43:28 AM Dec 21, 2013
 */

public class HondaH300MasterPartsDaoImpl extends TrioHibernateDaoSupport implements HondaH300MasterPartsDao {

	@Override
	public void saveOrUpdate(HondaH300MasterParts domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(HondaH300MasterParts domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH300MasterParts domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH300MasterParts domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH300MasterParts> findAll() {
		String hql = "from HondaH300MasterParts";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	public List<HondaH300MasterParts> findByExample(HondaH300MasterParts domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300MasterParts> findByCriteria(HondaH300MasterParts domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH300MasterParts findByPrimaryKey(HondaH300MasterParts domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH300MasterParts> findByPartNo(String partNo) {
		String hql = "from HondaH300MasterParts where partNo = :partNo";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("partNo", partNo);
		return q.list();
	}

	@Override
	@Transactional(readOnly=true)
	public HondaH300MasterParts findByPartNoManual(String partNo) {
		String hql = "from HondaH300MasterParts where partNo = :partNo";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("partNo", partNo);
		
		return (HondaH300MasterParts) q.uniqueResult();
	}

	@Override
	@Transactional(readOnly=true)
	public BigDecimal getHetByPartNO(String partNo) {
		String hql = "select p.het from HondaH300MasterParts p where p.partNo = :partNo";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("partNo", partNo);
		BigDecimal bd = (BigDecimal) q.uniqueResult();
		
		return bd;
	}

	@Override
	@Transactional(readOnly=true)
	public BigDecimal getHargaPokokByPartNo(String partNo) {
		String hql = "select p.hargaPokok from HondaH300MasterParts p where p.partNo = :partNo";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("partNo", partNo);
		BigDecimal bd = (BigDecimal) q.uniqueResult();
		
		return bd;
	}

}
