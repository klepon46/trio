package com.trio.dao.h300;

import com.trio.bean.h300.HondaH300Ahmpses;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 10:25:57 AM Feb 14, 2014
 */

public interface HondaH300AhmpsesDao extends TrioGenericDao<HondaH300Ahmpses> {

}
