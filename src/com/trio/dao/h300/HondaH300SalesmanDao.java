package com.trio.dao.h300;

import java.util.List;

import com.trio.bean.h300.HondaH300Salesman;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface HondaH300SalesmanDao extends TrioGenericDao<HondaH300Salesman> { 
	
	public List<HondaH300Salesman> findByStatusAndBussiness(String status, String bussiness);

}
