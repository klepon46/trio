package com.trio.dao.h300;

import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.bean.h300.HondaH300InoutManuals;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author Gusti Arya 11:37:21 AM Dec 30, 2013
 */

public interface HondaH300InoutManualsDao extends TrioGenericDao<HondaH300InoutManuals>{
	
	public void saveInOut(HondaH300InoutManuals domain, TrioH100Bundlingaks domain2, String gudang,String rak,String user);

}
