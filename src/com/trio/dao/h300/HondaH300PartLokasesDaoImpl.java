package com.trio.dao.h300;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h300.HondaH300PartLokases;
import com.trio.hibernate.TrioHibernateDaoSupport;

/**
 * @author Gusti Arya 9:09:50 AM Dec 27, 2013
 */

public class HondaH300PartLokasesDaoImpl extends TrioHibernateDaoSupport implements HondaH300PartLokasesDao{

	@Override
	public void saveOrUpdate(HondaH300PartLokases domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(HondaH300PartLokases domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH300PartLokases domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(HondaH300PartLokases domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH300PartLokases> findAll() {
		String hql = "from HondaH300PartLokases";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@Override
	public List<HondaH300PartLokases> findByExample(HondaH300PartLokases domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH300PartLokases> findByCriteria(HondaH300PartLokases domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HondaH300PartLokases findByPrimaryKey(HondaH300PartLokases domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH300PartLokases> findByGudRakNo(String gudang, String rak,
			String partNo) {
		String hql = "from HondaH300PartLokases a where a.hondaH300PartLokasesPK.rakLokLokasiKodeLokasi = :gudang "
				+ "and a.hondaH300PartLokasesPK.rakLokRakNoRak = :rak and  a.hondaH300PartLokasesPK.partpartNo = :partNo";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("gudang", gudang);
		q.setString("rak", rak);
		q.setString("partNo", partNo);
		
		return q.list();
	}

	@Override
	@Transactional(readOnly=true)
	public HondaH300PartLokases findByGudRakNoObj(String gudang, String rak,
			String partNo) {
		String hql = "from HondaH300PartLokases a where a.hondaH300PartLokasesPK.rakLokLokasiKodeLokasi = :gudang "
				+ "and a.hondaH300PartLokasesPK.rakLokRakNoRak = :rak and  a.hondaH300PartLokasesPK.partpartNo = :partNo";
		
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("gudang", gudang);
		q.setString("rak", rak);
		q.setString("partNo", partNo);
		HondaH300PartLokases hondaH300PartLokases = (HondaH300PartLokases) q.uniqueResult();
		
		return hondaH300PartLokases;
	}

}
