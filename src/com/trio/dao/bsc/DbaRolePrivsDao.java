package com.trio.dao.bsc;

import java.util.List;

import com.trio.bean.bsc.DbaRolePrivs;
import com.trio.hibernate.TrioGenericDao;

public interface DbaRolePrivsDao extends TrioGenericDao<DbaRolePrivs> {

	public List<DbaRolePrivs> getRoleByGrantee(String name);
	
	public boolean isManagerByUserSession(String userSession);
	
	public List<DbaRolePrivs> getListByGrantedRole(String role);
	
	public List<String> findRoleByDistinct();
	
	public List<String> findRoleDistinctByRole(String role);
	
}
