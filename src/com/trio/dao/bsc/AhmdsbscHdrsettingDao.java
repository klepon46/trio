package com.trio.dao.bsc;

import com.trio.bean.bsc.AhmdsbscHdrsetting;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 19, 2012
 */
public interface AhmdsbscHdrsettingDao extends TrioGenericDao<AhmdsbscHdrsetting> { 

}
