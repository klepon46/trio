package com.trio.dao.bsc;

import com.trio.bean.bsc.AhmdsbscDtlsetting;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 19, 2012
 */
public interface AhmdsbscDtlsettingDao extends TrioGenericDao<AhmdsbscDtlsetting> {
	public AhmdsbscDtlsetting findByVidAndVitemcode(String vid, String vitemcode);
}
