package com.trio.dao.bsc;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.bsc.DbaRolePrivs;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class DbaRolePrivsDaoImpl extends TrioHibernateDaoSupport implements DbaRolePrivsDao {

	@Override
	public void saveOrUpdate(DbaRolePrivs domain, String user) {
		// TODO Auto-generated method stub
	}

	@Override
	public void delete(DbaRolePrivs domain) {
		// TODO Auto-generated method stub

	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<DbaRolePrivs> findAll() {
		
		return null;
	}

	@Override
	public List<DbaRolePrivs> findByExample(DbaRolePrivs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<DbaRolePrivs> findByCriteria(DbaRolePrivs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<DbaRolePrivs> getRoleByGrantee(String name) {

		String str = "select a.granted_role from DbaRolePrivs a where a.grantee = :user";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createQuery(str);
		query.setString("user", name);
		List<DbaRolePrivs> lists = query.list();

		return lists;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=true)
	public boolean isManagerByUserSession(String userSession){
		
		boolean mgr = false;
		
		String str = "from DbaRolePrivs a where a.grantee = :userSession";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("userSession", userSession);
		
		List<DbaRolePrivs> list = q.list();
		for (DbaRolePrivs dba : list){
			if (dba.getGranted_role().equalsIgnoreCase("H1_MKT_MGR")){
				mgr = true;
			}
		}
		return mgr;
	}

	@Override
	public void save(DbaRolePrivs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(DbaRolePrivs domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DbaRolePrivs findByPrimaryKey(DbaRolePrivs domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<DbaRolePrivs> getListByGrantedRole(String role) {
		String st = "from DbaRolePrivs where granted_role = :role";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(st);
		q.setString("role", role);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<String> findRoleByDistinct() {
		String st = "select distinct a.granted_role from DbaRolePrivs a";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(st);
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<String> findRoleDistinctByRole(String role) {
		String st = "select distinct a.granted_role from DbaRolePrivs a where upper(a.granted_role) like '%"+role.toUpperCase()+"%'";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(st);
		return q.list();
	}
}
