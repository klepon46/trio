package com.trio.dao.h000;

import com.trio.bean.h000.HondaH000Propins;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH000PropinsDao extends TrioGenericDao<HondaH000Propins> { 

}
