package com.trio.dao.h000;

import com.trio.bean.h000.TrioH000Mstroleaccess;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Nov 1, 2012
 */
public interface TrioH000MstroleaccessDao extends TrioGenericDao<TrioH000Mstroleaccess> { 

}
