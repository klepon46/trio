package com.trio.dao.h000;

import java.util.List;

import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h000.HondaH000Items;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH000ItemsDaoImpl extends TrioHibernateDaoSupport implements HondaH000ItemsDao {

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(HondaH000Items domain, String user) {
		getHibernateTemplate().saveOrUpdate(domain);
	}

	@Override
	public void delete(HondaH000Items domain) {
		getHibernateTemplate().delete(domain);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly= true)
	public List<HondaH000Items> findAll() {
		List<HondaH000Items> list = getHibernateTemplate().find("from HondaH000Items");
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> findByExample(HondaH000Items domain) {
		List<HondaH000Items> list = getHibernateTemplate().findByExample(domain);
		return list;
	}

	@Override
	public List<HondaH000Items> findByCriteria(HondaH000Items domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(HondaH000Items domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH000Items domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH000Items findByPrimaryKey(HondaH000Items domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> getKdTipe() {
		
		String hql = "select distinct(kdTipe) from HondaH000Items";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		List<HondaH000Items> lists = q.list();
		
		return lists;
	}

	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> getKdItemByKdTipe(String kdTipe) {
		
		String hql = "from HondaH000Items where kdTipe like :kdTipe";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("kdTipe", kdTipe);
		List<HondaH000Items> lists = q.list();
		
		return lists;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> getKdItemByValTag() {
		String hql = "from HondaH000Items a where a.kdTipe in (select s.valChar from HondaH000Setups s where s.valId = 'H1_VARIASI_UNIT')";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> getKdItemByValTagAndKdTipe(String kdTipe) {
		String hql = "from HondaH000Items a where a.kdTipe in (select s.valTag from HondaH000Setups s where s.valId = 'H1_VARIASI_UNIT' and s.valTag = :valTag)";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		q.setString("valTag", kdTipe);
		return q.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> getKdItemByTrioStatus(String trioStatus, String kdTipe) {
		String sql = null;
		Query q = null;
		System.out.println("KD TIPE = " + kdTipe);
		if (kdTipe.equalsIgnoreCase("") || kdTipe == null || kdTipe.length() < 1) {
			System.out.println("masuk kode tipe null");
			System.out.println("KD TIPE = " + kdTipe);
			sql = "from HondaH000Items a where a.trioStatus = :trioStatus order by a.kdItem ";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sql);
			q.setString("trioStatus", trioStatus);
		} else {
			System.out.println("masuk kode tipe TIDAK null");
			System.out.println("KD TIPE = " + kdTipe);
			sql = "from HondaH000Items a where a.trioStatus = :trioStatus and kdTipe = :kdTipe order by a.kdItem ";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sql);
			q.setString("trioStatus", trioStatus);
			q.setString("kdTipe", kdTipe);
		}
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<HondaH000Items> getKdItemByTrioStatusIsNull(String kdTipe) {
		
		String sql = null;
		Query q = null;
		
		if (kdTipe.equalsIgnoreCase("") || kdTipe == null || kdTipe.length() < 1) {
			System.out.println("masuk kode tipe null");
			System.out.println("KD TIPE = " + kdTipe);
			sql = "from HondaH000Items a where a.trioStatus is null order by a.kdItem";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sql);
		} else {
			
			sql = "from HondaH000Items a where a.trioStatus is null and kdTipe = :kdTipe order by a.kdItem";
			q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(sql);
			q.setString("kdTipe", kdTipe);
		}
		return q.list();
	}
	
	
	@Transactional(readOnly=false)
	public void updateItemByKdItem(String kdItem, String status){
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("update HondaH000Items set trioStatus = :trioStatus  where kdItem = :kdItem ");
		query.setString("trioStatus", status);
		query.setString("kdItem", kdItem);
		int result = query.executeUpdate();
		System.out.println("Hasil Update = " + result);
	}


}
