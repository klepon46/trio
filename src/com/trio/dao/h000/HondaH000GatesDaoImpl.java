package com.trio.dao.h000;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h000.HondaH000Gates;
import com.trio.hibernate.TrioHibernateDaoSupport;

public class HondaH000GatesDaoImpl extends TrioHibernateDaoSupport implements HondaH000GatesDao {

	@SuppressWarnings("unchecked")
	@Transactional(readOnly= true)
	public List<HondaH000Gates> findAll() {
		return getHibernateTemplate().find("from HondaH000Gates");
	}

	@Override
	public void saveOrUpdate(HondaH000Gates domain, String user) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(HondaH000Gates domain) {
		// TODO Auto-generated method stub

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<HondaH000Gates> findByExample(HondaH000Gates domain) {
		List<HondaH000Gates> list = getHibernateTemplate().findByExample(domain);
		return list;
	}

	@Override
	public List<HondaH000Gates> findByCriteria(HondaH000Gates domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<HondaH000Gates> getHondaH000GatesByCriteria(HondaH000Gates HondaH000Gates) {

		DetachedCriteria criteria = DetachedCriteria.forClass(HondaH000Gates.class);
		criteria = criteria.add(Restrictions.eq("dblist", HondaH000Gates.getDblist()));

		List<HondaH000Gates> lists = getHibernateTemplate().findByCriteria(criteria);

		return lists;
	}

	@Transactional(readOnly=true)
	@Override
	public List<HondaH000Gates> getHondaH000GatesbyCriteria(HondaH000Gates clazz, int firstResult,
			int maxResults) {

		DetachedCriteria criteria = DetachedCriteria.forClass(HondaH000Gates.class);
		List<HondaH000Gates> lists = getHibernateTemplate().findByCriteria(criteria, firstResult, maxResults);

		return lists;
	}


	@Transactional(readOnly=true)
	public HondaH000Gates findByHondaH000GatesName(String dblist) {

		String str = "from HondaH000Gates where dblist = :dblist";
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		query.setString("dblist", dblist);
		HondaH000Gates hondaH000Gates = (HondaH000Gates) query.uniqueResult();

		return hondaH000Gates;
	}

	@Override
	public void save(HondaH000Gates domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(HondaH000Gates domain, String user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public HondaH000Gates findByPrimaryKey(HondaH000Gates domain) {
		// TODO Auto-generated method stub
		return null;
	}


}
