package com.trio.dao.h000;

import java.util.List;

import com.trio.bean.h000.TrioH000Mstmenu;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 31, 2012
 */
public interface TrioH000MstmenuDao extends TrioGenericDao<TrioH000Mstmenu> { 
	
	public void save(TrioH000Mstmenu mstMenu, String user);
	
	public void update(TrioH000Mstmenu mstMenu, String user);
	
	public TrioH000Mstmenu findByPrimaryKey(String vmenuid);
	
	public List<TrioH000Mstmenu> getListMenuByUser(String user);
}
