package com.trio.dao.h000;

import java.util.List;

import com.trio.bean.h000.HondaH000Gates;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH000GatesDao extends TrioGenericDao<HondaH000Gates> {
	
	public List<HondaH000Gates> getHondaH000GatesByCriteria(HondaH000Gates HondaH000Gates);

	public List<HondaH000Gates> getHondaH000GatesbyCriteria(HondaH000Gates clazz, int firstResult, int maxResults);

	public List<HondaH000Gates> findAll();

	public HondaH000Gates findByHondaH000GatesName (String dblist);
	
}
