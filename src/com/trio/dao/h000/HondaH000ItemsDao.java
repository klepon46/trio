package com.trio.dao.h000;

import java.util.List;

import com.trio.bean.h000.HondaH000Items;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH000ItemsDao extends TrioGenericDao<HondaH000Items> {
	
	
	public List<HondaH000Items> getKdTipe();
	public List<HondaH000Items> getKdItemByKdTipe(String kdTipe);
	public List<HondaH000Items> getKdItemByValTag();
	public List<HondaH000Items> getKdItemByValTagAndKdTipe(String kdTipe);
	public List<HondaH000Items> getKdItemByTrioStatus(String trioStatus, String kdTipe);
	public List<HondaH000Items> getKdItemByTrioStatusIsNull(String kdTipe);
	public void updateItemByKdItem(String kdItem, String status);
	
}
