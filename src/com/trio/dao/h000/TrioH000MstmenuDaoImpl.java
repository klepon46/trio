package com.trio.dao.h000;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.trio.bean.h000.TrioH000Mstmenu;
import com.trio.hibernate.TrioHibernateDaoSupport;
import com.trio.util.TrioDateUtil;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 31, 2012
 */
public class TrioH000MstmenuDaoImpl extends TrioHibernateDaoSupport implements TrioH000MstmenuDao {

	@Override
	@Transactional(readOnly=false)
	public void saveOrUpdate(TrioH000Mstmenu domain, String user) {
		TrioH000Mstmenu mstMenu = findByPrimaryKey(domain.getVmenuid());
		if (mstMenu == null){
			save(domain, user);
		}else{
			update(domain, user);
		}
	}

	@Override
	public void delete(TrioH000Mstmenu domain) {
		// TODO Auto-generated method stub
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<TrioH000Mstmenu> findAll() {
		List<TrioH000Mstmenu> list = getHibernateTemplate().find("from TrioH000Mstmenu order by vparent,norderer asc");
		return list;
	}

	@Override
	public List<TrioH000Mstmenu> findByExample(TrioH000Mstmenu domain) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<TrioH000Mstmenu> findByCriteria(TrioH000Mstmenu domain) {
		DetachedCriteria c = DetachedCriteria.forClass(TrioH000Mstmenu.class);
		if (domain.getVmenuid() != null){
			c = c.add(Restrictions.eq("vmenuid", domain.getVmenuid()));
		}
		if (domain.getVlocation() != null){
			c = c.add(Restrictions.eq("vlocation", domain.getVlocation()));
		}
		if (domain.getVimage() != null){
			c = c.add(Restrictions.eq("vimage", domain.getVimage()));
		}
		if (domain.getVtitle() != null){
			c = c.add(Restrictions.sqlRestriction("upper(vtitle) like ('%"+domain.getVtitle().toUpperCase()+"%') "));
		}
		if (domain.getVparent() != null){
			c = c.add(Restrictions.eq("vparent", domain.getVparent()));
		}
		return getHibernateTemplate().findByCriteria(c);
	}

	@Override
	@Transactional(readOnly=false)
	public void save(TrioH000Mstmenu mstMenu, String user) {
		mstMenu.getUserTrailing().setVcreaby(user);
		mstMenu.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
		getHibernateTemplate().persist(mstMenu);
		getHibernateTemplate().flush();
		getHibernateTemplate().clear();
	}

	@Override
	@Transactional(readOnly=false)
	public void update(TrioH000Mstmenu mstMenu, String user) {
		mstMenu.getUserTrailing().setVmodiby(user);
		mstMenu.getUserTrailing().setDmodi(TrioDateUtil.getLongSysDateOracle());
		getHibernateTemplate().merge(mstMenu);
		getHibernateTemplate().flush();
		getHibernateTemplate().clear();
	}

	@Override
	@Transactional(readOnly=true)
	public TrioH000Mstmenu findByPrimaryKey(String vmenuid) {
		String str = "from TrioH000Mstmenu where vmenuid = :vmenuid";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(str);
		q.setString("vmenuid", vmenuid);
		return (TrioH000Mstmenu) q.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true) 
	public List<TrioH000Mstmenu> getListMenuByUser(String user){
		String st = "select mm.* from TRIO_H000_MSTROLEACCESS mr " +
				" join TRIO_H000_MSTMENU mm on mm.VMENUID = mr.VMENUID " +
				"join DBA_ROLE_PRIVS dba on dba.GRANTED_ROLE = mr.VROLEID " +
				" where dba.GRANTEE = :user and mr.VSTATUS = 'A' " +
				"order by mm.vparent,mm.norderer asc";
		
		SQLQuery sq = getHibernateTemplate().getSessionFactory().getCurrentSession()
				.createSQLQuery(st);
		sq.setString("user", user);
		sq.addEntity(TrioH000Mstmenu.class);
		
		List<TrioH000Mstmenu> list = sq.list();
		return list;
	}

	@Override
	@Transactional(readOnly=true) 
	public TrioH000Mstmenu findByPrimaryKey(TrioH000Mstmenu domain) {
		String st = "from TrioH000Mstmenu where vmenuid = :vmenuid";
		Query q = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(st);
		q.setString("vmenuid", domain.getVmenuid());
		return (TrioH000Mstmenu) q.uniqueResult();
	}

}
