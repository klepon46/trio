package com.trio.dao.h000;


import com.trio.bean.h000.HondaH000Dealers;
import com.trio.hibernate.TrioGenericDao;

public interface HondaH000DealersDao extends TrioGenericDao<HondaH000Dealers> {
	
	public HondaH000Dealers findByKdDlr(String kdDlr);
	public HondaH000Dealers getDlrArea(String dealer);
	
}
