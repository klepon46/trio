package com.trio.dao.h000;

import com.trio.bean.h000.HondaH000Setdlrs;
import com.trio.hibernate.TrioGenericDao;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
public interface HondaH000SetdlrsDao extends TrioGenericDao<HondaH000Setdlrs> { 
	public HondaH000Setdlrs findByKdDealerAndKodeBisnis(String kdDlr, String kodeBisnis);
}
