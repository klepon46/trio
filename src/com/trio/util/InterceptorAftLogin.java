package com.trio.util;

import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

public class InterceptorAftLogin implements Initiator {

	@Override
	public void doAfterCompose(Page arg0) throws Exception {
		if(LoginManager.getIntance().isAuthenticated()){
			Executions.getCurrent().sendRedirect("index.zul");
		}
	}

	@Override
	public boolean doCatch(Throwable arg0) throws Exception {
		return false;
	}

	@Override
	public void doFinally() throws Exception {

	}

	@Override
	public void doInit(Page arg0, Map arg1) throws Exception {
	}

}
