package com.trio.bean.helper;

import java.math.BigDecimal;

/**
 * @author Gusti Arya 11:13:33 AM Dec 27, 2013
 */

public class TrioBeanUbahTipeHelper {
	
	private String noMesin;
	private String noRangka;
	private String lokasi;
	private String kodePaket;
	private String kodeItemAsal;
	private String kodeItemBaru;
	private String noPart;
	private Integer qty;
	private BigDecimal hargaNett;
	
	public TrioBeanUbahTipeHelper() {
		// TODO Auto-generated constructor stub
	}
	
	public TrioBeanUbahTipeHelper(String noPart) {
		super();
		this.noPart = noPart;
	}
	
	public String getNoMesin() {
		return noMesin;
	}
	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}
	public String getNoRangka() {
		return noRangka;
	}
	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getKodePaket() {
		return kodePaket;
	}
	public void setKodePaket(String kodePaket) {
		this.kodePaket = kodePaket;
	}
	public String getKodeItemAsal() {
		return kodeItemAsal;
	}
	public void setKodeItemAsal(String kodeItemAsal) {
		this.kodeItemAsal = kodeItemAsal;
	}
	public String getKodeItemBaru() {
		return kodeItemBaru;
	}
	public void setKodeItemBaru(String kodeItemBaru) {
		this.kodeItemBaru = kodeItemBaru;
	}
	public String getNoPart() {
		return noPart;
	}
	public void setNoPart(String noPart) {
		this.noPart = noPart;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public BigDecimal getHargaNett() {
		return hargaNett;
	}
	public void setHargaNett(BigDecimal hargaNett) {
		this.hargaNett = hargaNett;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioBeanUbahTipeHelper other = (TrioBeanUbahTipeHelper) obj;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioBeanUbahTipeHelper [noMesin=" + noMesin + "]";
	}
	

}
