package com.trio.bean.helper;

/**
 * @author Gusti Arya 7:39:40 PM Dec 25, 2013
 */

public class TrioBeanPartHelper {

	private String partNo;
	private String partName;
	private Integer qty;

	public TrioBeanPartHelper() {
		// TODO Auto-generated constructor stub
	}

	public String getPartNo() {
		return partNo;
	}
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioBeanPartHelper other = (TrioBeanPartHelper) obj;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioBeanPartHelper [partNo=" + partNo + "]";
	}

}
