package com.trio.bean.bsc;
/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 19, 2012
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="AHMDSBSC_DTLSETTING")
public class AhmdsbscDtlsetting extends TrioEntityUserTrail implements Serializable {
  
	private static final long serialVersionUID = 1L;

	  @Id
	  @Column(name="VID", nullable=false, length=20)
	  private String vid;
	
	  @Id
	  @Column(name="VITEMCODE", nullable=false, length=20)
	  private String vitemcode;
	
	  @Column(name="VITEMNAME", length=50)
	  private String vitemname;
	
	  @Column(name="VITEMDESC", length=30)
	  private String vitemdesc;
	
	  @Column(name="VSTATUS", length=1)
	  private String vstatus;
	
	  @JoinColumn(name="VID", referencedColumnName="VID", insertable=false, updatable=false)
	  @ManyToOne
	  private AhmdsbscHdrsetting ahmdsbscHdrsetting;
	
	  public AhmdsbscDtlsetting()  {
	  }
	
	  public AhmdsbscDtlsetting(String vid, String vitemcode) {
	    this.vid = vid;
	    this.vitemcode = vitemcode;
	  }

	public String getVid() {
		return vid;
	}
	
	public void setVid(String vid) {
		this.vid = vid;
	}
	
	public String getVitemcode() {
		return vitemcode;
	}
	
	public void setVitemcode(String vitemcode) {
		this.vitemcode = vitemcode;
	}
	
	public String getVitemname() {
		return vitemname;
	}
	
	public void setVitemname(String vitemname) {
		this.vitemname = vitemname;
	}
	
	public String getVitemdesc() {
		return vitemdesc;
	}
	
	public void setVitemdesc(String vitemdesc) {
		this.vitemdesc = vitemdesc;
	}
	
	public String getVstatus() {
		return vstatus;
	}

	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}

	public AhmdsbscHdrsetting getAhmdsbscHdrsetting() {
		return ahmdsbscHdrsetting;
	}
	
	public void setAhmdsbscHdrsetting(AhmdsbscHdrsetting ahmdsbscHdrsetting) {
		this.ahmdsbscHdrsetting = ahmdsbscHdrsetting;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((vid == null) ? 0 : vid.hashCode());
		result = prime * result
				+ ((vitemcode == null) ? 0 : vitemcode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AhmdsbscDtlsetting other = (AhmdsbscDtlsetting) obj;
		if (vid == null) {
			if (other.vid != null)
				return false;
		} else if (!vid.equals(other.vid))
			return false;
		if (vitemcode == null) {
			if (other.vitemcode != null)
				return false;
		} else if (!vitemcode.equals(other.vitemcode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AhmdsbscDtlsetting [vid=" + vid + ", vitemcode=" + vitemcode
				+ "]";
	}
	
}
