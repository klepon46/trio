package com.trio.bean.bsc;
/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 19, 2012
 */

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="AHMDSBSC_HDRSETTING")
public class AhmdsbscHdrsetting extends TrioEntityUserTrail  implements Serializable {
	  
	  private static final long serialVersionUID = 1L;
	
	  @Id
	  @Column(name="VID", nullable=false, length=20)
	  private String vid;
	
	  @Column(name="VDESCID", length=100)
	  private String vdescid;
	
	  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="ahmdsbscHdrsetting")
	  private Set<AhmdsbscDtlsetting> ahmdsbscDtlsettingSet;
	
	  public AhmdsbscHdrsetting()  {
		  this.ahmdsbscDtlsettingSet = new HashSet<AhmdsbscDtlsetting>();
	  }
	
	  public AhmdsbscHdrsetting(String vid)  {
	    this.vid = vid;
	  }

	public String getVid() {
		return vid;
	}

	public void setVid(String vid) {
		this.vid = vid;
	}

	public String getVdescid() {
		return vdescid;
	}

	public void setVdescid(String vdescid) {
		this.vdescid = vdescid;
	}

	public Set<AhmdsbscDtlsetting> getAhmdsbscDtlsettingSet() {
		return ahmdsbscDtlsettingSet;
	}

	public void setAhmdsbscDtlsettingSet(
			Set<AhmdsbscDtlsetting> ahmdsbscDtlsettingSet) {
		this.ahmdsbscDtlsettingSet = ahmdsbscDtlsettingSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((vid == null) ? 0 : vid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AhmdsbscHdrsetting other = (AhmdsbscHdrsetting) obj;
		if (vid == null) {
			if (other.vid != null)
				return false;
		} else if (!vid.equals(other.vid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AhmdsbscHdrsetting [vid=" + vid + "]";
	}
	  
}
