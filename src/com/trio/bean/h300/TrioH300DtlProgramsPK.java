package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrioH300DtlProgramsPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	
	@Column(name="ID_PROGRAM", length=30, nullable=false)
	private String idProgram;
	
	@Column(name="PART_NO", length = 25, nullable=false)
	private String partNo;
	
	public TrioH300DtlProgramsPK() {
		// TODO Auto-generated constructor stub
	}

	public String getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(String idProgram) {
		this.idProgram = idProgram;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idProgram == null) ? 0 : idProgram.hashCode());
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH300DtlProgramsPK other = (TrioH300DtlProgramsPK) obj;
		if (idProgram == null) {
			if (other.idProgram != null)
				return false;
		} else if (!idProgram.equals(other.idProgram))
			return false;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH300DtlProgramsPK [idProgram=" + idProgram + ", partNo="
				+ partNo + "]";
	}
}
