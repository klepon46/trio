package com.trio.bean.h300;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H300_MASTER_PARTS")
public class HondaH300MasterParts extends TrioEntityUserTrail implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PART_NO", length=25, nullable=false)
	private String partNo;
	
	@Column(name="MIN_STOCK")
	private Integer minStock;
	
	@Column(name="PART_RANGE", length=1)
	private String partRange;
	
	@Column(name="FRG", length=1)
	private String frg;
	
	@Column(name="SAFETY_STOCK")
	private Integer safetyStock;
	
	@Column(name="HARGA_JUAL", length=15, precision=13, scale=2)
	private BigDecimal  hargaJual;

	@Column(name="HARGA_TERENDAH", length=15, precision=13, scale=2)
	private BigDecimal hargaTerendah;
	
	@Column(name="MIN_ORDER")
	private Integer minOrder;
	
	@Column(name="SATUAN", length=10)
	private String satuan;
	
	@Column(name="PART_NAME", length=30, nullable=false)
	private String partName;
	
	@Column(name="MOTOR_TYPE", length=40)
	private String motorType;
	
	@Column(name="MAX_STOCK")
	private Integer maxStock;
	
	@Column(name="STATUS", length=1)
	private String status;
	
	@Column(name="HARGA_POKOK", length=15, precision=13, scale=2)
	private BigDecimal hargaPokok;
	
	@Column(name="HET", length=15, precision=13, scale=2)
	private BigDecimal het;
	
	@Column(name="PART_GROUP", length=10, nullable=false)
	private String partGroup;
	
	@Column(name="LEAD_TIME")
	private Integer leadTime;
	
	@Column(name="KELBRG_ID_KELBRG", length=6, nullable=false)
	private String kelbrgIdKelbrg;
	
	@Column(name="LOKASI", length=10)
	private String lokasi;
	
	@Column(name="RAK", length=20)
	private String rak;
	
	@Column(name="VPARTNUMB", length=1)
	private String vpartnumb;
	
	@Column(name="VFLORDHO", length=1)
	private String vflordho;
	
	@Column(name="NORDHTLMAX", length=1)
	private String nordhtlmax;
	
	@Column(name="VPARTSTAT", length=1)
	private String vpartstat;
	
	@Column(name="VPARTFUNC", length=1)
	private String vpartfunc;
	
	@Column(name="VPARTLTIME", length=1)
	private String vpartltime;
	
	@Column(name="VPARTCATEGORY", length=1)
	private String vpartcategory;
	
	@Column(name="VREFFPART", length=30)
	private String vreffpart;
	
	@Column(name="NMINORD_BIGDLR", length=10)
	private Integer nminordBigdlr;
	
	@Column(name="NMINORD_MEDDLR", length=10)
	private Integer nminordMeddlr;
	
	@Column(name="NMINORD_SMLDLR", length=10)
	private Integer nminordSmldlr;
	
	@Column(name="VPARTSOURCE", length=1)
	private String vpartsource;
	
	@Column(name="VPART_PNT", length=1)
	private String vpartPnt;
	
	@Column(name="T3_KELBRG_AR", length=30)
	private String t3KelbrgAr;

	public HondaH300MasterParts() {
		// TODO Auto-generated constructor stub
	}
	
	public HondaH300MasterParts(BigDecimal het) {
		this.het = het;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	public Integer getMinStock() {
		return minStock;
	}

	public void setMinStock(Integer minStock) {
		this.minStock = minStock;
	}

	public String getPartRange() {
		return partRange;
	}

	public void setPartRange(String partRange) {
		this.partRange = partRange;
	}

	public String getFrg() {
		return frg;
	}

	public void setFrg(String frg) {
		this.frg = frg;
	}

	public Integer getSafetyStock() {
		return safetyStock;
	}

	public void setSafetyStock(Integer safetyStock) {
		this.safetyStock = safetyStock;
	}

	public BigDecimal getHargaJual() {
		return hargaJual;
	}

	public void setHargaJual(BigDecimal hargaJual) {
		this.hargaJual = hargaJual;
	}

	public BigDecimal getHargaTerendah() {
		return hargaTerendah;
	}

	public void setHargaTerendah(BigDecimal hargaTerendah) {
		this.hargaTerendah = hargaTerendah;
	}

	public Integer getMinOrder() {
		return minOrder;
	}

	public void setMinOrder(Integer minOrder) {
		this.minOrder = minOrder;
	}

	public String getSatuan() {
		return satuan;
	}

	public void setSatuan(String satuan) {
		this.satuan = satuan;
	}

	public String getPartName() {
		return partName;
	}

	public void setPartName(String partName) {
		this.partName = partName;
	}

	public String getMotorType() {
		return motorType;
	}

	public void setMotorType(String motorType) {
		this.motorType = motorType;
	}

	public Integer getMaxStock() {
		return maxStock;
	}

	public void setMaxStock(Integer maxStock) {
		this.maxStock = maxStock;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public BigDecimal getHargaPokok() {
		return hargaPokok;
	}

	public void setHargaPokok(BigDecimal hargaPokok) {
		this.hargaPokok = hargaPokok;
	}

	public BigDecimal getHet() {
		return het;
	}

	public void setHet(BigDecimal het) {
		this.het = het;
	}

	public String getPartGroup() {
		return partGroup;
	}

	public void setPartGroup(String partGroup) {
		this.partGroup = partGroup;
	}

	public Integer getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(Integer leadTime) {
		this.leadTime = leadTime;
	}

	public String getKelbrgIdKelbrg() {
		return kelbrgIdKelbrg;
	}

	public void setKelbrgIdKelbrg(String kelbrgIdKelbrg) {
		this.kelbrgIdKelbrg = kelbrgIdKelbrg;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getRak() {
		return rak;
	}

	public void setRak(String rak) {
		this.rak = rak;
	}

	public String getVpartnumb() {
		return vpartnumb;
	}

	public void setVpartnumb(String vpartnumb) {
		this.vpartnumb = vpartnumb;
	}

	public String getVflordho() {
		return vflordho;
	}

	public void setVflordho(String vflordho) {
		this.vflordho = vflordho;
	}

	public String getNordhtlmax() {
		return nordhtlmax;
	}

	public void setNordhtlmax(String nordhtlmax) {
		this.nordhtlmax = nordhtlmax;
	}

	public String getVpartstat() {
		return vpartstat;
	}

	public void setVpartstat(String vpartstat) {
		this.vpartstat = vpartstat;
	}

	public String getVpartfunc() {
		return vpartfunc;
	}

	public void setVpartfunc(String vpartfunc) {
		this.vpartfunc = vpartfunc;
	}

	public String getVpartltime() {
		return vpartltime;
	}

	public void setVpartltime(String vpartltime) {
		this.vpartltime = vpartltime;
	}

	public String getVpartcategory() {
		return vpartcategory;
	}

	public void setVpartcategory(String vpartcategory) {
		this.vpartcategory = vpartcategory;
	}

	public String getVreffpart() {
		return vreffpart;
	}

	public void setVreffpart(String vreffpart) {
		this.vreffpart = vreffpart;
	}

	public Integer getNminordBigdlr() {
		return nminordBigdlr;
	}

	public void setNminordBigdlr(Integer nminordBigdlr) {
		this.nminordBigdlr = nminordBigdlr;
	}

	public Integer getNminordMeddlr() {
		return nminordMeddlr;
	}

	public void setNminordMeddlr(Integer nminordMeddlr) {
		this.nminordMeddlr = nminordMeddlr;
	}

	public Integer getNminordSmldlr() {
		return nminordSmldlr;
	}

	public void setNminordSmldlr(Integer nminordSmldlr) {
		this.nminordSmldlr = nminordSmldlr;
	}

	public String getVpartsource() {
		return vpartsource;
	}

	public void setVpartsource(String vpartsource) {
		this.vpartsource = vpartsource;
	}

	public String getVpartPnt() {
		return vpartPnt;
	}

	public void setVpartPnt(String vpartPnt) {
		this.vpartPnt = vpartPnt;
	}

	public String getT3KelbrgAr() {
		return t3KelbrgAr;
	}

	public void setT3KelbrgAr(String t3KelbrgAr) {
		this.t3KelbrgAr = t3KelbrgAr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300MasterParts other = (HondaH300MasterParts) obj;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300MasterParts [partNo=" + partNo + "]";
	}
	
}
