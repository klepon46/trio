package com.trio.bean.h300;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 11:22:08 AM Dec 30, 2013
 */

@Entity
@Table(name="HONDA_H300_DET_INOUT_MANUALS")
public class HondaH300DetInoutManuals extends TrioEntityUserTrail{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private HondaH300DetInoutManualsPK hondaH300DetInoutManualsPK;
	
	@Column(name="FLAG", length=1)
	private String flag;
	
	@Column(name="PART_NO", length=25, nullable=false)
	private String partNo;
	
	@Column(name="REMARK", length=50)
	private String remark;
	
	@Column(name="QTY", nullable=false)
	private Integer qty;
	
	@Column(name="RAK_LOK_LOKASI_KODE_LOKASI", length=10)
	private String rakLokLokasiKodeLokasi;
	
	@Column(name="RAK_LOK_RAK_NO_RAK", length=20)
	private String rakLokRakNoRak;
	
	@Column(name="HP", precision=20, scale=4)
	private BigDecimal hp;
	
	@ManyToOne
	@JoinColumn(name="INOUT_TRANS_NO", referencedColumnName="TRANS_NO", updatable=false, insertable=false)
	private HondaH300InoutManuals hondaH300InoutManuals;

	public HondaH300DetInoutManuals() {
		this.hondaH300DetInoutManualsPK = new HondaH300DetInoutManualsPK();
	}

	public HondaH300DetInoutManualsPK getHondaH300DetInoutManualsPK() {
		return hondaH300DetInoutManualsPK;
	}

	public void setHondaH300DetInoutManualsPK(
			HondaH300DetInoutManualsPK hondaH300DetInoutManualsPK) {
		this.hondaH300DetInoutManualsPK = hondaH300DetInoutManualsPK;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public String getRakLokLokasiKodeLokasi() {
		return rakLokLokasiKodeLokasi;
	}

	public void setRakLokLokasiKodeLokasi(String rakLokLokasiKodeLokasi) {
		this.rakLokLokasiKodeLokasi = rakLokLokasiKodeLokasi;
	}

	public String getRakLokRakNoRak() {
		return rakLokRakNoRak;
	}

	public void setRakLokRakNoRak(String rakLokRakNoRak) {
		this.rakLokRakNoRak = rakLokRakNoRak;
	}

	public BigDecimal getHp() {
		return hp;
	}

	public void setHp(BigDecimal hp) {
		this.hp = hp;
	}

	public HondaH300InoutManuals getHondaH300InoutManuals() {
		return hondaH300InoutManuals;
	}

	public void setHondaH300InoutManuals(HondaH300InoutManuals hondaH300InoutManuals) {
		this.hondaH300InoutManuals = hondaH300InoutManuals;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((hondaH300DetInoutManualsPK == null) ? 0
						: hondaH300DetInoutManualsPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300DetInoutManuals other = (HondaH300DetInoutManuals) obj;
		if (hondaH300DetInoutManualsPK == null) {
			if (other.hondaH300DetInoutManualsPK != null)
				return false;
		} else if (!hondaH300DetInoutManualsPK
				.equals(other.hondaH300DetInoutManualsPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300DetInoutManuals [hondaH300DetInoutManualsPK="
				+ hondaH300DetInoutManualsPK + "]";
	}
	
}
