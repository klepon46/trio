package com.trio.bean.h300;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 11:16:02 AM Dec 30, 2013
 */

@Entity
@Table(name="HONDA_H300_INOUT_MANUALS")
public class HondaH300InoutManuals extends TrioEntityUserTrail{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="TRANS_NO", length=30)
	private String transNo;
	
	@Column(name="TRANS_DATE", nullable=false)
	@Temporal(TemporalType.DATE)
	private Date transDate;
	
	@Column(name="REMARK", length=100)
	private String remark;
	
	@Column(name="TRANS_CODE", length=10, nullable=false)
	private String transCode;
	
	@Column(name="CUST_CODE", length=10)
	private String custCode;
	
	@Column(name="TRANS_TYPE", length=1)
	private String transType;
	
	@Column(name="FLAG", length=1)
	private String flag;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="hondaH300InoutManuals")
	private Set<HondaH300DetInoutManuals> hondaH300DetInoutManualsSet;
	
	
	public HondaH300InoutManuals() {
		
	}
	
	public String getTransNo() {
		return transNo;
	}

	public void setTransNo(String transNo) {
		this.transNo = transNo;
	}

	public Date getTransDate() {
		return transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getTransCode() {
		return transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public Set<HondaH300DetInoutManuals> getHondaH300DetInoutManualsSet() {
		return hondaH300DetInoutManualsSet;
	}

	public void setHondaH300DetInoutManualsSet(
			Set<HondaH300DetInoutManuals> hondaH300DetInoutManualsSet) {
		this.hondaH300DetInoutManualsSet = hondaH300DetInoutManualsSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((transNo == null) ? 0 : transNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300InoutManuals other = (HondaH300InoutManuals) obj;
		if (transNo == null) {
			if (other.transNo != null)
				return false;
		} else if (!transNo.equals(other.transNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300InoutManuals [transNo=" + transNo + "]";
	}
	
}
