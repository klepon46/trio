package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.usertrail.TrioPartEntityUserTrail;

@Entity
@Table(name="TRIO_H300_HDR_HADIAHS")
public class TrioH300HdrHadiahs extends TrioPartEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TrioH300HdrHadiahsPK trioH300HdrHadiahsPK;
	
	@Column(name="HADIAH", length=250)
	private String hadiah;
	
	public TrioH300HdrHadiahs() {
		// TODO Auto-generated constructor stub
	}

	public TrioH300HdrHadiahsPK getTrioH300HdrHadiahsPK() {
		return trioH300HdrHadiahsPK;
	}

	public void setTrioH300HdrHadiahsPK(TrioH300HdrHadiahsPK trioH300HdrHadiahsPK) {
		this.trioH300HdrHadiahsPK = trioH300HdrHadiahsPK;
	}

	public String getHadiah() {
		return hadiah;
	}

	public void setHadiah(String hadiah) {
		this.hadiah = hadiah;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioH300HdrHadiahsPK == null) ? 0 : trioH300HdrHadiahsPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH300HdrHadiahs other = (TrioH300HdrHadiahs) obj;
		if (trioH300HdrHadiahsPK == null) {
			if (other.trioH300HdrHadiahsPK != null)
				return false;
		} else if (!trioH300HdrHadiahsPK.equals(other.trioH300HdrHadiahsPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH300HdrHadiahs [trioH300HdrHadiahsPK="
				+ trioH300HdrHadiahsPK + "]";
	}
	
}
