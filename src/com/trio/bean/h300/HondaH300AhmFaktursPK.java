package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 2:31:07 PM Jan 16, 2014
 */

@Embeddable
public class HondaH300AhmFaktursPK implements Serializable {

	
private static final long serialVersionUID = 1L;
	
	@Column(name="INV_NO", length=25)
	private String invNo;
	
	@Column(name="PS_NO", length=30)
	private String psNo;
	
	@Column(name="PART_NO", length=25)
	private String partNo;
	
	@Column(name="INV_SEQ")
	private Integer invSeq;

	public HondaH300AhmFaktursPK() {
		// TODO Auto-generated constructor stub
	}

	public String getInvNo() {
		return invNo;
	}

	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}

	public String getPsNo() {
		return psNo;
	}

	public void setPsNo(String psNo) {
		this.psNo = psNo;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	public Integer getInvSeq() {
		return invSeq;
	}

	public void setInvSeq(Integer invSeq) {
		this.invSeq = invSeq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((invNo == null) ? 0 : invNo.hashCode());
		result = prime * result + ((invSeq == null) ? 0 : invSeq.hashCode());
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		result = prime * result + ((psNo == null) ? 0 : psNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300AhmFaktursPK other = (HondaH300AhmFaktursPK) obj;
		if (invNo == null) {
			if (other.invNo != null)
				return false;
		} else if (!invNo.equals(other.invNo))
			return false;
		if (invSeq == null) {
			if (other.invSeq != null)
				return false;
		} else if (!invSeq.equals(other.invSeq))
			return false;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		if (psNo == null) {
			if (other.psNo != null)
				return false;
		} else if (!psNo.equals(other.psNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300AhmFaktursPK [invNo=" + invNo + ", psNo=" + psNo
				+ ", partNo=" + partNo + ", invSeq=" + invSeq + "]";
	}
	
}
