package com.trio.bean.h300;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 3:59:51 PM Dec 26, 2013
 */

@Entity
@Table(name="HONDA_H300_PART_LOKASES")
public class HondaH300PartLokases extends TrioEntityUserTrail{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private HondaH300PartLokasesPK hondaH300PartLokasesPK;
	
	@Column(name="QTY_ONH")
	private Integer qtyOnh;
	
	@Column(name="QTY_BOOK")
	private Integer qtyBook;
	
	public HondaH300PartLokases() {
		// TODO Auto-generated constructor stub
	}

	public HondaH300PartLokasesPK getHondaH300PartLokasesPK() {
		return hondaH300PartLokasesPK;
	}
	
	public void setHondaH300PartLokasesPK(
			HondaH300PartLokasesPK hondaH300PartLokasesPK) {
		this.hondaH300PartLokasesPK = hondaH300PartLokasesPK;
	}

	public Integer getQtyOnh() {
		return qtyOnh;
	}

	public void setQtyOnh(Integer qtyOnh) {
		this.qtyOnh = qtyOnh;
	}

	public Integer getQtyBook() {
		return qtyBook;
	}

	public void setQtyBook(Integer qtyBook) {
		this.qtyBook = qtyBook;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((hondaH300PartLokasesPK == null) ? 0
						: hondaH300PartLokasesPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300PartLokases other = (HondaH300PartLokases) obj;
		if (hondaH300PartLokasesPK == null) {
			if (other.hondaH300PartLokasesPK != null)
				return false;
		} else if (!hondaH300PartLokasesPK.equals(other.hondaH300PartLokasesPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300PartLokases [hondaH300PartLokasesPK="
				+ hondaH300PartLokasesPK + "]";
	}
	
}
