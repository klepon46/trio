package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 4:00:37 PM Dec 26, 2013
 */

@Embeddable
public class HondaH300PartLokasesPK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="PART_PART_NO", length=25, nullable=false)
	private String partpartNo;
	
	@Column(name="RAK_LOK_LOKASI_KODE_LOKASI", length=10, nullable=false)
	private String rakLokLokasiKodeLokasi;
	
	@Column(name="RAK_LOK_RAK_NO_RAK", length=20)
	private String rakLokRakNoRak;
	
	public HondaH300PartLokasesPK() {
		// TODO Auto-generated constructor stub
	}

	public String getPartpartNo() {
		return partpartNo;
	}

	public void setPartpartNo(String partpartNo) {
		this.partpartNo = partpartNo;
	}

	public String getRakLokLokasiKodeLokasi() {
		return rakLokLokasiKodeLokasi;
	}

	public void setRakLokLokasiKodeLokasi(String rakLokLokasiKodeLokasi) {
		this.rakLokLokasiKodeLokasi = rakLokLokasiKodeLokasi;
	}

	public String getRakLokRakNoRak() {
		return rakLokRakNoRak;
	}

	public void setRakLokRakNoRak(String rakLokRakNoRak) {
		this.rakLokRakNoRak = rakLokRakNoRak;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((partpartNo == null) ? 0 : partpartNo.hashCode());
		result = prime
				* result
				+ ((rakLokLokasiKodeLokasi == null) ? 0
						: rakLokLokasiKodeLokasi.hashCode());
		result = prime * result
				+ ((rakLokRakNoRak == null) ? 0 : rakLokRakNoRak.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300PartLokasesPK other = (HondaH300PartLokasesPK) obj;
		if (partpartNo == null) {
			if (other.partpartNo != null)
				return false;
		} else if (!partpartNo.equals(other.partpartNo))
			return false;
		if (rakLokLokasiKodeLokasi == null) {
			if (other.rakLokLokasiKodeLokasi != null)
				return false;
		} else if (!rakLokLokasiKodeLokasi.equals(other.rakLokLokasiKodeLokasi))
			return false;
		if (rakLokRakNoRak == null) {
			if (other.rakLokRakNoRak != null)
				return false;
		} else if (!rakLokRakNoRak.equals(other.rakLokRakNoRak))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300PartLokasesPK [partpartNo=" + partpartNo
				+ ", rakLokLokasiKodeLokasi=" + rakLokLokasiKodeLokasi
				+ ", rakLokRakNoRak=" + rakLokRakNoRak + "]";
	}
	
}
