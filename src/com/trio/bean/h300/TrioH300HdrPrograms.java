package com.trio.bean.h300;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioPartEntityUserTrail;

@Entity
@Table(name="TRIO_H300_HDR_PROGRAMS")
public class TrioH300HdrPrograms extends TrioPartEntityUserTrail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PROGRAM", length=30, nullable=false)
	private String idProgram;
	
	@Column(name="NAMA_PROGRAM", length=50)
	private String namaProgram;
	
	@Column(name="START_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;
	
	@Column(name="END_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDate;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="trioH300HdrPrograms")
	private Set<TrioH300DtlPrograms> trioH300DtlProgramsSet;
	
	public TrioH300HdrPrograms(){
		
	}
	
	public String getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(String idProgram) {
		this.idProgram = idProgram;
	}

	public String getNamaProgram() {
		return namaProgram;
	}

	public void setNamaProgram(String namaProgram) {
		this.namaProgram = namaProgram;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public Set<TrioH300DtlPrograms> getTrioH300DtlProgramsSet() {
		return trioH300DtlProgramsSet;
	}

	public void setTrioH300DtlProgramsSet(
			Set<TrioH300DtlPrograms> trioH300DtlProgramsSet) {
		this.trioH300DtlProgramsSet = trioH300DtlProgramsSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((idProgram == null) ? 0 : idProgram.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH300HdrPrograms other = (TrioH300HdrPrograms) obj;
		if (idProgram == null) {
			if (other.idProgram != null)
				return false;
		} else if (!idProgram.equals(other.idProgram))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH300HdrPrograms [idProgram=" + idProgram + "]";
	}
}
