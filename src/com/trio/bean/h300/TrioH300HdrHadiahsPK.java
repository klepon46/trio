package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TrioH300HdrHadiahsPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="ID_PROGRAM", length=30, nullable=false)
	private String idProgram;
	
	@Column(name="POINT", nullable=false)
	private Integer point;

	public TrioH300HdrHadiahsPK() {
		// TODO Auto-generated constructor stub
	}
	
	public String getIdProgram() {
		return idProgram;
	}

	public void setIdProgram(String idProgram) {
		this.idProgram = idProgram;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idProgram == null) ? 0 : idProgram.hashCode());
		result = prime * result + ((point == null) ? 0 : point.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH300HdrHadiahsPK other = (TrioH300HdrHadiahsPK) obj;
		if (idProgram == null) {
			if (other.idProgram != null)
				return false;
		} else if (!idProgram.equals(other.idProgram))
			return false;
		if (point == null) {
			if (other.point != null)
				return false;
		} else if (!point.equals(other.point))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH300HdrHadiahsPK [idProgram=" + idProgram + ", point="
				+ point + "]";
	}
}
