package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioPartUserTrailing;

@Entity
@Table(name="TRIO_H300_DTL_PROGRAMS")
public class TrioH300DtlPrograms extends TrioPartUserTrailing implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TrioH300DtlProgramsPK trioH300DtlProgramsPK;
	
	@Column(name="POINT")
	private Integer point;

	@JoinColumn(name="ID_PROGRAM", referencedColumnName = "ID_PROGRAM", updatable=false, insertable=false)
	@ManyToOne(cascade=CascadeType.ALL)
	private TrioH300HdrPrograms trioH300HdrPrograms;
	
	public TrioH300DtlPrograms() {
		// TODO Auto-generated constructor stub
	}
	
	//Getter And Setter
	public TrioH300DtlProgramsPK getTrioH300DtlProgramsPK() {
		return trioH300DtlProgramsPK;
	}

	public void setTrioH300DtlProgramsPK(TrioH300DtlProgramsPK trioH300DtlProgramsPK) {
		this.trioH300DtlProgramsPK = trioH300DtlProgramsPK;
	}

	public Integer getPoint() {
		return point;
	}

	public void setPoint(Integer point) {
		this.point = point;
	}
	
	public TrioH300HdrPrograms getTrioH300HdrPrograms() {
		return trioH300HdrPrograms;
	}

	public void setTrioH300HdrPrograms(TrioH300HdrPrograms trioH300HdrPrograms) {
		this.trioH300HdrPrograms = trioH300HdrPrograms;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioH300DtlProgramsPK == null) ? 0 : trioH300DtlProgramsPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH300DtlPrograms other = (TrioH300DtlPrograms) obj;
		if (trioH300DtlProgramsPK == null) {
			if (other.trioH300DtlProgramsPK != null)
				return false;
		} else if (!trioH300DtlProgramsPK.equals(other.trioH300DtlProgramsPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH300DtlPrograms [trioH300DtlProgramsPK="
				+ trioH300DtlProgramsPK + "]";
	}
}
