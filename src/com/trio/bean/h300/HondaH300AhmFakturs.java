package com.trio.bean.h300;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Gusti Arya 2:21:10 PM Jan 16, 2014
 */

@Entity
@Table(name="HONDA_H300_AHM_FAKTURS")
public class HondaH300AhmFakturs implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private HondaH300AhmFaktursPK hondaH300AhmFaktursPK;

	@Column(name="DISC_CASH", precision=20, scale=4)
	private BigDecimal discCash;
	
	@Column(name="QTY")
	private Integer qty;
	
	@Column(name="INV_DATE")
	@Temporal(TemporalType.DATE)
	private Date invDate;
	
	@Column(name="PPN", precision=20, scale=4)
	private BigDecimal ppn;
	
	
	@Column(name="DPP", precision=20, scale=4)
	private BigDecimal dpp;
	
	@Column(name="QQ_CODE", length=5)
	private String qqCode;
	
	@Column(name="PPN_DUE_DATE")
	@Temporal(TemporalType.DATE)
	private Date ppnDueDate;
	
	@Column(name="DISC_CMP", precision=20, scale=4)
	private BigDecimal discCmp;
	
	@Column(name="DISC_INC", precision=20, scale=4)
	private BigDecimal discInc;
	
	@Column(name="CUST_CODE", length=5)
	private String custCode;
	
	@Column(name="PRICE", precision=20, scale=4)
	private BigDecimal price;
	
	@Column(name="OTHER_DISC1", precision=20, scale=4)
	private BigDecimal otherDisc1;
	
	@Column(name="OTHER_DISC2", precision=20, scale=4)
	private BigDecimal otherDisc2;
	
	@Column(name="DPP_DUE_DATE")
	@Temporal(TemporalType.DATE)
	private Date dppDueDate;
	
	@Column(name="PART_DESC", length=30)
	private String partDesc;
	
	public HondaH300AhmFakturs() {
		this.hondaH300AhmFaktursPK = new HondaH300AhmFaktursPK();
		
	}

	public BigDecimal getDiscCash() {
		return discCash;
	}

	public void setDiscCash(BigDecimal discCash) {
		this.discCash = discCash;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}


	public HondaH300AhmFaktursPK getHondaH300AhmFaktursPK() {
		return hondaH300AhmFaktursPK;
	}

	public void setHondaH300AhmFaktursPK(HondaH300AhmFaktursPK hondaH300AhmFaktursPK) {
		this.hondaH300AhmFaktursPK = hondaH300AhmFaktursPK;
	}

	public Date getInvDate() {
		return invDate;
	}

	public void setInvDate(Date invDate) {
		this.invDate = invDate;
	}

	public BigDecimal getPpn() {
		return ppn;
	}

	public void setPpn(BigDecimal ppn) {
		this.ppn = ppn;
	}

	public BigDecimal getDpp() {
		return dpp;
	}

	public void setDpp(BigDecimal dpp) {
		this.dpp = dpp;
	}

	public String getQqCode() {
		return qqCode;
	}

	public void setQqCode(String qqCode) {
		this.qqCode = qqCode;
	}

	public Date getPpnDueDate() {
		return ppnDueDate;
	}

	public void setPpnDueDate(Date ppnDueDate) {
		this.ppnDueDate = ppnDueDate;
	}

	public BigDecimal getDiscCmp() {
		return discCmp;
	}

	public void setDiscCmp(BigDecimal discCmp) {
		this.discCmp = discCmp;
	}

	public BigDecimal getDiscInc() {
		return discInc;
	}

	public void setDiscInc(BigDecimal discInc) {
		this.discInc = discInc;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getOtherDisc1() {
		return otherDisc1;
	}

	public void setOtherDisc1(BigDecimal otherDisc1) {
		this.otherDisc1 = otherDisc1;
	}

	public BigDecimal getOtherDisc2() {
		return otherDisc2;
	}

	public void setOtherDisc2(BigDecimal otherDisc2) {
		this.otherDisc2 = otherDisc2;
	}

	public Date getDppDueDate() {
		return dppDueDate;
	}

	public void setDppDueDate(Date dppDueDate) {
		this.dppDueDate = dppDueDate;
	}

	public String getPartDesc() {
		return partDesc;
	}

	public void setPartDesc(String partDesc) {
		this.partDesc = partDesc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((hondaH300AhmFaktursPK == null) ? 0 : hondaH300AhmFaktursPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300AhmFakturs other = (HondaH300AhmFakturs) obj;
		if (hondaH300AhmFaktursPK == null) {
			if (other.hondaH300AhmFaktursPK != null)
				return false;
		} else if (!hondaH300AhmFaktursPK.equals(other.hondaH300AhmFaktursPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300AhmFakturs [hondaH300AhmFaktursPK="
				+ hondaH300AhmFaktursPK + "]";
	}
	
	
	
	
	
}
