package com.trio.bean.h300;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:17:27 AM Feb 14, 2014
 */

public class HondaH300Ahmpses extends TrioEntityUserTrail{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="PS_NO", length=30)
	private String psNo;
	
	@Column(name="FAKTUR_NO", length=30)
	private String fakturNo;
	
	@Column(name="STATUS",length=2)
	private String status;
	
	@Column(name="PS_DATE")
	@Temporal(TemporalType.DATE)
	private Date psDate;
	
	public HondaH300Ahmpses() {
		// TODO Auto-generated constructor stub
	}

	public String getPsNo() {
		return psNo;
	}

	public void setPsNo(String psNo) {
		this.psNo = psNo;
	}

	public String getFakturNo() {
		return fakturNo;
	}

	public void setFakturNo(String fakturNo) {
		this.fakturNo = fakturNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getPsDate() {
		return psDate;
	}

	public void setPsDate(Date psDate) {
		this.psDate = psDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((psNo == null) ? 0 : psNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300Ahmpses other = (HondaH300Ahmpses) obj;
		if (psNo == null) {
			if (other.psNo != null)
				return false;
		} else if (!psNo.equals(other.psNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300Ahmpses [psNo=" + psNo + "]";
	}

}
