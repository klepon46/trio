package com.trio.bean.h300;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 11:22:22 AM Dec 30, 2013
 */

@Embeddable
public class HondaH300DetInoutManualsPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="INOUT_TRANS_NO", length=30)
	private String inoutTransNo;
	
	@Column(name="SEQ")
	private Integer seq;
	
	public HondaH300DetInoutManualsPK() {
		
	}

	public String getInoutTransNo() {
		return inoutTransNo;
	}

	public void setInoutTransNo(String inoutTransNo) {
		this.inoutTransNo = inoutTransNo;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((inoutTransNo == null) ? 0 : inoutTransNo.hashCode());
		result = prime * result + ((seq == null) ? 0 : seq.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH300DetInoutManualsPK other = (HondaH300DetInoutManualsPK) obj;
		if (inoutTransNo == null) {
			if (other.inoutTransNo != null)
				return false;
		} else if (!inoutTransNo.equals(other.inoutTransNo))
			return false;
		if (seq == null) {
			if (other.seq != null)
				return false;
		} else if (!seq.equals(other.seq))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH300DetInoutManualsPK [inoutTransNo=" + inoutTransNo
				+ ", seq=" + seq + "]";
	}

}
