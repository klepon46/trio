package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.bean.h000.HondaH000Dealers;
import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_FAKDOS")
public class HondaH100Fakdos extends TrioEntityUserTrail implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name="NO_DO", nullable=false, length=30)
  private String noDo;
  
  @Column(name="PODLR_NO_POINT", length=30)
  private String poDlrNoPoint;

  @Column(name="KD_DLR", length=10)
  private String kdDlr;

  @Column(name="JENIS_BAYAR", length=15)
  private String jenisBayar;

  @Column(name="KODE_SALESMAN", length=30)
  private String kodeSalesman;

  @Column(name="PERS_BIAYA", length=15)
  private String persBiaya;

  @Column(name="LOKASI_ASAL", length=10)
  private String lokasiAsal;

  @Column(name="KONSUMEN", length=40)
  private String konsumen;

  @Column(name="OTR", length=1)
  private String otr;

  @Column(name="NO_FAKTUR", length=30)
  private String noFaktur;

  @Column(name="PLAFON", length=1)
  private String plafon;

  @Column(name="TOP")
  private Integer top;

  @Column(name="NAMA_TAGIHAN", length=30)
  private String namaTagihan;

  @Column(name="FAK_RUNNO", length=10)
  private String fakRunno;

  @Column(name="ALAMAT_TAGIHAN", length=50)
  private String alamatTagihan;

  @Column(name="DO_RUNNO", length=10)
  private String doRunno;

  @Column(name="STATUS", length=1)
  private String status;

  @Column(name="TGL_FAK")
  @Temporal(TemporalType.DATE)
  private Date tglFak;

  @Column(name="NPWP", length=30)
  private String npwp;

  @Column(name="FAK_BULAN", length=2)
  private String fakBulan;

  @Column(name="CREA_DATE")
  @Temporal(TemporalType.DATE)
  private Date creaDate;

  @Column(name="FAK_TAHUN", length=4)
  private String fakTahun;

  @Column(name="MODI_DATE")
  @Temporal(TemporalType.DATE)
  private Date modiDate;

  @Column(name="DO_BULAN", length=2)
  private String doBulan;

  @Column(name="CREA_BY", length=10)
  private String creaBy;

  @Column(name="TGL_DO")
  @Temporal(TemporalType.DATE)
  private Date tglDo;

  @Column(name="BIAYALAIN", length=1)
  private String biayalain;

  @Column(name="NAMA_PENERIMA", length=30)
  private String namaPenerima;

  @Column(name="MODI_BY", length=10)
  private String modiBy;

  @Column(name="ALAMAT_PENERIMA", length=50)
  private String alamatPenerima;

  @Column(name="DO_TAHUN", length=4)
  private String doTahun;

  @Column(name="HARGA", precision=20, scale=2)
  private BigDecimal harga;

  @Column(name="VSTATPICK", length=1)
  private String vstatpick;

  @JoinColumn(name="PODLR_NO_POINT", referencedColumnName="NO_POINT", insertable=false, updatable=false)
  @ManyToOne
  private HondaH100Mstpodlrs podlrNoPoint;

  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="hondaH100Fakdos")
  private Set<HondaH100Dtlfakdos> hondaH100DtlfakdosSet;

  @JoinColumn(name="KD_DLR", referencedColumnName="KD_DLR", insertable=false,updatable=false)
  @ManyToOne
  private HondaH000Dealers hondaH000Dealers;
  
  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="hondaH100Fakdos")
  private Set<HondaH100Hdrpicklists> hondaH100HdrpicklistsSet;
  
  @OneToOne
  @PrimaryKeyJoinColumn
  private HondaH100Ars hondaH100Ars;
  
  @OneToOne
  @PrimaryKeyJoinColumn
  private TrioH100Ars trioH100Ars;
  
  @OneToOne(mappedBy="hondaH100Fakdos", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
  @JoinColumn
  private TrioH100Dologs trioH100Dologs;
  
  public HondaH100Fakdos() {
    
  }
  

  public HondaH100Fakdos(String noDo)  {
    this.noDo = noDo;
  }
  

public TrioH100Ars getTrioH100Ars() {
	return trioH100Ars;
}


public void setTrioH100Ars(TrioH100Ars trioH100Ars) {
	this.trioH100Ars = trioH100Ars;
}


public HondaH100Ars getHondaH100Ars() {
	return hondaH100Ars;
}

public void setHondaH100Ars(HondaH100Ars hondaH100Ars) {
	this.hondaH100Ars = hondaH100Ars;
}

	
	public HondaH100Mstpodlrs getPodlrNoPoint() {
	return podlrNoPoint;
	}

	public void setPodlrNoPoint(HondaH100Mstpodlrs podlrNoPoint) {
	this.podlrNoPoint = podlrNoPoint;
}

	public String getNoDo() {
		return noDo;
	}
	
	public void setNoDo(String noDo) {
		this.noDo = noDo;
	}
	
	public String getJenisBayar() {
		return jenisBayar;
	}
	
	public void setJenisBayar(String jenisBayar) {
		this.jenisBayar = jenisBayar;
	}
	
	public String getKodeSalesman() {
		return kodeSalesman;
	}
	
	public void setKodeSalesman(String kodeSalesman) {
		this.kodeSalesman = kodeSalesman;
	}
	
	public String getPersBiaya() {
		return persBiaya;
	}
	
	public void setPersBiaya(String persBiaya) {
		this.persBiaya = persBiaya;
	}
	
	public String getLokasiAsal() {
		return lokasiAsal;
	}
	
	public void setLokasiAsal(String lokasiAsal) {
		this.lokasiAsal = lokasiAsal;
	}
	
	public String getKonsumen() {
		return konsumen;
	}
	
	public void setKonsumen(String konsumen) {
		this.konsumen = konsumen;
	}
	
	public String getOtr() {
		return otr;
	}
	
	public void setOtr(String otr) {
		this.otr = otr;
	}
	
	public String getNoFaktur() {
		return noFaktur;
	}
	
	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}
	
	public String getPlafon() {
		return plafon;
	}
	
	public void setPlafon(String plafon) {
		this.plafon = plafon;
	}
	
	public Integer getTop() {
		return top;
	}
	
	public void setTop(Integer top) {
		this.top = top;
	}
	
	public String getNamaTagihan() {
		return namaTagihan;
	}
	
	public void setNamaTagihan(String namaTagihan) {
		this.namaTagihan = namaTagihan;
	}
	
	public String getFakRunno() {
		return fakRunno;
	}
	
	public void setFakRunno(String fakRunno) {
		this.fakRunno = fakRunno;
	}
	
	public String getAlamatTagihan() {
		return alamatTagihan;
	}
	
	public void setAlamatTagihan(String alamatTagihan) {
		this.alamatTagihan = alamatTagihan;
	}
	
	public String getDoRunno() {
		return doRunno;
	}
	
	public void setDoRunno(String doRunno) {
		this.doRunno = doRunno;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getTglFak() {
		return tglFak;
	}
	
	public void setTglFak(Date tglFak) {
		this.tglFak = tglFak;
	}
	
	public String getNpwp() {
		return npwp;
	}
	
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	
	public String getFakBulan() {
		return fakBulan;
	}
	
	public void setFakBulan(String fakBulan) {
		this.fakBulan = fakBulan;
	}
	
	public Date getCreaDate() {
		return creaDate;
	}
	
	public void setCreaDate(Date creaDate) {
		this.creaDate = creaDate;
	}
	
	public String getFakTahun() {
		return fakTahun;
	}
	
	public void setFakTahun(String fakTahun) {
		this.fakTahun = fakTahun;
	}
	
	public Date getModiDate() {
		return modiDate;
	}
	
	public void setModiDate(Date modiDate) {
		this.modiDate = modiDate;
	}
	
	public String getDoBulan() {
		return doBulan;
	}
	
	public void setDoBulan(String doBulan) {
		this.doBulan = doBulan;
	}
	
	public String getCreaBy() {
		return creaBy;
	}
	
	public void setCreaBy(String creaBy) {
		this.creaBy = creaBy;
	}
	
	public Date getTglDo() {
		return tglDo;
	}
	
	public void setTglDo(Date tglDo) {
		this.tglDo = tglDo;
	}
	
	public String getBiayalain() {
		return biayalain;
	}
	
	public void setBiayalain(String biayalain) {
		this.biayalain = biayalain;
	}
	
	public String getNamaPenerima() {
		return namaPenerima;
	}
	
	public void setNamaPenerima(String namaPenerima) {
		this.namaPenerima = namaPenerima;
	}
	
	public String getModiBy() {
		return modiBy;
	}
	
	public void setModiBy(String modiBy) {
		this.modiBy = modiBy;
	}
	
	public String getAlamatPenerima() {
		return alamatPenerima;
	}
	
	public void setAlamatPenerima(String alamatPenerima) {
		this.alamatPenerima = alamatPenerima;
	}
	
	public String getDoTahun() {
		return doTahun;
	}
	
	public void setDoTahun(String doTahun) {
		this.doTahun = doTahun;
	}
	
	public BigDecimal getHarga() {
		return harga;
	}
	
	public void setHarga(BigDecimal harga) {
		this.harga = harga;
	}
	
	public String getVstatpick() {
		return vstatpick;
	}
	
	public void setVstatpick(String vstatpick) {
		this.vstatpick = vstatpick;
	}
	
	public Set<HondaH100Dtlfakdos> getHondaH100DtlfakdosSet() {
		return hondaH100DtlfakdosSet;
	}
	
	public void setHondaH100DtlfakdosSet(
			Set<HondaH100Dtlfakdos> hondaH100DtlfakdosSet) {
		this.hondaH100DtlfakdosSet = hondaH100DtlfakdosSet;
	}
	
	public HondaH000Dealers getHondaH000Dealers() {
		return hondaH000Dealers;
	}

	public void setHondaH000Dealers(HondaH000Dealers hondaH000Dealers) {
		this.hondaH000Dealers = hondaH000Dealers;
	}

	public Set<HondaH100Hdrpicklists> getHondaH100HdrpicklistsSet() {
		return hondaH100HdrpicklistsSet;
	}

	public void setHondaH100HdrpicklistsSet(
			Set<HondaH100Hdrpicklists> hondaH100HdrpicklistsSet) {
		this.hondaH100HdrpicklistsSet = hondaH100HdrpicklistsSet;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getPoDlrNoPoint() {
		return poDlrNoPoint;
	}

	public void setPoDlrNoPoint(String poDlrNoPoint) {
		this.poDlrNoPoint = poDlrNoPoint;
	}
	
	
	public TrioH100Dologs getTrioH100Dologs() {
		return trioH100Dologs;
	}


	public void setTrioH100Dologs(TrioH100Dologs trioH100Dologs) {
		this.trioH100Dologs = trioH100Dologs;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((noDo == null) ? 0 : noDo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Fakdos other = (HondaH100Fakdos) obj;
		if (noDo == null) {
			if (other.noDo != null)
				return false;
		} else if (!noDo.equals(other.noDo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Fakdos [noDo=" + noDo + "]";
	}
	  
}
