package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class HondaH100DtlinvsPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name="NO_SHIPLIST", length=19, nullable=false)
	private String noShiplist;
	
	@Column(name="KD_ITEM", length=6, nullable=false)
	private String kdItem;
	
	
	public HondaH100DtlinvsPK() {
		// TODO Auto-generated constructor stub
	}


	public String getNoShiplist() {
		return noShiplist;
	}


	public void setNoShiplist(String noShiplist) {
		this.noShiplist = noShiplist;
	}


	public String getKdItem() {
		return kdItem;
	}


	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		result = prime * result
				+ ((noShiplist == null) ? 0 : noShiplist.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100DtlinvsPK other = (HondaH100DtlinvsPK) obj;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		if (noShiplist == null) {
			if (other.noShiplist != null)
				return false;
		} else if (!noShiplist.equals(other.noShiplist))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "HondaH100DtlinvsPK [noShiplist=" + noShiplist + ", kdItem="
				+ kdItem + "]";
	}
	
	
}
