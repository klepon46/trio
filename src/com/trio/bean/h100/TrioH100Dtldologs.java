package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name = "TRIO_H100_DTLDOLOGS")
public class TrioH100Dtldologs extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DOLOG_NO_DOLOG", length= 30, nullable = false)
	private String dologNoDolog;
	
	@Id
	@Column(name="KD_ITEM", length = 25, nullable = false)
	private String kdItem;
	
	@Column(name="QTY", nullable = true)
	private int qty;
	
	@Column(name="DISC_CASH", length=22, precision=20, scale=2)
	private BigDecimal discCash;
	
	@Column(name="DISC_POT_DO", length=22, precision=20, scale=2)
	private BigDecimal discPotDo;
	
	@Column(name="DISC_SUBSIDI", length=22, precision=20, scale=2)
	private BigDecimal discSubsidi;
	
	@Column(name="DISC_CASH_PERSEN", length=11, precision=8, scale=3)
	private BigDecimal discCashPersen;
	
	@Column(name="KD_PKT_AKS", length=25)
	private String kdPktAks;
	
	@JoinColumn(name="DOLOG_NO_DOLOG", referencedColumnName="NO_DOLOG", insertable = false, updatable = false)
	@ManyToOne(cascade=CascadeType.ALL)
	private TrioH100Dologs trioH100Dologs;
	
	public TrioH100Dtldologs() {
		
	}
	
	public TrioH100Dtldologs(String dologNoDolog, String kdItem ) {
		this.dologNoDolog = dologNoDolog;
		this.kdItem = kdItem;
	}

	public String getDologNoDolog() {
		return dologNoDolog;
	}

	public void setDologNoDolog(String dologNoDolog) {
		this.dologNoDolog = dologNoDolog;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public TrioH100Dologs getTrioH100Dologs() {
		return trioH100Dologs;
	}

	public void setTrioH100Dologs(TrioH100Dologs trioH100Dologs) {
		this.trioH100Dologs = trioH100Dologs;
	}
	
	public BigDecimal getDiscCash() {
		return discCash;
	}

	public void setDiscCash(BigDecimal discCash) {
		this.discCash = discCash;
	}

	public BigDecimal getDiscPotDo() {
		return discPotDo;
	}

	public void setDiscPotDo(BigDecimal discPotDo) {
		this.discPotDo = discPotDo;
	}

	public BigDecimal getDiscSubsidi() {
		return discSubsidi;
	}

	public void setDiscSubsidi(BigDecimal discSubsidi) {
		this.discSubsidi = discSubsidi;
	}
	

	public BigDecimal getDiscCashPersen() {
		return discCashPersen;
	}

	public void setDiscCashPersen(BigDecimal discCashPersen) {
		this.discCashPersen = discCashPersen;
	}

	public String getKdPktAks() {
		return kdPktAks;
	}

	public void setKdPktAks(String kdPktAks) {
		this.kdPktAks = kdPktAks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((dologNoDolog == null) ? 0 : dologNoDolog.hashCode());
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Dtldologs other = (TrioH100Dtldologs) obj;
		if (dologNoDolog == null) {
			if (other.dologNoDolog != null)
				return false;
		} else if (!dologNoDolog.equals(other.dologNoDolog))
			return false;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100Dtldologs [dologNoDolog=" + dologNoDolog + ", kdItem="
				+ kdItem + "]";
	}
	
}
