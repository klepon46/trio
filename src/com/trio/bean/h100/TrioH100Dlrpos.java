package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/** @author Saifi Ahmada Jan 22, 2013 3:20:53 PM  **/

@Entity
@Table(name="TRIO_H100_DLRPOS")
public class TrioH100Dlrpos extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="KD_DLR", nullable=false, length=10)
	private String kdDlr;
	@Id
	@Column(name="KD_DLRPOS" , nullable=false, length=10)
	private String kdDlrpos;
	@Column(name="NAMA_DLRPOS", length=50)
	private String namaDlrpos;
	@Column(name="ALAMAT_DLRPOS", length=50)
	private String alamatDlrpos;
	@Column(name="ASAL_JUAL", length=10)
	private String asalJual;
	@Column(name="STATUS", length=1)
	private String status;
	@Column(name="KD_KEC", length=10)
	private String kdKec;
	@Column(name="KD_KEL", length=10)
	private String kdKel;
	@Column(name="KECAMATAN", length=40)
	private String kecamatan;
	@Column(name="KELURAHAN", length=50)
	private String kelurahan;
	
//	@OneToMany(mappedBy="trioH100Dlrpos")
//	private Set<TrioH100DtlAreaDealers> trioH100DtlAreaDealersSet; 
	
	public TrioH100Dlrpos() {
	
	}
	
	public TrioH100Dlrpos(String kdDlr, String kdDlrpos) {
		this.kdDlr = kdDlr;
		this.kdDlrpos = kdDlrpos;
	}

//	public Set<TrioH100DtlAreaDealers> getTrioH100DtlAreaDealersSet() {
//		return trioH100DtlAreaDealersSet;
//	}
//
//	public void setTrioH100DtlAreaDealersSet(
//			Set<TrioH100DtlAreaDealers> trioH100DtlAreaDealersSet) {
//		this.trioH100DtlAreaDealersSet = trioH100DtlAreaDealersSet;
//	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getKdDlrpos() {
		return kdDlrpos;
	}

	public void setKdDlrpos(String kdDlrpos) {
		this.kdDlrpos = kdDlrpos;
	}

	public String getNamaDlrpos() {
		return namaDlrpos;
	}

	public void setNamaDlrpos(String namaDlrpos) {
		this.namaDlrpos = namaDlrpos;
	}

	public String getAlamatDlrpos() {
		return alamatDlrpos;
	}

	public void setAlamatDlrpos(String alamatDlrpos) {
		this.alamatDlrpos = alamatDlrpos;
	}

	public String getAsalJual() {
		return asalJual;
	}

	public void setAsalJual(String asalJual) {
		this.asalJual = asalJual;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getKdKec() {
		return kdKec;
	}

	public void setKdKec(String kdKec) {
		this.kdKec = kdKec;
	}

	public String getKdKel() {
		return kdKel;
	}

	public void setKdKel(String kdKel) {
		this.kdKel = kdKel;
	}

	public String getKecamatan() {
		return kecamatan;
	}

	public void setKecamatan(String kecamatan) {
		this.kecamatan = kecamatan;
	}

	public String getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(String kelurahan) {
		this.kelurahan = kelurahan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kdDlr == null) ? 0 : kdDlr.hashCode());
		result = prime * result
				+ ((kdDlrpos == null) ? 0 : kdDlrpos.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Dlrpos other = (TrioH100Dlrpos) obj;
		if (kdDlr == null) {
			if (other.kdDlr != null)
				return false;
		} else if (!kdDlr.equals(other.kdDlr))
			return false;
		if (kdDlrpos == null) {
			if (other.kdDlrpos != null)
				return false;
		} else if (!kdDlrpos.equals(other.kdDlrpos))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100Dlrpos [kdDlr=" + kdDlr + ", kdDlrpos=" + kdDlrpos
				+ "]";
	}
	
}

