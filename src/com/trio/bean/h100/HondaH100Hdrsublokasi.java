package com.trio.bean.h100;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_HDRSUBLOKASI")
public class HondaH100Hdrsublokasi extends TrioEntityUserTrail implements Serializable
{
	  private static final long serialVersionUID = 1L;
	
	  @Id
	  @Basic(optional=false)
	  @Column(name="VSUBLOKASI", nullable=false, length=20)
	  private String vsublokasi;
	  
	  @Id
	  @Basic(optional=false)
	  @Column(name="VKODELOKASI", nullable=false, length=4)
	  private String vkodelokasi;
	
	  @Column(name="NQTYCAP")
	  private Long nqtycap;
	
	  @Column(name="VALIASLOK", length=1)
	  private String valiaslok;
	
	  @Column(name="VLANTAI", length=1)
	  private String vlantai;
	
	  @Column(name="VKOLOM", length=1)
	  private String vkolom;
	
	  @Column(name="VBARIS", length=1)
	  private String vbaris;
	
	  @Column(name="VTYPDEDICAT", length=25)
	  private String vtypdedicat;
	
	  @Column(name="NQTYBOOKSTR")
	  private Long nqtybookstr;
	
	  @Column(name="NQTYBOOKPIC")
	  private Long nqtybookpic;
	
	  @Column(name="VSTATUS", length=1)
	  private String vstatus;
	
	  @JoinColumn(name="VKODELOKASI", referencedColumnName="KODE_LOKASI", nullable=false, insertable=false, updatable=false)
	  @ManyToOne(optional=false)
	  private HondaH100Gudangs hondaH100Gudangs;
	
	  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="hondaH100Hdrsublokasi")
	  private Set<HondaH100Dtlsublokasi> hondaH100DtlsublokasiSet;
	
	  public HondaH100Hdrsublokasi()  {
	    this.hondaH100DtlsublokasiSet = new HashSet<HondaH100Dtlsublokasi>();
	  }
	
	  public HondaH100Hdrsublokasi(String vsublokasi, String vkodelokasi) {
	    this.vsublokasi = vsublokasi;
	    this.vkodelokasi = vkodelokasi;
	  }

		public String getVsublokasi() {
			return vsublokasi;
		}
	
		public void setVsublokasi(String vsublokasi) {
			this.vsublokasi = vsublokasi;
		}
	
		public String getVkodelokasi() {
			return vkodelokasi;
		}
	
		public void setVkodelokasi(String vkodelokasi) {
			this.vkodelokasi = vkodelokasi;
		}
	
		public Long getNqtycap() {
			return nqtycap;
		}
	
		public void setNqtycap(Long nqtycap) {
			this.nqtycap = nqtycap;
		}
	
		public String getValiaslok() {
			return valiaslok;
		}
	
		public void setValiaslok(String valiaslok) {
			this.valiaslok = valiaslok;
		}
	
		public String getVlantai() {
			return vlantai;
		}
	
		public void setVlantai(String vlantai) {
			this.vlantai = vlantai;
		}
	
		public String getVkolom() {
			return vkolom;
		}
	
		public void setVkolom(String vkolom) {
			this.vkolom = vkolom;
		}
	
		public String getVbaris() {
			return vbaris;
		}
	
		public void setVbaris(String vbaris) {
			this.vbaris = vbaris;
		}
	
		public String getVtypdedicat() {
			return vtypdedicat;
		}
	
		public void setVtypdedicat(String vtypdedicat) {
			this.vtypdedicat = vtypdedicat;
		}
	
		public Long getNqtybookstr() {
			return nqtybookstr;
		}
	
		public void setNqtybookstr(Long nqtybookstr) {
			this.nqtybookstr = nqtybookstr;
		}
	
		public Long getNqtybookpic() {
			return nqtybookpic;
		}
	
		public void setNqtybookpic(Long nqtybookpic) {
			this.nqtybookpic = nqtybookpic;
		}
	
		public String getVstatus() {
			return vstatus;
		}
	
		public void setVstatus(String vstatus) {
			this.vstatus = vstatus;
		}
	
		public HondaH100Gudangs getHondaH100Gudangs() {
			return hondaH100Gudangs;
		}
	
		public void setHondaH100Gudangs(HondaH100Gudangs hondaH100Gudangs) {
			this.hondaH100Gudangs = hondaH100Gudangs;
		}
	
		public Set<HondaH100Dtlsublokasi> getHondaH100DtlsublokasiSet() {
			return hondaH100DtlsublokasiSet;
		}
	
		public void setHondaH100DtlsublokasiSet(
				Set<HondaH100Dtlsublokasi> hondaH100DtlsublokasiSet) {
			this.hondaH100DtlsublokasiSet = hondaH100DtlsublokasiSet;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result
					+ ((vkodelokasi == null) ? 0 : vkodelokasi.hashCode());
			result = prime * result
					+ ((vsublokasi == null) ? 0 : vsublokasi.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			HondaH100Hdrsublokasi other = (HondaH100Hdrsublokasi) obj;
			if (vkodelokasi == null) {
				if (other.vkodelokasi != null)
					return false;
			} else if (!vkodelokasi.equals(other.vkodelokasi))
				return false;
			if (vsublokasi == null) {
				if (other.vsublokasi != null)
					return false;
			} else if (!vsublokasi.equals(other.vsublokasi))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "HondaH100Hdrsublokasi [vsublokasi=" + vsublokasi
					+ ", vkodelokasi=" + vkodelokasi + "]";
		}
}