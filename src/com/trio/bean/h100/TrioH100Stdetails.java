package com.trio.bean.h100;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

import com.trio.bean.h000.HondaH000Dealers;
import com.trio.bean.h000.HondaH000Items;
import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="TRIO_H100_STDETAILS")
public class TrioH100Stdetails extends TrioEntityUserTrail implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="NO_MESIN")
    private String noMesin;
    
    @Column(name="FINANCE_COMPANY" , length=30)        
    private String financeCompany;
    
    @Column(name="TGL_PELAPORAN")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglPelaporan;
    
    @Column(name="NO_PELAPORAN", length=50)
    private String noPelaporan;
    
    @Column(name="NO_RANGKA", length=30)        
    private String noRangka;
    
    @Column(name="TGL_KONTRAK")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglKontrak;
    
    @Column(name="STATUS", length=1)
    private String status;
    
    @Column(name="SOLDOFFTR", length=1)
    private String soldofftr;
    
    @Column(name="TERJUAL_KE", length=50)
    private String terjualKe;
    
    @Column(name="LEGES", length=1)
    private String leges;
    
    @Column(name="TGL_DOAHM")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglDoahm;
    
    @Column(name="NO_DOAHM", length=30)       
    private String noDoahm;
    
    @Column(name="KD_DLR", length=10)
    private String kdDlr;
    
    @Column(name="TGL_DO")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglDo;
    
    @Column(name="NO_DO", length=30)
    private String noDo;
    
    @Column(name="TGL_FAK")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglFak;
    
    @Column(name="NO_FAKTUR", length=30)
    private String noFaktur;
    
    @Column(name="TGL_SJ")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglSj;
    
    @Column(name="NO_SJ", length=30)
    private String noSj;
    
    @Column(name="TGL_MOHON_FAKTUR")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglMohonFaktur;
    
    @Column(name="TGL_FAKSTNK")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglFakstnk;
    
    @Column(name="KONSUMEN_ID", length=30)
    private String konsumenId;
    
    @Column(name="NAMA_KONSUMEN", length=50)
    private String namaKonsumen;
    
    @Column(name="EKSPEDISI", length=20)
    private String ekspedisi;
    
    @Column(name="EKSPEDISI_TODLR", length=20)
    private String ekspedisiTodlr;
    
    @Column(name="LOKASI", length=10)
    private String lokasi;
    
    @Column(name="TH_PROD", length=4)
    private String thProd;
    
    @Column(name="KD_ITEM", length=20)
    private String kdItem;
    
    @Column(name="NO_FAKSTNK", length=30)
    private String noFakstnk; 
    
    @Column(name="STATUS_STDTL", length=1)
    private String statusStdtl;
    
    @Column(name="MOHON_LGS_RP", length=15)
    private Integer mohonLgsRp;
    
    @Column(name="TAGIH_LGS_RP",length=15)
    private Integer tagihLgsRp;
    
    @Column(name="NO_MOHON_LGS", length=30)        
    private String noMohonLgs;
    
    @Column(name="NO_TAGIH_LGS", length=30)        
    private String noTagihLgs; 
    
    @Column(name="TGL_PEMUTIHAN")        
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglPemutihan; 
    
    @Column(name="TGL_MOHON_LGS")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglMohonLgs;  
    
    @Column(name="TGL_TAGIH_LGS")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglTagihLgs;  
    
    @Column(name="REASON_TEL", length=1)
    private String reasonTel;  
    
    @Column(name="REASON_HP", length=1)
    private String reasonHp; 
    
    @Column(name="TGL_BELI_KONS")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date tglBeliKons; 
    
    @Column(name="JENIS_BAYAR", length=1)
    private String jenisBayar;
    
    @Column(name="DP", length=15)
    private Integer dp; 
    
    @Column(name="SP_ID", length=15)
    private String spId;
    
    @Column(name="LOCALSP_ID", length=15)
    private String localspId;
    
    @Column(name="ASAL_JUAL", length=10)
    private String asalJual; 
    
    @Column(name="KD_DLRPOS", length=3)
    private String kdDlrpos;
    
    @Column(name="JENIS_SLSFORCE", length=15)
    private String jenisSlsforce;
    
    @Column(name="KD_KEL", length=10)
    private String kdKel;
    
    @Column(name="KD_KEC", length=10)
    private String kdKec;
    
    @Column(name="THBLMD", length=15)
    private Integer thblmd; 
    
    @Column(name="FHBLMD", length=15)
    private Integer fhblmd;
    
    @Column(name="TMDPRO", length=15)
    private Integer tmdpro; 
    
    @Column(name="TTRALL", length=15)
    private Integer ttrall; 
    
    @Column(name="FTRALL", length=15)
    private Integer ftrall; 
    
    @Column(name="TTRAMD", length=15)
    private Integer ttramd;
    
    @Column(name="FTRAMD", length=15)
    private Integer ftramd; 
    
    @Column(name="TASAMD", length=15)
    private Integer tasamd;
    
    @Column(name="FASAMD", length=15)
    private Integer fasamd; 
    
    @Column(name="TTISID", length=15)
    private Integer ttisid; 
    
    @Column(name="FTISID", length=15)
    private Integer ftisid; 
    
    @Column(name="TTRMDD", length=15)
    private Integer ttrmdd; 
    
    @Column(name="FTRMDD", length=15)
    private Integer ftrmdd; 
    
    @Column(name="STATUS_PW", length=1)
    private String statusPw;
    
    @Column(name="NILAI_PW", length=15)
    private Integer nilaiPw;        
    
    @Column(name="NO_APPROVAL_PW", length=16)
    private String noApprovalPw;
    
    @JoinColumn(name="KD_DLR", referencedColumnName="KD_DLR", insertable=false, updatable=false)
    @ManyToOne(cascade= CascadeType.ALL)
    private HondaH000Dealers dealer;
    
    @JoinColumn(name="KD_KEL", referencedColumnName="KD_KEL", insertable=false, updatable=false)
    @ManyToOne(cascade= CascadeType.ALL)
    private TrioH100Kelurahans kelurahan;
    
    @JoinColumn(name="KONSUMEN_ID", referencedColumnName="KONSUMEN_ID", insertable=false, updatable=false)
    @ManyToOne(cascade= CascadeType.ALL)
    private HondaH100Customers customer;
    
    @JoinColumn(name="NO_APPROVAL_PW", referencedColumnName="NO_APPROVAL_PW", insertable=false, updatable=false)
    @ManyToOne
    private TrioH100PelangWils pelangWils;
    
    @JoinColumn(name="KD_ITEM", referencedColumnName="KD_ITEM", insertable=false, updatable=false)
    @ManyToOne
    private HondaH000Items hondaH000Items;

	public TrioH100Stdetails() {

	}
	
	public TrioH100Stdetails(String noMesin) {
        this.noMesin = noMesin;
    }
	
	public TrioH100PelangWils getPelangWils() {
		return pelangWils;
	}

	public void setPelangWils(TrioH100PelangWils pelangWils) {
		this.pelangWils = pelangWils;
	}

    public HondaH000Items getHondaH000Items() {
		return hondaH000Items;
	}

	public void setHondaH000Items(HondaH000Items hondaH000Items) {
		this.hondaH000Items = hondaH000Items;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getFinanceCompany() {
		return financeCompany;
	}

	public void setFinanceCompany(String financeCompany) {
		this.financeCompany = financeCompany;
	}

	public Date getTglPelaporan() {
		return tglPelaporan;
	}

	public void setTglPelaporan(Date tglPelaporan) {
		this.tglPelaporan = tglPelaporan;
	}

	public String getNoPelaporan() {
		return noPelaporan;
	}

	public void setNoPelaporan(String noPelaporan) {
		this.noPelaporan = noPelaporan;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public Date getTglKontrak() {
		return tglKontrak;
	}

	public void setTglKontrak(Date tglKontrak) {
		this.tglKontrak = tglKontrak;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSoldofftr() {
		return soldofftr;
	}

	public void setSoldofftr(String soldofftr) {
		this.soldofftr = soldofftr;
	}

	public String getTerjualKe() {
		return terjualKe;
	}

	public void setTerjualKe(String terjualKe) {
		this.terjualKe = terjualKe;
	}

	public String getLeges() {
		return leges;
	}

	public void setLeges(String leges) {
		this.leges = leges;
	}

	public Date getTglDoahm() {
		return tglDoahm;
	}

	public void setTglDoahm(Date tglDoahm) {
		this.tglDoahm = tglDoahm;
	}

	public String getNoDoahm() {
		return noDoahm;
	}

	public void setNoDoahm(String noDoahm) {
		this.noDoahm = noDoahm;
	}

	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public Date getTglDo() {
		return tglDo;
	}

	public void setTglDo(Date tglDo) {
		this.tglDo = tglDo;
	}

	public String getNoDo() {
		return noDo;
	}

	public void setNoDo(String noDo) {
		this.noDo = noDo;
	}

	public Date getTglFak() {
		return tglFak;
	}

	public void setTglFak(Date tglFak) {
		this.tglFak = tglFak;
	}

	public String getNoFaktur() {
		return noFaktur;
	}

	public void setNoFaktur(String noFaktur) {
		this.noFaktur = noFaktur;
	}

	public Date getTglSj() {
		return tglSj;
	}

	public void setTglSj(Date tglSj) {
		this.tglSj = tglSj;
	}

	public String getNoSj() {
		return noSj;
	}

	public void setNoSj(String noSj) {
		this.noSj = noSj;
	}

	public Date getTglMohonFaktur() {
		return tglMohonFaktur;
	}

	public void setTglMohonFaktur(Date tglMohonFaktur) {
		this.tglMohonFaktur = tglMohonFaktur;
	}

	public Date getTglFakstnk() {
		return tglFakstnk;
	}

	public void setTglFakstnk(Date tglFakstnk) {
		this.tglFakstnk = tglFakstnk;
	}

	public String getKonsumenId() {
		return konsumenId;
	}

	public void setKonsumenId(String konsumenId) {
		this.konsumenId = konsumenId;
	}

	public String getNamaKonsumen() {
		return namaKonsumen;
	}

	public void setNamaKonsumen(String namaKonsumen) {
		this.namaKonsumen = namaKonsumen;
	}

	public String getEkspedisi() {
		return ekspedisi;
	}

	public void setEkspedisi(String ekspedisi) {
		this.ekspedisi = ekspedisi;
	}

	public String getEkspedisiTodlr() {
		return ekspedisiTodlr;
	}

	public void setEkspedisiTodlr(String ekspedisiTodlr) {
		this.ekspedisiTodlr = ekspedisiTodlr;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getThProd() {
		return thProd;
	}

	public void setThProd(String thProd) {
		this.thProd = thProd;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public String getNoFakstnk() {
		return noFakstnk;
	}

	public void setNoFakstnk(String noFakstnk) {
		this.noFakstnk = noFakstnk;
	}

	public String getStatusStdtl() {
		return statusStdtl;
	}

	public void setStatusStdtl(String statusStdtl) {
		this.statusStdtl = statusStdtl;
	}

	public Integer getMohonLgsRp() {
		return mohonLgsRp;
	}

	public void setMohonLgsRp(Integer mohonLgsRp) {
		this.mohonLgsRp = mohonLgsRp;
	}

	public Integer getTagihLgsRp() {
		return tagihLgsRp;
	}

	public void setTagihLgsRp(Integer tagihLgsRp) {
		this.tagihLgsRp = tagihLgsRp;
	}

	public String getNoMohonLgs() {
		return noMohonLgs;
	}

	public void setNoMohonLgs(String noMohonLgs) {
		this.noMohonLgs = noMohonLgs;
	}

	public String getNoTagihLgs() {
		return noTagihLgs;
	}

	public void setNoTagihLgs(String noTagihLgs) {
		this.noTagihLgs = noTagihLgs;
	}

	public Date getTglPemutihan() {
		return tglPemutihan;
	}

	public void setTglPemutihan(Date tglPemutihan) {
		this.tglPemutihan = tglPemutihan;
	}

	public Date getTglMohonLgs() {
		return tglMohonLgs;
	}

	public void setTglMohonLgs(Date tglMohonLgs) {
		this.tglMohonLgs = tglMohonLgs;
	}

	public Date getTglTagihLgs() {
		return tglTagihLgs;
	}

	public void setTglTagihLgs(Date tglTagihLgs) {
		this.tglTagihLgs = tglTagihLgs;
	}

	public String getReasonTel() {
		return reasonTel;
	}

	public void setReasonTel(String reasonTel) {
		this.reasonTel = reasonTel;
	}

	public String getReasonHp() {
		return reasonHp;
	}

	public void setReasonHp(String reasonHp) {
		this.reasonHp = reasonHp;
	}

	public Date getTglBeliKons() {
		return tglBeliKons;
	}

	public void setTglBeliKons(Date tglBeliKons) {
		this.tglBeliKons = tglBeliKons;
	}

	public String getJenisBayar() {
		return jenisBayar;
	}

	public void setJenisBayar(String jenisBayar) {
		this.jenisBayar = jenisBayar;
	}

	public Integer getDp() {
		return dp;
	}

	public void setDp(Integer dp) {
		this.dp = dp;
	}

	public String getSpId() {
		return spId;
	}

	public void setSpId(String spId) {
		this.spId = spId;
	}

	public String getLocalspId() {
		return localspId;
	}

	public void setLocalspId(String localspId) {
		this.localspId = localspId;
	}

	public String getAsalJual() {
		return asalJual;
	}

	public void setAsalJual(String asalJual) {
		this.asalJual = asalJual;
	}

	public String getKdDlrpos() {
		return kdDlrpos;
	}

	public void setKdDlrpos(String kdDlrpos) {
		this.kdDlrpos = kdDlrpos;
	}

	public String getJenisSlsforce() {
		return jenisSlsforce;
	}

	public void setJenisSlsforce(String jenisSlsforce) {
		this.jenisSlsforce = jenisSlsforce;
	}

	public String getKdKel() {
		return kdKel;
	}

	public void setKdKel(String kdKel) {
		this.kdKel = kdKel;
	}

	public String getKdKec() {
		return kdKec;
	}

	public void setKdKec(String kdKec) {
		this.kdKec = kdKec;
	}

	public Integer getThblmd() {
		return thblmd;
	}

	public void setThblmd(Integer thblmd) {
		this.thblmd = thblmd;
	}

	public Integer getFhblmd() {
		return fhblmd;
	}

	public void setFhblmd(Integer fhblmd) {
		this.fhblmd = fhblmd;
	}

	public Integer getTmdpro() {
		return tmdpro;
	}

	public void setTmdpro(Integer tmdpro) {
		this.tmdpro = tmdpro;
	}

	public Integer getTtrall() {
		return ttrall;
	}

	public void setTtrall(Integer ttrall) {
		this.ttrall = ttrall;
	}

	public Integer getFtrall() {
		return ftrall;
	}

	public void setFtrall(Integer ftrall) {
		this.ftrall = ftrall;
	}

	public Integer getTtramd() {
		return ttramd;
	}

	public void setTtramd(Integer ttramd) {
		this.ttramd = ttramd;
	}

	public Integer getFtramd() {
		return ftramd;
	}

	public void setFtramd(Integer ftramd) {
		this.ftramd = ftramd;
	}

	public Integer getTasamd() {
		return tasamd;
	}

	public void setTasamd(Integer tasamd) {
		this.tasamd = tasamd;
	}

	public Integer getFasamd() {
		return fasamd;
	}

	public void setFasamd(Integer fasamd) {
		this.fasamd = fasamd;
	}

	public Integer getTtisid() {
		return ttisid;
	}

	public void setTtisid(Integer ttisid) {
		this.ttisid = ttisid;
	}

	public Integer getFtisid() {
		return ftisid;
	}

	public void setFtisid(Integer ftisid) {
		this.ftisid = ftisid;
	}

	public Integer getTtrmdd() {
		return ttrmdd;
	}

	public void setTtrmdd(Integer ttrmdd) {
		this.ttrmdd = ttrmdd;
	}

	public Integer getFtrmdd() {
		return ftrmdd;
	}

	public void setFtrmdd(Integer ftrmdd) {
		this.ftrmdd = ftrmdd;
	}

	public String getStatusPw() {
		return statusPw;
	}

	public void setStatusPw(String statusPw) {
		this.statusPw = statusPw;
	}

	public Integer getNilaiPw() {
		return nilaiPw;
	}

	public void setNilaiPw(Integer nilaiPw) {
		this.nilaiPw = nilaiPw;
	}

	public String getNoApprovalPw() {
		return noApprovalPw;
	}

	public void setNoApprovalPw(String noApprovalPw) {
		this.noApprovalPw = noApprovalPw;
	}

	public HondaH000Dealers getDealer() {
		return dealer;
	}

	public void setDealer(HondaH000Dealers dealer) {
		this.dealer = dealer;
	}

	public TrioH100Kelurahans getKelurahan() {
		return kelurahan;
	}

	public void setKelurahan(TrioH100Kelurahans kelurahan) {
		this.kelurahan = kelurahan;
	}

	public HondaH100Customers getCustomer() {
		if(customer==null)
			customer = new HondaH100Customers();
		return customer;
	}

	public void setCustomer(HondaH100Customers customer) {
		this.customer = customer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Stdetails other = (TrioH100Stdetails) obj;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Stdetails [noMesin=" + noMesin + "]";
	}

}

