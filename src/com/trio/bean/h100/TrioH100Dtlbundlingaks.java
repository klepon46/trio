package com.trio.bean.h100;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:44:37 AM Dec 26, 2013
 */

@Entity
@Table(name="TRIO_H100_DTLBUNDLINGAKS")
public class TrioH100Dtlbundlingaks extends TrioEntityUserTrail {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioH100DtlbundlingaksPK trioH100DtlbundlingaksPK;
	
	@Column(name="NO_RANGKA", length=30)
	private String noRangka;
	
	@Column(name="LOKASI", length=10)
	private String lokasi;
	
	@Column(name="KD_ITEM_BARU", length=25)
	private String kdItemBaru;
	
	@Column(name="QTY")
	private int qty;
	
	@Column(name="HARGA_NETT")
	private BigDecimal hargaNett;
	
	@Column(name="KD_PKT", length=12)
	private String kdPkt;
	
	@Column(name="KD_ITEM_ASAL", length=25)
	private String kdItemAsal;
	
	@ManyToOne
	@JoinColumn(name="NO_BUNDLING", referencedColumnName="NO_BUNDLING", insertable=false, updatable=false)
	private TrioH100Bundlingaks trioH100Bundlingaks;
	
	public TrioH100Dtlbundlingaks() {
		this.trioH100DtlbundlingaksPK = new TrioH100DtlbundlingaksPK();
	}

	public TrioH100DtlbundlingaksPK getTrioH100DtlbundlingaksPK() {
		return trioH100DtlbundlingaksPK;
	}

	public void setTrioH100DtlbundlingaksPK(
			TrioH100DtlbundlingaksPK trioH100DtlbundlingaksPK) {
		this.trioH100DtlbundlingaksPK = trioH100DtlbundlingaksPK;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getKdItemBaru() {
		return kdItemBaru;
	}

	public void setKdItemBaru(String kdItemBaru) {
		this.kdItemBaru = kdItemBaru;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public BigDecimal getHargaNett() {
		return hargaNett;
	}

	public void setHargaNett(BigDecimal hargaNett) {
		this.hargaNett = hargaNett;
	}
	
	public String getKdPkt() {
		return kdPkt;
	}

	public void setKdPkt(String kdPkt) {
		this.kdPkt = kdPkt;
	}

	public String getKdItemAsal() {
		return kdItemAsal;
	}

	public void setKdItemAsal(String kdItemAsal) {
		this.kdItemAsal = kdItemAsal;
	}

	public TrioH100Bundlingaks getTrioH100Bundlingaks() {
		return trioH100Bundlingaks;
	}

	public void setTrioH100Bundlingaks(TrioH100Bundlingaks trioH100Bundlingaks) {
		this.trioH100Bundlingaks = trioH100Bundlingaks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioH100DtlbundlingaksPK == null) ? 0
						: trioH100DtlbundlingaksPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Dtlbundlingaks other = (TrioH100Dtlbundlingaks) obj;
		if (trioH100DtlbundlingaksPK == null) {
			if (other.trioH100DtlbundlingaksPK != null)
				return false;
		} else if (!trioH100DtlbundlingaksPK
				.equals(other.trioH100DtlbundlingaksPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100Dtlbundlingaks [trioH100DtlbundlingaksPK="
				+ trioH100DtlbundlingaksPK + "]";
	}
	
}
