/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.bean.h000.HondaH000Dealers;
import com.trio.usertrail.TrioEntityUserTrail;

/**
 *
 * @author glassfish | Saipi Ramli
 */
@Entity
@Table(name="TRIO_H100_DTLAREADEALERS")
public class TrioH100DtlAreaDealers extends TrioEntityUserTrail  implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="KD_DLR", length=10, nullable=false) 
    private String kdDlr;
    @Id
    @Column(name="KD_KEC", length=10, nullable=false) 
    private String kdKec;
    @Id
    @Column(name="KD_DLRPOS", length=10, nullable=false)
    private String kdDlrpos;
    
    public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getKdKec() {
		return kdKec;
	}

	public void setKdKec(String kdKec) {
		this.kdKec = kdKec;
	}

	@Column(name="STATUS", length=1)
    private String status;
    
    @JoinColumn(name="KD_DLR", referencedColumnName="KD_DLR", insertable=false, updatable=false)
    @ManyToOne(cascade= CascadeType.ALL)
    private HondaH000Dealers hondaH000Dealers;
    
    @JoinColumn(name="KD_KEC", referencedColumnName="KD_KEC", insertable=false, updatable=false)
    @ManyToOne(cascade= CascadeType.ALL)
    private TrioH100Kecamatans trioH100Kecamatans;
    
//    @ManyToOne(cascade=CascadeType.ALL)
//    @JoinColumn(name="KD_DLRPOS", referencedColumnName="KD_DLRPOS", insertable=false, updatable=false)
//    private TrioH100Dlrpos trioH100Dlrpos;
    
    public TrioH100DtlAreaDealers() {
        
    }
    
    public TrioH100DtlAreaDealers(String kdDlr, String kdKec, String KdDlrpos ) {
        this.kdDlr = kdDlr;
        this.kdKec = kdKec;		
        this.kdDlrpos = kdDlrpos;
    }

//    public TrioH100Dlrpos getTrioH100Dlrpos() {
//		return trioH100Dlrpos;
//	}
//
//	public void setTrioH100Dlrpos(TrioH100Dlrpos trioH100Dlrpos) {
//		this.trioH100Dlrpos = trioH100Dlrpos;
//	}

	public String getKdDlrpos() {
		return kdDlrpos;
	}

	public void setKdDlrpos(String kdDlrpos) {
		this.kdDlrpos = kdDlrpos;
	}

	public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	public HondaH000Dealers getHondaH000Dealers() {
		return hondaH000Dealers;
	}

	public void setHondaH000Dealers(HondaH000Dealers hondaH000Dealers) {
		this.hondaH000Dealers = hondaH000Dealers;
	}

	public TrioH100Kecamatans getTrioH100Kecamatans() {
		return trioH100Kecamatans;
	}

	public void setTrioH100Kecamatans(TrioH100Kecamatans trioH100Kecamatans) {
		this.trioH100Kecamatans = trioH100Kecamatans;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kdDlr == null) ? 0 : kdDlr.hashCode());
		result = prime * result
				+ ((kdDlrpos == null) ? 0 : kdDlrpos.hashCode());
		result = prime * result + ((kdKec == null) ? 0 : kdKec.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlAreaDealers other = (TrioH100DtlAreaDealers) obj;
		if (kdDlr == null) {
			if (other.kdDlr != null)
				return false;
		} else if (!kdDlr.equals(other.kdDlr))
			return false;
		if (kdDlrpos == null) {
			if (other.kdDlrpos != null)
				return false;
		} else if (!kdDlrpos.equals(other.kdDlrpos))
			return false;
		if (kdKec == null) {
			if (other.kdKec != null)
				return false;
		} else if (!kdKec.equals(other.kdKec))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlAreaDealers [kdDlr=" + kdDlr + ", kdKec=" + kdKec
				+ ", kdDlrpos=" + kdDlrpos + "]";
	}

}

