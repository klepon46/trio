package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:11:23 AM Dec 10, 2013
 */

@Entity
@Table(name="TRIO_H100_DTLHARGAS")
public class TrioH100DtlHargas extends TrioEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private TrioH100DtlHargasPK trioH100DtlHargasPK;
	
	@Column(name="NILAI_AKS")
	private BigDecimal nilaiAks;

	public TrioH100DtlHargasPK getTrioH100DtlHargasPK() {
		return trioH100DtlHargasPK;
	}

	public void setTrioH100DtlHargasPK(TrioH100DtlHargasPK trioH100DtlHargasPK) {
		this.trioH100DtlHargasPK = trioH100DtlHargasPK;
	}

	public BigDecimal getNilaiAks() {
		return nilaiAks;
	}

	public void setNilaiAks(BigDecimal nilaiAks) {
		this.nilaiAks = nilaiAks;
	}
	
	public TrioH100DtlHargas() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioH100DtlHargasPK == null) ? 0 : trioH100DtlHargasPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlHargas other = (TrioH100DtlHargas) obj;
		if (trioH100DtlHargasPK == null) {
			if (other.trioH100DtlHargasPK != null)
				return false;
		} else if (!trioH100DtlHargasPK.equals(other.trioH100DtlHargasPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlHargas [trioH100DtlHargasPK=" + trioH100DtlHargasPK
				+ "]";
	}
	
}
