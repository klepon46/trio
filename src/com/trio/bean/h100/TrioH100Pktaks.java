package com.trio.bean.h100;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:02:24 AM Dec 20, 2013
 */

@Entity
@Table(name="TRIO_H100_PKTAKS")
public class TrioH100Pktaks extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KD_PKT", length=12)
	private String kdPkt;

	@Column(name="NAMA_PKT", length=50)
	private String namaPkt;

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="trioH100Pktaks")
	private Set<TrioH100DtlpktaksPart> trioH100DtlpktaksPartsSet;

	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="trioH100Pktaks")
	private Set<TrioH100DtlpktaksItem> trioH100DtlpktaksItemSet;

	public TrioH100Pktaks() {
		// TODO Auto-generated constructor stub
	}

	public String getKdPkt() {
		return kdPkt;
	}

	public void setKdPkt(String kdPkt) {
		this.kdPkt = kdPkt;
	}

	public String getNamaPkt() {
		return namaPkt;
	}

	public void setNamaPkt(String namaPkt) {
		this.namaPkt = namaPkt;
	}

	public Set<TrioH100DtlpktaksPart> getTrioH100DtlpktaksPartsSet() {
		return trioH100DtlpktaksPartsSet;
	}

	public void setTrioH100DtlpktaksPartsSet(
			Set<TrioH100DtlpktaksPart> trioH100DtlpktaksPartsSet) {
		this.trioH100DtlpktaksPartsSet = trioH100DtlpktaksPartsSet;
	}
	
	public Set<TrioH100DtlpktaksItem> getTrioH100DtlpktaksItemSet() {
		return trioH100DtlpktaksItemSet;
	}

	public void setTrioH100DtlpktaksItemSet(
			Set<TrioH100DtlpktaksItem> trioH100DtlpktaksItemSet) {
		this.trioH100DtlpktaksItemSet = trioH100DtlpktaksItemSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kdPkt == null) ? 0 : kdPkt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Pktaks other = (TrioH100Pktaks) obj;
		if (kdPkt == null) {
			if (other.kdPkt != null)
				return false;
		} else if (!kdPkt.equals(other.kdPkt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100Pktaks [kdPkt=" + kdPkt + "]";
	}


}
