package com.trio.bean.h100;


import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_HARGAS")
public class HondaH100Hargas extends TrioEntityUserTrail implements Serializable {
  
	  private static final long serialVersionUID = 1L;
	
	  @Id
	  @Column(name="KD_ITEM", nullable=false, length=25)
	  private String kdItem;
	  
	  @Id
	  @Column(name="AREA", nullable=false, length=10)
	  private String area;
	
	  @Column(name="DISCOUNT", length=20)
	  private BigInteger discount;
	
	  @Column(name="HARGA", precision=20, scale=2)
	  private BigDecimal harga;
	
	  @Column(name="HARGA_STD", precision=20, scale=2)
	  private BigDecimal hargaStd;
	
	  @Column(name="BIAYA_LAIN", precision=20, scale=2)
	  private BigDecimal biayaLain;
	
	  @Column(name="BIAYA_STNK", precision=20, scale=2)
	  private BigDecimal biayaStnk;
	  
	  @Column(name="APPROVE", length=1)
	  private Integer approve;
	  
	  @Column(name="HRG_BELI", precision=20, scale=2)
	  private BigDecimal hrgBeli;
	  
	  @Column(name="PROFIT_MD", length=20, precision=20)
	  private BigDecimal profitMd;
	  
	  @Column(name="BY_ANGKUT1", length=20, precision=20)
	  private BigDecimal byAngkut1;
	  
	  @Column(name="SALES_INSENTIF", length=20, precision=20)
	  private BigDecimal salesInsentif;
	  
	  @Column(name="BY_ANGKUT2", length=20, precision=20)
	  private BigDecimal byAngkut2;
	  
	  @Column(name="PROFIT_DLR", length=20, precision=20)
	  private BigDecimal profitDlr;
	  
	  @Column(name="HET_SBLM_PPN", length=20, precision=20)
	  private BigDecimal hetSblmPpn;
	  
	  @Column(name="PPN", length=20, precision=20)
	  private BigDecimal ppn;
	  
	  @Column(name="HET_PPN", length=20, precision=20)
	  private BigDecimal hetPpn;
	  
	  @Column(name="BBN_DLL", length=20, precision=20)
	  private BigDecimal bbnDll;
	  
	  @Column(name="HARGA_OTR", length=20, precision=20)
	  private BigDecimal hargaOtr;
	  
	  @Column(name="TAMB_CABANG", length=20, precision=20)
	  private BigDecimal tambCabang;
	  
	  @Column (name="AKSESORIS", length=20, precision=20)
	  private BigDecimal aksesoris;
	  
	  @Column(name="PRICELIST", length=20, precision=20)
	  private BigDecimal pricelist;
	
	  public HondaH100Hargas() {
	    
	  }
	
	  public HondaH100Hargas(String kdItem, String area) {
	    this.kdItem = kdItem;
	    this.area = area;
	  }
	
	  public BigInteger getDiscount() {
	    return this.discount;
	  }
	
	  public void setDiscount(BigInteger discount) {
	    this.discount = discount;
	  }
	
	  public BigDecimal getHarga() {
	    return this.harga;
	  }
	
	  public void setHarga(BigDecimal harga) {
	    this.harga = harga;
	  }
	
	  public BigDecimal getHargaStd() {
	    return this.hargaStd;
	  }
	
	  public void setHargaStd(BigDecimal hargaStd) {
	    this.hargaStd = hargaStd;
	  }
	
	  public BigDecimal getBiayaLain() {
	    return this.biayaLain;
	  }
	
	  public void setBiayaLain(BigDecimal biayaLain) {
	    this.biayaLain = biayaLain;
	  }
	
	  public BigDecimal getBiayaStnk() {
	    return this.biayaStnk;
	  }
	
	  public void setBiayaStnk(BigDecimal biayaStnk) {
	    this.biayaStnk = biayaStnk;
	  }

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public Integer getApprove() {
		return approve;
	}

	public void setApprove(Integer approve) {
		this.approve = approve;
	}

	public BigDecimal getHrgBeli() {
		return hrgBeli;
	}

	public void setHrgBeli(BigDecimal hrgBeli) {
		this.hrgBeli = hrgBeli;
	}

	public BigDecimal getProfitMd() {
		return profitMd;
	}

	public void setProfitMd(BigDecimal profitMd) {
		this.profitMd = profitMd;
	}

	public BigDecimal getByAngkut1() {
		return byAngkut1;
	}

	public void setByAngkut1(BigDecimal byAngkut1) {
		this.byAngkut1 = byAngkut1;
	}

	public BigDecimal getSalesInsentif() {
		return salesInsentif;
	}

	public void setSalesInsentif(BigDecimal salesInsentif) {
		this.salesInsentif = salesInsentif;
	}

	public BigDecimal getByAngkut2() {
		return byAngkut2;
	}

	public void setByAngkut2(BigDecimal byAngkut2) {
		this.byAngkut2 = byAngkut2;
	}

	public BigDecimal getProfitDlr() {
		return profitDlr;
	}

	public void setProfitDlr(BigDecimal profitDlr) {
		this.profitDlr = profitDlr;
	}


	public BigDecimal getHetSblmPpn() {
		return hetSblmPpn;
	}

	public void setHetSblmPpn(BigDecimal hetSblmPpn) {
		this.hetSblmPpn = hetSblmPpn;
	}

	public BigDecimal getPpn() {
		return ppn;
	}

	public void setPpn(BigDecimal ppn) {
		this.ppn = ppn;
	}

	public BigDecimal getHetPpn() {
		return hetPpn;
	}

	public void setHetPpn(BigDecimal hetPpn) {
		this.hetPpn = hetPpn;
	}

	public BigDecimal getBbnDll() {
		return bbnDll;
	}

	public void setBbnDll(BigDecimal bbnDll) {
		this.bbnDll = bbnDll;
	}

	public BigDecimal getHargaOtr() {
		return hargaOtr;
	}

	public void setHargaOtr(BigDecimal hargaOtr) {
		this.hargaOtr = hargaOtr;
	}

	public BigDecimal getTambCabang() {
		return tambCabang;
	}

	public void setTambCabang(BigDecimal tambCabang) {
		this.tambCabang = tambCabang;
	}

	public BigDecimal getAksesoris() {
		return aksesoris;
	}

	public void setAksesoris(BigDecimal aksesoris) {
		this.aksesoris = aksesoris;
	}

	public BigDecimal getPricelist() {
		return pricelist;
	}

	public void setPricelist(BigDecimal pricelist) {
		this.pricelist = pricelist;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Hargas other = (HondaH100Hargas) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Hargas [kdItem=" + kdItem + ", area=" + area + "]";
	}
  
}