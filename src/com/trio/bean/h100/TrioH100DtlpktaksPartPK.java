package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 10:40:12 AM Dec 20, 2013
 */

@Embeddable
public class TrioH100DtlpktaksPartPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="KD_PKT", length=12)
	private String kdPkt;
	
	@Column(name="PART_NO", length=25)
	private String partNo;
	
	public TrioH100DtlpktaksPartPK() {
		
	}

	public String getKdPkt() {
		return kdPkt;
	}

	public void setKdPkt(String kdPkt) {
		this.kdPkt = kdPkt;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kdPkt == null) ? 0 : kdPkt.hashCode());
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlpktaksPartPK other = (TrioH100DtlpktaksPartPK) obj;
		if (kdPkt == null) {
			if (other.kdPkt != null)
				return false;
		} else if (!kdPkt.equals(other.kdPkt))
			return false;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlpktaksPartPK [kdPkt=" + kdPkt + ", partNo=" + partNo
				+ "]";
	}
	
	
}
