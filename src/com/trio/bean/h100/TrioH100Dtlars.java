package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="TRIO_H100_DTLARS")
public class TrioH100Dtlars extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="NO_DO", length=30, nullable = false)
	private String noDo;
	
	@Column(name="JUMLAH" , nullable = false)
	private BigInteger jumlah;
	
	@Column(name="BENTUK_PEMBAYARAN", length=20 , nullable=false)
	private String bentukPembayaran;
	
	@Column(name="NAMA_BANK", length=30)
	private String namaBank;
	
	@Column(name="NO_CEK_BG", length=30)
	private String noCekBg;
	
	@Column(name="TGL_LUNAS", length=7, nullable = false)
	@Temporal(TemporalType.DATE)
	private Date tglLunas;
	
	@JoinColumn(name="NO_DO", referencedColumnName="NO_DO", insertable = false, updatable = false)
	@ManyToOne(fetch=FetchType.LAZY)
	private TrioH100Ars trioH100Ars;
	
	
	
	public TrioH100Dtlars() {

	}
	
	public String getNoDo() {
		return noDo;
	}

	public void setNoDo(String noDo) {
		this.noDo = noDo;
	}

	public BigInteger getJumlah() {
		return jumlah;
	}

	public void setJumlah(BigInteger jumlah) {
		this.jumlah = jumlah;
	}

	public String getBentukPembayaran() {
		return bentukPembayaran;
	}

	public void setBentukPembayaran(String bentukPembayaran) {
		this.bentukPembayaran = bentukPembayaran;
	}

	public String getNamaBank() {
		return namaBank;
	}

	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}

	public String getNoCekBg() {
		return noCekBg;
	}

	public void setNoCekBg(String noCekBg) {
		this.noCekBg = noCekBg;
	}

	public Date getTglLunas() {
		return tglLunas;
	}

	public void setTglLunas(Date tglLunas) {
		this.tglLunas = tglLunas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((noDo == null) ? 0 : noDo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Dtlars other = (TrioH100Dtlars) obj;
		if (noDo == null) {
			if (other.noDo != null)
				return false;
		} else if (!noDo.equals(other.noDo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100Dtlars [noDo=" + noDo + "]";
	}

	public TrioH100Ars getTrioH100Ars() {
		return trioH100Ars;
	}

	public void setTrioH100Ars(TrioH100Ars trioH100Ars) {
		this.trioH100Ars = trioH100Ars;
	}

}
