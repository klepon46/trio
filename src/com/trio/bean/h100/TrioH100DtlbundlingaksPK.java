package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 10:47:45 AM Dec 26, 2013
 */

@Embeddable
public class TrioH100DtlbundlingaksPK implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Column(name="NO_BUNDLING", length=16)
	private String noBundling;
	
	@Column(name="NO_MESIN", length=30)
	private String noMesin;
	
	@Column(name="PART_NO", length=25)
	private String partNo;
	
	public TrioH100DtlbundlingaksPK() {
		// TODO Auto-generated constructor stub
	}

	public String getNoBundling() {
		return noBundling;
	}

	public void setNoBundling(String noBundling) {
		this.noBundling = noBundling;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getPartNo() {
		return partNo;
	}

	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((noBundling == null) ? 0 : noBundling.hashCode());
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlbundlingaksPK other = (TrioH100DtlbundlingaksPK) obj;
		if (noBundling == null) {
			if (other.noBundling != null)
				return false;
		} else if (!noBundling.equals(other.noBundling))
			return false;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlbundlingaksPK [noBundling=" + noBundling
				+ ", noMesin=" + noMesin + ", partNo=" + partNo + "]";
	}

}
