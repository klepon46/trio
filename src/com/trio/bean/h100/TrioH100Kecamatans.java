/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trio.bean.h100;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 *
 * @author glassfish | Saipi Ramli
 */
@Entity
@Table(name="TRIO_H100_KECAMATANS")
public class TrioH100Kecamatans extends TrioEntityUserTrail implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name="KD_KEC" , length=10)
    private String kdKec;
    
    @Column(name="KD_KOTA", length=10) private String kdKota;
    
    @Column(name="PROPINSI", length=30) private String propinsi;
    
    @Column(name="KOTA", length=40) private String kota;
    
    @Column(name="KECAMATAN", length=40) private String kecamatan;
    
    @Column(name="STATUS", length=1) private String status;
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy="trioH100Kecamatans")
    private Set<TrioH100DtlAreaDealers> trioH100DtlAreaDealersSet; 
    
    public TrioH100Kecamatans() {
    	this.trioH100DtlAreaDealersSet = new HashSet<TrioH100DtlAreaDealers>();
    }

    
    public Set<TrioH100DtlAreaDealers> getTrioH100DtlAreaDealersSet() {
		return trioH100DtlAreaDealersSet;
	}


	public void setTrioH100DtlAreaDealersSet(
			Set<TrioH100DtlAreaDealers> trioH100DtlAreaDealersSet) {
		this.trioH100DtlAreaDealersSet = trioH100DtlAreaDealersSet;
	}


	public TrioH100Kecamatans(String kdKec) {
        this.kdKec = kdKec;
    }

    public String getKdKec() {
        return kdKec;
    }

    public void setKdKec(String kdKec) {
        this.kdKec = kdKec;
    }

    public String getKdKota() {
        return kdKota;
    }

    public void setKdKota(String kdKota) {
        this.kdKota = kdKota;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getPropinsi() {
        return propinsi;
    }

    public void setPropinsi(String propinsi) {
        this.propinsi = propinsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrioH100Kecamatans other = (TrioH100Kecamatans) obj;
        if ((this.kdKec == null) ? (other.kdKec != null) : !this.kdKec.equals(other.kdKec)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.kdKec != null ? this.kdKec.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "Kecamatan{" + "kdKec=" + kdKec + '}';
    }
}

