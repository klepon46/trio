package com.trio.bean.h100;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_HDRPICKLISTS")
public class HondaH100Hdrpicklists extends TrioEntityUserTrail implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name="VNOPICKLIST", nullable=false, length=30)
  private String vnopicklist;


  @Column(name="VKODELOKASI", length=20)
  private String vkodelokasi;

  @Column(name="VSTATUS", length=1)
  private String vstatus;

  @Column(name="VJENISPICK", length=1)
  private String vjenispick;

  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="hondaH100Hdrpicklists")
  private Set<HondaH100Dtlpicklists> hondaH100DtlpicklistsSet;

  @JoinColumn(name="VNODO", referencedColumnName="NO_DO", nullable=false)
  @ManyToOne(optional=false)
  private HondaH100Fakdos hondaH100Fakdos;

//  @JoinColumn(name="VNOMUTASI", referencedColumnName="NO_MUTASI")
//  @ManyToOne(optional=false)
//  private HondaH100Mstmutasis hondaH100Mstmutasis;

  public HondaH100Hdrpicklists() {
   
  }

  public HondaH100Hdrpicklists(String vnopicklist) {
    this.vnopicklist = vnopicklist;
  }

	public String getVnopicklist() {
		return vnopicklist;
	}
	
	public void setVnopicklist(String vnopicklist) {
		this.vnopicklist = vnopicklist;
	}
	
	public String getVkodelokasi() {
		return vkodelokasi;
	}
	
	public void setVkodelokasi(String vkodelokasi) {
		this.vkodelokasi = vkodelokasi;
	}
	
	public String getVstatus() {
		return vstatus;
	}
	
	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}
	
	public String getVjenispick() {
		return vjenispick;
	}
	
	public void setVjenispick(String vjenispick) {
		this.vjenispick = vjenispick;
	}
	
	public Set<HondaH100Dtlpicklists> getHondaH100DtlpicklistsSet() {
		return hondaH100DtlpicklistsSet;
	}
	
	public void setHondaH100DtlpicklistsSet(
			Set<HondaH100Dtlpicklists> hondaH100DtlpicklistsSet) {
		this.hondaH100DtlpicklistsSet = hondaH100DtlpicklistsSet;
	}
	
	public HondaH100Fakdos getHondaH100Fakdos() {
		return hondaH100Fakdos;
	}
	
	public void setHondaH100Fakdos(HondaH100Fakdos hondaH100Fakdos) {
		this.hondaH100Fakdos = hondaH100Fakdos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((vnopicklist == null) ? 0 : vnopicklist.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Hdrpicklists other = (HondaH100Hdrpicklists) obj;
		if (vnopicklist == null) {
			if (other.vnopicklist != null)
				return false;
		} else if (!vnopicklist.equals(other.vnopicklist))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Hdrpicklists [vnopicklist=" + vnopicklist + "]";
	}
  
}