/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author glassfish | Saipi Ramli
 */
@Embeddable
public class TrioH100DtlAreaDealersPK implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Column(name="KD_DLR", length=10) private String kdDlr;
    
    @Column(name="KD_KEC", length=10) private String kdKec;

    public TrioH100DtlAreaDealersPK() {
    }

    public TrioH100DtlAreaDealersPK(String kdDlr, String kdKec) {
        this.kdDlr = kdDlr;
        this.kdKec = kdKec;
    }

    public String getKdDlr() {
        return kdDlr;
    }

    public void setKdDlr(String kdDlr) {
        this.kdDlr = kdDlr;
    }

    public String getKdKec() {
        return kdKec;
    }

    public void setKdKec(String kdKec) {
        this.kdKec = kdKec;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TrioH100DtlAreaDealersPK other = (TrioH100DtlAreaDealersPK) obj;
        if ((this.kdDlr == null) ? (other.kdDlr != null) : !this.kdDlr.equals(other.kdDlr)) {
            return false;
        }
        if ((this.kdKec == null) ? (other.kdKec != null) : !this.kdKec.equals(other.kdKec)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + (this.kdDlr != null ? this.kdDlr.hashCode() : 0);
        hash = 23 * hash + (this.kdKec != null ? this.kdKec.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return "DtlAreaDealersPK{" + "kdDlr=" + kdDlr + ", kdKec=" + kdKec + '}';
    }

}
