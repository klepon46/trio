package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 11:02:35 AM Dec 20, 2013
 */

@Entity
@Table(name="TRIO_H100_DTLPKTAKS_ITEM")
public class TrioH100DtlpktaksItem extends TrioEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="KD_PKT", length=12)
	private String kdPkt;

	@Id
	@Column(name="KD_ITEM", length=25)
	private String kdItem;
	
	@Column(name="KD_ITEM_BARU", length=25)
	private String kdItemBaru;

	@ManyToOne
	@JoinColumn(name="KD_PKT", referencedColumnName="KD_PKT", insertable=false, updatable=false)
	private TrioH100Pktaks trioH100Pktaks;
	
	public TrioH100DtlpktaksItem() {
		// TODO Auto-generated constructor stub
	}

	public String getKdPkt() {
		return kdPkt;
	}

	public void setKdPkt(String kdPkt) {
		this.kdPkt = kdPkt;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	
	public String getKdItemBaru() {
		return kdItemBaru;
	}

	public void setKdItemBaru(String kdItemBaru) {
		this.kdItemBaru = kdItemBaru;
	}

	public TrioH100Pktaks getTrioH100Pktaks() {
		return trioH100Pktaks;
	}

	public void setTrioH100Pktaks(TrioH100Pktaks trioH100Pktaks) {
		this.trioH100Pktaks = trioH100Pktaks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		result = prime * result + ((kdPkt == null) ? 0 : kdPkt.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlpktaksItem other = (TrioH100DtlpktaksItem) obj;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		if (kdPkt == null) {
			if (other.kdPkt != null)
				return false;
		} else if (!kdPkt.equals(other.kdPkt))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlpktaksItem [kdPkt=" + kdPkt + ", kdItem=" + kdItem
				+ "]";
	}
	
}
