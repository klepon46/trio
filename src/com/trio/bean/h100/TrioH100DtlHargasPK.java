package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author Gusti Arya 10:22:38 AM Dec 10, 2013
 */

@Embeddable
public class TrioH100DtlHargasPK implements Serializable{

	private static final long serialVersionUID = 1L;

	@Column(name="AREA")
	private String area;
	
	@Column(name="KD_TIPE")
	private String kdTipe;
	
	@Column(name="KD_PKT_AKS")
	private String kdPktAks;
	
	public TrioH100DtlHargasPK() {
		// TODO Auto-generated constructor stub
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getKdTipe() {
		return kdTipe;
	}

	public void setKdTipe(String kdTipe) {
		this.kdTipe = kdTipe;
	}

	public String getKdPktAks() {
		return kdPktAks;
	}

	public void setKdPktAks(String kdPktAks) {
		this.kdPktAks = kdPktAks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((area == null) ? 0 : area.hashCode());
		result = prime * result
				+ ((kdPktAks == null) ? 0 : kdPktAks.hashCode());
		result = prime * result + ((kdTipe == null) ? 0 : kdTipe.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlHargasPK other = (TrioH100DtlHargasPK) obj;
		if (area == null) {
			if (other.area != null)
				return false;
		} else if (!area.equals(other.area))
			return false;
		if (kdPktAks == null) {
			if (other.kdPktAks != null)
				return false;
		} else if (!kdPktAks.equals(other.kdPktAks))
			return false;
		if (kdTipe == null) {
			if (other.kdTipe != null)
				return false;
		} else if (!kdTipe.equals(other.kdTipe))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlHargasPK [area=" + area + ", kdTipe=" + kdTipe
				+ ", kdPktAks=" + kdPktAks + "]";
	}
	
	
}
