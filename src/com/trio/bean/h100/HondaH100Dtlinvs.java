package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioEntityUserTrail;



@Entity
@Table(name="HONDA_H100_DTLINVS")
public class HondaH100Dtlinvs extends TrioEntityUserTrail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private HondaH100DtlinvsPK hondaH100DtlinvsPK;
	
	@Column(name="QTY", length=4, precision=4)
	private BigInteger qty;
	
	@Column(name="AMT_POKOK", length=22, precision=20, scale=2)
	private BigDecimal amtPokok;
	
	@Column(name="AMT_PPN", length=22, precision=20, scale=2)
	private BigDecimal amtPpn;
	
	@Column(name="AMT_PPH", length=22, precision=20, scale=2)
	private BigDecimal amtPph;
	
	@Column(name="DISC_Q", length=22, precision=20, scale=2)
	private BigDecimal discQ;
	
	@Column(name="DISC_C", length=22, precision=20, scale=2)
	private BigDecimal discC;
	
	@Column(name="DISC_O", length=22, precision=20, scale=2)
	private BigDecimal discO;
	
	@Column(name="CREA_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date creaDate;
	
	public HondaH100Dtlinvs() {
		// TODO Auto-generated constructor stub
	}

	public HondaH100DtlinvsPK getHondaH100DtlinvsPK() {
		return hondaH100DtlinvsPK;
	}

	public void setHondaH100DtlinvsPK(HondaH100DtlinvsPK hondaH100DtlinvsPK) {
		this.hondaH100DtlinvsPK = hondaH100DtlinvsPK;
	}

	
	public BigInteger getQty() {
		return qty;
	}

	public void setQty(BigInteger qty) {
		this.qty = qty;
	}

	public BigDecimal getAmtPokok() {
		return amtPokok;
	}

	public void setAmtPokok(BigDecimal amtPokok) {
		this.amtPokok = amtPokok;
	}

	public BigDecimal getAmtPpn() {
		return amtPpn;
	}

	public void setAmtPpn(BigDecimal amtPpn) {
		this.amtPpn = amtPpn;
	}

	public BigDecimal getAmtPph() {
		return amtPph;
	}

	public void setAmtPph(BigDecimal amtPph) {
		this.amtPph = amtPph;
	}

	public BigDecimal getDiscQ() {
		return discQ;
	}

	public void setDiscQ(BigDecimal discQ) {
		this.discQ = discQ;
	}

	public BigDecimal getDiscC() {
		return discC;
	}

	public void setDiscC(BigDecimal discC) {
		this.discC = discC;
	}

	public BigDecimal getDiscO() {
		return discO;
	}

	public void setDiscO(BigDecimal discO) {
		this.discO = discO;
	}
	
	public Date getCreaDate() {
		return creaDate;
	}

	public void setCreaDate(Date creaDate) {
		this.creaDate = creaDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((hondaH100DtlinvsPK == null) ? 0 : hondaH100DtlinvsPK
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Dtlinvs other = (HondaH100Dtlinvs) obj;
		if (hondaH100DtlinvsPK == null) {
			if (other.hondaH100DtlinvsPK != null)
				return false;
		} else if (!hondaH100DtlinvsPK.equals(other.hondaH100DtlinvsPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Dtlinvs [hondaH100DtlinvsPK=" + hondaH100DtlinvsPK
				+ "]";
	}

}
