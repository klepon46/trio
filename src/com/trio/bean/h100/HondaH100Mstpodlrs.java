package com.trio.bean.h100;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.bean.h000.HondaH000Dealers;
import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_MSTPODLRS")
public class HondaH100Mstpodlrs extends TrioEntityUserTrail implements Serializable {
  
		private static final long serialVersionUID = 1L;
	
		@Id
		@Column(name="NO_POINT", nullable=false, length=30)
		private String noPoint;

	  @Column(name="RUNNO", length=5)
	  private String runno;
	
	  @Column(name="BULAN", length=2)
	  private String bulan;
	
	  @Column(name="NO_POAHM", length=30)
	  private String noPoahm;
	
	  @Column(name="NO_PODLR", length=30)
	  private String noPodlr;
	
	  @Column(name="TGL_PO")
	  @Temporal(TemporalType.DATE)
	  private Date tglPo;
	
	  @Column(name="TAHUN", length=4)
	  private String tahun;
	
	  @Column(name="STATUS", length=1)
	  private String status;
	  
	  @Column(name="DEALER_KD_DLR")
	  private String kdDlr;
	
	  @Column(name="JENIS_PO", length=1)
	  private String jenisPo;
	
	  @Column(name="VALID_PO", length=1)
	  private String validPo;
	
	  @Column(name="TGL_AWAL")
	  @Temporal(TemporalType.DATE)
	  private Date tglAwal;
	
	  @Column(name="TGL_AKHIR")
	  @Temporal(TemporalType.DATE)
	  private Date tglAkhir;
	
	  @Column(name="FLAG_APPR", length=1)
	  private String flagAppr;
	  
	  @Column(name="VNAMA", length=100)
	  private String vnama;
	
	  @JoinColumn(name="DEALER_KD_DLR", referencedColumnName="KD_DLR", insertable=false, updatable=false)
	  @ManyToOne
	  private HondaH000Dealers dealerKdDlr;
	
	  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="hondaH100Mstpodlrs")
	  private Set<HondaH100Dtlpodlrfixes> hondaH100DtlpodlrfixesSet; 
	  
	  @OneToMany(mappedBy="podlrNoPoint")
	  private Set<HondaH100Fakdos> hondaH100FakdosSet;
	  
	  public HondaH100Mstpodlrs( ) {
		  this.hondaH100DtlpodlrfixesSet = new HashSet<HondaH100Dtlpodlrfixes>();
		  this.hondaH100FakdosSet = new HashSet<HondaH100Fakdos>();
	  }
	  
	  public HondaH100Mstpodlrs(String noPoint) {
		  this.noPoint = noPoint;
	  }

	public String getNoPoint() {
		return noPoint;
	}

	public void setNoPoint(String noPoint) {
		this.noPoint = noPoint;
	}

	public String getRunno() {
		return runno;
	}

	public void setRunno(String runno) {
		this.runno = runno;
	}

	public String getBulan() {
		return bulan;
	}

	public void setBulan(String bulan) {
		this.bulan = bulan;
	}

	public String getNoPoahm() {
		return noPoahm;
	}

	public void setNoPoahm(String noPoahm) {
		this.noPoahm = noPoahm;
	}

	public String getNoPodlr() {
		return noPodlr;
	}

	public void setNoPodlr(String noPodlr) {
		this.noPodlr = noPodlr;
	}

	public Date getTglPo() {
		return tglPo;
	}

	public void setTglPo(Date tglPo) {
		this.tglPo = tglPo;
	}

	public String getTahun() {
		return tahun;
	}

	public void setTahun(String tahun) {
		this.tahun = tahun;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getKdDlr() {
		return kdDlr;
	}

	public void setKdDlr(String kdDlr) {
		this.kdDlr = kdDlr;
	}

	public String getJenisPo() {
		return jenisPo;
	}

	public void setJenisPo(String jenisPo) {
		this.jenisPo = jenisPo;
	}

	public String getValidPo() {
		return validPo;
	}

	public void setValidPo(String validPo) {
		this.validPo = validPo;
	}

	public Date getTglAwal() {
		return tglAwal;
	}

	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}

	public Date getTglAkhir() {
		return tglAkhir;
	}

	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}

	public String getFlagAppr() {
		return flagAppr;
	}

	public void setFlagAppr(String flagAppr) {
		this.flagAppr = flagAppr;
	}

	public String getVnama() {
		return vnama;
	}

	public void setVnama(String vnama) {
		this.vnama = vnama;
	}

	public HondaH000Dealers getDealerKdDlr() {
		return dealerKdDlr;
	}

	public void setDealerKdDlr(HondaH000Dealers dealerKdDlr) {
		this.dealerKdDlr = dealerKdDlr;
	}

	public Set<HondaH100Dtlpodlrfixes> getHondaH100DtlpodlrfixesSet() {
		return hondaH100DtlpodlrfixesSet;
	}

	public void setHondaH100DtlpodlrfixesSet(
			Set<HondaH100Dtlpodlrfixes> hondaH100DtlpodlrfixesSet) {
		this.hondaH100DtlpodlrfixesSet = hondaH100DtlpodlrfixesSet;
	}

	public Set<HondaH100Fakdos> getHondaH100FakdosSet() {
		return hondaH100FakdosSet;
	}

	public void setHondaH100FakdosSet(Set<HondaH100Fakdos> hondaH100FakdosSet) {
		this.hondaH100FakdosSet = hondaH100FakdosSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((noPoint == null) ? 0 : noPoint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Mstpodlrs other = (HondaH100Mstpodlrs) obj;
		if (noPoint == null) {
			if (other.noPoint != null)
				return false;
		} else if (!noPoint.equals(other.noPoint))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Mstpodlrs [noPoint=" + noPoint + "]";
	}
	
}
