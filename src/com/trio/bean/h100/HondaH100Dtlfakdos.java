package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.bean.h000.HondaH000Items;
import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_DTLFAKDOS")
public class HondaH100Dtlfakdos extends TrioEntityUserTrail implements Serializable
{
  private static final long serialVersionUID = 1L;

  @Id
  @Basic(optional=false)
  @Column(name="KD_ITEM", nullable=false, length=25)
  private String kdItem;

  @Id
  @Basic(optional=false)
  @Column(name="FAKDO_NO_DO", nullable=false, length=30)
  private String fakdoNoDo;

  @Column(name="HARGA_KOSONG", precision=20, scale=2)
  private BigDecimal hargaKosong;

  @Column(name="HARGA_STD", precision=20, scale=2)
  private BigDecimal hargaStd;

  @Column(name="DISCOUNT_PERSEN", precision=8, scale=3)
  private BigDecimal discountPersen;

  @Column(name="QTY")
  private Long qty;

  @Column(name="HARGA_NETTO", precision=20, scale=2)
  private BigDecimal hargaNetto;

  @Column(name="DISCOUNT", precision=20, scale=2)
  private BigDecimal discount;

  @Column(name="BIAYA_LAIN", precision=20, scale=2)
  private BigDecimal biayaLain;

  @Column(name="BIAYA_STNK", precision=20, scale=2)
  private BigDecimal biayaStnk;

  @Column(name="QTY_SISA")
  private Long qtySisa;

  @Column(name="QTY_JUAL")
  private Long qtyJual;

  @Column(name="NQTYPICK")
  private Long nqtypick;

  @Column(name="NQTYLSTPICK", length=10)
  private Long nqtylstpick;

  @JoinColumn(name="FAKDO_NO_DO", referencedColumnName="NO_DO", insertable=false, updatable=false)
  @ManyToOne
  private HondaH100Fakdos hondaH100Fakdos;

  @JoinColumn(name="KD_ITEM", referencedColumnName="KD_ITEM", nullable=false, insertable=false, updatable=false)
  @ManyToOne(optional=false)
  private HondaH000Items hondaH000Items;

  public HondaH100Dtlfakdos() {
    
  }

  public HondaH100Dtlfakdos(String kdItem, String fakdoNoDo) {
	  this.fakdoNoDo = fakdoNoDo;
	  this.kdItem = kdItem;
    
  }

	public String getKdItem() {
		return kdItem;
	}
	
	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}
	
	public String getFakdoNoDo() {
		return fakdoNoDo;
	}
	
	public void setFakdoNoDo(String fakdoNoDo) {
		this.fakdoNoDo = fakdoNoDo;
	}
	
	public BigDecimal getHargaKosong() {
		return hargaKosong;
	}
	
	public void setHargaKosong(BigDecimal hargaKosong) {
		this.hargaKosong = hargaKosong;
	}
	
	public BigDecimal getHargaStd() {
		return hargaStd;
	}
	
	public void setHargaStd(BigDecimal hargaStd) {
		this.hargaStd = hargaStd;
	}
	
	public BigDecimal getDiscountPersen() {
		return discountPersen;
	}
	
	public void setDiscountPersen(BigDecimal discountPersen) {
		this.discountPersen = discountPersen;
	}
	
	public Long getQty() {
		return qty;
	}
	
	public void setQty(Long qty) {
		this.qty = qty;
	}
	
	public BigDecimal getHargaNetto() {
		return hargaNetto;
	}
	
	public void setHargaNetto(BigDecimal hargaNetto) {
		this.hargaNetto = hargaNetto;
	}
	
	public BigDecimal getDiscount() {
		return discount;
	}
	
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public BigDecimal getBiayaLain() {
		return biayaLain;
	}
	
	public void setBiayaLain(BigDecimal biayaLain) {
		this.biayaLain = biayaLain;
	}
	
	public BigDecimal getBiayaStnk() {
		return biayaStnk;
	}
	
	public void setBiayaStnk(BigDecimal biayaStnk) {
		this.biayaStnk = biayaStnk;
	}
	
	public Long getQtySisa() {
		return qtySisa;
	}
	
	public void setQtySisa(Long qtySisa) {
		this.qtySisa = qtySisa;
	}
	
	public Long getQtyJual() {
		return qtyJual;
	}
	
	public void setQtyJual(Long qtyJual) {
		this.qtyJual = qtyJual;
	}
	
	public Long getNqtypick() {
		return nqtypick;
	}
	
	public void setNqtypick(Long nqtypick) {
		this.nqtypick = nqtypick;
	}
	
	public Long getNqtylstpick() {
		return nqtylstpick;
	}
	
	public void setNqtylstpick(Long nqtylstpick) {
		this.nqtylstpick = nqtylstpick;
	}
	
	public HondaH100Fakdos getHondaH100Fakdos() {
		return hondaH100Fakdos;
	}
	
	public void setHondaH100Fakdos(HondaH100Fakdos hondaH100Fakdos) {
		this.hondaH100Fakdos = hondaH100Fakdos;
	}
	
	public HondaH000Items getHondaH000Items() {
		return hondaH000Items;
	}
	
	public void setHondaH000Items(HondaH000Items hondaH000Items) {
		this.hondaH000Items = hondaH000Items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((fakdoNoDo == null) ? 0 : fakdoNoDo.hashCode());
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Dtlfakdos other = (HondaH100Dtlfakdos) obj;
		if (fakdoNoDo == null) {
			if (other.fakdoNoDo != null)
				return false;
		} else if (!fakdoNoDo.equals(other.fakdoNoDo))
			return false;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Dtlfakdos [kdItem=" + kdItem + ", fakdoNoDo="
				+ fakdoNoDo + "]";
	}
	
}