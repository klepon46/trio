package com.trio.bean.h100;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
@Entity
@Table(name="HONDA_H100_ARS")
public class HondaH100Ars extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_DO", length=30, nullable=false)
	private String noDo;

	@Column(name="AMT_PPN", length=20)
	private BigInteger amtPPN;

	@Column(name="TOP_POKOK")
	@Temporal(TemporalType.TIMESTAMP)
	private Date topPokok;

	@Column(name="STATUS_POKOK", length=1)
	private String statusPokok;

	@Column(name="STATUS_PPN", length=1)
	private String statusPPN;

	@Column(name="DOKREF_POKOK", length=30)
	private String dokRefPokokl;

	@Column(name="DOKREF_PPN", length=30)
	private String dokRefPPN;

	@Column(name="AMT_POKOK", length=20)
	private BigInteger amtPokok;

	@Column(name="TGL_DO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglDo;

	@Column(name="TOP_PPN")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglPPN;

	
	@OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="hondaH100Ars")
	@BatchSize(size=10)
	private HondaH100Fakdos hondaH100Fakdos;
	

	public HondaH100Ars() {
		// TODO Auto-generated constructor stub
	}

	public HondaH100Fakdos getHondaH100Fakdos() {
		return hondaH100Fakdos;
	}

	public void setHondaH100Fakdos(HondaH100Fakdos hondaH100Fakdos) {
		this.hondaH100Fakdos = hondaH100Fakdos;
	}

	public HondaH100Ars(String noDo) {
		this.noDo = noDo; 
	}

	public String getNoDo() {
		return noDo;
	}

	public void setNoDo(String noDo) {
		this.noDo = noDo;
	}

	public BigInteger getAmtPPN() {
		return amtPPN;
	}

	public void setAmtPPN(BigInteger amtPPN) {
		this.amtPPN = amtPPN;
	}

	public Date getTopPokok() {
		return topPokok;
	}

	public void setTopPokok(Date topPokok) {
		this.topPokok = topPokok;
	}

	public String getStatusPokok() {
		return statusPokok;
	}

	public void setStatusPokok(String statusPokok) {
		this.statusPokok = statusPokok;
	}

	public String getStatusPPN() {
		return statusPPN;
	}

	public void setStatusPPN(String statusPPN) {
		this.statusPPN = statusPPN;
	}

	public String getDokRefPokokl() {
		return dokRefPokokl;
	}

	public void setDokRefPokokl(String dokRefPokokl) {
		this.dokRefPokokl = dokRefPokokl;
	}

	public String getDokRefPPN() {
		return dokRefPPN;
	}

	public void setDokRefPPN(String dokRefPPN) {
		this.dokRefPPN = dokRefPPN;
	}

	public BigInteger getAmtPokok() {
		return amtPokok;
	}

	public void setAmtPokok(BigInteger amtPokok) {
		this.amtPokok = amtPokok;
	}

	public Date getTglDo() {
		return tglDo;
	}

	public void setTglDo(Date tglDo) {
		this.tglDo = tglDo;
	}

	public Date getTglPPN() {
		return tglPPN;
	}

	public void setTglPPN(Date tglPPN) {
		this.tglPPN = tglPPN;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((noDo == null) ? 0 : noDo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Ars other = (HondaH100Ars) obj;
		if (noDo == null) {
			if (other.noDo != null)
				return false;
		} else if (!noDo.equals(other.noDo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Ars [noDo=" + noDo + "]";
	}

}
