package com.trio.bean.h100;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_GUDANGS")
public class HondaH100Gudangs extends TrioEntityUserTrail  implements Serializable {
	  
	 private static final long serialVersionUID = 1L;
	
	  @Id
	  @Column(name="KODE_LOKASI", nullable=false, length=10)
	  private String kodeLokasi;
	
	  @Column(name="JUMLAH_RAK")
	  private Integer jumlahRak;
	
	  @Column(name="ALAMAT_LOKASI", length=50)
	  private String alamatLokasi;
	
	  @Column(name="BUSINESS", length=5)
	  private String business;
	
	  @Column(name="STATUS", length=1)
	  private String status;
	
	  @Column(name="VKETLOKASI", length=50)
	  private String vketlokasi;
	
	  @Column(name="VALIASLOK", length=6)
	  private String valiaslok;
	
	  @Column(name="NQTYBOOK", length=10)
	  private Integer nqtybook;
	
	  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="hondaH100Gudangs")
	  private Set<HondaH100Hdrsublokasi> hondaH100HdrsublokasiSet; 
	  
	//  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="lokasiAsal")
	//  @MapKey(name="hondaH100MstmutasisPK")
	//  private Map<HondaH100MstmutasisPK, HondaH100Mstmutasis> hondaH100MstMutasisCollectionAsal;
	
	//  @OneToMany(cascade={javax.persistence.CascadeType.ALL}, mappedBy="tujuan")
	//  @MapKey(name="hondaH100MstmutasisPK")
	//  private Map<HondaH100MstmutasisPK, HondaH100Mstmutasis> hondaH100MstMutasisCollectionTujuan;
	
	  public HondaH100Gudangs()  {
	    this.hondaH100HdrsublokasiSet = new HashSet<HondaH100Hdrsublokasi>();
	  }
	
	  public HondaH100Gudangs(String kodeLokasi) {
	    this.kodeLokasi = kodeLokasi;
	  }

	public String getKodeLokasi() {
		return kodeLokasi;
	}

	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}

	public Integer getJumlahRak() {
		return jumlahRak;
	}

	public void setJumlahRak(Integer jumlahRak) {
		this.jumlahRak = jumlahRak;
	}

	public String getAlamatLokasi() {
		return alamatLokasi;
	}

	public void setAlamatLokasi(String alamatLokasi) {
		this.alamatLokasi = alamatLokasi;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVketlokasi() {
		return vketlokasi;
	}

	public void setVketlokasi(String vketlokasi) {
		this.vketlokasi = vketlokasi;
	}

	public String getValiaslok() {
		return valiaslok;
	}

	public void setValiaslok(String valiaslok) {
		this.valiaslok = valiaslok;
	}

	public Integer getNqtybook() {
		return nqtybook;
	}

	public void setNqtybook(Integer nqtybook) {
		this.nqtybook = nqtybook;
	}

	public Set<HondaH100Hdrsublokasi> getHondaH100HdrsublokasiSet() {
		return hondaH100HdrsublokasiSet;
	}

	public void setHondaH100HdrsublokasiSet(
			Set<HondaH100Hdrsublokasi> hondaH100HdrsublokasiSet) {
		this.hondaH100HdrsublokasiSet = hondaH100HdrsublokasiSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((kodeLokasi == null) ? 0 : kodeLokasi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Gudangs other = (HondaH100Gudangs) obj;
		if (kodeLokasi == null) {
			if (other.kodeLokasi != null)
				return false;
		} else if (!kodeLokasi.equals(other.kodeLokasi))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Gudangs [kodeLokasi=" + kodeLokasi + "]";
	}
	  
}
