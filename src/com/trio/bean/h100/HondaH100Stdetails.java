package com.trio.bean.h100;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.bean.h000.HondaH000Items;
import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_STDETAILS")
public class HondaH100Stdetails extends TrioEntityUserTrail implements Serializable {
  
	private static final long serialVersionUID = 1L;
	
	  @Id
	  @Basic(optional=false)
	  @Column(name="LOKASI", nullable=false, length=10)
	  private String lokasi;

	  @Id
	  @Basic(optional=false)
	  @Column(name="KD_ITEM", nullable=false, length=25)
	  private String kdItem;

	  @Id
	  @Basic(optional=false)
	  @Column(name="NO_MESIN", nullable=false, length=30)
	  private String noMesin;
  
	  @Column(name="NO_PPUD", length=30)
	  private String noPpud;
	
	  @Column(name="NO_FAKSTNK", length=30)
	  private String noFakstnk;
	
	  @Column(name="STATUS", length=1)
	  private String status;
	
	  @Column(name="NAMA_KAPAL", length=30)
	  private String namaKapal;
	
	  @Column(name="NO_RANGKA", length=30)
	  private String noRangka;
	
	  @Column(name="TH_PRODUKSI")
	  private Short thProduksi;
	
	  @Column(name="STAT_STOCK", length=1)
	  private String statStock;
	
	  @Column(name="VNOPICKLIST", length=30)
	  private String vnopicklist;
	
	  @Column(name="VSUBLOKASI", length=6)
	  private String vsublokasi;
	
	  @Column(name="VNOSPGTEMP", length=30)
	  private String vnospgtemp;
	
	  @Column(name="DSTARTRECV")
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dstartrecv;
	
	  @Column(name="DENDRECV")
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dendrecv;
	
	  @Column(name="DSTARTPICK")
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dstartpick;
	
	  @Column(name="DENDPICK")
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dendpick;
	
	  @Column(name="DSTARTSHP")
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dstartshp;
	
	  @Column(name="DENDSHP")
	  @Temporal(TemporalType.TIMESTAMP)
	  private Date dendshp;
	
	  @Column(name="VSTATPICK", length=1)
	  private String vstatpick;
	
	  @Column(name="VSEL", length=6)
	  private String vsel;
	
	  @JoinColumn(name="KD_ITEM", referencedColumnName="KD_ITEM", nullable=false, insertable=false, updatable=false)
	  @ManyToOne(optional=false)
	  private HondaH000Items hondaH000Items;
	
	  public HondaH100Stdetails()  {
	    
	  }
	  
	  public HondaH100Stdetails(String lokasi, String kdItem, String noMesin) {
	    this.lokasi = lokasi;
	    this.kdItem = kdItem;
	    this.noMesin = noMesin;
	  }
	  
	  
	  
	public HondaH100Stdetails(String noMesin,String noRangka, String lokasi, String kdItem) {
		this.lokasi = lokasi;
		this.kdItem = kdItem;
		this.noMesin = noMesin;
		this.noRangka = noRangka;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getKdItem() {
		return kdItem;
	}

	public void setKdItem(String kdItem) {
		this.kdItem = kdItem;
	}

	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoPpud() {
		return noPpud;
	}

	public void setNoPpud(String noPpud) {
		this.noPpud = noPpud;
	}

	public String getNoFakstnk() {
		return noFakstnk;
	}

	public void setNoFakstnk(String noFakstnk) {
		this.noFakstnk = noFakstnk;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNamaKapal() {
		return namaKapal;
	}

	public void setNamaKapal(String namaKapal) {
		this.namaKapal = namaKapal;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public Short getThProduksi() {
		return thProduksi;
	}

	public void setThProduksi(Short thProduksi) {
		this.thProduksi = thProduksi;
	}

	public String getStatStock() {
		return statStock;
	}

	public void setStatStock(String statStock) {
		this.statStock = statStock;
	}

	public String getVnopicklist() {
		return vnopicklist;
	}

	public void setVnopicklist(String vnopicklist) {
		this.vnopicklist = vnopicklist;
	}

	public String getVsublokasi() {
		return vsublokasi;
	}

	public void setVsublokasi(String vsublokasi) {
		this.vsublokasi = vsublokasi;
	}

	public String getVnospgtemp() {
		return vnospgtemp;
	}

	public void setVnospgtemp(String vnospgtemp) {
		this.vnospgtemp = vnospgtemp;
	}

	public Date getDstartrecv() {
		return dstartrecv;
	}

	public void setDstartrecv(Date dstartrecv) {
		this.dstartrecv = dstartrecv;
	}

	public Date getDendrecv() {
		return dendrecv;
	}

	public void setDendrecv(Date dendrecv) {
		this.dendrecv = dendrecv;
	}

	public Date getDstartpick() {
		return dstartpick;
	}

	public void setDstartpick(Date dstartpick) {
		this.dstartpick = dstartpick;
	}

	public Date getDendpick() {
		return dendpick;
	}

	public void setDendpick(Date dendpick) {
		this.dendpick = dendpick;
	}

	public Date getDstartshp() {
		return dstartshp;
	}

	public void setDstartshp(Date dstartshp) {
		this.dstartshp = dstartshp;
	}

	public Date getDendshp() {
		return dendshp;
	}

	public void setDendshp(Date dendshp) {
		this.dendshp = dendshp;
	}

	public String getVstatpick() {
		return vstatpick;
	}

	public void setVstatpick(String vstatpick) {
		this.vstatpick = vstatpick;
	}

	public String getVsel() {
		return vsel;
	}

	public void setVsel(String vsel) {
		this.vsel = vsel;
	}

	public HondaH000Items getHondaH000Items() {
		return hondaH000Items;
	}

	public void setHondaH000Items(HondaH000Items hondaH000Items) {
		this.hondaH000Items = hondaH000Items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((kdItem == null) ? 0 : kdItem.hashCode());
		result = prime * result + ((lokasi == null) ? 0 : lokasi.hashCode());
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Stdetails other = (HondaH100Stdetails) obj;
		if (kdItem == null) {
			if (other.kdItem != null)
				return false;
		} else if (!kdItem.equals(other.kdItem))
			return false;
		if (lokasi == null) {
			if (other.lokasi != null)
				return false;
		} else if (!lokasi.equals(other.lokasi))
			return false;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH100Stdetails [lokasi=" + lokasi + ", kdItem=" + kdItem
				+ ", noMesin=" + noMesin + "]";
	}
	  
}
