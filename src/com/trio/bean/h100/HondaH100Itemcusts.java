package com.trio.bean.h100;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioEntityUserTrail;

@Entity
@Table(name="HONDA_H100_ITEMCUSTS")
public class HondaH100Itemcusts extends TrioEntityUserTrail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "NO_MESIN", length=30, nullable = false)
	private String noMesin;
	
//	@Id
//	@Column(name = "CUST_KONSUMEN_ID", length=10, nullable=false)
//	private String custKonsumenId;
	
	@Column(name="JAMINAN", length=50)
	private String jaminan;
	
	@Column(name="TGl_RESI")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglResi;
	
	@Column(name="KETERANGAN", length=100)
	private String keterangan;
	
	@Column(name="NAMA_STNK", length=50)
	private String namaStnk;
	
	@Column(name="NO_RESI", length=20)
	private String noResi;
	
	@Column(name="NO_STNK", length=30)
	private String noStnk;
	
	@Column(name = "STATUS_JAM", length=1)
	private String statusJam;
	
	@Column(name="TGL_MOHON_STNK")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglMohonStnk;
	
	@Column(name="TGL_KLR_JAM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglKlrJam;
	
	@Column(name="PENGAMBIL_STNK", length=30)
	private String pengambilStnk;
	
	@Column(name="PENGAMBIL_JAM", length=50)
	private String pengambilJam;
	
	@Column(name="NO_BPKB", length=30)
	private String noBpkb;
	
	@Column(name="TGL_BLHAMBIL_JAM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglBlhambilJam;
	
	@Column(name="PENGURUS_JAM", length=50)
	@Temporal(TemporalType.TIMESTAMP)
	private Date pengurusJam;
	
	@Column(name="PENGAMBIL_BPKB", length=30)
	private String pengambilBpkb;
	
	@Column(name="TGL_JADI_STNK")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglJadiStnk;
	
	@Column(name="NO_RANGKA", length=30)
	private String noRangka;
	
	@Column(name="TGL_MSK_JAM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglMskJam;
	
	@Column(name="PENGURUS_BPKB", length=30)
	private String pengurusBpkb;
	
	@Column(name="TGL_TERIMA_BPKB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date  tglTerimaBpkb;
	
	@Column(name="NAMA_BPKB", length=50)
	private String namaBpkb;
	
	@Column(name="NO_POLISI", length=10)
	private String noPolisi;
	
	@Column(name="STATUS", length=1)
	private String status;
	
	@Column(name="TGL_AMBIL_STNK")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglAmbilStnk;
	
	@Column(name="TGL_AMBIL_BPKB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tglAmbilBpkb;
	
	@Column(name="ITEM_KD_ITEM", length=25)
	private String itemKdItem;
	
	@Column(name="ETD_STNK")
	@Temporal(TemporalType.TIMESTAMP)
	private Date etdStnk;
	
	@Column(name="ETD_BPKB")
	@Temporal(TemporalType.TIMESTAMP)
	private Date etdBpkb;
	
	@Column(name="VKDSALES", length=10)
	private String vkdsales;
	
	@Column(name="DTGLBELI")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dtglbeli;
	
	@Column(name="VKDDLR", length=5)
	private String vkddlr;
	
	@Column(name="VNMSALES", length=50)
	private String vnmsales;

	
	public HondaH100Itemcusts() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getNoMesin() {
		return noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getJaminan() {
		return jaminan;
	}

	public void setJaminan(String jaminan) {
		this.jaminan = jaminan;
	}

	public Date getTglResi() {
		return tglResi;
	}

	public void setTglResi(Date tglResi) {
		this.tglResi = tglResi;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getNamaStnk() {
		return namaStnk;
	}

	public void setNamaStnk(String namaStnk) {
		this.namaStnk = namaStnk;
	}

	public String getNoResi() {
		return noResi;
	}

	public void setNoResi(String noResi) {
		this.noResi = noResi;
	}

	public String getNoStnk() {
		return noStnk;
	}

	public void setNoStnk(String noStnk) {
		this.noStnk = noStnk;
	}

	public String getStatusJam() {
		return statusJam;
	}

	public void setStatusJam(String statusJam) {
		this.statusJam = statusJam;
	}

	public Date getTglMohonStnk() {
		return tglMohonStnk;
	}

	public void setTglMohonStnk(Date tglMohonStnk) {
		this.tglMohonStnk = tglMohonStnk;
	}

	public Date getTglKlrJam() {
		return tglKlrJam;
	}

	public void setTglKlrJam(Date tglKlrJam) {
		this.tglKlrJam = tglKlrJam;
	}

	public String getPengambilStnk() {
		return pengambilStnk;
	}

	public void setPengambilStnk(String pengambilStnk) {
		this.pengambilStnk = pengambilStnk;
	}

	public String getPengambilJam() {
		return pengambilJam;
	}

	public void setPengambilJam(String pengambilJam) {
		this.pengambilJam = pengambilJam;
	}

	public String getNoBpkb() {
		return noBpkb;
	}

	public void setNoBpkb(String noBpkb) {
		this.noBpkb = noBpkb;
	}

	public Date getTglBlhambilJam() {
		return tglBlhambilJam;
	}

	public void setTglBlhambilJam(Date tglBlhambilJam) {
		this.tglBlhambilJam = tglBlhambilJam;
	}

	public Date getPengurusJam() {
		return pengurusJam;
	}

	public void setPengurusJam(Date pengurusJam) {
		this.pengurusJam = pengurusJam;
	}

	public String getPengambilBpkb() {
		return pengambilBpkb;
	}

	public void setPengambilBpkb(String pengambilBpkb) {
		this.pengambilBpkb = pengambilBpkb;
	}

	public Date getTglJadiStnk() {
		return tglJadiStnk;
	}

	public void setTglJadiStnk(Date tglJadiStnk) {
		this.tglJadiStnk = tglJadiStnk;
	}

	public String getNoRangka() {
		return noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public Date getTglMskJam() {
		return tglMskJam;
	}

	public void setTglMskJam(Date tglMskJam) {
		this.tglMskJam = tglMskJam;
	}

	public String getPengurusBpkb() {
		return pengurusBpkb;
	}

	public void setPengurusBpkb(String pengurusBpkb) {
		this.pengurusBpkb = pengurusBpkb;
	}

	public Date getTglTerimaBpkb() {
		return tglTerimaBpkb;
	}

	public void setTglTerimaBpkb(Date tglTerimaBpkb) {
		this.tglTerimaBpkb = tglTerimaBpkb;
	}

	public String getNamaBpkb() {
		return namaBpkb;
	}

	public void setNamaBpkb(String namaBpkb) {
		this.namaBpkb = namaBpkb;
	}

	public String getNoPolisi() {
		return noPolisi;
	}

	public void setNoPolisi(String noPolisi) {
		this.noPolisi = noPolisi;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getTglAmbilStnk() {
		return tglAmbilStnk;
	}

	public void setTglAmbilStnk(Date tglAmbilStnk) {
		this.tglAmbilStnk = tglAmbilStnk;
	}

	public Date getTglAmbilBpkb() {
		return tglAmbilBpkb;
	}

	public void setTglAmbilBpkb(Date tglAmbilBpkb) {
		this.tglAmbilBpkb = tglAmbilBpkb;
	}

	public String getItemKdItem() {
		return itemKdItem;
	}

	public void setItemKdItem(String itemKdItem) {
		this.itemKdItem = itemKdItem;
	}

	public Date getEtdStnk() {
		return etdStnk;
	}

	public void setEtdStnk(Date etdStnk) {
		this.etdStnk = etdStnk;
	}

	public Date getEtdBpkb() {
		return etdBpkb;
	}

	public void setEtdBpkb(Date etdBpkb) {
		this.etdBpkb = etdBpkb;
	}

	public String getVkdsales() {
		return vkdsales;
	}

	public void setVkdsales(String vkdsales) {
		this.vkdsales = vkdsales;
	}

	public Date getDtglbeli() {
		return dtglbeli;
	}

	public void setDtglbeli(Date dtglbeli) {
		this.dtglbeli = dtglbeli;
	}

	public String getVkddlr() {
		return vkddlr;
	}

	public void setVkddlr(String vkddlr) {
		this.vkddlr = vkddlr;
	}

	public String getVnmsales() {
		return vnmsales;
	}

	public void setVnmsales(String vnmsales) {
		this.vnmsales = vnmsales;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((noMesin == null) ? 0 : noMesin.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH100Itemcusts other = (HondaH100Itemcusts) obj;
		if (noMesin == null) {
			if (other.noMesin != null)
				return false;
		} else if (!noMesin.equals(other.noMesin))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "HondaH100Itemcusts [noMesin=" + noMesin + "]";
	}
	
}
