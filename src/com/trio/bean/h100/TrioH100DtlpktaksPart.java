package com.trio.bean.h100;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:35:13 AM Dec 20, 2013
 */

@Entity
@Table(name="TRIO_H100_DTLPKTAKS_PART")
public class TrioH100DtlpktaksPart extends TrioEntityUserTrail implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private TrioH100DtlpktaksPartPK trioH100DtlpktaksPartPK;
	
	@Column(name="QTY")
	private int qty;
	
	@ManyToOne
	@JoinColumn(name="KD_PKT",referencedColumnName="KD_PKT", insertable=false, updatable=false)
	private TrioH100Pktaks trioH100Pktaks;
	
	public TrioH100DtlpktaksPart() {
		this.trioH100DtlpktaksPartPK = new TrioH100DtlpktaksPartPK();
	}

	public TrioH100DtlpktaksPartPK getTrioH100DtlpktaksPartPK() {
		return trioH100DtlpktaksPartPK;
	}

	public void setTrioH100DtlpktaksPartPK(
			TrioH100DtlpktaksPartPK trioH100DtlpktaksPartPK) {
		this.trioH100DtlpktaksPartPK = trioH100DtlpktaksPartPK;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public TrioH100Pktaks getTrioH100Pktaks() {
		return trioH100Pktaks;
	}

	public void setTrioH100Pktaks(TrioH100Pktaks trioH100Pktaks) {
		this.trioH100Pktaks = trioH100Pktaks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((trioH100DtlpktaksPartPK == null) ? 0
						: trioH100DtlpktaksPartPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100DtlpktaksPart other = (TrioH100DtlpktaksPart) obj;
		if (trioH100DtlpktaksPartPK == null) {
			if (other.trioH100DtlpktaksPartPK != null)
				return false;
		} else if (!trioH100DtlpktaksPartPK
				.equals(other.trioH100DtlpktaksPartPK))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100DtlpktaksPart [trioH100DtlpktaksPartPK="
				+ trioH100DtlpktaksPartPK + "]";
	}
	
}
