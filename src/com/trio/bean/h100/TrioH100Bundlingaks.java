package com.trio.bean.h100;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author Gusti Arya 10:39:43 AM Dec 26, 2013
 */

@Entity
@Table(name="TRIO_H100_BUNDLINGAKS")
public class TrioH100Bundlingaks extends TrioEntityUserTrail {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_BUNDLING", length=16)
	private String noBundling;
	
	@Column(name="TGL_BUNDLING")
	@Temporal(TemporalType.DATE)
	private Date tglBundling;
	
	@Column(name="REFERENCE", length=30)
	private String reference;
	
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="trioH100Bundlingaks")
	private Set<TrioH100Dtlbundlingaks> trioH100DtlbundlingaksSet;

	public TrioH100Bundlingaks() {
		
	}
	
	public String getNoBundling() {
		return noBundling;
	}

	public void setNoBundling(String noBundling) {
		this.noBundling = noBundling;
	}

	public Date getTglBundling() {
		return tglBundling;
	}

	public void setTglBundling(Date tglBundling) {
		this.tglBundling = tglBundling;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Set<TrioH100Dtlbundlingaks> getTrioH100DtlbundlingaksSet() {
		return trioH100DtlbundlingaksSet;
	}

	public void setTrioH100DtlbundlingaksSet(
			Set<TrioH100Dtlbundlingaks> trioH100DtlbundlingaksSet) {
		this.trioH100DtlbundlingaksSet = trioH100DtlbundlingaksSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((noBundling == null) ? 0 : noBundling.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH100Bundlingaks other = (TrioH100Bundlingaks) obj;
		if (noBundling == null) {
			if (other.noBundling != null)
				return false;
		} else if (!noBundling.equals(other.noBundling))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH100Bundlingaks [noBundling=" + noBundling + "]";
	}
	
}
