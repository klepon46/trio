package com.trio.bean.h000;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 31, 2012
 */
@Entity
@Table(name="TRIO_H000_MSTROLEACCESS") 
public class TrioH000Mstroleaccess extends TrioEntityUserTrail implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="VROLEID", length=30, nullable=false)
	private String vroleid;
	
	@Id
	@Column(name="VMENUID", length = 12, nullable=false)
	private String vmenuid;
	
	@Column(name="VSTATUS")
	private String vstatus;
	
//	@OneToMany(mappedBy="trioH000Mstroleaccess") 
//	private Set<DbaRolePrivs> dbaRolePrivsSet; 
	
	public TrioH000Mstroleaccess() {
		// TODO Auto-generated constructor stub
	}
	
	public TrioH000Mstroleaccess(String vmenuid, String vroleid) {
		this.vmenuid = vmenuid;
		this.vroleid = vroleid;
	}

//	public Set<DbaRolePrivs> getDbaRolePrivsSet() {
//		return dbaRolePrivsSet;
//	}
//
//	public void setDbaRolePrivsSet(Set<DbaRolePrivs> dbaRolePrivsSet) {
//		this.dbaRolePrivsSet = dbaRolePrivsSet;
//	}

	public String getVmenuid() {
		return vmenuid;
	}

	public void setVmenuid(String vmenuid) {
		this.vmenuid = vmenuid;
	}

	public String getVroleid() {
		return vroleid;
	}

	public void setVroleid(String vroleid) {
		this.vroleid = vroleid;
	}

	public String getVstatus() {
		return vstatus;
	}

	public void setVstatus(String vstatus) {
		this.vstatus = vstatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((vmenuid == null) ? 0 : vmenuid.hashCode());
		result = prime * result + ((vroleid == null) ? 0 : vroleid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrioH000Mstroleaccess other = (TrioH000Mstroleaccess) obj;
		if (vmenuid == null) {
			if (other.vmenuid != null)
				return false;
		} else if (!vmenuid.equals(other.vmenuid))
			return false;
		if (vroleid == null) {
			if (other.vroleid != null)
				return false;
		} else if (!vroleid.equals(other.vroleid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TrioH000Mstroleaccess [vmenuid=" + vmenuid + ", vroleid="
				+ vroleid + "]";
	}
	
}
