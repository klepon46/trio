package com.trio.bean.h000;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.trio.usertrail.TrioEntityUserTrail;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 12, 2012
 */
@Entity
@Table(name="HONDA_H000_SETDLRS") 
public class HondaH000Setdlrs extends TrioEntityUserTrail implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="KD_BISNIS", length=10, nullable=false)
	private String kdBisnis;
	
	@Id
	@Column(name="DEALER_KD_DLR", length=10, nullable=false)
	private String delaerKdDlr;
	
	@Column(name="TOP", length=6)
	private BigInteger top;
	
	@Column(name="PRA_AR", length=20, scale=2)
	private BigDecimal parAR;
	
	@Column(name="AR", length=20, scale=2)
	private BigDecimal ar;
	
	@Column(name="PLAFON", length=20, scale=2)
	private BigDecimal plafon;
	
	public HondaH000Setdlrs() {
		
	}
	
	public HondaH000Setdlrs(String kdBisnis, String delaerKdDlr ) {
		this.kdBisnis = kdBisnis;
		this.delaerKdDlr = delaerKdDlr;
	}

	public String getKdBisnis() {
		return kdBisnis;
	}

	public void setKdBisnis(String kdBisnis) {
		this.kdBisnis = kdBisnis;
	}

	public String getDelaerKdDlr() {
		return delaerKdDlr;
	}

	public void setDelaerKdDlr(String delaerKdDlr) {
		this.delaerKdDlr = delaerKdDlr;
	}

	public BigInteger getTop() {
		return top;
	}

	public void setTop(BigInteger top) {
		this.top = top;
	}

	public BigDecimal getParAR() {
		return parAR;
	}

	public void setParAR(BigDecimal parAR) {
		this.parAR = parAR;
	}

	public BigDecimal getAr() {
		return ar;
	}

	public void setAr(BigDecimal ar) {
		this.ar = ar;
	}

	public BigDecimal getPlafon() {
		return plafon;
	}

	public void setPlafon(BigDecimal plafon) {
		this.plafon = plafon;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((delaerKdDlr == null) ? 0 : delaerKdDlr.hashCode());
		result = prime * result
				+ ((kdBisnis == null) ? 0 : kdBisnis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH000Setdlrs other = (HondaH000Setdlrs) obj;
		if (delaerKdDlr == null) {
			if (other.delaerKdDlr != null)
				return false;
		} else if (!delaerKdDlr.equals(other.delaerKdDlr))
			return false;
		if (kdBisnis == null) {
			if (other.kdBisnis != null)
				return false;
		} else if (!kdBisnis.equals(other.kdBisnis))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH000Setdlrs [kdBisnis=" + kdBisnis + ", delaerKdDlr="
				+ delaerKdDlr + "]";
	}
	
}
