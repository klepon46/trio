package com.trio.bean.h000;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="HONDA_H000_GATES")
public class HondaH000Gates  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="DBLIST")
	private String dblist;
	
	@Column(name="DBCODE")
	private String dbcode;
	
	@Column(name="DBPSWD")
	private String dbpswd;
	
	@Column(name="DBSANDI")
	private String dbsandi;
	
	@Column(name="DBSPID")
	private String dbspid;
	
	@Column(name="DBSECT")
	private String dbsect;
	
	@Column(name="DBDATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dbdate;

	public String getDblist() {
		return dblist;
	}

	public void setDblist(String dblist) {
		this.dblist = dblist;
	}

	public String getDbcode() {
		return dbcode;
	}

	public void setDbcode(String dbcode) {
		this.dbcode = dbcode;
	}

	public String getDbpswd() {
		return dbpswd;
	}

	public void setDbpswd(String dbpswd) {
		this.dbpswd = dbpswd;
	}

	public String getDbsandi() {
		return dbsandi;
	}

	public void setDbsandi(String dbsandi) {
		this.dbsandi = dbsandi;
	}

	public String getDbspid() {
		return dbspid;
	}

	public void setDbspid(String dbspid) {
		this.dbspid = dbspid;
	}

	public String getDbsect() {
		return dbsect;
	}

	public void setDbsect(String dbsect) {
		this.dbsect = dbsect;
	}

	public Date getDbdate() {
		return dbdate;
	}

	public void setDbdate(Date dbdate) {
		this.dbdate = dbdate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((dblist == null) ? 0 : dblist.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		HondaH000Gates other = (HondaH000Gates) obj;
		if (dblist == null) {
			if (other.dblist != null)
				return false;
		} else if (!dblist.equals(other.dblist))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "HondaH000Gates [dblist=" + dblist + "]";
	}
	
	

}
