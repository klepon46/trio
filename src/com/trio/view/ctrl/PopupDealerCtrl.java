package com.trio.view.ctrl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Dealers;
import com.trio.view.ctrl.renderer.DealerListRenderer;

public class PopupDealerCtrl extends TrioBasePage {

	private static final long serialVersionUID = 20111130143824L;
	private AnnotateDataBinder binder;
	private Listbox dealerList;
	private Grid editDealerGrid;
	private Button createDealer;
	private Button updateDealer;
	private Button deleteDealer;
	private Button searchDealer;
	
	private HondaH000Dealers _dealer;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		_dealer = new HondaH000Dealers();
		dealerList.setModel(new ListModelList(
				getMasterFacade().getHondaH000DealersDao().findAll()));
		dealerList.setItemRenderer(new DealerListRenderer());
	}
	
	private ListModelList getModel() {
		return (ListModelList) dealerList.getModel();
	}
	
	public void onClick$resetDealer() {
		dealerList.clearSelection();
		dealerList.setModel(new ListModelList(getMasterFacade().getHondaH000DealersDao().findAll()));
		_dealer = new HondaH000Dealers();
		binder.loadComponent(editDealerGrid);
		createDealer.setDisabled(false);
		updateDealer.setDisabled(true);
		deleteDealer.setDisabled(true);
		//page.getFellow("contactDiv").setVisible(false);
	}
	
	//set selection to edit data
	public void onSelect$dealerList() {
		_dealer = (HondaH000Dealers) dealerList.getSelectedItem().getValue();
		binder.loadComponent(editDealerGrid);
		createDealer.setDisabled(true);
		updateDealer.setDisabled(false);
		deleteDealer.setDisabled(false);
		// used for Hibernate lazy-loading
		//_dealer = (Dealer) ServiceLocator.getHibernateSession().merge(_dealer);
		//Event event = new Event("onLoad", page.getFellow("contactDiv"), _dealer);
		//EventQueues.lookup("loadContact", EventQueues.DESKTOP, true).publish(event);
	}
	
	//register onClick event for creating new object into list model
	public void onClick$createDealer() throws InterruptedException {
		if (_dealer.getKdDlr() == null && _dealer.getDlrNama() == null) {
			Messagebox.show("no new content to add");
		} else {
			getMasterFacade().getHondaH000DealersDao().saveOrUpdate(_dealer, getUserSession());
			getModel().add(_dealer);
		}
	}
	
	//register onClick event for updating edited data in list model
	public void onClick$updateDealer() throws InterruptedException {
		Listitem listItem = dealerList.getSelectedItem();
		getMasterFacade().getHondaH000DealersDao().saveOrUpdate(_dealer, getUserSession());
		listItem.setValue(_dealer);
		int index = dealerList.getSelectedIndex();
		getModel().set(index, _dealer);
	}
	
	//register onClick event for removing data in list model
	public void onClick$deleteDealer() throws InterruptedException {
		Messagebox.show("Are you sure to delete?", null, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
			public void onEvent(Event event) throws Exception {
				if(event.getName().equals("onYes")) {
					getModel().remove(_dealer);
					getMasterFacade().getHondaH000DealersDao().delete(_dealer);
				}
			}
		});
	}
	
	public void onClick$searchDealer() throws InterruptedException {
		HondaH000Dealers d = new HondaH000Dealers();
		d.setKdDlr(_dealer.getKdDlr());
		d.setDlrNama(_dealer.getDlrNama());
		d.setDlrAlamat1(_dealer.getDlrAlamat1());
		
		//dealerList.setModel(new ListModelList(getMasterFacade().getHondaH000DealersDao().getDealerByCriteria(d)));  
		dealerList.setModel(new ListModelList(getMasterFacade().getHondaH000DealersDao().findByExample(d)));
		 
	}
	
	public HondaH000Dealers getDealer() {
		return _dealer;
	}
	
	public void setDealer(HondaH000Dealers dealer) {
		_dealer = dealer;
	}
}
