package com.trio.view.ctrl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Button;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Items;

public class AccessoriesCtrl extends TrioBasePage{

	private static final long serialVersionUID = 1L;


	private Popup popKodeTipe;
	private Listbox popKodeTipeList;

	private Textbox tbKodeTipe;
	private Button btKodeTipeLOV;

	private Textbox tbKodeItem;
	private Textbox btKodeItemLOV;

	private Popup popKodeItem;
	private Listbox popKodeItemList;

	private Listbox listBoxAcc;
	private Button btnAdd;
	private Button btnExport;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
	}


	public void onClick$btKodeTipeLOV(){

		List<HondaH000Items> list = getMasterFacade().getHondaH000ItemsDao().getKdTipe();
		popKodeTipeList.setModel(new ListModelList(list));
		popKodeTipe.open(btKodeTipeLOV);

	}

	public void onSelect$popKodeTipeList(){
		System.out.println("masuk onselect kodeTipe");
		System.out.println(popKodeTipeList.getSelectedItem().getLabel());
		tbKodeTipe.setValue(popKodeTipeList.getSelectedItem().getLabel());
		popKodeTipe.close();
	}

	public void onClick$btKodeItemLOV(){
		//		System.out.println("Kode Item LOV");
		//		List<HondaH000Items> list = getMasterFacade().getHondaH000ItemsDao()
		//				.getKdItemByKdTipe(tbKodeTipe.getValue());
		//		
		//		System.out.println(list);
		//		
		//		popKodeItemList.setModel(new ListModelList(list));
		//		popKodeItemList.setItemRenderer(new ListitemRenderer() {
		//			@Override
		//			public void render(Listitem item, Object data) throws Exception {
		//				HondaH000Items items = (HondaH000Items) data;
		//				item.setValue(items);
		//				new Listcell(items.getKdItem()).setParent(item);
		//				new Listcell(items.getKet2()).setParent(item);
		//				new Listcell(items.getKetWarna()).setParent(item);
		//			}
		//		});
		//		popKodeItem.open(300, 120);


		System.out.println("add");

		List<HondaH000Items> lists = getMasterFacade().getHondaH000ItemsDao().findAll();
		listBoxAcc.setModel(new ListModelList(lists));
		listBoxAcc.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {

				HondaH000Items hondaItems = (HondaH000Items) data;
				item.setValue(hondaItems);

				new Listcell(hondaItems.getKdTipe()).setParent(item);
				new Listcell(hondaItems.getKdItem()).setParent(item);
				new Listcell(hondaItems.getKdWarna()).setParent(item);
				new Listcell(hondaItems.getKet1()).setParent(item);

				listBoxAcc.setPageSize(20);
			}
		});

	}


	public void onSelect$popKodeItemList(){
		HondaH000Items items = (HondaH000Items) popKodeItemList.getSelectedItem().getValue();
		tbKodeItem.setValue(items.getKdItem());
		popKodeItem.close();

	}

	public void onClick$buttonAdd(){

		System.out.println("add");

		List<HondaH000Items> lists = getMasterFacade().getHondaH000ItemsDao().findAll();
		listBoxAcc.setModel(new ListModelList(lists));
		listBoxAcc.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {

				HondaH000Items hondaItems = (HondaH000Items) data;
				item.setValue(hondaItems);

				new Listcell(hondaItems.getKdTipe()).setParent(item);
				new Listcell(hondaItems.getKdItem()).setParent(item);
				new Listcell(hondaItems.getKdWarna()).setParent(item);
				new Listcell(hondaItems.getKet1()).setParent(item);
				
				listBoxAcc.setPageSize(10);

			}
		});

	}

	public void onClick$btnExport() throws IOException{
		eksportExcel(listBoxAcc, "oyee");
	}

	public void eksportExcel(Listbox box, String noFile) throws IOException{

		HSSFWorkbook workbook  = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("KUDA Software");

		HSSFRow row = sheet.createRow(0);
		HSSFFont fontRedBold = workbook.createFont();
		HSSFFont fontNormal = workbook.createFont();
		fontRedBold.setColor(HSSFFont.COLOR_RED);
		fontRedBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		fontNormal.setColor(HSSFFont.COLOR_NORMAL);
		fontNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

		//Style
		HSSFCellStyle cellStyleRedBold = workbook.createCellStyle();
		HSSFCellStyle cellStyleNormal = workbook.createCellStyle();
		cellStyleRedBold.setFont(fontRedBold);
		cellStyleNormal.setFont(fontNormal);

		//headers
		int i = 0;
		row = sheet.createRow(0);
		for(Object head : box.getHeads()){
			for(Object header : ((Listhead)head).getChildren()){
				String h = ((Listheader)header).getLabel();
				HSSFCell cell = row.createCell(i);
				cell.setCellStyle(cellStyleRedBold);
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(h);
				i++;
			}
		}

		//detaglio
		int x = 1;
		int y = 0;
		for(Object item : box.getItems()){
			row = sheet.createRow(x);
			y=0;
			for(Object lbCell : ((Listitem)item).getChildren()){
				String h;
				h = ((Listcell)lbCell).getLabel();
				HSSFCell cell = row.createCell(y);
				cell.setCellStyle(cellStyleNormal);
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(h);
				y++;
			}
			x++;
		}


		FileOutputStream  fOut = new FileOutputStream(noFile);

		workbook.write(fOut);
		fOut.flush();

		fOut.close();

		File file = new File(noFile);
		Filedownload.save(file, "XLS");
	}
}
