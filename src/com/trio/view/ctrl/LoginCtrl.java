package com.trio.view.ctrl;


import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.trio.base.TrioBasePage;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.EnkripsiPassword;
import com.trio.util.LoginManager;


public class LoginCtrl extends TrioBasePage {
	
	private static final Logger logger = Logger.getLogger(LoginCtrl.class); 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Textbox nameTxb, passwordTxb;
	Combobox connCmbo;
	Button confirmBtn, cancelBtn;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		nameTxb.setFocus(true);
		connCmbo.setSelectedItem((Comboitem) connCmbo.getFirstChild());
		
	}
	
	public void onClick$confirmBtn() throws InterruptedException{ 
		/*
		 * validation empty 
		 */
		
		if (nameTxb.getValue().equalsIgnoreCase("") || nameTxb.getValue() == null){
			Messagebox.show("Username harus diisi");
			return;
		}
		if (passwordTxb.getValue().equalsIgnoreCase("") || passwordTxb.getValue() == null){
			Messagebox.show("Password harus diisi");
			return;
		}
		
		if(connCmbo.getValue().equals("UNIT")){
			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		}else{
			DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		}
		doLogin();
	}
	
	public void onClick$cancelBtn(){
		nameTxb.setValue(null);
		passwordTxb.setValue(null);
		nameTxb.setFocus(true);
	}

		
//	public void onOK(){
//		
//		doLogin();
//	}
	
	public void doLogin(){
		
		logger.info("masuk login");
		
		LoginManager lm = LoginManager.getIntance(Sessions.getCurrent());
		/*
		 * mendaftarkan username ke session
		 * @param username
		 * @return nameTxb.getValue()
		 */
		Sessions.getCurrent().setAttribute("username", nameTxb.getValue());
		Sessions.getCurrent().setAttribute("connection", connCmbo.getValue());
		lm.logIn(nameTxb.getValue(),enkripsi(nameTxb.getValue(), passwordTxb.getValue()));
		if(lm.isAuthenticated()){
			logger.info("sukses login");
			execution.sendRedirect("index.zul");
			
		}else {
			logger.info("gagal login");
			try {
				
				Messagebox.show("Username/Password anda salah", "Warning", Messagebox.OK,
						Messagebox.EXCLAMATION);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public void onOK(){
		if(connCmbo.getValue().equals("UNIT")){
			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		}else{
			DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		}
		doLogin();
	}
	
	public String enkripsi(String userName, String password){
		EnkripsiPassword ep = new EnkripsiPassword();
		String hasil = ep.EnkripsiPass(userName, password);
		return hasil;
		
	}
	
}
