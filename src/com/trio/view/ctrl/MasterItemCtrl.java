package com.trio.view.ctrl;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.SelectEvent;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Textbox;

import com.lowagie.text.ListItem;
import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Items;
import com.trio.bean.h100.HondaH100Dtlfakdos;
import com.trio.bean.h100.TrioH100Dtldologs;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.ItemListRenderer;

/** @author Saifi Ahmada Jun 4, 2015 2:44:05 PM  **/

public class MasterItemCtrl extends TrioBasePage {

	private static final long serialVersionUID = 1L;
	
	private AnnotateDataBinder binder;
	
	private Listbox itemList;
	
	private Radiogroup radioStatus;
	
	private Button btnTampilkan;
	
	private Button btnReset;
	
	private Textbox kdTipeText;
	
	private Checkbox checkAll;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		checkAll.setChecked(false);
		radioStatus.setSelectedIndex(0); 
		kdTipeText.setText(null);
		String kdTipe = kdTipeText.getValue();
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		String radio = radioStatus.getSelectedItem().getValue();
		itemList.setModel(new ListModelList(getMasterFacade().getHondaH000ItemsDao().getKdItemByTrioStatus(radio, kdTipe)));
		itemList.setItemRenderer(new ItemListRenderer());
	}
	
	public void onClick$btnTampilkan(){
		String kdTipe = kdTipeText.getValue();
		checkAll.setChecked(false);
		System.out.println("KD TIPE = " + kdTipe); 
		String radio = radioStatus.getSelectedItem().getValue();
		System.out.println("radio = " + radio);
		if (radio.equalsIgnoreCase("Y") || radio.equalsIgnoreCase("N")) {
			System.out.println("masuk Y N");
			itemList.setModel(new ListModelList(getMasterFacade().getHondaH000ItemsDao().getKdItemByTrioStatus(radio, kdTipe))); 
		} else {
			System.out.println("masuk O");
			itemList.setModel(new ListModelList(getMasterFacade().getHondaH000ItemsDao().getKdItemByTrioStatusIsNull(kdTipe)));
		}
	}
	
	public void onClick$btnReset(){
		checkAll.setChecked(false); 
		kdTipeText.setText(null);  
		String kdTipe = kdTipeText.getValue();
		radioStatus.setSelectedIndex(0); 
		String radio = radioStatus.getSelectedItem().getValue();
		itemList.setModel(new ListModelList(getMasterFacade().getHondaH000ItemsDao().getKdItemByTrioStatus(radio, kdTipe))); 
	}
	
	public void onOK$kdTipeText(){
		onClick$btnTampilkan();
	}
	
	@SuppressWarnings("unchecked")
	public void onClick$btnUpdate(){
		System.out.println("tes select");
		String radio = radioStatus.getSelectedItem().getValue();
		Listbox lb = (Listbox) itemList.getFellow("win").getFellow("itemList");
		List<Listitem> items = lb.getItems();

		for (Listitem item : items){
			List<Listcell> listLC = item.getChildren();

			HondaH000Items objectItem = (HondaH000Items) item.getValue();
			for (Listcell cell : listLC){

				if (cell.getFirstChild() instanceof Checkbox && cell.getColumnIndex() == 5){
					Checkbox cb = (Checkbox) cell.getFirstChild();
					if (cb.isChecked()) {
						if (radio.equalsIgnoreCase("Y")) {
							System.out.println("kd item " + objectItem.getKdItem()); 
							System.out.println("dicek bro");
							getMasterFacade().getHondaH000ItemsDao().updateItemByKdItem(objectItem.getKdItem(), "N");
						} else if (radio.equalsIgnoreCase("N") || radio.equalsIgnoreCase("O") ) {
							System.out.println("kd item " + objectItem.getKdItem()); 
							System.out.println("dicek bro");
							getMasterFacade().getHondaH000ItemsDao().updateItemByKdItem(objectItem.getKdItem(), "Y");
						}
						
					} 
					
				}
			}
		}
		String kdTipe = kdTipeText.getValue();
		if (radio.equalsIgnoreCase("Y") || radio.equalsIgnoreCase("N")) {
			System.out.println("masuk Y N");
			itemList.setModel(new ListModelList(getMasterFacade().getHondaH000ItemsDao().getKdItemByTrioStatus(radio, kdTipe))); 
		} else {
			System.out.println("masuk O");
			itemList.setModel(new ListModelList(getMasterFacade().getHondaH000ItemsDao().getKdItemByTrioStatusIsNull(kdTipe)));
		}
		
	}
	
	public void onCheck$checkAll() {
        System.out.println("Check All");
        Listbox lb = (Listbox) itemList.getFellow("win").getFellow("itemList");
		List<Listitem> items = lb.getItems();
		
		for (Listitem item : items){
			List<Listcell> listLC = item.getChildren();
			for (Listcell cell : listLC){

				if (cell.getFirstChild() instanceof Checkbox && cell.getColumnIndex() == 5){
					Checkbox cb = (Checkbox) cell.getFirstChild();
					if (checkAll.isChecked()) {
						cb.setChecked(true); 
					} else {
						cb.setChecked(false);
					}
				}
				
			}
		}
		
    }
	
}
