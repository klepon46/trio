package com.trio.view.ctrl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Setdlrs;
import com.trio.bean.h000.HondaH000Setups;
import com.trio.bean.h100.HondaH100Dtlfakdos;
import com.trio.bean.h100.HondaH100Fakdos;
import com.trio.bean.h100.TrioH100Dologs;
import com.trio.bean.h100.TrioH100Dtldologs;
import com.trio.component.TrioPopup;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 15, 2012
 */
public class ApproveDoLogistikCtrl extends TrioBasePage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Window win;
	
	private Textbox tbNoDOLogistik,tbNamaDealerExt;
	private Button btnLOVDOLogistik;

	private Button btnLOVValTag;

	private TrioH100Dologs dologs;

	private HondaH000Setups setup;

	private Grid editGrid;
	private AnnotateDataBinder binder;

	private Intbox ibTop;
	private Decimalbox dbDiskonPersen, dbPlafon, dbAR, dbOpenPlafon, dbEstimasiPlafon, dbDiskonPersenPlafon, dbEstimasiPlafonAwal;
	private Textbox tbKdDealer,tbNoDO,tbValTag;
	private Radiogroup radGroupAksesoris;

	private Listbox lbDtldologs;
	private Intbox ibTotQtyDO;
	private Intbox ibTotQtyAp;

	private Button btnSimpan;
	private Button btnBatal;

	private Decimalbox dbTotalDiskon, dbTotalHargaNetto, dbTotalHarga, dbTotalPPN, dbGrandTotal;

	private BigInteger sumHarga;
	private BigDecimal grandTot = new BigDecimal(0);

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		dbDiskonPersen.setValue("0");
		btnSimpan.setDisabled(true);
		btnBatal.setDisabled(true);
	}

	//LOV NO DO LOGISTIK
	public void onClick$btnLOVDOLogistik() {
		System.out.println("Test1");
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		
		List<TrioH100Dologs> list = getMasterFacade().getTrioH100DologsDao().findDologsByStatus("N");
		final Listbox lb = new Listbox();
		Listhead listhead = new Listhead();
		Listheader listheader, listheader2;
		final TrioPopup pop = new TrioPopup(list, new DologLOVRenderer(), lb, "400px", "300px");

		listheader = new Listheader("No Dolog");
		listheader2 = new Listheader("Kode Dealer");
		listheader.setParent(listhead);
		listheader2.setParent(listhead);
		listhead.setParent(lb);

		lb.addEventListener(Events.ON_SELECT, new EventListener() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				TrioH100Dologs dologs = (TrioH100Dologs) lb.getSelectedItem().getValue();
				setDologs(dologs);

				binder.loadComponent(editGrid);
				btnLOVValTag.setDisabled(false);
				btnBatal.setDisabled(false);
				setListboxDtldologs(dologs.getTrioH100DtldologsSet());
				sumHarga= getMasterFacade().getTrioH100DologsDao().getSumHarga(dologs.getNoDolog());
				System.out.println("isi sum = " + sumHarga);

				if(sumHarga==null){
					sumHarga = BigInteger.valueOf(0);
					System.out.println("sum Harga didalam if = " + sumHarga);
				}

				dbEstimasiPlafonAwal.setValue(new BigDecimal(sumHarga));

				if(getDologs().getAks().equalsIgnoreCase("A")){
					radGroupAksesoris.setSelectedIndex(0);
				}else{
					radGroupAksesoris.setSelectedIndex(1);
				}

				pop.close();
			}
		});

		lb.setParent(pop);
		pop.setParent(win);
		pop.open(self);
	}

	//LOV Cara Pembayaran
	public void onClick$btnLOVValTag() {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		System.out.println("Test2");
		
		List<HondaH000Setups> list = getMasterFacade().getHondaH000SetupsDao().findByValIdAndIn("H1_CARA_BAYAR");
		final Listbox lb = new Listbox();
		Listhead listhead = new Listhead();

		Listheader listheader;

		final TrioPopup pop = new TrioPopup(list, new BayarLOVRenderer(), lb, "400px", "300px");

		listheader = new Listheader("Val Tag");
		listheader.setParent(listhead);
		listhead.setParent(lb);
		lb.addEventListener(Events.ON_SELECT, new EventListener() {

			@Override
			public void onEvent(Event arg0) throws Exception {
				HondaH000Setups s = (HondaH000Setups) lb.getSelectedItem().getValue();
				setSetup(s);

				HondaH000Setdlrs setDlr = getMasterFacade().getHondaH000SetdlrsDao().findByKdDealerAndKodeBisnis(tbKdDealer.getValue(), "A");


				if (s.getValTag().equalsIgnoreCase("C")){
					dbDiskonPersen.setDisabled(false);

					HondaH000Setups stp = getMasterFacade().getHondaH000SetupsDao().findByValIdValTag("H1_CONFIG", "CASH_DISCOUNT_PCT");

					final String valChar = "0"+stp.getValChar();

					dbDiskonPersen.addEventListener(Events.ON_CHANGE, new EventListener() {

						@Override
						public void onEvent(Event arg0) throws Exception {
							if (dbDiskonPersen.getValue().compareTo(new BigDecimal(valChar)) > 0){
								Messagebox.show("Nilai diskon persen yang diisi maksimal "+valChar);
								dbDiskonPersen.setValue(new BigDecimal(valChar));
								return;
							}
							setListboxDtldologs(getDologs().getTrioH100DtldologsSet());
							setGrid();
							dbDiskonPersenPlafon.setValue(((dbEstimasiPlafon.getValue().add(dbAR.getValue())).divide(dbPlafon.getValue(), MathContext.DECIMAL32))
									.multiply(new BigDecimal(100)));
						}
					});
					dbDiskonPersen.setValue(new BigDecimal(valChar));
					ibTop.setValue(0);

				} else if (s.getValTag().equalsIgnoreCase("R")){
					dbDiskonPersen.setValue(new BigDecimal("0"));
					dbDiskonPersen.setDisabled(true);
					ibTop.setValue(setDlr.getTop().intValue());

				} else if (s.getValTag().equalsIgnoreCase("K")){
					dbDiskonPersen.setValue(new BigDecimal("0"));
					dbDiskonPersen.setDisabled(true);
					ibTop.setValue(setDlr.getTop().intValue());
				}

				dbPlafon.setValue(setDlr.getPlafon());
				if(dbPlafon.getValue().equals(new BigDecimal(0))){
					alert("Nilai plafon harus diisi");
					return;
				}
				
				// getPlafon
				BigDecimal plafon = new BigDecimal(0);
				plafon = setDlr.getPlafon() == null ? new BigDecimal(0) : setDlr.getPlafon() ;

				//getAR
				BigInteger arDlr = getMasterFacade().getHondaH100ArsDao().getARByKdDlr(tbKdDealer.getValue());
				dbAR.setValue(new BigDecimal(arDlr));

				BigDecimal openPlafon = plafon.subtract(new BigDecimal(arDlr));
				dbOpenPlafon.setValue(openPlafon);

				System.out.println("dbEstimasiPlafon = " + dbEstimasiPlafon.getValue());
				System.out.println("dbPlafon = " + dbPlafon.getValue());

				setListboxDtldologs(getDologs().getTrioH100DtldologsSet());
				dbDiskonPersenPlafon.setValue(((dbEstimasiPlafon.getValue().add(dbAR.getValue())).divide(dbPlafon.getValue(), MathContext.DECIMAL32))
						.multiply(new BigDecimal(100)));
				setGrid();

				binder.loadComponent(editGrid);
				btnSimpan.setDisabled(false);
				tbNamaDealerExt.setDisabled(false);
				pop.close();

			}
		});
		lb.setParent(pop);
		pop.setParent(win);
		pop.open(self);

	}

	public TrioH100Dologs getDologs() {
		return dologs;
	}

	public void setDologs(TrioH100Dologs dologs) {
		this.dologs = dologs;
	}

	public HondaH000Setups getSetup() {
		return setup;
	}

	public void setSetup(HondaH000Setups setup) {
		this.setup = setup;
	}

	private void setListboxDtldologs(Set<TrioH100Dtldologs> set){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		lbDtldologs.setModel(new ListModelList(set));
		lbDtldologs.setItemRenderer(new ListitemRenderer() {

			BigDecimal totalDiskon = new BigDecimal(0);
			BigDecimal totalHargaNetto = new BigDecimal(0);
			BigDecimal totalHarga = new BigDecimal(0);
			BigDecimal totalPPN = new BigDecimal(0);
			BigDecimal grandTotal = new BigDecimal(0);
			BigDecimal diskonJml = new BigDecimal(0);

			String persenPPN = getMasterFacade().getHondaH000SetupsDao().findValCharByValIdValTag("H1_CONFIG", "PPN");
			Long sumQty = getMasterFacade().getTrioH100DtldologsDao().sumQtyByNoDolog(tbNoDOLogistik.getValue());
			
			
			
			@Override
			public void render(Listitem item, Object data) throws Exception {
				final TrioH100Dtldologs dtl = (TrioH100Dtldologs) data;
				item.setValue(data);
				
				//ListItem Kodeitem
				new Listcell(dtl.getKdItem()).setParent(item);
				
				//ListItem kode paket
				new Listcell(dtl.getKdPktAks()).setParent(item);

				//List Item Qty DO
				new Listcell(String.valueOf(dtl.getQty())).setParent(item);

				//List Item Qty Approve
				Listcell lc = new Listcell();
				final Decimalbox dbJumlah =  new Decimalbox();
				final Decimalbox dbNetto = new Decimalbox();
				final Intbox tbQtyApp = new Intbox(dtl.getQty());
				final Decimalbox dbDiscTotal = new Decimalbox();
				final Decimalbox dbDiscount = new Decimalbox();
				final Decimalbox dbPotDO = new Decimalbox();
				final Decimalbox dbSubsidi = new Decimalbox();
				final BigDecimal harga = getMasterFacade().getHondaH100HargasDao().findHargaStdByKdItemAndKdDlr(dtl.getKdItem(), tbKdDealer.getValue());

				tbQtyApp.setCols(3);
				tbQtyApp.setMaxlength(3);
				tbQtyApp.setConstraint("no negative,no empty,no zero");
				tbQtyApp.setParent(lc);
				tbQtyApp.addEventListener(Events.ON_BLUR, new EventListener() {

					@Override
					public void onEvent(Event arg0) throws Exception {
						if (tbQtyApp.getValue() > dtl.getQty()){
							Messagebox.show("Qty Approve tidak boleh melebihi Qty DO");
							tbQtyApp.setValue(dtl.getQty());
							setGrid();
						}

						BigDecimal jmlDiskon = dbDiskonPersen.getValue().multiply(harga); 
						jmlDiskon = jmlDiskon.divide(new BigDecimal(100));
						//						jmlDiskon = jmlDiskon.multiply(new BigDecimal(tbQtyApp.getValue()));
						dbDiscount.setValue(jmlDiskon); 

						dbPotDO.setValue(new BigDecimal(0));
						dbSubsidi.setValue(new BigDecimal(0));
						dbDiscTotal.setValue(jmlDiskon);

						BigDecimal netto = harga.multiply(new BigDecimal(tbQtyApp.getValue()));
						dbNetto.setValue(netto.subtract(jmlDiskon));

						dbJumlah.setValue(harga.multiply(new BigDecimal(tbQtyApp.getValue())));

						setGrid();

						dbDiskonPersenPlafon.setValue(((dbEstimasiPlafon.getValue().add(dbAR.getValue())).divide(dbPlafon.getValue(), MathContext.DECIMAL32))
								.multiply(new BigDecimal(100)));

					}
				});
				lc.setParent(item);

				//Listitem dbHarga
				lc = new Listcell();
				final Decimalbox dbHarga = new Decimalbox(harga);
				dbHarga.setFormat("###,###.##");
				dbHarga.setStyle("background:transparent; color: black !important;");
				dbHarga.setDisabled(true);
				dbHarga.setParent(lc);
				lc.setParent(item);

				//ListItem dbDiscount Total
				lc = new Listcell();
				dbDiscTotal.setFormat("###,###.##");
				dbDiscTotal.setDisabled(true);
				dbDiscTotal.setParent(lc);
				lc.setParent(item);

				//Listitem dbDiscountCash
				lc = new Listcell();
				BigDecimal jmlDiskon = dbDiskonPersen.getValue().multiply(harga); 
				jmlDiskon = jmlDiskon.divide(new BigDecimal(100));
				//				jmlDiskon = jmlDiskon.multiply(new BigDecimal(tbQtyApp.getValue()));
				diskonJml = jmlDiskon;
				dbDiscount.setValue(jmlDiskon); 
				dbDiscTotal.setValue(dbDiscount.getValue());
				BigDecimal netto = harga.multiply(new BigDecimal(tbQtyApp.getValue()));

				totalDiskon = totalDiskon.add(diskonJml.multiply(new BigDecimal(tbQtyApp.getValue())));
				dbTotalDiskon.setValue(totalDiskon);

				ibTotQtyDO.setValue(safeLongToInt(sumQty));
				ibTotQtyAp.setValue(safeLongToInt(sumQty));
				
				dbTotalDiskon.setDisabled(true);
				totalHargaNetto = totalHargaNetto.add(netto.subtract(jmlDiskon));
				dbTotalHargaNetto.setValue(totalHargaNetto);
				dbTotalHargaNetto.setDisabled(true);
				totalHarga = totalHarga.add(harga.multiply(new BigDecimal(tbQtyApp.getValue())));
				dbTotalHarga.setValue(totalHarga);
				dbTotalHarga.setDisabled(true);

				totalPPN = totalHargaNetto.multiply(new BigDecimal(persenPPN)).divide(new BigDecimal(100));

				grandTotal = totalHargaNetto.add(totalPPN); 
				dbTotalPPN.setValue(totalPPN);
				dbGrandTotal.setValue(grandTotal);

				grandTot = grandTotal;
				System.out.println("grandtot di setList = " + grandTot);
				dbEstimasiPlafon.setValue(dbEstimasiPlafonAwal.getValue().add(grandTot));

				dbNetto.setValue(netto.subtract(jmlDiskon)); 
				dbDiscount.setFormat("###,###.##");
				dbDiscount.setStyle("background:transparent; color: black !important;");
				dbDiscount.setDisabled(true);


				//				dbDiscount.addEventListener(Events.ON_OK, new EventListener() {
				//					
				//					@Override
				//					public void onEvent(Event arg0) throws Exception {
				//						dbDiscount.setValue(dbDiscount.getValue());
				//						dbNetto.setValue(harga.subtract(dbDiscount.getValue()));  
				//					}
				//				});
				dbDiscount.setParent(lc);
				lc.setParent(item);


				//ListItem dbDiscount potongan DO
				lc = new Listcell();
				dbPotDO.setFormat("###,###.##");
				dbPotDO.setValue(new BigDecimal(0));
				dbPotDO.setConstraint("no empty, no negative");
				dbPotDO.setParent(lc);
				lc.setParent(item);

				dbPotDO.addEventListener(Events.ON_CHANGE, new EventListener() {

					@Override
					public void onEvent(Event arg0) throws Exception {
						if(dbPotDO.getValue().compareTo(harga) > 0){
							alert("Nilai Discount Tidak boleh lebih besar dari Harga");
							dbPotDO.setValue(new BigDecimal(0));
							return;
						}

						BigDecimal temp = harga.subtract(dbPotDO.getValue());
						BigDecimal value = (temp.multiply(dbDiskonPersen.getValue()).divide(new BigDecimal(100)));
						dbDiscount.setValue(value);
						dbDiscTotal.setValue(dbPotDO.getValue().add(dbDiscount.getValue()).add(dbSubsidi.getValue()));
						dbNetto.setValue((harga.multiply(new BigDecimal(tbQtyApp.getValue()))).subtract(dbDiscTotal.getValue()));
						setGrid();

						dbDiskonPersenPlafon.setValue(((dbEstimasiPlafon.getValue().add(dbAR.getValue())).divide(dbPlafon.getValue(), MathContext.DECIMAL32))
								.multiply(new BigDecimal(100)));
					}
				});


				//LisItem dbDiscount Lunas Subsidi
				lc = new Listcell();
				dbSubsidi.setFormat("###,###.##");
				dbSubsidi.setValue(new BigDecimal(0));
				dbSubsidi.setConstraint("no empty, no negative");
				dbSubsidi.addEventListener(Events.ON_CHANGE, new EventListener() {

					@Override
					public void onEvent(Event arg0) throws Exception {
						if(dbSubsidi.getValue().compareTo(harga) > 0){
							alert("Nilai Discount tidak boleh lebih besar dari harga");
							dbSubsidi.setValue(new BigDecimal(0));
							return;
						}

						dbDiscTotal.setValue(dbPotDO.getValue().add(dbDiscount.getValue()).add(dbSubsidi.getValue()));
						dbNetto.setValue((harga.multiply(new BigDecimal(tbQtyApp.getValue()))).subtract(dbDiscTotal.getValue()));

						setGrid();

						dbDiskonPersenPlafon.setValue(((dbEstimasiPlafon.getValue().add(dbAR.getValue())).divide(dbPlafon.getValue(), MathContext.DECIMAL32))
								.multiply(new BigDecimal(100)));
					}
				});
				dbSubsidi.setParent(lc);
				lc.setParent(item);

				//Lisitem dbHargaNetto
				lc = new Listcell();
				dbNetto.setFormat("###,###.##");
				dbNetto.setStyle("background:transparent; color: black !important;");
				dbNetto.setDisabled(true);
				dbNetto.setParent(lc);
				lc.setParent(item);

				//Lisitem dbJumlah
				lc = new Listcell();
				dbJumlah.setValue(harga.multiply(new BigDecimal(tbQtyApp.getValue())));
				dbJumlah.setFormat("###,###.##");
				dbJumlah.setStyle("background:transparent; color: black !important;");
				dbJumlah.setDisabled(true);
				dbJumlah.setParent(lc);
				lc.setParent(item);
			}
		});

	}

	class DologLOVRenderer implements ListitemRenderer{

		@Override
		public void render(Listitem item, Object data) throws Exception {

			TrioH100Dologs dologs = (TrioH100Dologs) data;
			item.setValue(dologs);

			new Listcell(dologs.getNoDolog()).setParent(item);
			new Listcell(dologs.getKdDlr()).setParent(item);

		}
	}

	class BayarLOVRenderer implements ListitemRenderer {

		@Override
		public void render(Listitem item, Object data) throws Exception {
			HondaH000Setups setup = (HondaH000Setups) data;
			item.setValue(setup);

			new Listcell(setup.getValChar()).setParent(item);
		}
	}

	public void onClick$btnSimpan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		btnSimpan.setDisabled(true);
		BigDecimal hargaStd = null, discTot = null;

		HondaH100Fakdos fakdos = new HondaH100Fakdos();

		fakdos.setJenisBayar(tbValTag.getValue());
		fakdos.setKodeSalesman(getDologs().getKodeSalesman());
		fakdos.setLokasiAsal(getDologs().getLokasiAsal());
		fakdos.setOtr("F");

		if(dbPlafon.getValue().compareTo(dbAR.getValue()) >= 0){
			fakdos.setPlafon("Y");
		}else{
			fakdos.setPlafon("T");
		}

		fakdos.setPoDlrNoPoint(getDologs().getHondaH100Mstpodlrs().getNoPoint());
		fakdos.setTop(ibTop.getValue());

		fakdos.setKdDlr(getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getKdDlr());

		if(getDologs().getAks().equalsIgnoreCase("A")){
			String namaTagihan = "AKS-"+getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getDlrNama() 
					+  tbNamaDealerExt.getValue();
			if(namaTagihan.length() > 30){
				fakdos.setNamaTagihan(namaTagihan.substring(0,30));
			}else {
				fakdos.setNamaTagihan(namaTagihan);
			}
		}else{
			fakdos.setNamaTagihan(getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getDlrNama()
					+ tbNamaDealerExt.getValue());
		}

		fakdos.setAlamatTagihan(getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getDlrAlamat1()
				+ " " + getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getDlrAlamat2());

		fakdos.setAlamatPenerima(fakdos.getAlamatTagihan());
		System.out.println("alamat penerima = " + fakdos.getAlamatPenerima());

		fakdos.setStatus("A");
		fakdos.setNpwp(getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getDlrNpwp());

		fakdos.setBiayalain("T");
		fakdos.setNamaPenerima(getDologs().getHondaH100Mstpodlrs().getDealerKdDlr().getDlrNama());

		//podlr_no_point
		Set<TrioH100Dtldologs> dtlDologSet = new HashSet<TrioH100Dtldologs>();
		Set<HondaH100Dtlfakdos> dtlFakdosSet = new HashSet<HondaH100Dtlfakdos>();
		Listbox lb = (Listbox) lbDtldologs.getFellow("win").getFellow("lbDtldologs");
		List<Listitem> items = lb.getItems();

		for (Listitem item : items){
			List<Listcell> listLC = item.getChildren();
			TrioH100Dtldologs dtlDologs = (TrioH100Dtldologs) item.getValue();
			HondaH100Dtlfakdos dtlFakdos = new HondaH100Dtlfakdos();
			for (Listcell cell : listLC){

				if (cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 4){
					Decimalbox db = (Decimalbox) cell.getFirstChild();
					hargaStd = db.getValue();
					dtlFakdos.setHargaStd(hargaStd);
				}

				if (cell.getFirstChild() instanceof Intbox && cell.getColumnIndex() == 3){
					Intbox ibQtyApp = (Intbox) cell.getFirstChild();
					dtlFakdos.setQty(ibQtyApp.getValue().longValue());
					dtlFakdos.setQtySisa(dtlFakdos.getQty());
				}

				//sebelumnya disni

				if(cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 6){
					Decimalbox dbDiscCash = (Decimalbox) cell.getFirstChild();
					dtlDologs.setDiscCash(dbDiscCash.getValue());
				}

				if(cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 7){
					Decimalbox dbDiscPotDo = (Decimalbox) cell.getFirstChild();
					dtlDologs.setDiscPotDo(dbDiscPotDo.getValue());
				}

				if(cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 8){
					Decimalbox dbDiscSubsidi = (Decimalbox) cell.getFirstChild();
					dtlDologs.setDiscSubsidi(dbDiscSubsidi.getValue());
				}

				if (cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 5){
					Decimalbox dbDiskon = (Decimalbox) cell.getFirstChild();
					discTot = dbDiskon.getValue();
					dtlFakdos.setDiscount(dbDiskon.getValue());
				}

				if (cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 9){
					Decimalbox dbHargaNetto = (Decimalbox) cell.getFirstChild();
					BigDecimal value = dbHargaNetto.getValue().subtract(discTot);
					dtlFakdos.setHargaKosong(hargaStd.subtract(discTot));
					dtlFakdos.setHargaNetto(hargaStd.subtract(discTot));
				}


				dtlFakdos.setKdItem(dtlDologs.getKdItem());
				dtlDologs.setDiscCashPersen(dbDiskonPersen.getValue());

				dtlFakdos.setBiayaLain(new BigDecimal(0));
				dtlFakdos.setBiayaStnk(new BigDecimal(0));
				dtlFakdos.setQtyJual(0L);

			}
			BigDecimal k = (discTot.divide(hargaStd,MathContext.DECIMAL32)).multiply(new BigDecimal(100));
			dtlFakdos.setDiscountPersen(k);

			dtlDologSet.add(dtlDologs);
			dtlFakdosSet.add(dtlFakdos);
		}

		getDologs().setTrioH100DtldologsSet(dtlDologSet);
		fakdos.setHondaH100DtlfakdosSet(dtlFakdosSet);
		fakdos.setTrioH100Dologs(getDologs());

		String nodo = getMasterFacade().getHondaH100FakdosDao().saveTransaction(fakdos, getDologs().getNoDolog(),  getUserSession());
		tbNoDO.setValue(nodo);
		try {
			Messagebox.show("Transaksi berhasil dengan No DO "+nodo);
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}
		
		btnLOVValTag.setDisabled(true);
		btnBatal.setDisabled(true);
	}

	public void onClick$btnReset(){
		setSetup(null);
		setDologs(null);
		lbDtldologs.setModel(new ListModelList());
		binder.loadComponent(editGrid);
		btnLOVValTag.setDisabled(true);
		btnSimpan.setDisabled(true);
		dbDiskonPersen.setDisabled(true);
		dbDiskonPersen.setValue(new BigDecimal(0));
		tbNoDO.setValue(null);
		ibTop.setValue(0);
		ibTotQtyAp.setValue(0);
		ibTotQtyDO.setValue(0);
		dbPlafon.setValue(new BigDecimal(0));
		dbAR.setValue(new BigDecimal(0));
		dbOpenPlafon.setValue(new BigDecimal(0));
		dbEstimasiPlafonAwal.setValue(new BigDecimal(0));
		dbEstimasiPlafon.setValue(new BigDecimal(0));
		dbDiskonPersenPlafon.setValue(new BigDecimal(0));

		dbTotalDiskon.setValue(new BigDecimal(0));
		dbTotalHarga.setValue(new BigDecimal(0));
		dbTotalHargaNetto.setValue(new BigDecimal(0));
		dbTotalPPN.setValue(new BigDecimal(0));
		dbGrandTotal.setValue(new BigDecimal(0));
		btnBatal.setDisabled(true);
	}

	public void onClick$btnBatal(){
		String noDoBatal = getMasterFacade().getTrioH100DologsDao().batalDo(dologs.getNoDolog());
		alert("Berhasil batal DO " + noDoBatal);
		onClick$btnReset();
	}

	public void setGrid(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String persenPPN = getMasterFacade().getHondaH000SetupsDao().findValCharByValIdValTag("H1_CONFIG", "PPN");

		Listbox lb = (Listbox) lbDtldologs.getFellow("win").getFellow("lbDtldologs");
		List<Listitem> items = lb.getItems();
		Integer jumQty = 0;
		Integer totJumQty = 0;
		BigDecimal totalDiskon = new BigDecimal(0);
		BigDecimal totalHarga = new BigDecimal(0);
		BigDecimal totalHargaNetto = new BigDecimal(0);
		BigDecimal totalPPN = new BigDecimal(0);
		BigDecimal grandTotal = new BigDecimal(0);
		BigDecimal mtp = new BigDecimal(0);

		for (Listitem item : items){
			List<Listcell> listLC = item.getChildren();
			TrioH100Dtldologs dtlDologs = (TrioH100Dtldologs) item.getValue();
			HondaH100Dtlfakdos dtlFakdos = new HondaH100Dtlfakdos();


			for (Listcell cell : listLC){

				if(cell.getFirstChild() instanceof Intbox && cell.getColumnIndex() == 3){
					Intbox ib = (Intbox) cell.getFirstChild();
					jumQty = ib.getValue();
					totJumQty = totJumQty + ib.getValue();
				}

				if (cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 5){
					Decimalbox db = (Decimalbox) cell.getFirstChild();
					mtp = db.getValue().multiply(new BigDecimal(jumQty.intValue()));
				}

				if (cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 9){
					Decimalbox dbHargaNetto = (Decimalbox) cell.getFirstChild();
					totalHargaNetto = totalHargaNetto.add(dbHargaNetto.getValue());
				}

				if (cell.getFirstChild() instanceof Decimalbox && cell.getColumnIndex() == 10){
					Decimalbox dbHarga = (Decimalbox) cell.getFirstChild();
					totalHarga = totalHarga.add(dbHarga.getValue());
				}
				//Inner For
			}
			//OUTER for 
			totalDiskon = totalDiskon.add(mtp);
			System.out.println("total diskon = " + totalDiskon);

		}
		
		ibTotQtyAp.setValue(totJumQty);
		dbTotalDiskon.setValue(totalDiskon);
		dbTotalHarga.setValue(totalHarga);
		dbTotalHargaNetto.setValue(totalHarga.subtract(totalDiskon));
		totalPPN = (dbTotalHargaNetto.getValue()).multiply(new BigDecimal(persenPPN)).divide(new BigDecimal(100));
		grandTotal = (dbTotalHargaNetto.getValue()).add(totalPPN); 
		dbTotalPPN.setValue(totalPPN);
		dbGrandTotal.setValue(grandTotal);

		dbEstimasiPlafon.setValue(new BigDecimal(sumHarga)
		.add(dbGrandTotal.getValue()));
	}
	
	public static int safeLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}
	
}
