package com.trio.view.ctrl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Menu;
import org.zkoss.zul.Menubar;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 21, 2012
 */
public class MenuBar extends GenericForwardComposer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Menubar mb;
	private Menu menu;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		this.createMenu();
	}
	
	private void createMenu(){
		mb = new Menubar();
		menu = new Menu();
		menu.setLabel("Test Menu");
		menu.setParent(mb);
	}
	
	
	

}
