package com.trio.view.ctrl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Dealers;
import com.trio.bean.h000.HondaH000Setdlrs;
import com.trio.bean.h100.HondaH100Dtlinvs;
import com.trio.bean.h100.HondaH100Dtlpodlrfixes;
import com.trio.bean.h100.HondaH100Hargas;
import com.trio.bean.h100.HondaH100Mstpodlrs;
import com.trio.bean.h100.HondaH100Stglobals;
import com.trio.bean.h100.TrioH100Dologs;
import com.trio.bean.h100.TrioH100DtlHargas;
import com.trio.bean.h100.TrioH100Dtldologs;
import com.trio.bean.h300.HondaH300Salesman;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;
import com.trio.view.ctrl.renderer.MstpodlrRenderer;
import com.trio.view.ctrl.renderer.PopupPoInternalRenderer;
import com.trio.view.ctrl.renderer.SalesmanRenderer;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 15, 2012
 */
public class EntryDoLogistikCtrl extends TrioBasePage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Window winConfirm;

	private Radiogroup radStatus;
	private Checkbox cbAksesoris;
	private Button btCek;

	private Textbox tbKodeDealer;
	private Textbox tbNamaDealer;

	private List<HondaH000Dealers> list;
	private Popup popDealer;
	private Textbox popDlrNama;

	private Button popSearch;
	private Button popReset;
	private Listbox popDealerList;

	private Button btnLovDealer;

	private Textbox tbTglDoLogistik;
	private Textbox tbNoDoLogistik;

	private Button btnLovNoPoInternal;
	private Popup popInternal;
	private Listbox popInternalList;

	private Textbox tbNoPoInternal;

	private Button btnLovSalesman;
	private Textbox tbKodeSalesman;
	private Textbox tbNamaSalesman;

	private Popup popSalesman;
	private Listbox popSalesmanList;

	private Textbox tbLokasi;
	private Textbox tbAlamat;
	private Button btnLovLokasi;

	private Popup popLokasi;
	private Listbox popLokasiList;

	private Textbox tbOpenPlafon;

	private Button btnLovItem;
	private Button btnLovKdAks;
	private Button btnPopSearchItem;
	private Popup popItem;
	private Popup popKdAks;
	private Listbox popItemList;
	private Listbox popKdAksList;
	private Textbox kdItem;
	private Textbox kdPaketAks;
	private Textbox popKditem;

	private Intbox tbQtyOh, tbQtyBook, tbQtyNRFS, tbQtyRFS, tbQtyPO, tbQtySisa, tbQtyDO;
	private Button btnAddDetail;

	private List<TrioH100Dtldologs> listDtlDologs;
	private Listbox lbDtldologList;
	private Intbox ibJumQty;
	private Button btNew;
	private Button btSimpan;
	private Button btRefresh;
	private Button btDelete;

	private int qty = 0;
	private String kodeDealer="";

	private List<String> noPointListString;
	private List<String> kdItemList;
	private Map<String, String> mapKdAks;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		super.doAfterCompose(comp);
		listDtlDologs = new ArrayList<TrioH100Dtldologs>();
		btSimpan.setDisabled(true);
		tbNamaDealer.setDisabled(true);
		disableTextBox();
		mapKdAks = new HashMap<String, String>();
		System.out.println(getConnectionSession());
	}

	public void onCheck$cbAksesoris(){
		if(cbAksesoris.isChecked()){
			cbAksesoris.setValue("A");
			btnLovKdAks.setDisabled(false);
			System.out.println(cbAksesoris.getValue());
		}else{
			cbAksesoris.setValue("N");
			kdPaketAks.setValue(null);
			btnLovKdAks.setDisabled(true);
			System.out.println(cbAksesoris.getValue());
		}

	}

	//Otomatis Mengisi textbox namadealer dengan mengetik kode dealer saja
	public void onChange$tbKodeDealer(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String value = tbKodeDealer.getValue().toUpperCase().trim();
		tbKodeDealer.setValue(value);
		try {
			HondaH000Dealers dealers = getMasterFacade().getHondaH000DealersDao().findByKdDlr(value);
			tbNamaDealer.setValue(dealers.getDlrNama());
		} catch (Exception e) {
			e.printStackTrace();
			alert("Kode Dealer Tidak Ditemukan");
		}
		tbKodeDealer.setDisabled(true);
	}

	//popup Dealer
	public void onClick$btnLovDealer() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		list = getMasterFacade().getHondaH100MstpodlrsDao().findByStatus("N", null);
		popDealerList.setModel(new ListModelList(list));
		popDealerList.setItemRenderer(new MstpodlrRenderer());
	}

	public void onClick$popSearch(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String dlrNamaText = popDlrNama.getText().trim();
		List<HondaH000Dealers> list = getMasterFacade().getHondaH100MstpodlrsDao().findByStatus("N", dlrNamaText);
		popDealerList.setModel(new ListModelList(list));
		popDealerList.setItemRenderer(new MstpodlrRenderer());
	}

	public void onClick$popReset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		popDlrNama.setValue(null);
		List<HondaH000Dealers> list = getMasterFacade().getHondaH100MstpodlrsDao().findByStatus("N", null);
		popDealerList.setModel(new ListModelList(list));
		popDealerList.setItemRenderer(new MstpodlrRenderer());
	}

	public void onSelect$popDealerList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH000Dealers dlr = (HondaH000Dealers) popDealerList.getSelectedItem().getValue();
		tbKodeDealer.setValue(dlr.getKdDlr());
		tbNamaDealer.setValue(dlr.getDlrNama());
		tbOpenPlafon.setValue(""+getEstimasiOpenPlafon(dlr.getKdDlr()));
		popDealer.close();
		tbKodeDealer.setDisabled(true);
	}//end of popup dealer

	//popup Internal
	public void onClick$btnLovNoPoInternal(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		noPointListString = new ArrayList<String>();
		kdItemList = new ArrayList<String>();

		String kdDlrText = tbKodeDealer.getValue().trim();
		String bulan = TrioDateUtil.getCurrentDateTime("MM");
		String tahun = TrioDateUtil.getCurrentDateTime("yyyy");
		String jenisPo = radStatus.getSelectedItem().getValue();

		if(jenisPo.equalsIgnoreCase("A")){
			//ambil nopoint
			List<HondaH100Mstpodlrs> noPointLists = getMasterFacade().getHondaH100MstpodlrsDao().findWaitingListHeader(kdDlrText);
			System.out.println(noPointLists.size());
			
			//Jika noPointListst = 0 row, maka ditambahkan dengan noPoint buatan , agar tidak error pada saat
			//membaca detail kdItem pada bagian dtlPoList.
			if(noPointLists.size() <= 0){
				HondaH100Mstpodlrs dlrs = new HondaH100Mstpodlrs();
				dlrs.setNoPoint("");
				noPointLists.add(dlrs);
			}
			
			//noPointTdiDimasukkan ke List Baru
			for(HondaH100Mstpodlrs current : noPointLists){
				String noPoint = current.getNoPoint();
				noPointListString.add(noPoint);
			}

			
			//ambil kode Item berdasarkan list yang baru saja dimasukkan diatas
			List<HondaH100Dtlpodlrfixes> dtlPoList = getMasterFacade().getHondaH100DtlpodlrfixesDao().getKodeItemByNoPoint(noPointListString);
			
			
			//ambil kode itemnya dan masukkan ke list baru
			for(HondaH100Dtlpodlrfixes current : dtlPoList){
				kdItemList.add(current.getKdItem());
			}

			System.out.println("kdItemSize = " + kdItemList);
		}

		List<HondaH100Mstpodlrs> list = getMasterFacade().getHondaH100MstpodlrsDao()
				.findByStatusAndKdDlr("N", kdDlrText, jenisPo, bulan, tahun);
		popInternalList.setModel(new ListModelList(list));
		popInternalList.setItemRenderer(new PopupPoInternalRenderer());
	}

	public void onSelect$popInternalList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH100Mstpodlrs podlrs = (HondaH100Mstpodlrs) popInternalList.getSelectedItem().getValue();
		tbNoPoInternal.setValue(podlrs.getNoPoint());
		kodeDealer = podlrs.getDealerKdDlr().getKdDlr();
		popInternal.close();
	}//end of popupinternal

	//popup Salesman
	public void onClick$btnLovSalesman(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		List list = getMasterFacade().getHondaH300SalesmanDao().findByStatusAndBussiness("A", "A");
		popSalesmanList.setModel(new ListModelList(list));
		popSalesmanList.setItemRenderer(new SalesmanRenderer());
	}

	public void onSelect$popSalesmanList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH300Salesman sales = (HondaH300Salesman) popSalesmanList.getSelectedItem().getValue();
		tbKodeSalesman.setValue(sales.getKodeSales());
		tbNamaSalesman.setValue(sales.getNamaSales());
		popSalesman.close();
	}//end of popup salesman

	//popup Lokasi
	public void onClick$btnLovLokasi(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		List<String> list = getMasterFacade().getHondaH100StglobalsDao().findByQtyOnHandMoreZero();
		popLokasiList.setModel(new ListModelList(list));
		popLokasiList.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				String g = (String) data; 
				item.setValue(g);
				new Listcell(g).setParent(item);
			}
		});
	}

	public void onSelect$popLokasiList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String global = (String) popLokasiList.getSelectedItem().getValue();
		tbLokasi.setValue(global);
		String alamat = getMasterFacade().getHondaH100GudangsDao().findByLokasi(global).getAlamatLokasi();
		tbAlamat.setValue(alamat);
		popLokasi.close();
	}//end of popuplokasi

	public BigDecimal getEstimasiOpenPlafon(String kdDlr){
		/*
		 * EstimasiOpenPlafon = Plafon - AR - DO_Logistik
		 * Plafon = 
		 * AR =
		 * DO_Logistik =
		 */

		// getPlafon
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		BigDecimal plafon = new BigDecimal(0);
		HondaH000Setdlrs setDlr = getMasterFacade().getHondaH000SetdlrsDao().findByKdDealerAndKodeBisnis(kdDlr, "A");

		plafon = setDlr.getPlafon() == null ? new BigDecimal(0) : setDlr.getPlafon() ;

		//getAR
		BigInteger arDlr = getMasterFacade().getHondaH100ArsDao().getARByKdDlr(kdDlr);

		//getDO_Logistik
		BigDecimal doLogistik = getMasterFacade().getHondaH100FakdosDao().getSumHargaDOLogistisByKdDlr(kdDlr);

		BigDecimal arDlrDec = new BigDecimal(arDlr);
		arDlrDec.add(doLogistik);

		System.out.println("Jumlah plafon "+plafon);
		System.out.println("Jumlah AR "+arDlr);
		System.out.println("Jumlah DO Logistik "+doLogistik);
		System.out.println("Jumlah AR + DO Logistik "+arDlrDec);

		BigDecimal est = plafon.subtract(arDlrDec);
		return est;
	}

	//popup kd item
	public void onClick$btnLovItem(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		List<HondaH100Stglobals> list = getMasterFacade().getHondaH100StglobalsDao()
				.getKdItemByLokasiandPoDlr(tbLokasi.getValue().trim(), tbNoPoInternal.getValue().trim());
		popItemList.setModel(new ListModelList(list));
		popItemList.setItemRenderer(new ListitemRenderer() { 

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH100Stglobals globals = (HondaH100Stglobals) data;
				item.setValue(globals);

				new Listcell(globals.getKdItem()).setParent(item);
				new Listcell(globals.getQtyOnhand().toString()).setParent(item);
				if(globals.getQtyBook() == null){
					globals.setQtyBook(0L);
				}
				new Listcell(globals.getQtyBook().toString()).setParent(item);
			}
		});
	}

	//popup kodePaketAksesoris
	public void onClick$btnLovKdAks(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH000Dealers dealerArea = getMasterFacade().getHondaH000DealersDao().getDlrArea(tbKodeDealer.getValue());
		List<TrioH100DtlHargas> list = getMasterFacade().getTrioH100DtlHargasDao()
				.findByKdItem(kdItem.getValue().substring(0,3), dealerArea.getDlrArea());
		
		popKdAksList.setModel(new ListModelList(list));
		popKdAksList.setItemRenderer(new ListitemRenderer() {
			
			@Override
			public void render(Listitem item, Object data) throws Exception {
				TrioH100DtlHargas hargas = (TrioH100DtlHargas) data;
				String nilaiAks = TrioStringUtil.getPemisahRibuan(hargas.getNilaiAks());
				item.setValue(hargas);
				
				
				new Listcell(hargas.getTrioH100DtlHargasPK().getKdPktAks()).setParent(item);
				new Listcell(nilaiAks).setParent(item);
			}
		});
		
	}
	
	public void onClick$btnPopSearchItem(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH100Stglobals globals = new HondaH100Stglobals();
		globals.setKdItem(popKditem.getValue());
		globals.setLokasi(tbLokasi.getValue());
		popItemList.setModel(new ListModelList(getMasterFacade().getHondaH100StglobalsDao().findByCriteria(globals)));
		//		popItemList.setItemRenderer(new ListitemRenderer() {

	}

	//ketika item di lov kode item di klik
	public void onSelect$popItemList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH100Stglobals globals = (HondaH100Stglobals) popItemList.getSelectedItem().getValue();
		kdItem.setValue(globals.getKdItem());
		tbQtyOh.setValue(globals.getQtyOnhand().intValue());
		tbQtyBook.setValue(globals.getQtyBook().intValue());

		if(globals.getQtyRusak()==null || globals.getQtyRusak().equals(null)){
			tbQtyNRFS.setValue(0);
		}else{
			tbQtyNRFS.setValue(globals.getQtyRusak().intValue());
		}

		tbQtyRFS.setValue(tbQtyOh.getValue() - tbQtyBook.getValue());

		//get Qty PO
		Long qtyPO = getMasterFacade().getHondaH100DtlpodlrfixesDao().getQtyOrderByPoIntAndKdItem(tbNoPoInternal.getValue(), globals.getKdItem());
		tbQtyPO.setValue(qtyPO.intValue());
		//get Qty Open
		Long qtyOpen = getMasterFacade().getHondaH100DtlfakdosDao().getQtyPoOpenByPoIntAndKdItem(tbNoPoInternal.getValue(), globals.getKdItem());
		Long qtySisa = qtyPO - qtyOpen;
		tbQtySisa.setValue(qtySisa.intValue());
		kdPaketAks.setValue(null);

		popItem.close();
	}
	
	//ketika popup kd paket aksesoris di klik
	public void onSelect$popKdAksList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		TrioH100DtlHargas hargas = (TrioH100DtlHargas) popKdAksList.getSelectedItem().getValue();
		kdPaketAks.setValue(hargas.getTrioH100DtlHargasPK().getKdPktAks());
		
		popKdAks.close();
	}
	
	//END OF LOV KODE ITEM AREA


	public void onClick$btnAddDetail() throws InterruptedException{
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);

		String kdItemValue = kdItem.getValue().toUpperCase();
		String kodeDealerValue = tbKodeDealer.getValue().toUpperCase();

		HondaH100Hargas cekApprove = getMasterFacade().getHondaH100HargasDao()
				.getNilaiApprove(kdItemValue, kodeDealerValue);
		List<HondaH100Dtlinvs> listHarga = getMasterFacade().getHondaH100DtlinvsDao()
				.getHargaFakTerbaru(kdItemValue.substring(0,3));
		BigDecimal bd = getMasterFacade().getHondaH100HargasDao()
				.getHrgBeli(kdItemValue, kodeDealerValue);
		String strListHarga = "0";
		if(listHarga.size() > 0){
			strListHarga = String.valueOf(listHarga.get(0));
		}
		
		String strBd = String.valueOf(bd);
		BigDecimal hargaBeliTerakhir = new BigDecimal(strListHarga);

		//mencek agar pada listDtldologs tidak terdapat kdItem yang sama
		for(TrioH100Dtldologs current : listDtlDologs){
			if(current.getKdItem().equals(kdItem.getValue())){
				alert("Kode Item yang dimasukkan tidak boleh sama");
				return;
			}
		}

		if(tbQtyDO.getValue() > tbQtySisa.getValue()){
			alert("Nilai DO tidak boleh lebih besar dari Nilai sisa PO");
			return ;
		}else if (tbQtyDO.getValue() > tbQtyRFS.getValue()) {
			alert("Nilai O tidak boleh melebihi nilai RFS");
			return;
		}else if (tbQtyDO.getValue() <= 0 || tbQtyDO.getValue()==null){ 
			alert("Nilai DO tidak boleh dibawah 0 atau kosong");
			return;
		}else if(cekApprove == null){
			alert("Harga untuk kode item ini belom dimasukkan");
			return;
		}else if(!(cekApprove.getApprove() == 1)){
			alert("Harga Dealer untuk Kode Item ini Belum di Approve");
			return;
		}else if(bd.compareTo(hargaBeliTerakhir) == -1){
//			alert("Harga Beli Terakhir Tidak Sama Dengan Data Harga Beli Pada Master Harga");
			alert("Harga Master Lebih Kecil Dari Harga Beli Terakhir");
			return;
		}else if(cbAksesoris.isChecked() && (kdPaketAks.getValue() == null || kdPaketAks.getValue().equals(""))){
			alert("Kode Paket Aksesoris Harus Diisi");
			return;
		}else if (!kodeDealer.equals(tbKodeDealer.getValue())){
			System.out.println("kode dealer dari mspodealer = " + kodeDealer);
			alert("No PO Internal tidak sesuai dengan dealer yang dipilih");
			return;
		}else if (radStatus.getSelectedItem().getValue().equalsIgnoreCase("A")){
			if(kdItemList.contains(kdItemValue)){
				if(Messagebox.show("Kode Item[" +kdItemValue+"] terdapat di waiting list, Lanjutkan ?","New WMS", Messagebox.YES|Messagebox.NO,
						Messagebox.QUESTION)==Messagebox.NO){
					System.out.println("NO");
					return;
				}else{
					System.out.println("YES");
				}
			}
		}else {

			int qtyDo = tbQtyDO.getValue();
			int qtyOh = tbQtyOh.getValue();
			int qtyRfs = tbQtyRFS.getValue();
			int qtySisa = tbQtySisa.getValue();

			int resultOh = qtyOh - qtyDo;
			int resultRfs = qtyRfs - qtyDo;
			int resultSisa = qtySisa - qtyDo;

			tbQtyOh.setValue(resultOh);
			tbQtyRFS.setValue(resultRfs);
			tbQtySisa.setValue(resultSisa);
		}

		TrioH100Dtldologs dtldologs = new TrioH100Dtldologs();
		dtldologs.setKdItem(kdItemValue);
		dtldologs.setQty(tbQtyDO.getValue());
		listDtlDologs.add(dtldologs);

		System.out.println("Ukuran Dtl "+listDtlDologs.size()); 
		System.out.println("dari list = " + listDtlDologs.get(0));

		lbDtldologList.setModel(new ListModelList(listDtlDologs));
		lbDtldologList.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				final TrioH100Dtldologs dtl = (TrioH100Dtldologs) data;
				item.setValue(dtl);
				new Listcell(dtl.getKdItem()).setParent(item);
				new Listcell(String.valueOf(dtl.getQty())).setParent(item); 

				final Listcell lc = new Listcell();
				Checkbox cb = new Checkbox();
				cb.setParent(lc);
				lc.setParent(item);
			}
		});


		qty = ibJumQty.getValue() + tbQtyDO.getValue();
		ibJumQty.setValue(qty);
		
		mapKdAks.put(kdItemValue, kdPaketAks.getValue());

		btSimpan.setDisabled(false);
		btnLovDealer.setDisabled(true);
		btnLovNoPoInternal.setDisabled(true);
		btnLovSalesman.setDisabled(true);
		btnLovLokasi.setDisabled(true);
		cbAksesoris.setDisabled(true);
	}


	public void onClick$btSimpan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		int qty = 0;
		int totalHarga = 0;
		String kdItem ="";

		TrioH100Dologs dologs = new TrioH100Dologs();
		Set<TrioH100Dtldologs> dtldologs = new HashSet<TrioH100Dtldologs>();
		Listbox box = (Listbox) lbDtldologList.getFellow("win").getFellow("lbDtldologList");
		List<Listitem> lists = box.getItems();

		//membaca setiap row yang ada pada list
		for(Listitem currentList : lists){

			TrioH100Dtldologs dtl = (TrioH100Dtldologs) currentList.getValue();
			List<Component> com = currentList.getChildren();

			//membaca setiap column pada row
			for (Component currentComp : com){
				Listcell lc = (Listcell) currentComp;
				if(lc.getColumnIndex() == 0){
					kdItem = lc.getLabel();
				}

				if(lc.getColumnIndex()==1){
					qty = Integer.parseInt(lc.getLabel());
				}

				
				
				dtl.setQty(qty);
				dtl.setKdItem(kdItem);
				dtl.setKdPktAks(mapKdAks.get(kdItem));
				dtldologs.add(dtl);//masukan ke dalam set
			}

			if(kdItem.isEmpty() || kdItem==null){
				continue;
			}
			
			BigDecimal harga = getMasterFacade().getHondaH100HargasDao().getHargasByKdItemAndKdDlr(kdItem, tbKodeDealer.getValue());
			int iHarga = harga.intValue();
			int hasil = iHarga * qty;
			totalHarga += hasil; // total semua harga yang telah di kali qty
		}

		dologs.setKdDlr(tbKodeDealer.getValue());
		dologs.setPoDlrNoPont(tbNoPoInternal.getValue());
		dologs.setKodeSalesman(tbKodeSalesman.getValue());
		dologs.setNamaSalesman(tbNamaSalesman.getValue());
		dologs.setLokasiAsal(tbLokasi.getValue());
		dologs.setAlamatLokasi(tbAlamat.getValue());
		dologs.setStatus("N");
		dologs.setHarga(BigInteger.valueOf(totalHarga));
		dologs.setAks(cbAksesoris.getValue());
		dologs.setTrioH100DtldologsSet(dtldologs);

		//		simpan semua dtl yang sudah dimasukkan, dan mengambilikan nilai no_dolog
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String noDolog= getMasterFacade().getTrioH100DologsDao().saveTransaction(dologs, getUserSession());
		alert("Berhasil simpan dengan no dolog " + noDolog);
		tbNoDoLogistik.setValue(noDolog);
		disableButton();
	}

	public void onClick$btNew(){
		reset();
		btSimpan.setDisabled(true);
	}

	public void onClick$btRefresh(){
		lbDtldologList.clearSelection();
		lbDtldologList.setModel(new ListModelList());
		listDtlDologs = new ArrayList<TrioH100Dtldologs>();
		//		onSelect$popItemList();
		kdItem.setValue(null);
		tbQtyOh.setValue(null);
		tbQtyBook.setValue(null);
		tbQtyNRFS.setValue(null);
		tbQtyRFS.setValue(null);
		tbQtyPO.setValue(null);
		tbQtySisa.setValue(null);
		tbQtyDO.setValue(null);
		btSimpan.setDisabled(true);
		cbAksesoris.setDisabled(false);
	}

	public void onClick$btDelete() throws InterruptedException{

		Listbox box = (Listbox) lbDtldologList.getFellow("win").getFellow("lbDtldologList");
		List<Listitem> item = box.getItems();
		ListModel model = box.getModel();
		Checkbox checkBox = new Checkbox();
		String kdItem ="";
		String marma="";
		List<Component> coms;

		System.out.println(item);

		for(Listitem currentItem : item){
			List<Component> com =  currentItem.getChildren();
			for(Component currentCom : com){
				Listcell list = (Listcell) currentCom;

				if(list.getColumnIndex()==0){
					kdItem = (String) list.getLabel();
				}

				if(list.getFirstChild() instanceof Checkbox){
					Checkbox cb = (Checkbox) list.getFirstChild();
					checkBox = cb;

				}
			}

			if(checkBox.isChecked()){

				for(Component current : com){
					Listcell list = (Listcell) current;

					if(list.getColumnIndex()==0){
						marma = list.getLabel();
						System.out.println("MARMA!! = " + marma);
					}

					if(list.getColumnIndex()==1){
						String quantity = list.getLabel();
						Integer nQty = Integer.valueOf(quantity);
						ibJumQty.setValue(ibJumQty.getValue() - nQty);

						if(ibJumQty.getValue().equals(0)){
							btSimpan.setDisabled(true);
						}
					}
				}
				com.clear();
				afterDelete(marma);
			}
		}
	}


	private void afterDelete(String marma){

		Iterator<TrioH100Dtldologs> iterator = listDtlDologs.iterator();
		while (iterator.hasNext()) {
			TrioH100Dtldologs trioH100Dtldologs = (TrioH100Dtldologs) iterator
					.next();
			if(trioH100Dtldologs.getKdItem().equals(marma)){
				iterator.remove();
			}
		}
	}

	private void reset(){

		cbAksesoris.setDisabled(false);
		
		tbKodeDealer.setDisabled(false);
		tbKodeDealer.setValue(null);
		tbNamaDealer.setValue(null);
		tbNoPoInternal.setValue(null);
		tbKodeSalesman.setValue(null);
		tbNamaSalesman.setValue(null);
		tbLokasi.setValue(null);
		tbOpenPlafon.setValue(null);
		tbAlamat.setValue(null);
		tbNoDoLogistik.setValue(null);
		cbAksesoris.setChecked(false);
		cbAksesoris.setValue("N");

		kdItem.setValue(null);
		kdPaketAks.setValue(null);
		tbQtyOh.setValue(null);
		tbQtyBook.setValue(null);
		tbQtyNRFS.setValue(null);
		tbQtyRFS.setValue(null);
		tbQtyPO.setValue(null);
		tbQtySisa.setValue(null);
		tbQtyDO.setValue(null);
		ibJumQty.setValue(0);

		btnLovDealer.setDisabled(false);
		btnLovNoPoInternal.setDisabled(false);
		btnLovSalesman.setDisabled(false);
		btnLovLokasi.setDisabled(false);
		btnAddDetail.setDisabled(false);
		btDelete.setDisabled(false);
		btSimpan.setDisabled(false);
		btRefresh.setDisabled(false);

		lbDtldologList.clearSelection();
		lbDtldologList.setModel(new ListModelList());
		listDtlDologs = new ArrayList<TrioH100Dtldologs>();

	}

	private void disableTextBox(){
		//		tbKodeDealer.setDisabled(true);
		tbNamaDealer.setDisabled(true);
		tbNoPoInternal.setDisabled(true);
		tbKodeSalesman.setDisabled(true);
		tbNamaSalesman.setDisabled(true);
		tbLokasi.setDisabled(true);
		tbOpenPlafon.setDisabled(true);
		tbAlamat.setDisabled(true);
	}

	private void disableButton(){
		btnLovDealer.setDisabled(true);
		btnLovNoPoInternal.setDisabled(true);
		btnLovSalesman.setDisabled(true);
		btnLovLokasi.setDisabled(true);
		btnAddDetail.setDisabled(true);
		btDelete.setDisabled(true);
		btSimpan.setDisabled(true);
		btRefresh.setDisabled(true);
	}
}
