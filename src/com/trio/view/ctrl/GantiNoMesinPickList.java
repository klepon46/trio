package com.trio.view.ctrl;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h100.HondaH100Dtlpicklists;
import com.trio.bean.h100.HondaH100Hdrpicklists;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.DtlpicklistRenderer;
import com.trio.view.ctrl.renderer.PopupGantiNomesinPicklist;

public class GantiNoMesinPickList extends TrioBasePage {
	
	private static final long serialVersionUID = 1L;
	private AnnotateDataBinder binder;
	
	private Textbox tbNoPicklist;
	private Textbox tbNodo;
	private Textbox tbNamaDealer;
	
	private Grid grid;
	
	private Button btnLOV;
	private Button btnTampil;
	private Button btnSimpan;
	private Listbox lbPickList;
	
	private Button popBtnCari;
	private Button popBtnReset;
	private Popup popup;
	
	private HondaH100Hdrpicklists hondaH100Hdrpicklists;
	
	private Listbox lbDtlpicklist;
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		hondaH100Hdrpicklists = new HondaH100Hdrpicklists();
		
		btnSimpan.setDisabled(true);
		
	}
	
	public void onClick$btnTampil() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		List<HondaH100Dtlpicklists> list = getMasterFacade().getHondaH100DtlpicklistsDao().findByVnopicklist(tbNoPicklist.getValue());
		lbDtlpicklist.setModel(new ListModelList(list));
		lbDtlpicklist.setItemRenderer(new DtlpicklistRenderer());
		
		btnSimpan.setDisabled(false);
	}
	
	public void onClick$btnSimpan() throws InterruptedException {
		System.out.println("simpan ditekan");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		int checked = 0;
		
		List<HondaH100Dtlpicklists> dtlPicklist = new ArrayList<HondaH100Dtlpicklists>();
		List<HondaH100Dtlpicklists> dtlPicklistLama = new ArrayList<HondaH100Dtlpicklists>();
		
		String vnomesin = null;
		String vsel = null;
		String noMesinLabel = null;
		String kdItemAwal = null;
		String selLabel = null;
		Listbox box = (Listbox) lbDtlpicklist.getFellow("win").getFellow("lbDtlpicklist");
		List<Listitem> item = box.getItems();
		
		for (Listitem listItem : item){
			HondaH100Dtlpicklists dtl = new HondaH100Dtlpicklists();
			HondaH100Dtlpicklists d = new HondaH100Dtlpicklists();
			List<Component> com = listItem.getChildren();
			for (Component compCell : com){	
				Listcell list = (Listcell) compCell;
				
				if (list.getColumnIndex() == 0){
					noMesinLabel = list.getLabel(); 
				}
				
				if(list.getColumnIndex() == 1){
					kdItemAwal = list.getLabel();
				}
				
				if (list.getColumnIndex() == 2){
					selLabel = list.getLabel(); 
				}
				
				if (list.getFirstChild() instanceof Textbox && list.getColumnIndex() == 4){
					Textbox tb = (Textbox) list.getFirstChild();
					vnomesin = tb.getValue();
				}
				if (list.getFirstChild() instanceof Textbox && list.getColumnIndex() == 5){
					Textbox tb = (Textbox) list.getFirstChild();
					vsel = tb.getValue();
				}
				if (list.getFirstChild() instanceof Checkbox){
					Checkbox cb = (Checkbox) list.getFirstChild();
					if (cb.isChecked()){
						
						d.setVnopicklist(tbNoPicklist.getValue());
						d.setVnomesin(vnomesin);
						d.setVsel(vsel);
						dtlPicklist.add(d);
						dtl.setVnopicklist(tbNoPicklist.getValue()); 
						dtl.setVnomesin(noMesinLabel);
						dtl.setVsel(selLabel);
						
						dtlPicklistLama.add(dtl);
						checked++;
					}
				}
			}
		}
		
		if (checked < 1){
			Messagebox.show("Checkbok belum dipilih");
			return;
		}
		
		try{
			getMasterFacade().getHondaH100DtlpicklistsDao().updateDtlpicklistStdetailDtlsublokasi(dtlPicklistLama, dtlPicklist, getUserSession());
			Messagebox.show("Transaksi berhasil");
			reset();
		}catch (Exception e) {
			e.printStackTrace();
			Messagebox.show("Terjadi Error "+e.getMessage());
		}
		
		
	}
	
	public void onClick$btnReset() throws InterruptedException {
		reset();
	}
	
	public void onClick$btnResetDetail() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		List<HondaH100Dtlpicklists> list = getMasterFacade().getHondaH100DtlpicklistsDao().findByVnopicklist(tbNoPicklist.getValue());
		lbDtlpicklist.setModel(new ListModelList(list));
		lbDtlpicklist.setItemRenderer(new DtlpicklistRenderer());
		
		btnSimpan.setDisabled(false);
	}
	
	public void reset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		lbDtlpicklist.setModel(new ListModelList(new ArrayList<HondaH100Dtlpicklists>())); 
		tbNoPicklist.setValue(null);
		tbNodo.setValue(null);
		tbNamaDealer.setValue(null);
		btnSimpan.setDisabled(true);
	}
	
	public void onClick$btnLOV(){
		System.out.println("INI LOV");
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		List<HondaH100Hdrpicklists> list = getMasterFacade().getHondaH100HdrpicklistsDao().findByVstatusAndVjenispick("0", "A");
		lbPickList.setModel(new ListModelList(list));
		lbPickList.setItemRenderer(new PopupGantiNomesinPicklist());
	}
	
	public void onClick$popBtnCari() throws InterruptedException {
		
	}
	
	public void onClick$popBtnReset() throws InterruptedException {
		
	}
	
	public void onSelect$lbPickList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		hondaH100Hdrpicklists = (HondaH100Hdrpicklists) lbPickList.getSelectedItem().getValue();
		
		tbNoPicklist.setValue(hondaH100Hdrpicklists.getVnopicklist());
		tbNodo.setValue(hondaH100Hdrpicklists.getHondaH100Fakdos().getNoDo());
		tbNamaDealer.setValue(hondaH100Hdrpicklists.getHondaH100Fakdos().getHondaH000Dealers().getDlrNama());
		popup.close();
	}

	public HondaH100Hdrpicklists getHondaH100Hdrpicklists() {
		return hondaH100Hdrpicklists;
	}

	public void setHondaH100Hdrpicklists(HondaH100Hdrpicklists hondaH100Hdrpicklists) {
		this.hondaH100Hdrpicklists = hondaH100Hdrpicklists;
	}

}
