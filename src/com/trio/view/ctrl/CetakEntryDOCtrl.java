package com.trio.view.ctrl;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.zk.ui.Component;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;

import com.trio.base.TrioBasePage;
import com.trio.bean.bsc.AhmdsbscDtlsetting;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;

public class CetakEntryDOCtrl extends TrioBasePage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Datebox dateAwal;
	private Datebox dateAkhir;
	private Button btnReport;
	private Button btnReset;
	private Jasperreport report;
	private String reportSrc;
	Map parameters = new HashMap();
	
	@Override
	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
	}
	
	public CetakEntryDOCtrl() throws SQLException{
		super();
	}
	
	public void showReport(String tglAwal, String tglAkhir){
		
		 File file = null;
		 String path = null;
		 
		 AhmdsbscDtlsetting dtl = getMasterFacade().getAhmdsbscDtlsettingDao().findByVidAndVitemcode("RPT001_RPTCFG", "RPTDIRNAME");
		 
		 
		 if(dtl != null){
			 path = dtl.getVitemname();
		 }
		 
		 
//		 file = new File(path+"rekapEntry.jasper");
		 file = new File(path+"rekapEntryDO_1.jasper");
		 
		 if(file != null){
			 reportSrc = file.getAbsolutePath();
			 System.out.println("reportSrc = "  + reportSrc);
		 }else{
			 reportSrc = null;
		 }
		 
//		 System.out.println("isi paramters = " + parameters);
		 
		 parameters.put("TGL_AWAL", tglAwal);
		 parameters.put("TGL_AKHIR", tglAkhir);
		 
		 report.setSrc(reportSrc);
		 report.setParameters(parameters);
		 report.setDataConnection(getReportConnection());
		 report.setType("pdf");
		 
	}
	
	
	public void onClick$btnReport(){
		SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
		String tglAwal = sd.format(dateAwal.getValue());
		String tglAkhir = sd.format(dateAkhir.getValue());
		
		showReport(tglAwal, tglAkhir);
	}
	
	public void onBlur$dateAwal(){
			SimpleDateFormat sd= new SimpleDateFormat("yyyyMMdd");
			String sdDateAwal = sd.format(dateAwal.getValue());
			dateAkhir.setConstraint("after "+sdDateAwal);
//			dateAkhir.setConstraint("no empty");
	}
	
	public void onClick$btnReset(){
		dateAwal.setValue(null);
		dateAkhir.setValue(null);
		report.setSrc(null);
	}
}
