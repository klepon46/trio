package com.trio.view.ctrl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.crypto.Data;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.zkoss.zk.ui.Component;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.api.Datebox;

import com.trio.base.TrioBasePage;
import com.trio.bean.bsc.DbaRolePrivs;
import com.trio.bean.h000.HondaH000Dealers;
import com.trio.bean.h100.TrioH100DtlAreaDealers;
import com.trio.bean.h100.TrioH100PelangWils;
import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;
import com.trio.view.ctrl.renderer.DealerListRenderer;
import com.trio.view.ctrl.renderer.StdetailListRenderer;
import com.trio.view.ctrl.renderer.StdetailListRenderer2;
import com.trio.view.ctrl.renderer.StdetailListRendererBpkb;
import com.trio.view.ctrl.renderer.StdetailListRendererSpv;

public class PelangwilCtrl extends TrioBasePage {

	private static final long serialVersionUID = 20111130143824L;
	private AnnotateDataBinder binder;

	private Textbox kdDlr;
	private Button LOV;

	private Popup popDealer;
	private Listbox popDealerList;

	private Radiogroup radioApprove;
	private Radio r1;
	private Radio r2;
	private Button btnTampil;
	private Checkbox cbJatah;

	private Listbox dealerList;
	private Grid editDealerGrid;

	private TrioH100DtlAreaDealers _dealer;

	private Listbox stDetailList;

	private Button btnSimpan;
	private Button btnExport;
	private Button btnReset;
	private Button close;

	private String nilaiPwConfig;
	private String noApproval;
	private Label lblNoApproval;

	private HondaH000Dealers popObject;
	private Textbox popKdDlr;
	private Textbox popDlrNama;

	private Button popSearch;
	private Button popReset;

	private Datebox startDate;
	private Datebox endDate;

	private boolean isMgr;
	private List<DbaRolePrivs> isMgr2;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		_dealer = new TrioH100DtlAreaDealers();
		btnSimpan.setDisabled(true);
		cbJatah.setVisible(false);
		nilaiPwConfig = getMasterFacade().getHondaH000SetupsDao().findByValIdValTag("H1_CONFIG", "NOMINAL_PELANG_WIL").getValChar();
		popObject = new HondaH000Dealers();

		this.isMgr2 =  getMasterFacade().getDbaRolePrivsDao().getRoleByGrantee(getUserSession());
		//		this.isMgr = getMasterFacade().getDbaRolePrivsDao().isManagerByUserSession(getUserSession());

	}

	private ListModelList getModel() {
		return (ListModelList) dealerList.getModel();
	}

	public TrioH100DtlAreaDealers getDealer() {
		return _dealer;
	}

	public void setDealer(TrioH100DtlAreaDealers dealer) {
		_dealer = dealer;
	}

	public void onClick$btnTampil() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		TrioH100Stdetails st = new TrioH100Stdetails();
		st.setKdDlr(_dealer.getKdDlr().toUpperCase());
		String statusPw = radioApprove.getSelectedItem().getValue();
		st.setStatusPw(statusPw);
		System.out.println("kode dealer = " + st.getKdDlr());

		if(st.getKdDlr().equals("%")){
			TrioH100Stdetails stNew = new TrioH100Stdetails();
			stNew.setStatusPw(statusPw);

			if(stNew.getStatusPw().equalsIgnoreCase("A")){

				if (isMgr2.contains("H1_MONITORING_PW_MGR")){
					stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(stNew)));
					stDetailList.setItemRenderer(new StdetailListRenderer());
					System.out.println("manager login ");
					btnSimpan.setDisabled(false);
				}else if(isMgr2.contains("H1_MONITORING_PW_FAK")){
					stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(stNew)));
					System.out.println("BPKB LOGIN");
					stDetailList.setItemRenderer(new StdetailListRendererBpkb());
					btnSimpan.setDisabled(true);
				}else{
					stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(stNew)));
					stDetailList.setItemRenderer(new StdetailListRendererSpv());
					System.out.println("supervisor login ");
					cbJatah.setVisible(true);
					cbJatah.setChecked(false);
					btnSimpan.setDisabled(false);
				}
			}else{
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(stNew)));
				stDetailList.setItemRenderer(new StdetailListRenderer2());
				btnSimpan.setDisabled(true);
			}

			lblNoApproval.setValue(null);
			btnExport.setDisabled(false);
			return;
		}


		if (statusPw.equalsIgnoreCase("A")){

			if (isMgr2.contains("H1_MONITORING_PW_MGR")){
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(st)));
				stDetailList.setItemRenderer(new StdetailListRenderer());
				System.out.println("manager login ");
				btnSimpan.setDisabled(false);
			}else if(isMgr2.contains("H1_MONITORING_PW_FAK")){
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(st)));
				System.out.println("BPKB LOGIN");
				stDetailList.setItemRenderer(new StdetailListRendererBpkb());
				btnSimpan.setDisabled(true);
			}else {
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(st)));
				stDetailList.setItemRenderer(new StdetailListRendererSpv());
				System.out.println("supervisor login ");
				cbJatah.setVisible(true);
				cbJatah.setChecked(false);
				btnSimpan.setDisabled(false);
			}

		}else {
			//stDetailList.setModel(new ListModelList(getMasterFacade().getStdetailDao().getStdetailByCriteria(st))); 
			stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(st)));
			stDetailList.setItemRenderer(new StdetailListRenderer2());
			btnSimpan.setDisabled(true);
		}
		lblNoApproval.setValue(null);
		btnExport.setDisabled(false);
	}


	public void onCheck$cbJatah(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		System.out.println("checkbox di check ");
		String nilaiPw = "";

		Listbox box = (Listbox) stDetailList.getFellow("win").getFellow("stDetailList");
		List<Listitem> item = box.getItems();
		ListModel model = box.getModel();

		for (Listitem listItem : item){
			TrioH100Stdetails st = (TrioH100Stdetails)listItem.getValue();

			List<Component> com = listItem.getChildren();
			for (Component compCell : com){	
				Listcell list = (Listcell) compCell;
				if (list.getColumnIndex()== 12){
					nilaiPw = (String) list.getLabel();
					list.setLabel("0");
					System.out.println("label column index 12 = "+nilaiPw);

					if(cbJatah.isChecked()==false){
						list.setLabel(TrioStringUtil.getPemisahRibuan(nilaiPwConfig));
					}

					if (nilaiPw.contains(".")){
						nilaiPw = nilaiPw.replaceAll("\\.", "");
					}

				}
			}
		}

	}

	public void onClick$btnSimpan() throws InterruptedException{ 
		System.out.println("This is btnSimpan");
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String dealer1="".toUpperCase().trim();

		int loop = 0;
		int check = 0;
		int nilai = 0;
		long count = 0;

		String nilaiPw = "";
		String bulanLoop="";
		String namaDealer = "".trim();
		String noMesin = "";	
		Checkbox checkBox = new Checkbox();

		TrioH100PelangWils pw = new TrioH100PelangWils();
		Set<TrioH100Stdetails> setStdetail = new HashSet<TrioH100Stdetails>();
		Listbox box = (Listbox) stDetailList.getFellow("win").getFellow("stDetailList");
		List<Listitem> item = box.getItems();
		ListModel model = box.getModel();

		for (Listitem listItem : item){
			TrioH100Stdetails st = (TrioH100Stdetails)listItem.getValue();

			List<Component> com = listItem.getChildren();
			for (Component compCell : com){	
				Listcell list = (Listcell) compCell;
				if (isMgr2.contains("H1_MONITORING_PW_MGR"))
				{
					if (list.getFirstChild() instanceof Intbox){
						Intbox ib = (Intbox) list.getFirstChild();
						nilaiPw = ib.getValue().toString();
					}
				}else{
					if (list.getColumnIndex()== 12){
						nilaiPw = (String) list.getLabel();
						if (nilaiPw.contains(".")){
							nilaiPw = nilaiPw.replaceAll("\\.", "");
						}
					}
				}

				if(list.getColumnIndex()==1){
					namaDealer = list.getLabel();
				}

				if(list.getColumnIndex()==0){
					noMesin = list.getLabel();
				}

				if (list.getFirstChild() instanceof Checkbox){
					Checkbox cb = (Checkbox) list.getFirstChild();
					checkBox = cb;

					if (cb.isChecked()){
						check++;

						if (nilaiPw == null || nilaiPw.equalsIgnoreCase("")){
							Messagebox.show("Nilai pelanggaran masih kosong");
							return;
						}


						if(namaDealer!=null && nilai==0){
							dealer1 = namaDealer;
							System.out.println("dealer 1 = " + dealer1);
							System.out.println("nama dealer = " + namaDealer);
							nilai++;
						} 


						if(!namaDealer.equals(dealer1)){
							System.out.println("nama dealer1 dalam equal = " + dealer1);
							System.out.println("nama dealer dalam equal = " + namaDealer);
							System.out.println("tidak sama");
							Messagebox.show("Kode dealer harus sama", "Error", Messagebox.YES, Messagebox.ERROR);
							return;
						}

						if (Integer.valueOf(nilaiPw) < 0){
							Messagebox.show("Nilai pelanggaran wilayah kurang dari 0");
							return;
						}

						st.setNilaiPw(Integer.valueOf(nilaiPw));	
						setStdetail.add(st);
					}
				}

			}//inner for


			if(checkBox.isChecked()){
				if(!isMgr2.contains("H1_MONITORING_PW_MGR")){
					for(Component current : com){
						Listcell listTod = (Listcell) current;

						if(listTod.getColumnIndex()==0){
							String noMesinTod = listTod.getLabel();
							System.out.println("noMesintod = " + noMesinTod);
							SimpleDateFormat formatter= new SimpleDateFormat("MM-yyyy");
							Date date = getMasterFacade().getHondaH100ItemcustsDao().getTglMohonStnk(noMesinTod);
							String bulan = formatter.format(date);

							String bulanAmbilJumlah = formatter.format(TrioDateUtil.addMonth(date, -1));
							BigDecimal getJumlah = getMasterFacade().getHondaH100ItemcustsDao().getJumlahPenjualan(bulanAmbilJumlah, dealer1);
							BigDecimal getJatah = getJumlah.multiply(new BigDecimal(0.06));
							getJatah = getJatah.setScale(0, RoundingMode.HALF_UP);
							System.out.println("bulan = " + bulan);

							if(loop == 0){
								bulanLoop = bulan;
								count = getMasterFacade().getTrioH100PelangWilsDao().getCountApprove(dealer1, bulan);
								System.out.println("count awal = " + count);
								count = count + 1;
								loop++;
							}

							if(!bulanLoop.equals(bulan) && loop > 0){
								count = getMasterFacade().getTrioH100PelangWilsDao().getCountApprove(dealer1, bulan);
								System.out.println("count awal = " + count);
								count = count + 1;
							}

							System.out.println("count = " + count);
							System.out.println("jatah = " + getJatah.longValue());
							if(cbJatah.isChecked()&& count > getJatah.longValue()){
								System.out.println("Nilai Pw dalam if = " + nilaiPw);
								Messagebox.show("Approve [" + noMesinTod+ "] tanpa biaya gagal karena melebihi jatah");
								return;
							}
							count++;
							System.out.println("sukses");
						}
					}
				}
			}
		}//outer for
		if (check < 1){
			Messagebox.show("Data tidak ada yang dipilih");
			return;
		}


		pw.setKdDlr(dealer1);
		pw.setStDetails(setStdetail);
		String noApproval =  getMasterFacade().getTrioH100PelangWilsDao().saveTransaction(pw, getUserSession());
		lblNoApproval.setValue("No Approval "+noApproval);
		Messagebox.show("Transaksi berhasil dengan No Approval :"+noApproval);
		cbJatah.setChecked(false);
		checkBox.setChecked(false);
		TrioH100Stdetails st = new TrioH100Stdetails();
		st.setKdDlr(_dealer.getKdDlr().toUpperCase());
		st.setStatusPw("A");

		if(kdDlr.getValue().equals("%")){
			TrioH100Stdetails stNew = new TrioH100Stdetails();
			stNew.setStatusPw("A");

			if (isMgr2.contains("H1_MONITORING_PW_MGR")){
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(stNew)));
				stDetailList.setItemRenderer(new StdetailListRenderer());
				System.out.println("manager login ");
			}else {
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(stNew)));
				stDetailList.setItemRenderer(new StdetailListRendererSpv());
				System.out.println("supervisor login ");
			}

		}else{

			if (isMgr2.contains("H1_MONITORING_PW_MGR")){
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(st)));
				stDetailList.setItemRenderer(new StdetailListRenderer());
				System.out.println("manager login ");
			}else {
				stDetailList.setModel(new ListModelList(getMasterFacade().getTrioH100StdetailsDao().findByExample(st)));
				stDetailList.setItemRenderer(new StdetailListRendererSpv());
				System.out.println("supervisor login ");
			}
		}
	}

	public void onClick$btnExport() throws IOException, SQLException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String dateAwal = sdf.format(startDate.getValue());
		String dateAkhir = sdf.format(endDate.getValue());

		if(radioApprove.getSelectedItem().getValue().equalsIgnoreCase("A")){
			excelBelumApprove();
		}else{
			excelSudahApprove(dateAwal,dateAkhir);
		}

	}

	public void onClick$btnReset(){
		reset();
		r1.setSelected(true);
	}

	public void onClick$close(){


	}

	//START OF LOV AREA
	public void onClick$LOV() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		//untuk memberishkan listbox
		reset();

		popKdDlr.setValue("");
		int x = 0;
		String kode = "";
		List<TrioH100Stdetails> list1 = getMasterFacade().getTrioH100StdetailsDao()
				.findByQuery(radioApprove.getSelectedItem().getValue());
		List<TrioH100Stdetails> nLists = new ArrayList<TrioH100Stdetails>();

		//Distinct kode dealer
		for(TrioH100Stdetails current :list1){
			x++;

			if(x == 1) {
				kode = current.getKdDlr();
				nLists.add(current);
			}

			if(x > 1 && !kode.equals(current.getKdDlr())){
				kode = current.getKdDlr();
				nLists.add(current);
			}
		}

		popDealerList.setModel(new ListModelList(nLists));
		popDealerList.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				TrioH100Stdetails stDetails = (TrioH100Stdetails) data;
				item.setValue(stDetails);
				new Listcell(stDetails.getKdDlr()).setParent(item);
				new Listcell(stDetails.getDealer().getDlrNama()).setParent(item);
				new Listcell(stDetails.getDealer().getDlrAlamat1()).setParent(item);
			}
		});
	}

	public void onSelect$popDealerList(){
		//		HondaH000Dealers dlr = (HondaH000Dealers) popDealerList.getSelectedItem().getValue();
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		TrioH100Stdetails stDtl = (TrioH100Stdetails) popDealerList.getSelectedItem().getValue();
		System.out.println("Kd dler = "+stDtl.getKdDlr());
		_dealer.setKdDlr(stDtl.getKdDlr());
		binder.loadComponent(kdDlr);
		popDealer.close();
		kdDlr.setDisabled(true);
	}

	public void onClick$popSearch() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		int x = 0;
		String kode = "";
		List<TrioH100Stdetails> list1 = getMasterFacade().getTrioH100StdetailsDao()
				.findByQuery( popKdDlr.getValue().toUpperCase(), radioApprove.getSelectedItem().getValue());
		List<TrioH100Stdetails> nLists = new ArrayList<TrioH100Stdetails>();

		//Distinct kode dealer
		for(TrioH100Stdetails current :list1){
			x++;

			if(x == 1) {
				kode = current.getKdDlr();
				nLists.add(current);
			}

			if(x > 1 && !kode.equals(current.getKdDlr())){
				kode = current.getKdDlr();
				nLists.add(current);
			}
		}
		popDealerList.setModel(new ListModelList(nLists));
	}

	public void onClick$popReset() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		System.out.println("klik pop btnReset");
		List<HondaH000Dealers> listDealer = getMasterFacade().getHondaH000DealersDao().findAll();
		popDealerList.setModel(new ListModelList(listDealer));
		popDealerList.setItemRenderer(new DealerListRenderer());
		popKdDlr.setValue(null);
		popDlrNama.setValue(null);
		popObject = new HondaH000Dealers();
	}
	//END OF LOV AREA

	public void onCheck$r1(){
		startDate.setDisabled(true);
		endDate.setDisabled(true);
		startDate.setValue(null);
		endDate.setValue(null);
	}

	public void onCheck$r2(){
		startDate.setDisabled(false);
		endDate.setDisabled(false);
		startDate.setValue(new Date());
		endDate.setValue(new Date());
	}


	private void reset(){
		btnSimpan.setDisabled(true);
		btnExport.setDisabled(true);
		kdDlr.setValue(null);
		stDetailList.setModel(new ListModelList());
		stDetailList.clearSelection();
		lblNoApproval.setValue(null);
		kdDlr.setDisabled(false);
		cbJatah.setVisible(false);
	}

	private void eksportExcelFromListBox(Listbox box, String noFile) throws IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HSSFWorkbook workbook  = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Sheet 1");

		HSSFRow row = sheet.createRow(0);
		HSSFFont fontRedBold = workbook.createFont();
		HSSFFont fontNormal = workbook.createFont();
		fontRedBold.setColor(HSSFFont.COLOR_RED);
		fontRedBold.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		fontNormal.setColor(HSSFFont.COLOR_NORMAL);
		fontNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

		//Style
		HSSFCellStyle cellStyleRedBold = workbook.createCellStyle();
		HSSFCellStyle cellStyleNormal = workbook.createCellStyle();
		cellStyleRedBold.setFont(fontRedBold);
		cellStyleNormal.setFont(fontNormal);

		//headers
		int i = 0;
		row = sheet.createRow(0);
		for(Object head : box.getHeads()){
			for(Object header : ((Listhead)head).getChildren()){
				String h = ((Listheader)header).getLabel();
				HSSFCell cell = row.createCell(i);
				cell.setCellStyle(cellStyleRedBold);
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(h);
				i++;
			}
		}


		int x = 1;
		int y = 0;
		for(Object item : box.getItems()){
			row = sheet.createRow(x);
			y=0;
			for(Object lbCell : ((Listitem)item).getChildren()){
				String h;
				h = ((Listcell)lbCell).getLabel();
				HSSFCell cell = row.createCell(y);
				cell.setCellStyle(cellStyleNormal);
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);
				cell.setCellValue(h);
				y++;
			}
			x++;
		}


		FileOutputStream  fOut = new FileOutputStream(noFile);

		workbook.write(fOut);
		fOut.flush();

		fOut.close();

		File file = new File(noFile);
		Filedownload.save(file, "XLS");
	}


	private void excelBelumApprove() throws SQLException, IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		Connection connection = getReportConnection();
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();


		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = "select " +
				"this_.NO_MESIN as NO_MESIN, " +
				"this_.KD_DLR as Kode_Dealer, " +
				"this_.NAMA_KONSUMEN as NAMA_KONSUMEN, " +
				"hondah100c2_.ALAMAT1 as ALAMAT, " +
				"trioh100ke5_.KOTA as KOTA, " +
				"trioh100ke5_.KECAMATAN as KECAMATAN, " +
				"trioh100ke5_.KELURAHAN as KELURAHAN, " +
				"(select VAL_CHAR from HONDA_H000_SETUPS where VAL_ID = 'H1_CONFIG' and VAL_TAG = 'NOMINAL_PELANG_WIL') as NILAI_PW_CONFIG, " +
				"this_.NO_APPROVAL_PW as NO_APPROVAL, " +
				"trioh100pe6_.APPROVED_BY as APPROVED_BY, " +
				"trioh100pe6_.TGL_TAGIH_PW as TGL_TAGIH, " +
				"trioh100pe6_.TGL_LUNAS_PW as TGL_LUNAS, " +
				"this_.NILAI_PW as NILAI_PW " +
				"from TRIO_H100_STDETAILS this_, " +
				"HONDA_H100_CUSTOMERS hondah100c2_, " +
				"HONDA_H000_DEALERS hondah000d3_, " +
				"HONDA_H000_ITEMS hondah000i4_, " +
				"TRIO_H100_KELURAHANS trioh100ke5_, " +
				"TRIO_H100_PELANGWILS trioh100pe6_ " +
				"where this_.KONSUMEN_ID=hondah100c2_.KONSUMEN_ID(+) " +
				"and this_.KD_DLR=hondah000d3_.KD_DLR(+) " +
				"and this_.KD_ITEM=hondah000i4_.KD_ITEM(+) " +
				"and this_.KD_KEL=trioh100ke5_.KD_KEL(+) " +
				"and this_.NO_APPROVAL_PW=trioh100pe6_.NO_APPROVAL_PW(+) " +
				"and ( this_.STATUS_PW='A' )";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream("belomApprove.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("belomApprove.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	private void excelSudahApprove(String dateAwal, String dateAkhir) throws SQLException, IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		Connection connection = getReportConnection();
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();


		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select " +
						"this_.NO_MESIN as NO_MESIN, " +
						"this_.KD_DLR as Kode_Dealer, " +
						"this_.NAMA_KONSUMEN as NAMA_KONSUMEN, " +
						"hondah100c2_.ALAMAT1 as ALAMAT, " +
						"trioh100ke5_.KOTA as KOTA, " +
						"trioh100ke5_.KECAMATAN as KECAMATAN, " +
						"trioh100ke5_.KELURAHAN as KELURAHAN, " +
						"(select VAL_CHAR from HONDA_H000_SETUPS where VAL_ID = 'H1_CONFIG' and VAL_TAG = 'NOMINAL_PELANG_WIL') as NILAI_PW_CONFIG, " +
						"this_.NO_APPROVAL_PW as NO_APPROVAL, " +
						"trioh100pe6_.APPROVED_BY as APPROVED_BY, " +
						"trioh100pe6_.TGL_TAGIH_PW as TGL_TAGIH, " +
						"trioh100pe6_.TGL_LUNAS_PW as TGL_LUNAS, " +
						"this_.NILAI_PW as NILAI_PW " +
						"from TRIO_H100_STDETAILS this_, " +
						"HONDA_H100_CUSTOMERS hondah100c2_, " +
						"HONDA_H000_DEALERS hondah000d3_, " +
						"HONDA_H000_ITEMS hondah000i4_, " +
						"TRIO_H100_KELURAHANS trioh100ke5_, " +
						"TRIO_H100_PELANGWILS trioh100pe6_ " +
						"where this_.KONSUMEN_ID=hondah100c2_.KONSUMEN_ID(+) " +
						"and this_.KD_DLR=hondah000d3_.KD_DLR(+) " +
						"and this_.KD_ITEM=hondah000i4_.KD_ITEM(+) " +
						"and this_.KD_KEL=trioh100ke5_.KD_KEL(+) " +
						"and this_.NO_APPROVAL_PW=trioh100pe6_.NO_APPROVAL_PW(+) " +
						"and to_char(this_.TGL_MOHON_FAKTUR,'YYYYMMDD') >= ? " +
						"and to_char(this_.TGL_MOHON_FAKTUR,'YYYYMMDD') <= ? " +
						"and ( this_.STATUS_PW='B' ) " +
						"order by this_.TGL_MOHON_FAKTUR";


		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream("sudahApprove.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("sudahApprove.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}


	public String getNilaiPwConfig() {
		return nilaiPwConfig;
	}

	public void setNilaiPwConfig(String nilaiPwConfig) {
		this.nilaiPwConfig = nilaiPwConfig;
	}


}
