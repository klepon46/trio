package com.trio.view.ctrl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h100.TrioH100Kecamatans;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.KecamatanListRenderer;

public class KecamatanCtrl extends TrioBasePage {

	private static final long serialVersionUID = 20111130143824L;
	private AnnotateDataBinder binder;
	private Listbox kecamatanList;
	private Grid editKecamatanGrid;
	private Button createKecamatan;
	private Button updateKecamatan;
	private Button deleteKecamatan;
	private Button searchKecamatan;
	
	//private KecamatanManager manager = ServiceLocator.getKecamatanManager();
	private TrioH100Kecamatans _kecamatan;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		_kecamatan = new TrioH100Kecamatans();
		kecamatanList.setModel(new ListModelList(getMasterFacade().getTrioH100KecamatansDao().findAll()));
		kecamatanList.setItemRenderer(new KecamatanListRenderer());
	}
	
	private ListModelList getModel() {
		return (ListModelList) kecamatanList.getModel();
	}
	
	public void onClick$resetKecamatan() {
		kecamatanList.clearSelection();
		kecamatanList.setModel(new ListModelList(getMasterFacade().getTrioH100KecamatansDao().findAll()));
		_kecamatan = new TrioH100Kecamatans();
		binder.loadComponent(editKecamatanGrid);
		createKecamatan.setDisabled(false);
		updateKecamatan.setDisabled(true);
		deleteKecamatan.setDisabled(true);
		//page.getFellow("contactDiv").setVisible(false);
	}
	
	//set selection to edit data
	public void onSelect$kecamatanList() {
		_kecamatan = (TrioH100Kecamatans) kecamatanList.getSelectedItem().getValue();
		binder.loadComponent(editKecamatanGrid);
		createKecamatan.setDisabled(true);
		updateKecamatan.setDisabled(false);
		deleteKecamatan.setDisabled(false);
		// used for Hibernate lazy-loading
		//_kecamatan = (Kecamatan) ServiceLocator.getHibernateSession().merge(_kecamatan);
		//Event event = new Event("onLoad", page.getFellow("contactDiv"), _kecamatan);
		//EventQueues.lookup("loadContact", EventQueues.DESKTOP, true).publish(event);
	}
	
	//register onClick event for creating new object into list model
	public void onClick$createKecamatan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		if (_kecamatan.getKdKec() == null && _kecamatan.getKecamatan() == null) {
			Messagebox.show("no new content to add");
		} else {
			getMasterFacade().getTrioH100KecamatansDao().saveOrUpdate(_kecamatan, getUserSession());
			getModel().add(_kecamatan);
		}
	}
	
	//register onClick event for updating edited data in list model
	public void onClick$updateKecamatan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		Listitem listItem = kecamatanList.getSelectedItem();
		getMasterFacade().getTrioH100KecamatansDao().saveOrUpdate(_kecamatan, getUserSession());
		listItem.setValue(_kecamatan);
		int index = kecamatanList.getSelectedIndex();
		getModel().set(index, _kecamatan);
	}
	
	//register onClick event for removing data in list model
	public void onClick$deleteKecamatan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		Messagebox.show("Are you sure to delete?", null, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
			public void onEvent(Event event) throws Exception {
				if(event.getName().equals("onYes")) {
					getModel().remove(_kecamatan);
					getMasterFacade().getTrioH100KecamatansDao().delete(_kecamatan);
				}
			}
		});
	}
	
	public void onClick$searchKecamatan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		//kecamatanList.setModel(new ListModelList(getMasterFacade().getTrioH100KecamatansDao().getKecamatanByCriteria(_kecamatan)));  
		kecamatanList.setModel(new ListModelList(getMasterFacade().getTrioH100KecamatansDao().findByExample(_kecamatan)));
		 
	}
	
	public TrioH100Kecamatans getKecamatan() {
		return _kecamatan;
	}
	
	public void setKecamatan(TrioH100Kecamatans kecamatan) {
		_kecamatan = kecamatan;
	}
}
