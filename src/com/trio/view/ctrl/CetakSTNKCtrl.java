package com.trio.view.ctrl;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;

import org.zkoss.util.media.AMedia;
import org.zkoss.zkex.zul.Jasperreport;
import org.zkoss.zul.Button;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.trio.base.TrioBasePage;
import com.trio.bean.bsc.AhmdsbscDtlsetting;
import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.bean.h300.TrioH300HdrHadiahs;
import com.trio.bean.h300.TrioH300HdrPrograms;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;

public class CetakSTNKCtrl extends TrioBasePage {

	Button btnReportes;
	Window winReportes;
	Jasperreport report;
	Listbox format;
	Iframe frame;
	String reportSrc,jrxmlSrc,folder;
	Map parameters = new HashMap();
	AMedia am,bm;
	Connection con;
	JRDataSource reportSource;
	List list = null;
	private Textbox noMesin;
	private Radiogroup radioFaktur;
	private Button btnReset;
	
	public CetakSTNKCtrl() throws SQLException {
		super();
		
	}
	
	public void showReportZKtag() {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		File fileReport = null;
		
		String path = null;
		
		AhmdsbscDtlsetting dtl = getMasterFacade().getAhmdsbscDtlsettingDao().findByVidAndVitemcode("RPT001_RPTCFG", "RPTDIRNAME");
		
		if (dtl != null){
			path = dtl.getVitemname();
		}
		
		if (radioFaktur.getSelectedItem().getValue().equalsIgnoreCase("0")){
			fileReport = new File(path+"T10mdmun004_2_new.jasper");
		}else{
//			fileReport = new File("/home/glassfish/AHMRPT/zkreport/T10mdmun004_1.jasper");
			fileReport = new File(path+"T10mdmun004_1_new.jasper");
		}
		
		if (fileReport != null){
			reportSrc = fileReport.getAbsolutePath();
		}else{
			reportSrc = null;
		}
		//"JBB1E1310298"
		parameters.put("NOMESIN_UPARAM", noMesin.getValue().trim() );
		report.setParameters(parameters);
		report.setDataConnection(getReportConnection());
		
		report.setType("pdf");
		report.setSrc(reportSrc);
		
	}
	
	public void onClick$btnReportes() throws JRException, IOException, InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		String noMesinString = noMesin.getValue().trim();
		TrioH100Stdetails st = null;
		
		if (noMesinString.equalsIgnoreCase("") || noMesinString == null){
			Messagebox.show("No mesin harus diisi"); 
			return;
		}
		st =  getMasterFacade().getTrioH100StdetailsDao().findByNomesin(noMesinString);
		if (st == null){
			Messagebox.show("No mesin tidak ditemukan"); 
			return;
		}
		//parameter A = termasuk pelanggaran wilayah
		st = getMasterFacade().getTrioH100StdetailsDao().findByNomesinAndStatusPW(noMesinString, "A");
		if (st != null){
			Messagebox.show("No mesin termasuk kategori pelanggaran wilayah"); 
			return;
		}
		showReportZKtag();
	}
	
	public void onClick$btnReset(){
		noMesin.setValue(null);
		report.setSrc(null);
	}

}