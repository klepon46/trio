package com.trio.view.ctrl;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Textbox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Dealers;
import com.trio.bean.h000.HondaH000Propins;
import com.trio.bean.h100.TrioH100Dlrpos;
import com.trio.bean.h100.TrioH100DtlAreaDealers;
import com.trio.bean.h100.TrioH100Kecamatans;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.DealerListRenderer;
import com.trio.view.ctrl.renderer.DealerPOSListRenderer;
import com.trio.view.ctrl.renderer.DtlAreaDealersListRenderer;
import com.trio.view.ctrl.renderer.KecamatanListRenderer;
import com.trio.view.ctrl.renderer.PropinsiRenderer;

public class DtlAreaDealersCtrl extends TrioBasePage {

	private static final long serialVersionUID = 1L;

	private AnnotateDataBinder binder;
	private Listbox dealerList;
	private Grid editDealerGrid;
	private Button createDealer;
	private Button searchDealer;

	private Textbox kdDlr;
	private Textbox kdDlrPOS;

	private Popup popDealer;
	private Popup popDealerPOS;
	private Listbox popDealerList;
	private Listbox popDealerPOSList;
	private Button LOV;
	private Button LOVDLRPOS;
	private Button dealerButtonLov;
	private Button dealerPOSButtonLov;
	private Textbox dealerBoxLov;
	private Textbox dealerPOSBoxLov;

	private Popup popKecamatan;
	private Listbox popKecamatanList;

	private Button kecButtonLov;
	private Textbox kecBoxLov;
	private Combobox status;
	private Comboitem ci1, ci2;
	private Radiogroup radioStatus;
	private Radiogroup radioInput;

	private TrioH100DtlAreaDealers _dealer;

	private Popup popKabupaten;
	private Listbox popKabupatenList;
	private Button LOV3;
	private Button propButtonLov;
	private Textbox kdKotKab;
	private Textbox propBoxLov;

	private String vkode;
	private Label lblDealer;
	private Label lblDealerPOS;


	private Hbox hbKecamatan;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		_dealer = new TrioH100DtlAreaDealers();
		dealerList.setModel(new ListModelList(
				getMasterFacade().getTrioH100DtlAreaDealersDao().findAll()));
		dealerList.setItemRenderer(new DtlAreaDealersListRenderer());

	}

	private ListModelList getModel() {
		return (ListModelList) dealerList.getModel();
	}

	public void onClick$resetDealer() {
		reset();
	}

	public void resetSimpan(String kdDlr){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		TrioH100DtlAreaDealers dlr = new TrioH100DtlAreaDealers();
		dlr.setKdDlr(kdDlr);
		dlr.setStatus(radioStatus.getSelectedItem().getValue());
		dealerList.setModel(new ListModelList(
				getMasterFacade().getTrioH100DtlAreaDealersDao().findByCriteria(dlr)));
		dealerList.setItemRenderer(new DtlAreaDealersListRenderer());
	}

	public void reset(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		dealerList.clearSelection();
		dealerList.setModel(new ListModelList(getMasterFacade().getTrioH100DtlAreaDealersDao().findAll()));
		dealerList.setItemRenderer(new DtlAreaDealersListRenderer());
		_dealer = new TrioH100DtlAreaDealers();
		binder.loadComponent(editDealerGrid);
		createDealer.setDisabled(false);
		radioStatus.setSelectedIndex(0);
		LOV3.setDisabled(true);

		kdKotKab.setValue(null);

		hbKecamatan.setVisible(false);
		lblDealer.setValue(null);
		createDealer.setDisabled(true);
		LOV.setDisabled(false);
		kdDlr.setDisabled(false);
		LOVDLRPOS.setDisabled(true);
		radioInput.setSelectedIndex(0);
		lblDealerPOS.setValue(null);
	}

	public void onSelect$dealerList() {
		_dealer = (TrioH100DtlAreaDealers) dealerList.getSelectedItem().getValue();
		if (_dealer.getStatus().equalsIgnoreCase("A")){
			radioStatus.setSelectedIndex(0);
		}else{
			radioStatus.setSelectedIndex(1);
		}
		binder.loadComponent(editDealerGrid);

	}

	public void onClick$createDealer() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		if (_dealer.getKdDlr() == null ) {
			Messagebox.show("Kode dealer harus diisi");
			reset();
			return;
		}
		_dealer.setKdDlr(_dealer.getKdDlr().toUpperCase());

		if (getMasterFacade().getHondaH000DealersDao().findByKdDlr(_dealer.getKdDlr()) == null){ 
			Messagebox.show("Kode dealer tidak ditemukan");
			reset();
			return;
		}

		Listbox lb = (Listbox) popKecamatanList.getFellow("dealerDiv").getFellow("popKecamatanList");
		List<Listitem> items = lb.getItems();

		for (Listitem item : items){

			TrioH100Kecamatans kecamatan = (TrioH100Kecamatans) item.getValue();
			List<Listcell> cells = item.getChildren();
			for (Listcell cell : cells){

				if (cell.getFirstChild() instanceof Checkbox){

					Checkbox cb = (Checkbox) cell.getFirstChild();
					if (cb.isChecked()){
						_dealer.setKdKec(kecamatan.getKdKec());
						_dealer.setStatus(radioStatus.getSelectedItem().getValue());
						String ri = radioInput.getSelectedItem().getValue();
						if (ri.equalsIgnoreCase("D")){ 
							_dealer.setKdDlrpos(_dealer.getKdDlr());
						}else if (ri.equalsIgnoreCase("P")){
							_dealer.setKdDlrpos(kdDlrPOS.getValue());
						}
						getMasterFacade().getTrioH100DtlAreaDealersDao().saveOrUpdate(_dealer, getUserSession());
					}
				}
			}
		}
		resetSimpan(_dealer.getKdDlr());

	}

	public void onClick$searchDealer() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		_dealer.setStatus(radioStatus.getSelectedItem().getValue());
		TrioH100DtlAreaDealers dtl = new TrioH100DtlAreaDealers();
		String st = radioInput.getSelectedItem().getValue();
		if (st.equalsIgnoreCase("P")){
			dtl.setKdDlrpos(kdDlrPOS.getValue());
		}else if (st.equalsIgnoreCase("D")){
			dtl.setKdDlr(kdDlr.getValue());
		}

		dtl.setKdKec(null);
		dtl.setStatus(radioStatus.getSelectedItem().getValue());
		dealerList.setModel(new ListModelList(
				getMasterFacade().getTrioH100DtlAreaDealersDao().findByCriteria(dtl)));
		dealerList.setItemRenderer(new DtlAreaDealersListRenderer());
	}

	public TrioH100DtlAreaDealers getDealer() {
		return _dealer;
	}

	public void setDealer(TrioH100DtlAreaDealers dealer) {
		_dealer = dealer;
	}

	public void onCheck$radioInput(){

		String st = radioInput.getSelectedItem().getValue();
		if (st.equalsIgnoreCase("D")){
			LOV.setDisabled(false);
			LOVDLRPOS.setDisabled(true);
		}else if (st.equalsIgnoreCase("P")){
			LOVDLRPOS.setDisabled(false);
			LOV.setDisabled(true);
		}
	}

	//START OF LOV DEALER
	public void onClick$LOV() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		popDealerList.setModel(new ListModelList(
				getMasterFacade().getHondaH000DealersDao().findAll()));
		popDealerList.setItemRenderer(new DealerListRenderer());
		LOV3.setDisabled(false);

	}

	public void onClick$LOVDLRPOS() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		popDealerPOSList.setModel(new ListModelList(
				getMasterFacade().getTrioH100DlrposDao().findAll()));
		popDealerPOSList.setItemRenderer(new DealerPOSListRenderer());
		LOV3.setDisabled(false);

	}

	public void onSelect$popDealerList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		HondaH000Dealers dlr = (HondaH000Dealers) popDealerList.getSelectedItem().getValue();
		lblDealer.setValue(dlr.getDlrNama());
		System.out.println("Kd dler = "+dlr.getKdDlr());
		_dealer.setKdDlr(dlr.getKdDlr());
		binder.loadComponent(kdDlr);
		_dealer.setStatus(radioStatus.getSelectedItem().getValue());
		dealerList.setModel(new ListModelList(
				getMasterFacade().getTrioH100DtlAreaDealersDao().findByCriteria(_dealer)));
		dealerList.setItemRenderer(new DtlAreaDealersListRenderer());
		LOV.setDisabled(true);
		kdDlr.setDisabled(true);
		popDealer.close();
	}

	public void onSelect$popDealerPOSList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		System.out.println("diklik");
		TrioH100Dlrpos pos = (TrioH100Dlrpos) popDealerPOSList.getSelectedItem().getValue();

		lblDealerPOS.setValue(pos.getNamaDlrpos());
		kdDlr.setValue(pos.getKdDlr());
		_dealer.setKdDlr(pos.getKdDlr());
		_dealer.setKdDlrpos(pos.getKdDlrpos());
		binder.loadComponent(kdDlrPOS);

		popDealerPOS.close();
	}

	public void onClick$dealerButtonLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		HondaH000Dealers d = new HondaH000Dealers();
		d.setDlrNama(dealerBoxLov.getValue());
		popDealerList.setModel(new ListModelList(getMasterFacade()
				.getHondaH000DealersDao().findByCriteria(d)));
		popDealerList.setItemRenderer(new DealerListRenderer());

	}

	public void onClick$dealerPOSButtonLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		System.out.println("ditekan bro");
		TrioH100Dlrpos p = new TrioH100Dlrpos();
		p.setNamaDlrpos(dealerPOSBoxLov.getValue());

		popDealerPOSList.setModel(new ListModelList(getMasterFacade()
				.getTrioH100DlrposDao().findByCriteria(p)));
		popDealerPOSList.setItemRenderer(new DealerPOSListRenderer());

	}

	public void onOK$dealerBoxLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		HondaH000Dealers d = new HondaH000Dealers();
		d.setDlrNama(dealerBoxLov.getValue());
		popDealerList.setModel(new ListModelList(getMasterFacade()
				.getHondaH000DealersDao().findByCriteria(d)));
		popDealerList.setItemRenderer(new DealerListRenderer());
	}

	//END OF LOV DEALER


	//START OF LOV KECAMATAN
	public void onClick$LOV2() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		TrioH100Kecamatans kec = new TrioH100Kecamatans();
		kec.setKdKota(getVkode());

		System.out.println("kd kota "+getVkode()); 
		popKecamatanList.setModel(new ListModelList(
				getMasterFacade().getTrioH100KecamatansDao().findByCriteria(kec)));
		popKecamatanList.setItemRenderer(new KecamatanListRenderer());
		hbKecamatan.setVisible(true);
		createDealer.setDisabled(false);
	}

	public void pilihKabupaten(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		TrioH100Kecamatans kec = new TrioH100Kecamatans();
		kec.setKdKota(getVkode());

		System.out.println("kd kota "+getVkode()); 
		popKecamatanList.setModel(new ListModelList(
				getMasterFacade().getTrioH100KecamatansDao().findByCriteria(kec)));
		popKecamatanList.setItemRenderer(new KecamatanListRenderer());
		hbKecamatan.setVisible(true);
		createDealer.setDisabled(false);
	}

	public void onSelect$popKecamatanList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		TrioH100Kecamatans kec = (TrioH100Kecamatans) popKecamatanList.getSelectedItem().getValue();

		_dealer.setKdKec(kec.getKdKec());
		popKecamatan.close();
	}

	public void onClick$kecButtonLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		TrioH100Kecamatans k = new TrioH100Kecamatans();
		k.setKecamatan(kecBoxLov.getValue());
		k.setKdKota(getVkode());
		popKecamatanList.setModel(new ListModelList(getMasterFacade()
				.getTrioH100KecamatansDao().findByCriteria(k)));
		popKecamatanList.setItemRenderer(new KecamatanListRenderer());

	}
	//END OF LOV KECAMATAN

	//START OF LOV KABUPATEN
	public void onClick$LOV3() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		popKabupatenList.setModel(new ListModelList(
				getMasterFacade().getHondaH000PropinsDao().findAll()));
		popKabupatenList.setItemRenderer(new PropinsiRenderer());

	}

	public void onSelect$popKabupatenList(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		HondaH000Propins prop = (HondaH000Propins) popKabupatenList.getSelectedItem().getValue();
		kdKotKab.setValue(prop.getVdeskripsi());
		System.out.println("vkode diset =="+prop.getVkode()); 
		setVkode(prop.getVkode());
		pilihKabupaten();
		popKabupaten.close();
	}

	public void onOK$propBoxLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		HondaH000Propins prop = new HondaH000Propins();
		prop.setVdeskripsi(propBoxLov.getValue());
		popKabupatenList.setModel(new ListModelList(getMasterFacade()
				.getHondaH000PropinsDao().findByCriteria(prop)));
		popKabupatenList.setItemRenderer(new PropinsiRenderer());
	}

	public void onClick$propButtonLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		HondaH000Propins prop = new HondaH000Propins();
		prop.setVdeskripsi(propBoxLov.getValue());
		popKabupatenList.setModel(new ListModelList(getMasterFacade()
				.getHondaH000PropinsDao().findByCriteria(prop)));
		popKabupatenList.setItemRenderer(new PropinsiRenderer());

	}
	//END OF LOV KABUPATEN

	public String getVkode() {
		return vkode;
	}

	public void setVkode(String vkode) {
		this.vkode = vkode;
	}

}
