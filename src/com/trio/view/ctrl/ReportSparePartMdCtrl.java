package com.trio.view.ctrl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Radiogroup;

import com.trio.base.TrioBasePage;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;

/**
 * @author Gusti Arya 9:27:32 AM Jan 17, 2014
 */

public class ReportSparePartMdCtrl extends TrioBasePage {

	private static final long serialVersionUID = 1L;

	private Radiogroup radioGroup;
	private Combobox combo, combo2;

	private Datebox startDate;
	private Datebox endDate;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);

		if(startDate.getValue() == null){
			startDate.setValue(new Date());
		}

		if(endDate.getValue()==null){
			endDate.setValue(new Date());
		}

	}

	public void onClick$btnCreateFile() throws SQLException, IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		Connection connection = getReportConnection();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		String dateAwal = sdf.format(startDate.getValue());
		String dateAkhir = sdf.format(endDate.getValue());


		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("intransit")){
			createIntransit(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("pembelian")){
			createPembelian(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("penerimaan")){
			createPenerimaan(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("penjualan")){
			createPenjualan(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("retur")){
			createRetur(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("hgp")){
			CreateHgp(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("tireTube")){
			createTireTube(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("oil")){
			createOil(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("gmo")){
			createGmo(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("pacc")){
			createAccessories(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("acc")){
			createMerchandise(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("tools")){
			createTools(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("tire")){
			createTire(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("tube")){
			createTube(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("all")){
			createAll(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("analisa")){
			createAnalisa(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("oilkpb")){
			createOilKpb(connection, dateAwal, dateAkhir);
		}

		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("dealer")){
			createMasterDealer(connection);
		}
		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("kelompok")){
			createKelompokBarang(connection, dateAwal, dateAkhir);
		}
		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("newReport")){
			createBarangBaru(connection);
		}
		if(radioGroup.getSelectedItem().getValue().equalsIgnoreCase("kelompok2")){
			createKelompokBarang2(connection, dateAwal, dateAkhir);
		}
	}


	public void createIntransit(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = "select b.QTY,a.STATUS,b.PS_NO,b.INV_DATE,b.PPN,b.DPP,b.INV_NO,b.PPN_DUE_DATE,b.DISC_CMP,b.PART_NO,b.CUST_CODE,b.PRICE,b.DPP_DUE_DATE,b.PART_DESC "
				+ "from honda_h300_ahm_fakturs b, honda_h300_ahmpses a where a.ps_no = b.ps_no and a.status ='I' order by inv_date";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 4) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 5) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportIntransit.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportIntransit.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}


	public void createPembelian(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		System.out.println("dateAwal" +  dateAwal);
		System.out.println("dateAkhir " + dateAkhir);

		// awal coding Arya
		/* 
		String sQuery = "select SUM(QTY),PS_NO,INV_DATE,PPN,DPP,INV_NO,PPN_DUE_DATE,DISC_CMP,PART_NO,CUST_CODE,PRICE,DPP_DUE_DATE,PART_DESC " 
				+ "from honda_h300_ahm_fakturs " 
				+ "where to_char(inv_date,'YYYYMMDD') >= ? and to_char(INV_DATE,'YYYYMMDD') <= ? "
				+ "group by PS_NO,INV_DATE,PPN,DPP,INV_NO,PPN_DUE_DATE,DISC_CMP,PART_NO,CUST_CODE,PRICE,DPP_DUE_DATE,PART_DESC " 
				+ "order by inv_date";
		*/		
		// akhir coding Arya
		
		String sQuery = "select SUM(QTY),PS_NO,INV_DATE,PPN,DPP,INV_NO,PPN_DUE_DATE,DISC_CMP,f.PART_NO,CUST_CODE,PRICE,DPP_DUE_DATE,f.PART_DESC, mp.KELBRG_ID_KELBRG " 
				+ "from honda_h300_ahm_fakturs f "
				+ "join HONDA_H300_MASTER_PARTS mp on mp.PART_NO = f.PART_NO "
				+ "where to_char(inv_date,'YYYYMMDD') >= ? and to_char(INV_DATE,'YYYYMMDD') <= ? "
				+ "group by PS_NO,INV_DATE,PPN,DPP,INV_NO,PPN_DUE_DATE,DISC_CMP,f.PART_NO,CUST_CODE,PRICE,DPP_DUE_DATE,f.PART_DESC ,mp.KELBRG_ID_KELBRG " 
				+ "order by inv_date";
		
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 3) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 4) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						if(rs.getString(colName).matches(".*[a-zA-z].*") || rs.getString(colName).matches(".*[-].*")){
							cell.setCellValue(rs.getString(colName));
						}else{
							cell.setCellValue(rs.getDouble(colName));
						}
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportPembelian.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportPembelian.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createPenerimaan(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));


		String sQuery = "select a.REC_NO,a.REC_DATE,a.PS_NO,a.STORING_DATE,b.PO_NO,b.QTY,b.PART_PART_NO,c.price "
				+ "from honda_h300_receives a, honda_h300_detail_receives b, honda_h300_ahm_fakturs  c "
				+ "where a.rec_no = b.receive_rec_no " 
				+ "and to_char(a.rec_date,'YYYYMMDD') >= ? and to_char(a.REC_DATE,'YYYYMMDD') <= ? "
				+ "and c.ps_no = a.ps_no " 
				+ "and c.part_no = b.part_part_no " 
				+ "and b.status = 'S'";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 7) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else{
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportPenerimaan.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportPenerimaan.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createPenjualan(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		DataFormat format = xlsWorkBook.createDataFormat();
		CellStyle style = xlsWorkBook.createCellStyle();
		style.setDataFormat(format.getFormat("###,###,00"));

		String sQuery = "select a.dealer_kd_dlr,d.dlr_nama, a.kd_trans,a.fak_date, a.fak_no, a.payment_due, a.payment_type, a.cash_disc as disc_cash_head,a.prosen_disc as disc_persen_head ,a.net_price as net_head, "
				+ "a.tot_price as tot_head, b.price, b.hpp, b.cash_disc as cash_disc_det, b.qty, b.net_price as net_det,  b.prosen_disc as disc_persen_det, b.part_part_no, c.kelbrg_id_kelbrg " 
				+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d " 
				+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' "
				+ "and a.KD_TRANS <> 'I' " 
				+ "and to_char(a.fak_date,'YYYYMMDD') >= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
				+ "and b.part_part_no = c.part_no "
				+ "and a.dealer_kd_dlr = d.kd_dlr " 
				+ "order by a.dealer_kd_dlr, c.kelbrg_id_kelbrg";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0){
					Cell cell = dataRow.createCell(colIndex++);
					if(rs.getString(colName).matches(".*[a-zA-z].*")){
						cell.setCellValue(rs.getString(colName));
					}else{
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
				}else if (colIndex == 7) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex == 8) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex == 9) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==10) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==11) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==12) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==13) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==14) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==15) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==16) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex ==17) {
					Cell cell = dataRow.createCell(colIndex++);
					if(rs.getString(colName).matches(".*[a-zA-z].*") || rs.getString(colName).matches(".*[-].*")){
						cell.setCellValue(rs.getString(colName));
					}else{
						cell.setCellValue(rs.getDouble(colName));
					}
				}else{
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportPenjualan.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportPenjualan.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createRetur(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###.00"));

		String sQuery = "select a.kd_dlr, c.dlr_nama, a.retur_date,  a.tgl_jatuhtempo, a.retur_no, b.fak_no,b.part_part_no, sum(b.price), sum(b.disc), sum(b.net_price),sum(b.qty),sum(qty_fak), d.t3_kelbrg_ar "
				+ "from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h000_dealers c, honda_h300_master_parts d "
				+ "where a.kd_dlr = c.kd_dlr " 
				+ "and b.part_part_no = d.part_no " 
				+ "and a.retur_no = b.retsal_retur_no " 
				+ "and TO_CHAR(A.RETUR_DATE,'YYYYMMDD') >= ? and TO_CHAR(A.RETUR_DATE,'YYYYMMDD') <= ? "
				+ "group by a.kd_dlr, c.dlr_nama, a.retur_date, a.retur_no, b.fak_no, a.tgl_jatuhtempo,b.part_part_no, d.t3_kelbrg_ar";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0){
					Cell cell = dataRow.createCell(colIndex++);
					if(rs.getString(colName).matches(".*[a-zA-z].*")){
						cell.setCellValue(rs.getString(colName));
					}else{
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
				}else if (colIndex == 7) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex == 8) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex == 9) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex == 10) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else if (colIndex == 11) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}else{
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportRetur.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportRetur.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void CreateHgp(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.total, kuda.total_retur, kuda.total-kuda.total_retur as JUMLAH from "
						+ "((select * from "
						+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr,d.dlr_nama, dlr_kota, dlr_rule, sum(b.net_price) as total, 0 as TOTAL_RETUR "
						+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d " 
						+ "where a.fak_no = b.faktur_fak_no(+) "
						+ "and NVL(a.status,'X') <> 'B' and d.dlr_status='A' and a.KD_TRANS <> 'I' " 
						+ "and to_char(a.fak_date,'YYYYMMDD') >= ? "
						+ "and to_char(a.fak_date,'YYYYMMDD') <= ? " 
						+ "and b.part_part_no = c.part_no(+) " 
						+ "and a.dealer_kd_dlr = d.kd_dlr(+) " 
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_TIRETUBE') " 
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_OIL') " 
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_GMO') " 
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_TOOLS') " 
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_PACC') " 
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_ACC') " 
						+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama,d.dlr_kota,d.dlr_rule) jual) "
						+ "UNION ALL "
						+ "(select * from "
						+ "(select "
						+ "to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR,d.dlr_nama,d.dlr_kota,d.dlr_rule, 0 as TOTAL ,sum(b.NET_PRICE) as total_retur "
						+ "from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c, HONDA_H000_DEALERS d "
						+ "where a.retur_no = b.retsal_retur_no "
						+ "and b.part_part_no = c.part_no "
						+ "and a.kd_dlr = d.kd_dlr "
						+ "and to_char(a.retur_date, 'YYYYMMDD') >= ? "
						+ "and to_char(a.retur_date, 'YYYYMMDD') <= ? "
						+ "and c.kelbrg_id_kelbrg not in ( select val_char from honda_h000_setups where val_id='H3_TIRETUBE' ) "
						+ "and c.kelbrg_id_kelbrg not in ( select val_char from honda_h000_setups where val_id='H3_OIL') "
						+ "and c.kelbrg_id_kelbrg not in ( select val_char from honda_h000_setups where val_id='H3_GMO') "
						+ "and c.kelbrg_id_kelbrg not in ( select val_char from honda_h000_setups where val_id='H3_TOOLS' ) "
						+ "and c.kelbrg_id_kelbrg not in ( select val_char from honda_h000_setups where val_id='H3_PACC' ) "
						+ "and c.kelbrg_id_kelbrg not in ( select val_char from honda_h000_setups where val_id='H3_ACC'  ) "
						+ "group by to_char(a.retur_date,'YYYYMM'), a.KD_DLR, d.dlr_nama,  d.dlr_kota, d.dlr_rule) retur)) kuda";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 5) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
					cell.setCellStyle(style);
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportHGP.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportHGP.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createTireTube(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = " select jual.periode, jual.kd_dlr, dlr_nama, dlr_kota, dlr_rule, total, nvl(retur,0) retur, total-nvl(retur,0) jumlah from "
				+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.net_price) as total "
				+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d "
				+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' and d.dlr_status='A' "
				+ "and a.KD_TRANS <> 'I' and to_char(a.fak_date,'YYYYMMDD') >= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
				+ "and b.part_part_no = c.part_no and a.dealer_kd_dlr = d.kd_dlr and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_TIRETUBE') "
				+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama,d.dlr_kota,d.dlr_rule, a.kd_trans) jual, "
				+ "(select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR,sum(b.NET_PRICE) retur "
				+ "from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c "
				+ "where a.retur_no = b.retsal_retur_no and b.part_part_no = c.part_no "
				+ "and to_char(a.retur_date, 'YYYYMMDD') >= ? and to_char(a.retur_date, 'YYYYMMDD') <= ? "
				+ "and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_TIRETUBE') "
				+ "group by to_char(a.retur_date,'YYYYMM'), a.KD_DLR) retur "
				+ "where jual.periode=retur.periode(+) and jual.kd_dlr=retur.kd_dlr(+) order by periode, kd_dlr, dlr_nama ";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 5) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
					cell.setCellStyle(style);
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportTireTube.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportTireTube.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}

	public void createOil(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_OIL' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_OIL') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		//PART_DETAIL
		HSSFSheet sheetPartDetail = xlsWorkBook.createSheet("PART_DETAIL");
		rowIndex = 0;

		sQuery = 
				"select * from " +
						"((select * from " +
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, " +
						"d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, b.PART_PART_NO, b.qty as QTY_JUAL, " +
						"b.net_price as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, " +
						"honda_h000_dealers d where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						"select " +
						"val_char " +
						"from honda_h000_setups " +
						"where val_id='H3_OIL' " +
						") order by d.DLR_NAMA) jual ) " +
						"union all " +
						"( select * from " +
						"(select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR, e.dlr_nama, " +
						"e.dlr_kota, e.dlr_rule, kd_trans, b.PART_PART_NO, 0 as QTY_JUAL, " +
						"0 as TOTAL_RP, b.qty as retur_qty, b.NET_PRICE as retur_rp " +
						"from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c, " +
						"honda_h300_fakturs d, HONDA_H000_DEALERS e where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no  " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						"select " +
						"val_char " +
						"from honda_h000_setups " +
						"where val_id='H3_OIL' " +
						")) " +
						"retur)) kuda";

		ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetPartDetail.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetPartDetail.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = sheetPartDetail.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		//PART_TYPE
		HSSFSheet sheetOilType = xlsWorkBook.createSheet("TYPE_OIL");
		rowIndex = 0;

		sQuery = 
				"select * from " +
						"((select * from " +
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, " +
						"d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, b.PART_PART_NO, b.qty as QTY_JUAL, " +
						"b.net_price as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP, e.VAL_CHAR as TYPE_OIL " +
						"from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, " +
						"honda_h000_dealers d, honda_h000_setups e where a.fak_no = b.faktur_fak_no " +
						"and b.part_part_no = e.VAL_TAG " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						"select " +
						"val_char " +
						"from honda_h000_setups " +
						"where val_id='H3_OIL' " +
						") order by d.DLR_NAMA) jual ) " +
						"union all " +
						"( select * from " +
						"(select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR, e.dlr_nama, " +
						"e.dlr_kota, e.dlr_rule, kd_trans, b.PART_PART_NO, 0 as QTY_JUAL, " +
						"0 as TOTAL_RP, b.qty as retur_qty, b.NET_PRICE as retur_rp, f.VAL_CHAR as TYPE_OIL " +
						"from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c, " +
						"honda_h300_fakturs d, HONDA_H000_DEALERS e, HONDA_H000_SETUPS f where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no  " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and b.part_part_no = f.val_char " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						"select " +
						"val_char " +
						"from honda_h000_setups " +
						"where val_id='H3_OIL' " +
						")) " +
						"retur)) kuda";

		ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetOilType.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetOilType.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = sheetOilType.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}


		FileOutputStream fOut = new FileOutputStream("reportOil.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportOil.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createOilKpb(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS = 'KPB' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_OIL' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_OIL') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportOilKPB.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportOilKPB.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createGmo(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_GMO' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_GMO') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";


		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportGmo.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportGmo.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createAccessories(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_PACC' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_PACC') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportAccesories.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportAccesories.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createMerchandise(Connection connection, String dateAwal,
			String dateAkhir) throws IOException, SQLException {

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_ACC' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_ACC') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportMerchandise.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportMerchandise.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}

	public void createTools(Connection connection, String dateAwal,
			String dateAkhir) throws SQLException, IOException {

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_TOOLS' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_TOOLS') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportTools.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportTools.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}

	public void createTire(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in ('TIRE','TIRE1','TR' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in ('TIRE','TIRE1','TR') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportTire.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportTire.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}

	public void createTube(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in ('TB','TBHGP','TBVL' ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in ('TB','TBHGP','TBVL') " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 0) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportTube.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportTube.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createAll(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));

		String sQuery = 
				"select jual.periode, jual.kd_dlr, jual.dlr_nama, jual.dlr_kota, jual.dlr_rule, jual.total as total_hgp, tt.total_tt as total_tiretube, "
						+ "oil.total_qty as total_qty_oil, oil.total_rp as total_rp_oil, gmo.total_qty as total_qty_gmo, gmo.total_rp as total_rp_oil, acc.total_rp as total_Accessories_Merchandise from "
						+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr,d.dlr_nama, dlr_kota, dlr_rule, sum(b.net_price) as total "
						+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d "
						+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' and d.dlr_status='A' "
						+ "and a.KD_TRANS <> 'I' and to_char(a.fak_date,'YYYYMMDD')>= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
						+ "and b.part_part_no = c.part_no and a.dealer_kd_dlr = d.kd_dlr and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_TIRETUBE') "
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_OIL') and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_GMO') "
						+ "and c.kelbrg_id_kelbrg not in (select val_char from honda_h000_setups where val_id='H3_TOOLS') "
						+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama,d.dlr_kota,d.dlr_rule) jual " 
						+ "left join " 
						+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr as dealer_kode ,d.dlr_nama as dealer_name, dlr_kota, dlr_rule, a.kd_trans, sum(b.net_price) as total_tt "
						+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d "
						+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' and d.dlr_status='A' "
						+ "and a.KD_TRANS <> 'I' and to_char(a.fak_date,'YYYYMMDD') >= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
						+ "and b.part_part_no = c.part_no and a.dealer_kd_dlr = d.kd_dlr and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_TIRETUBE') "
						+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama,d.dlr_kota,d.dlr_rule, a.kd_trans) tt " 
						+ "on jual.kd_dlr = tt.dealer_kode "
						+ "left join "
						+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr as dealer_kode,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty, sum(b.net_price) as total_rp "
						+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d "
						+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' and d.dlr_status='A' "
						+ "and a.KD_TRANS <> 'I' and to_char(a.fak_date,'YYYYMMDD') >= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
						+ "and b.part_part_no = c.part_no and a.dealer_kd_dlr = d.kd_dlr and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_OIL') "
						+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans) oil "
						+ "on jual.kd_dlr = oil.dealer_kode "
						+ "left join "
						+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr as dealer_kode,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty, sum(b.net_price) as total_rp "
						+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d "
						+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' and d.dlr_status='A' "
						+ "and a.KD_TRANS <> 'I' and to_char(a.fak_date,'YYYYMMDD') >= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
						+ "and b.part_part_no = c.part_no and a.dealer_kd_dlr = d.kd_dlr and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_GMO') "
						+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans) gmo "
						+ "on jual.kd_dlr = gmo.dealer_kode "
						+ "left join "
						+ "(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr as dealer_kode,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty, sum(b.net_price) as total_rp "
						+ "from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, honda_h000_dealers d "
						+ "where a.fak_no = b.faktur_fak_no and NVL(a.status,'X') <> 'B' and d.dlr_status='A' "
						+ "and a.KD_TRANS <> 'I' and to_char(a.fak_date,'YYYYMMDD') >= ? and to_char(a.fak_date,'YYYYMMDD') <= ? "
						+ "and b.part_part_no = c.part_no and a.dealer_kd_dlr = d.kd_dlr and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_PACC') "
						+ "and c.kelbrg_id_kelbrg in (select val_char from honda_h000_setups where val_id='H3_ACC') "
						+ "group by to_char(a.fak_date,'YYYYMM'), a.dealer_kd_dlr,d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans) acc "
						+ "on jual.kd_dlr = acc.dealer_kode "
						+ "order by periode, kd_dlr, dlr_nama";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		ps.setString(5, dateAwal);
		ps.setString(6, dateAkhir);
		ps.setString(7, dateAwal);
		ps.setString(8, dateAkhir);
		ps.setString(9, dateAwal);
		ps.setString(10, dateAkhir);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if (colIndex == 5) {
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getInt(colName));
					cell.setCellStyle(style);
				}else
					if (colIndex == 6) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 7) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex == 8) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==9) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==10) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}else if (colIndex ==11) {
						Cell cell = dataRow.createCell(colIndex++);
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
					else{
						dataRow.createCell(colIndex++).setCellValue(
								new HSSFRichTextString(rs.getString(colName)));
					}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportAll.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportAll.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createAnalisa(Connection connection,String dateAwal,String dateAkhir) throws SQLException, IOException{
		HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
		HSSFSheet sheetOil = hssfWorkbook.createSheet("AHM OIL");

		int rowIndex = 0;
		DataFormat format = hssfWorkbook.createDataFormat();
		CellStyle style = hssfWorkbook.createCellStyle();
		style.setDataFormat(format.getFormat("###,###,00"));

		//OIL
		String sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('TB','TBHGP','TBVL') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = sheetOil.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetOil.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetOil.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//TIRE
		HSSFSheet sheetTire = hssfWorkbook.createSheet("TIRE");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('TIRE','TIRE1','TR') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetTire.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetTire.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetTire.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}


		//CHAIN KIT
		HSSFSheet sheetChain = hssfWorkbook.createSheet("CHAIN KIT");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('CDKGP','CDKIT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetChain.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetChain.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetChain.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//ELECTRIC PART
		HSSFSheet sheetElectric = hssfWorkbook.createSheet("ELECTRICAL PART");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('ELECT','EPHVL','EPINS','EPMTI','EPSDT','EPTDI') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetElectric.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetElectric.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetElectric.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}


		//AHM PART
		HSSFSheet shetAhmPart= hssfWorkbook.createSheet("AHM PART");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('AH','AHM') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = shetAhmPart.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			shetAhmPart.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = shetAhmPart.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}


		//BRAKE SYSTEM
		HSSFSheet sheetBrake= hssfWorkbook.createSheet("BRAKE");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('BRAKE') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetBrake.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetBrake.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetBrake.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//BATTERY
		HSSFSheet sheetBattery= hssfWorkbook.createSheet("BATTERY");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('BATT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetBattery.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetBattery.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetBattery.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//SHOCK
		HSSFSheet sheetShock= hssfWorkbook.createSheet("SHOCK");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('SHOCK') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetShock.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetShock.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetShock.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//PLASTIC PART
		HSSFSheet sheetPlastic= hssfWorkbook.createSheet("PLASTIC PART");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('PLAST','PT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetPlastic.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetPlastic.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetPlastic.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//PISTON KIT
		HSSFSheet sheetPiston= hssfWorkbook.createSheet("PISTON");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('PSKIT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetPiston.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetPiston.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetPiston.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//DRIVE BELT
		HSSFSheet sheetBelt= hssfWorkbook.createSheet("BELT");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('BLDRV') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetBelt.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetBelt.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetBelt.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//SPARK PLUG
		HSSFSheet sheetSpark= hssfWorkbook.createSheet("SPARK PLUG");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('SPLUG','SPLUR') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetSpark.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetSpark.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetSpark.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//ACCESSORIES
		HSSFSheet sheetAcc= hssfWorkbook.createSheet("ACCESSORIES");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('PACC') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetAcc.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetAcc.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetAcc.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//BEARING
		HSSFSheet sheetBearing= hssfWorkbook.createSheet("BEARING");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('BRNG') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetBearing.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetBearing.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetBearing.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//CABLE
		HSSFSheet sheetCable= hssfWorkbook.createSheet("CABLE");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('CABLE','CB') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetCable.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetCable.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetCable.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//CAM CHAIN KIT
		HSSFSheet sheetCamChain= hssfWorkbook.createSheet("CAM CHAIN");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('CCKIT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetCamChain.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetCamChain.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetCamChain.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//DISK CLUTCH
		HSSFSheet sheetDisk= hssfWorkbook.createSheet("DISK CLUTCH");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('DISK','DIHVL') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetDisk.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetDisk.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetDisk.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//IMPOR PART
		HSSFSheet sheetImport= hssfWorkbook.createSheet("IMPORT PART");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('IMPOR') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetImport.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetImport.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetImport.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}


		//TUBE
		HSSFSheet sheetTube= hssfWorkbook.createSheet("TUBE");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('TB','TBHGP','TBVL') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetTube.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetTube.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetTube.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//CONROD
		HSSFSheet sheetConrod= hssfWorkbook.createSheet("CONROD");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('CRKIT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetConrod.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetConrod.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetConrod.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}

		//OTHERS
		HSSFSheet sheetOthers= hssfWorkbook.createSheet("OTHERS");
		rowIndex = 0;

		sQuery = 
				"SELECT " +
						"TO_CHAR(a.FAK_DATE,'YYYY') TAHUN, " +
						"TO_CHAR(a.FAK_DATE,'MONTH') BULAN , " +
						"TO_CHAR(a.FAK_DATE,'MM') BULAN_1 , " +
						"SUM(b.qty) QTY, " +
						"SUM(b.net_price) AMOUNT " +
						"FROM honda_h300_fakturs a, honda_h300_detail_fakturs b " +
						"WHERE TO_CHAR(a.FAK_DATE,'YYYY')>= '2013' " +
						"AND NVL(b.status,'X') <> 'B' " +
						"AND a.fak_no = b.faktur_fak_no " +
						"AND b.part_part_no IN " +
						"( " +
						"   select " +
						"   PART_NO " +
						"   from HONDA_H300_MASTER_PARTS " +
						"   where KELBRG_ID_KELBRG in ('OIL','TIRE','TIRE1','TR','CDKGP','CDKIT','ELECT', 'EPHVL', 'APINS', 'EPMTI', 'EPSDT', 'EPTDI','AH', 'AHM', 'BRAKE', 'BATT', 'SHOCK', 'PLAST', 'PT', 'PSKIT', 'BLDRV', 'SPLUG', 'SPLUR', 'PACC', 'BRNG', 'CABLE', 'CB', 'CCKIT', 'DISK', 'DHVL', 'IMPOR', 'TB', 'TBHGP', 'TBVL', 'CRKIT') " +
						") " +
						"GROUP BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MONTH'), TO_CHAR(a.FAK_DATE,'MM') " +
						"ORDER BY TO_CHAR(a.FAK_DATE,'YYYY'), TO_CHAR(a.FAK_DATE,'MM') ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetOthers.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetOthers.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetOthers.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					cell.setCellValue(rs.getDouble(colName));
					cell.setCellStyle(style);
				}
			}
		}


		FileOutputStream fOut = new FileOutputStream("reportAnalisa.xls");
		hssfWorkbook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportAnalisa.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");


	}

	public void createMasterDealer(Connection connection) throws SQLException, IOException{
		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###,00"));

		String sQuery = 
				"select a.KD_DLR, a.DLR_NAMA,a.DLR_ALAMAT1, a.DLR_ALAMAT2, a.DLR_PROPINSI, " 
						+"a.DLR_TELEPON1, a.DLR_TELEPON2, MIN(b.CREA_DATE) as TGL_AWAL_BELI, a.DLR_PEMILIK " 
						+"from HONDA_H000_DEALERS a left join HONDA_H300_FAKTURS b "
						+"on a.KD_DLR = b.DEALER_KD_DLR "
						+"GROUP by a.KD_DLR, a.DLR_NAMA,a.DLR_ALAMAT1,a.DLR_ALAMAT2, " 
						+"a.DLR_PROPINSI, a.DLR_TELEPON1, a.DLR_TELEPON2,a.DLR_PEMILIK";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0){
					Cell cell = dataRow.createCell(colIndex++);
					if(rs.getString(colName).matches(".*[a-zA-z].*")){
						cell.setCellValue(rs.getString(colName));
					}else{
						cell.setCellValue(rs.getDouble(colName));
						cell.setCellStyle(style);
					}
				}else{
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportDealer.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportDealer.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");
	}

	public void createKelompokBarang(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException{

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet("Part Header");

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));
		
		String stringIN = "'KUDA'";
		
		if ( combo.getValue().equalsIgnoreCase("AHM OIL dan GMO") ){
			stringIN = "'GMO','OIL'";
		} else if ( combo.getValue().equalsIgnoreCase("AHM PART") ) {
			stringIN = "'AHM'";
		} else if ( combo.getValue().equalsIgnoreCase("BATTERY") ) {
			stringIN = "'BATT'";
		} else if ( combo.getValue().equalsIgnoreCase("BEARING") ) {
			stringIN = "'BRNG','BRNG3'";
		} else if ( combo.getValue().equalsIgnoreCase("BRAKE SYSTEM") ) {
			stringIN = "'BRAKE','FLUID'";
		} else if ( combo.getValue().equalsIgnoreCase("CABLE") ) {
			stringIN = "'CABLE'";
		} else if ( combo.getValue().equalsIgnoreCase("CAM CHAIN KIT") ) {
			stringIN = "'CCKIT'";
		} else if ( combo.getValue().equalsIgnoreCase("CONROD KIT") ) {
			stringIN = "'CRKIT'";
		} else if ( combo.getValue().equalsIgnoreCase("DISK CLUCTH") ) {
			stringIN = "'DIHVL','DISK'";
		} else if ( combo.getValue().equalsIgnoreCase("DRIVE BELT") ) {
			stringIN = "'BLDRV'";
		} else if ( combo.getValue().equalsIgnoreCase("DRIVE CHAIN KIT") ) {
			stringIN = "'CDKGP','CDKIT'";
		} else if ( combo.getValue().equalsIgnoreCase("ELECTRICAL PART") ) {
			stringIN = "'ELECT','EPHVL'";
		} else if ( combo.getValue().equalsIgnoreCase("HGA") ) {
			stringIN = "'PACC'";
		} else if ( combo.getValue().equalsIgnoreCase("IMPORT PART") ) {
			stringIN = "'IMPOR'";
		} else if ( combo.getValue().equalsIgnoreCase("OTHERS") ) {
			stringIN = "'ACC','ACCEC','AH','BBIST','BM','BR','BRNG2','BS','BT','CB','CD','CHAIN','COMP','COOL','CS','EPINS','EPMTI','EPSDT','EPTDI','ET','GS','GSA','GSB','GST','HAH','HELM','HM','HNMTI','HPLAS','HRW','HSD','LGS','LSIST','MUF','N','NF','OAHM1','OAHM2','OC','OFCC','OINS','OISTC','OKGD','OMTI','OND','ORPL','OSEAL','OTDI','OTHER','OTHR','PA','PAINT','PS','PSTN','PT','RBR','RIMWH','RPHVL','RPIST','RSKIT','RW','RW2','RWHVL','SA','SD','SDN','SDN2','SE','SPGUI','SPOKE','STR','TAS','TB','TR','VALVE','VV'";
		} else if ( combo.getValue().equalsIgnoreCase("PISTON KIT") ) {
			stringIN = "'PSKIT'";
		} else if ( combo.getValue().equalsIgnoreCase("PLASTIC PART") ) {
			stringIN = "'PLAST'";
		} else if ( combo.getValue().equalsIgnoreCase("SHOCK ABSORBER") ) {
			stringIN = "'SAOIL','SHOCK'";
		} else if ( combo.getValue().equalsIgnoreCase("SPARK PLUG") ) {
			stringIN = "'SPLUG','SPLUR'";
		} else if ( combo.getValue().equalsIgnoreCase("TIRE") ) {
			stringIN = "'TIRE','TIRE1'";
		} else if ( combo.getValue().equalsIgnoreCase("TUBE") ) {
			stringIN = "'TBHGP','TBVL'";
		} else if ( combo.getValue().equalsIgnoreCase("TOOLS") ) {
			stringIN = "'TL'";
		}
		/*
		String sQuery = 
				"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from "+ 
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (select val_tag from honda_h000_setups where val_id='H1_KEL_BRG' and val_tag = ? ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (select val_tag from honda_h000_setups where val_id='H1_KEL_BRG' and val_tag = ?) " +
						"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		*/
		
		String sQuery = 
		"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
				"((select * from "+ 
				"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
				"from honda_h300_fakturs a, " +
				"honda_h300_detail_fakturs b, " +
				"honda_h300_master_parts c, " +
				"honda_h000_dealers d " +
				"where a.fak_no = b.faktur_fak_no " +
				"and NVL(a.status,'X') <> 'B' " +
				"and d.dlr_status='A' " +
				"and a.KD_TRANS <> 'I' " +
				"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
				"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
				"and b.part_part_no = c.part_no " +
				"and a.dealer_kd_dlr = d.kd_dlr " +
				"and c.kelbrg_id_kelbrg in ( "+ stringIN +" ) " +
				"group by to_char(a.fak_date,'YYYYMM'), " +
				"a.dealer_kd_dlr, " +
				"d.dlr_nama, " +
				"dlr_kota, " +
				"dlr_rule, " +
				"a.kd_trans " +
				") jual) " +
				"union all " +
				"( select * from " +
				"( select to_char(a.retur_date,'YYYYMMDD') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
				"from trio_h300_retsals a, " +
				"trio_h300_detail_retsals b, " +
				"honda_h300_master_parts c, " +
				"honda_h300_fakturs d, " +
				"HONDA_H000_DEALERS e " +
				"where a.retur_no = b.retsal_retur_no " +
				"and b.part_part_no = c.part_no " +
				"and b.fak_no=d.fak_no " +
				"and d.dealer_kd_dlr = e.kd_dlr " +
				"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
				"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
				"and c.kelbrg_id_kelbrg in (" + stringIN + ") " +
				"group by to_char(a.retur_date,'YYYYMMDD'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda";
		

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		//ps.setString(3, combo.getValue());
		//ps.setString(4, dateAwal);
		//ps.setString(5, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		//ps.setString(6, combo.getValue());
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		//PART_DETAIL
		HSSFSheet sheetPartDetail = xlsWorkBook.createSheet("PART_DETAIL");
		rowIndex = 0;
		/*
		sQuery = 
				"select * from " +
						"((select * from " +
						"( " +
						"select " +
						"to_char(a.fak_date,'YYYYMM') periode, " +
						"a.dealer_kd_dlr kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans, " +
						"b.PART_PART_NO, " +
						"b.qty as QTY_JUAL, " +
						"b.net_price as total_rp, " +
						"0 as RETUR_QTY, " +
						"0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						"select " +
						"VAL_TAG " +
						"from honda_h000_setups " +
						"where val_id='H1_KEL_BRG' " +
						"and VAL_TAG = ? " +
						") " +
						"order by d.DLR_NAMA " +
						") jual ) " +
						"union all " +
						"(select * from " +
						"( " +
						"select " +
						"to_char(a.retur_date,'YYYYMM') periode, " +
						"a.KD_DLR, " +
						"e.dlr_nama, " +
						"e.dlr_kota, " +
						"e.dlr_rule, " +
						"kd_trans, " +
						"b.PART_PART_NO, " +
						"0 as QTY_JUAL, " +
						"0 as TOTAL_RP, " +
						"b.qty as retur_qty, " +
						"b.NET_PRICE as retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						"select " +
						"VAL_TAG " +
						"from honda_h000_setups " +
						"where val_id='H1_KEL_BRG' " +
						"and VAL_TAG = ? " +
						")) retur )) kuda";
		*/
		
		sQuery = 
				"select * from " +
						"((select * from " +
						"( " +
						"select " +
						"to_char(a.fak_date,'YYYYMM') periode, " +
						"a.dealer_kd_dlr kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans, " +
						"b.PART_PART_NO, " +
						"b.qty as QTY_JUAL, " +
						"b.net_price as total_rp, " +
						"0 as RETUR_QTY, " +
						"0 as RETUR_RP, " +
						"c.KELBRG_ID_KELBRG " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						stringIN +
						") " +
						"order by d.DLR_NAMA " +
						") jual ) " +
						"union all " +
						"(select * from " +
						"( " +
						"select " +
						"to_char(a.retur_date,'YYYYMM') periode, " +
						"a.KD_DLR, " +
						"e.dlr_nama, " +
						"e.dlr_kota, " +
						"e.dlr_rule, " +
						"kd_trans, " +
						"b.PART_PART_NO, " +
						"0 as QTY_JUAL, " +
						"0 as TOTAL_RP, " +
						"b.qty as retur_qty, " +
						"b.NET_PRICE as retur_rp, " +
						"c.KELBRG_ID_KELBRG " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +
						"( " + stringIN + ")) retur )) kuda";

		ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		//ps.setString(3, combo.getValue());
		//ps.setString(4, dateAwal);
		//ps.setString(5, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		//ps.setString(6, combo.getValue());
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetPartDetail.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetPartDetail.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = sheetPartDetail.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		//PART_TYPE
		HSSFSheet sheetOilType = xlsWorkBook.createSheet("TYPE_PART");
		rowIndex = 0;
		//* bgn coding Arya */
		/* 
		sQuery = 
				"select * from " +  
						"((select * from " +  
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, " +  
						"d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, b.PART_PART_NO, b.qty as QTY_JUAL, " +  
						"b.net_price as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP, e.VAL_CHAR as TYPE_PART " +  
						"from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, " +  
						"honda_h000_dealers d, honda_h000_setups e where a.fak_no = b.faktur_fak_no " +  
						"and c.KELBRG_ID_KELBRG = e.VAL_TAG " +  
						"and NVL(a.status,'X') <> 'B' " +  
						"and d.dlr_status='A' " +  
						"and a.KD_TRANS <> 'I' " +  
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +  
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +  
						"and b.part_part_no = c.part_no " +  
						"and a.dealer_kd_dlr = d.kd_dlr " +  
						"and c.kelbrg_id_kelbrg in " +  
						"( " +  
						"select " +  
						"VAL_TAG " +  
						"from honda_h000_setups " +  
						"where val_id='H1_KEL_BRG' and VAL_TAG = ? " +  
						") order by d.DLR_NAMA) jual ) " +  
						"union all " +  
						"( select * from " +  
						"(select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR, e.dlr_nama, " +  
						"e.dlr_kota, e.dlr_rule, kd_trans, b.PART_PART_NO, 0 as QTY_JUAL, " +  
						"0 as TOTAL_RP, b.qty as retur_qty, b.NET_PRICE as retur_rp, f.VAL_CHAR as TYPE_OIL " +  
						"from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c, " +  
						"honda_h300_fakturs d, HONDA_H000_DEALERS e, HONDA_H000_SETUPS f where a.retur_no = b.retsal_retur_no " +  
						"and b.part_part_no = c.part_no " +   
						"and b.fak_no=d.fak_no " +  
						"and d.dealer_kd_dlr = e.kd_dlr " +  
						"and c.KELBRG_ID_KELBRG = f.val_tag " +  
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +  
						"( " +  
						"select " +  
						"VAL_TAG " +  
						"from honda_h000_setups " +  
						"where val_id='H1_KEL_BRG' and VAL_TAG = ? " +  
						")) " +  
						"retur)) kuda";
						*/
		// end coding arya
		
		sQuery = 
				"select * from " +  
						"((select * from " +  
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, " +  
						"d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, b.PART_PART_NO, b.qty as QTY_JUAL, " +  
						"b.net_price as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP, e.VAL_CHAR as TYPE_PART " +  
						"from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, " +  
						"honda_h000_dealers d, honda_h000_setups e where a.fak_no = b.faktur_fak_no " +  
						"and c.KELBRG_ID_KELBRG = e.VAL_TAG " +  
						"and NVL(a.status,'X') <> 'B' " +  
						"and d.dlr_status='A' " +  
						"and a.KD_TRANS <> 'I' " +  
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +  
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +  
						"and b.part_part_no = c.part_no " +  
						"and a.dealer_kd_dlr = d.kd_dlr " +  
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						stringIN +
						") " +
						"order by d.DLR_NAMA) jual ) " +  
						"union all " +  
						"( select * from " +  
						"(select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR, e.dlr_nama, " +  
						"e.dlr_kota, e.dlr_rule, kd_trans, b.PART_PART_NO, 0 as QTY_JUAL, " +  
						"0 as TOTAL_RP, b.qty as retur_qty, b.NET_PRICE as retur_rp, f.VAL_CHAR as TYPE_OIL " +  
						"from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c, " +  
						"honda_h300_fakturs d, HONDA_H000_DEALERS e, HONDA_H000_SETUPS f where a.retur_no = b.retsal_retur_no " +  
						"and b.part_part_no = c.part_no " +   
						"and b.fak_no=d.fak_no " +  
						"and d.dealer_kd_dlr = e.kd_dlr " +  
						"and c.KELBRG_ID_KELBRG = f.val_tag " +  
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +  
						"( " +
						stringIN +
						"))" +  
						"retur)) kuda";
		
		//stringIN +

		ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		//ps.setString(3, combo.getValue());
		//ps.setString(4, dateAwal);
		//ps.setString(5, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		//ps.setString(6, combo.getValue());
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetOilType.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetOilType.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = sheetOilType.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}


		
		String fileName = "KelBarang_" + combo.getValue().toUpperCase() + "_" +getTglCreateFile()+".xls";
		FileOutputStream fOut = new FileOutputStream(fileName);
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File(fileName);
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}
	
	public void createKelompokBarang2(Connection connection, String dateAwal, String dateAkhir) throws SQLException, IOException {

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet("Part Header");

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###"));
		
		String stringIN = "'KUDA'";
		
		if ( combo2.getValue().equalsIgnoreCase("AHM OIL dan GMO") ){
			stringIN = "'GMO','OIL'";
		} else if ( combo2.getValue().equalsIgnoreCase("AHM PART") ) {
			stringIN = "'AHM'";
		} else if ( combo2.getValue().equalsIgnoreCase("BATTERY") ) {
			stringIN = "'BATT'";
		} else if ( combo2.getValue().equalsIgnoreCase("BEARING") ) {
			stringIN = "'BRNG','BRNG3'";
		} else if ( combo2.getValue().equalsIgnoreCase("BRAKE SYSTEM") ) {
			stringIN = "'BRAKE'";
		} else if ( combo2.getValue().equalsIgnoreCase("CABLE") ) {
			stringIN = "'CABLE'";
		} else if ( combo2.getValue().equalsIgnoreCase("CAM CHAIN KIT") ) {
			stringIN = "'CCKIT'";
		} else if ( combo2.getValue().equalsIgnoreCase("DISK CLUCTH") ) {
			stringIN = "'DISK'";
		} else if ( combo2.getValue().equalsIgnoreCase("DRIVE BELT") ) {
			stringIN = "'BLDRV'";
		} else if ( combo2.getValue().equalsIgnoreCase("DRIVE CHAIN KIT (HGP)") ) {
			stringIN = "'CDKGP'";
		} else if ( combo2.getValue().equalsIgnoreCase("ELECTRICAL PART") ) {
			stringIN = "'ELECT'";
		} else if ( combo2.getValue().equalsIgnoreCase("ACCESSORIES") ) {
			stringIN = "'PACC'";
		} else if ( combo2.getValue().equalsIgnoreCase("MERCHANDISE") ) {
			stringIN = "'ACCEC', 'HELM'";
		} else if ( combo2.getValue().equalsIgnoreCase("PISTON KIT") ) {
			stringIN = "'PSKIT'";
		} else if ( combo2.getValue().equalsIgnoreCase("PLASTIC PART") ) {
			stringIN = "'PLAST'";
		} else if ( combo2.getValue().equalsIgnoreCase("SHOCK ABSORBER") ) {
			stringIN = "'SAOIL','SHOCK'";
		} else if ( combo2.getValue().equalsIgnoreCase("SPARK PLUG") ) {
			stringIN = "'SPLUG','SPLUR'";
		} else if ( combo2.getValue().equalsIgnoreCase("TIRETUBE") ) {
			stringIN = "'TBHGP', 'TBVL', 'TIRE', 'TIRE1'";
		} else if ( combo2.getValue().equalsIgnoreCase("TOOLS") ) {
			stringIN = "'TL'";
		} else if ( combo2.getValue().equalsIgnoreCase("OTHERS") ) {
			stringIN = "'FLUID','CHAIN','CRKIT','COOL','DIHVL','CDKIT','EPHVL','GST','LGS','IMPOR','TAS','ACC','AH','BBIST','BM','BR','BRNG2','BS','BT','CB','CD','COMP','CS','EPINS','EPMTI','EPSDT','EPTDI','ET','GS','GSA','GSB','HAH','HM','HNMTI','HPLAS','HRW','HSD','LSIST','N','NF','OAHM1','OAHM2','OC','OFCC','OINS','OISTC','OKGD','OMTI','OND','ORPL','OTDI','OTHER','OTHR','PA','PAINT','PS','PT','RW','RW2','SA','SDN2','SE','TB','TR','VV','MUF','OSEAL','PSTN','RSKIT','RIMWH','RWHVL','RPIST','RPHVL','RBR','SPOKE','SD','SDN','SPGUI','STR','VALVE'";
		}
		
		System.out.println("STRING IN  == " + stringIN);
		
		String sQuery = 
				"select periode ,kd_dlr, dlr_nama, dlr_kota, dlr_rule, kd_trans, sum(total_qty) total_qty, sum(retur_qty) retur_qty, sum(total_rp) total_rp, sum(retur_rp) retur_rp  from ( " +
						"select kuda.periode, kuda.kd_dlr, kuda.dlr_nama, kuda.dlr_kota, kuda.dlr_rule, kuda.kd_trans, kuda.total_qty, kuda.retur_qty, kuda.total_rp, kuda.RETUR_RP  from " +
						"((select * from " +
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, sum(b.qty) as total_qty , sum(b.net_price) as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in (  "+ stringIN +"   ) " +
						"group by to_char(a.fak_date,'YYYYMM'), " +
						"a.dealer_kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans " +
						") jual) " +
						"union all " +
						"( select * from " +
						"( select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans, 0 as total_qty, 0 as total_rp, sum(b.qty) retur_qty, sum(b.NET_PRICE) retur_rp " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in (  "+ stringIN +"  ) " +
						"group by to_char(a.retur_date,'YYYYMM'), a.KD_DLR, e.dlr_nama, e.dlr_kota, e.dlr_rule, kd_trans )retur)) kuda ) group by kd_dlr, dlr_nama , dlr_kota , dlr_rule, kd_trans, periode order by kd_dlr ";
		

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		//ps.setString(3, combo.getValue());
		//ps.setString(4, dateAwal);
		//ps.setString(5, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		//ps.setString(6, combo.getValue());
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				
				if (colName.equalsIgnoreCase("TOTAL_QTY")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("RETUR_QTY")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("TOTAL_RP")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("RETUR_RP")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				}else {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
				}
			}
		}

		//PART_DETAIL
		HSSFSheet sheetPartDetail = xlsWorkBook.createSheet("PART_DETAIL");
		rowIndex = 0;
		
		sQuery = 
				"select * from " +
						"((select * from " +
						"( " +
						"select " +
						"to_char(a.fak_date,'YYYYMM') periode, " +
						"a.dealer_kd_dlr kd_dlr, " +
						"d.dlr_nama, " +
						"dlr_kota, " +
						"dlr_rule, " +
						"a.kd_trans, " +
						"b.PART_PART_NO, " +
						"b.qty as QTY_JUAL, " +
						"b.net_price as total_rp, " +
						"0 as RETUR_QTY, " +
						"0 as RETUR_RP, " +
						"c.KELBRG_ID_KELBRG " +
						"from honda_h300_fakturs a, " +
						"honda_h300_detail_fakturs b, " +
						"honda_h300_master_parts c, " +
						"honda_h000_dealers d " +
						"where a.fak_no = b.faktur_fak_no " +
						"and NVL(a.status,'X') <> 'B' " +
						"and d.dlr_status='A' " +
						"and a.KD_TRANS <> 'I' " +
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +
						"and b.part_part_no = c.part_no " +
						"and a.dealer_kd_dlr = d.kd_dlr " +
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						stringIN +
						") " +
						"order by d.DLR_NAMA " +
						") jual ) " +
						"union all " +
						"(select * from " +
						"( " +
						"select " +
						"to_char(a.retur_date,'YYYYMM') periode, " +
						"a.KD_DLR, " +
						"e.dlr_nama, " +
						"e.dlr_kota, " +
						"e.dlr_rule, " +
						"kd_trans, " +
						"b.PART_PART_NO, " +
						"0 as QTY_JUAL, " +
						"0 as TOTAL_RP, " +
						"b.qty as retur_qty, " +
						"b.NET_PRICE as retur_rp, " +
						"c.KELBRG_ID_KELBRG " +
						"from trio_h300_retsals a, " +
						"trio_h300_detail_retsals b, " +
						"honda_h300_master_parts c, " +
						"honda_h300_fakturs d, " +
						"HONDA_H000_DEALERS e " +
						"where a.retur_no = b.retsal_retur_no " +
						"and b.part_part_no = c.part_no " +
						"and b.fak_no=d.fak_no " +
						"and d.dealer_kd_dlr = e.kd_dlr " +
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +
						"( " + stringIN + ")) retur )) kuda";

		ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		//ps.setString(3, combo.getValue());
		//ps.setString(4, dateAwal);
		//ps.setString(5, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		//ps.setString(6, combo.getValue());
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetPartDetail.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetPartDetail.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = sheetPartDetail.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				
				if (colName.equalsIgnoreCase("QTY_JUAL")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("TOTAL_RP")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("RETUR_QTY")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("RETUR_RP")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else if (colName.equalsIgnoreCase("TOTAL_RP")) {
					dataRow.createCell(colIndex++).setCellValue(
							rs.getBigDecimal(colName).doubleValue());
				} else {
					dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
				}
			}
		}

		//PART_TYPE
		HSSFSheet sheetOilType = xlsWorkBook.createSheet("TYPE_PART");
		rowIndex = 0;
		
		sQuery = 
				"select * from " +  
						"((select * from " +  
						"(select to_char(a.fak_date,'YYYYMM') periode, a.dealer_kd_dlr kd_dlr, " +  
						"d.dlr_nama, dlr_kota, dlr_rule, a.kd_trans, b.PART_PART_NO, b.qty as QTY_JUAL, " +  
						"b.net_price as total_rp, 0 as RETUR_QTY, 0 as RETUR_RP, e.VAL_CHAR as TYPE_PART " +  
						"from honda_h300_fakturs a, honda_h300_detail_fakturs b, honda_h300_master_parts c, " +  
						"honda_h000_dealers d, honda_h000_setups e where a.fak_no = b.faktur_fak_no " +  
						"and c.KELBRG_ID_KELBRG = e.VAL_TAG " +  
						"and NVL(a.status,'X') <> 'B' " +  
						"and d.dlr_status='A' " +  
						"and a.KD_TRANS <> 'I' " +  
						"and to_char(a.fak_date,'YYYYMMDD') >= ? " +  
						"and to_char(a.fak_date,'YYYYMMDD') <= ? " +  
						"and b.part_part_no = c.part_no " +  
						"and a.dealer_kd_dlr = d.kd_dlr " +  
						"and c.kelbrg_id_kelbrg in " +
						"( " +
						stringIN +
						") " +
						"order by d.DLR_NAMA) jual ) " +  
						"union all " +  
						"( select * from " +  
						"(select to_char(a.retur_date,'YYYYMM') periode, a.KD_DLR, e.dlr_nama, " +  
						"e.dlr_kota, e.dlr_rule, kd_trans, b.PART_PART_NO, 0 as QTY_JUAL, " +  
						"0 as TOTAL_RP, b.qty as retur_qty, b.NET_PRICE as retur_rp, f.VAL_CHAR as TYPE_OIL " +  
						"from trio_h300_retsals a, trio_h300_detail_retsals b, honda_h300_master_parts c, " +  
						"honda_h300_fakturs d, HONDA_H000_DEALERS e, HONDA_H000_SETUPS f where a.retur_no = b.retsal_retur_no " +  
						"and b.part_part_no = c.part_no " +   
						"and b.fak_no=d.fak_no " +  
						"and d.dealer_kd_dlr = e.kd_dlr " +  
						"and c.KELBRG_ID_KELBRG = f.val_tag " +  
						"and to_char(a.retur_date, 'YYYYMMDD') >= ? " +
						"and to_char(a.retur_date, 'YYYYMMDD') <= ? " +
						"and c.kelbrg_id_kelbrg in " +  
						"( " +
						stringIN +
						"))" +  
						"retur)) kuda";
		
		//stringIN +

		ps = connection.prepareStatement(sQuery);
		ps.setString(1, dateAwal);
		ps.setString(2, dateAkhir);
		//ps.setString(3, combo.getValue());
		//ps.setString(4, dateAwal);
		//ps.setString(5, dateAkhir);
		ps.setString(3, dateAwal);
		ps.setString(4, dateAkhir);
		//ps.setString(6, combo.getValue());
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetOilType.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetOilType.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = sheetOilType.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}
		
		String fileName = "KelBarang2_" + combo2.getValue().toUpperCase() + "_" +getTglCreateFile()+".xls";
		FileOutputStream fOut = new FileOutputStream(fileName);
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File(fileName);
		Filedownload.save(file, "XLS");

		System.out.println("done");

	}

	public void createBarangBaru(Connection connection) throws SQLException, IOException{

		HSSFWorkbook xlsWorkBook = new HSSFWorkbook();
		HSSFSheet xlsSheet = xlsWorkBook.createSheet();

		int rowIndex = 0;
		CellStyle style = xlsWorkBook.createCellStyle();
		DataFormat format = xlsWorkBook.createDataFormat();
		style.setDataFormat(format.getFormat("###,###.00"));

		String sQuery = 
				"select a.KELBRG_ID_KELBRG, '' AS SUB_KATEGORI ,SUM(b.QTY/12) as rata_rata_qty,  SUM(B.NET_PRICE/12) as RATA_RATA_AMOUNT, kuda.qty as qty_ONH,kuda.AMOUNT_TOT, " +
						"curr.QTY_CURRENT, curr.amount_Current ,noncurr.QTY_NONCURRENT , noncurr.amount_NONCurrent, other.QTY_OTHER, other.AMOUNT_OTHER, " +
						"dead.DEAD_STOCK, dead.DEAD_STOCK_AMOUNT " +
						"from HONDA_H300_MASTER_PARTS a " +
						"left join " +
						"	(SELECT DF.* FROM " +
						"	HONDA_H300_DETAIL_FAKTURS DF " +
						"	WHERE  TO_CHAR(DF.CREA_DATE, 'YYYYMMDD') >= to_char(add_months(sysdate,-12),'YYYYMMDD')) b on a.PART_NO = b.PART_PART_NO " +
						"left join " +
						"	(select KELBRG_ID_KELBRG, SUM(a.QTY_ONH) as QTY, SUM(a.QTY_ONH * b.HET) as AMOUNT_TOT from HONDA_H300_PART_LOKASES a join HONDA_H300_MASTER_PARTS b " +
						"	on a.PART_PART_NO = b.PART_NO " +
						"	where b.KELBRG_ID_KELBRG in " +
						"	( " +
						"   'IMPOR','TAS','PLAST', 'STR',   'RIMWH',   'MUF',   'BRAKE',   'RBR',   'VALVE',   'BRNG',   'SPOKE',   'ELECT',   'CHAIN',   'CABLE',   'SPLUG',   'SPLUR',   'SDN', " +
						"   'SHOCK',   'TIRE',   'TBHGP',   'TBVL',   'GST',   'PSTN',   'SDN',   'DISK',   'RPIST',   'BATT',   'OSEAL',   'RPHVL',   'DIHVL',   'CDKIT',   'RWHVL',   'ACCEC',   'PACC', " +
						"   'HELM',   'CRKIT',   'CCKIT',   'EPHVL',   'TL',   'RSKIT',   'CDKGP',  'PSKIT',   'COOL',   'FLUID',   'SPGUI',   'BLDRV',   'SAOIL',   'LGS') " +
						"	group by b.KELBRG_ID_KELBRG) kuda " +
						"on a.KELBRG_ID_KELBRG = kuda.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select a.KELBRG_ID_KELBRG, SUM(b.QTY_ONH) as QTY_CURRENT, SUM(b.QTY_ONH * a.HET) as AMOUNT_CURRENT from HONDA_H300_MASTER_PARTS a left join HONDA_H300_PART_LOKASES b " +
						"	on a.PART_NO = b.PART_PART_NO " +
						"	where a.VPARTSTAT = 'C' " +
						"	and a.KELBRG_ID_KELBRG in " +
						"	( " +
						"   'IMPOR','TAS','PLAST', 'STR',   'RIMWH',   'MUF',   'BRAKE',   'RBR',   'VALVE',   'BRNG',   'SPOKE',   'ELECT',   'CHAIN',   'CABLE',   'SPLUG',   'SPLUR',   'SDN', " +
						"   'SHOCK',   'TIRE',   'TBHGP',   'TBVL',   'GST',   'PSTN',   'SDN',   'DISK',   'RPIST',   'BATT',   'OSEAL',   'RPHVL',   'DIHVL',   'CDKIT',   'RWHVL',   'ACCEC',   'PACC', " +
						"   'HELM',   'CRKIT',   'CCKIT',   'EPHVL',   'TL',   'RSKIT',   'CDKGP',  'PSKIT',   'COOL',   'FLUID',   'SPGUI',   'BLDRV',   'SAOIL',   'LGS') " +
						"	GROUP by a.KELBRG_ID_KELBRG) curr " +
						"on a.KELBRG_ID_KELBRG = curr.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select a.KELBRG_ID_KELBRG, SUM(b.QTY_ONH) as QTY_NONCURRENT, SUM(b.QTY_ONH * a.HET) as AMOUNT_NONCURRENT from HONDA_H300_MASTER_PARTS a left join HONDA_H300_PART_LOKASES b " +
						"	on a.PART_NO = b.PART_PART_NO " +
						"	where a.VPARTSTAT = 'N' " +
						"	and a.KELBRG_ID_KELBRG in " +
						"	( " +
						"   'IMPOR','TAS','PLAST', 'STR',   'RIMWH',   'MUF',   'BRAKE',   'RBR',   'VALVE',   'BRNG',   'SPOKE',   'ELECT',   'CHAIN',   'CABLE',   'SPLUG',   'SPLUR',   'SDN', " +
						"   'SHOCK',   'TIRE',   'TBHGP',   'TBVL',   'GST',   'PSTN',   'SDN',   'DISK',   'RPIST',   'BATT',   'OSEAL',   'RPHVL',   'DIHVL',   'CDKIT',   'RWHVL',   'ACCEC',   'PACC', " +
						"   'HELM',   'CRKIT',   'CCKIT',   'EPHVL',   'TL',   'RSKIT',   'CDKGP',  'PSKIT',   'COOL',   'FLUID',   'SPGUI',   'BLDRV',   'SAOIL',   'LGS') " +
						"	GROUP by a.KELBRG_ID_KELBRG) noncurr " +
						"on a.KELBRG_ID_KELBRG = noncurr.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select a.KELBRG_ID_KELBRG, SUM(b.QTY_ONH) as QTY_OTHER, SUM(b.QTY_ONH * a.HET) as AMOUNT_OTHER from HONDA_H300_MASTER_PARTS a left join HONDA_H300_PART_LOKASES b " +
						"	on a.PART_NO = b.PART_PART_NO " +
						"	where a.VPARTSTAT <> 'C' " +
						"	and a.VPARTSTAT <> 'N' " +
						"	and a.KELBRG_ID_KELBRG in " +
						"	( " +
						"   'IMPOR','TAS','PLAST', 'STR',   'RIMWH',   'MUF',   'BRAKE',   'RBR',   'VALVE',   'BRNG',   'SPOKE',   'ELECT',   'CHAIN',   'CABLE',   'SPLUG',   'SPLUR',   'SDN', " +
						"   'SHOCK',   'TIRE',   'TBHGP',   'TBVL',   'GST',   'PSTN',   'SDN',   'DISK',   'RPIST',   'BATT',   'OSEAL',   'RPHVL',   'DIHVL',   'CDKIT',   'RWHVL',   'ACCEC',   'PACC', " +
						"   'HELM',   'CRKIT',   'CCKIT',   'EPHVL',   'TL',   'RSKIT',   'CDKGP',  'PSKIT',   'COOL',   'FLUID',   'SPGUI',   'BLDRV',   'SAOIL',   'LGS') " +
						"	GROUP by a.KELBRG_ID_KELBRG) other " +
						"on a.KELBRG_ID_KELBRG = other.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select master.KELBRG_ID_KELBRG, SUM(lokases.QTY_ONH) as DEAD_STOCK, SUM(lokases.QTY_ONH * master.HET) as DEAD_STOCK_AMOUNT from HONDA_H300_MASTER_PARTS master left join HONDA_H300_PART_LOKASES lokases " +
						"	on master.PART_NO = lokases.PART_PART_NO " +
						"		left join ( " +
						"			select DISTINCT(PART_PART_NO) as PART_NO_KUDA from HONDA_H300_DETAIL_FAKTURS where to_char(CREA_DATE,'YYYYMMDD') >= to_char(add_months(sysdate,-24),'YYYYMMDD') " +
						"		)kuda_dalam " +
						"	on lokases.PART_PART_NO = kuda_dalam.PART_NO_KUDA " +
						"	where master.KELBRG_ID_KELBRG in " +
						"	( " +
						"   'IMPOR','TAS','PLAST', 'STR',   'RIMWH',   'MUF',   'BRAKE',   'RBR',   'VALVE',   'BRNG',   'SPOKE',   'ELECT',   'CHAIN',   'CABLE',   'SPLUG',   'SPLUR',   'SDN', " +
						"   'SHOCK',   'TIRE',   'TBHGP',   'TBVL',   'GST',   'PSTN',   'SDN',   'DISK',   'RPIST',   'BATT',   'OSEAL',   'RPHVL',   'DIHVL',   'CDKIT',   'RWHVL',   'ACCEC',   'PACC', " +
						"   'HELM',   'CRKIT',   'CCKIT',   'EPHVL',   'TL',   'RSKIT',   'CDKGP',  'PSKIT',   'COOL',   'FLUID',   'SPGUI',   'BLDRV',   'SAOIL',   'LGS') " +
						"	and lokases.QTY_ONH > 0 " +
						"	and kuda_dalam.PART_NO_KUDA is null " +
						"	group by master.KELBRG_ID_KELBRG) dead " +
						"on a.KELBRG_ID_KELBRG = dead.KELBRG_ID_KELBRG " +
						"where a.KELBRG_ID_KELBRG in " +
						"( " +
						"   'IMPOR','TAS','PLAST', 'STR',   'RIMWH',   'MUF',   'BRAKE',   'RBR',   'VALVE',   'BRNG',   'SPOKE',   'ELECT',   'CHAIN',   'CABLE',   'SPLUG',   'SPLUR',   'SDN', " +
						"   'SHOCK',   'TIRE',   'TBHGP',   'TBVL',   'GST',   'PSTN',   'SDN',   'DISK',   'RPIST',   'BATT',   'OSEAL',   'RPHVL',   'DIHVL',   'CDKIT',   'RWHVL',   'ACCEC',   'PACC', " +
						"   'HELM',   'CRKIT',   'CCKIT',   'EPHVL',   'TL',   'RSKIT',   'CDKGP',  'PSKIT',   'COOL',   'FLUID',   'SPGUI',   'BLDRV',   'SAOIL',   'LGS') " +
						"group by  a.KELBRG_ID_KELBRG, kuda.qty, kuda.AMOUNT_TOT, curr.QTY_CURRENT, curr.amount_current, " +
						"noncurr.QTY_NONCURRENT, noncurr.amount_NONCurrent,other.QTY_OTHER, other.AMOUNT_OTHER, dead.DEAD_STOCK, dead.DEAD_STOCK_AMOUNT " +
						"union all " +
						"(select master.KELBRG_ID_KELBRG,'CARBU', sum(fakturs.qty/12) as RATA_RATA_QTY, sum(fakturs.NET_PRICE/12) as rata_rata_amount, " +
						"kuda.QTY as QTY_ONH, kuda.AMOUNT_TOT, curr.QTY as CURRENT_QTY, curr.AMOUNT_TOT as CURRENT_AMOUNT, noncurr.QTY as NONCURRENT_QTY, " +
						"noncurr.AMOUNT_TOT as NONCURRENT_AMOUNT, other.QTY as OTHER_QTY, other.AMOUNT_TOT as OTHER_AMOUNT, " +
						"dead.DEAD_STOCK, dead.DEAD_STOCK_AMOUNT  from HONDA_H300_MASTER_PARTS master " +
						"left join HONDA_H300_DETAIL_FAKTURS fakturs " +
						"on master.PART_NO = fakturs.PART_PART_NO " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO like '16100%') " +
						"	group by y.KELBRG_ID_KELBRG) kuda " +
						"on master.KELBRG_ID_KELBRG = kuda.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO like '16100%') " +
						"	and y.VPARTSTAT = 'C' " +
						"	group by y.KELBRG_ID_KELBRG) curr " +
						"on master.KELBRG_ID_KELBRG = curr.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO like '16100%') " +
						"	and y.VPARTSTAT = 'N' " +
						"	group by y.KELBRG_ID_KELBRG) noncurr " +
						"on master.KELBRG_ID_KELBRG = noncurr.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO like '16100%') " +
						"	and y.VPARTSTAT <> 'C' " +
						"	and y.VPARTSTAT <> 'N' " +
						"	group by y.KELBRG_ID_KELBRG) other " +
						"on master.KELBRG_ID_KELBRG = other.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select master.KELBRG_ID_KELBRG, SUM(lokases.QTY_ONH) as DEAD_STOCK, SUM(lokases.QTY_ONH * master.HET) as DEAD_STOCK_AMOUNT from HONDA_H300_MASTER_PARTS master left join HONDA_H300_PART_LOKASES lokases " +
						"	on master.PART_NO = lokases.PART_PART_NO " +
						"		left join ( " +
						"			select DISTINCT(PART_PART_NO) as PART_NO_KUDA from HONDA_H300_DETAIL_FAKTURS where to_char(CREA_DATE,'YYYYMMDD') >= to_char(add_months(sysdate,-24),'YYYYMMDD') " +
						"		)kuda_dalam " +
						"	on lokases.PART_PART_NO = kuda_dalam.PART_NO_KUDA " +
						"	where lokases.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO like '16100%') " +
						"	and lokases.QTY_ONH > 0 " +
						"	and kuda_dalam.PART_NO_KUDA is null " +
						"	group by master.KELBRG_ID_KELBRG) dead " +
						"on master.KELBRG_ID_KELBRG = dead.KELBRG_ID_KELBRG " +
						"where master.PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO like '16100%') " +
						"and to_char(fakturs.CREA_DATE,'YYYYMMDD') >= to_char(add_months(sysdate,-12),'YYYYMMDD') " +
						"group by master.KELBRG_ID_KELBRG, kuda.QTY, kuda.AMOUNT_TOT, curr.QTY, curr.AMOUNT_TOT,noncurr.QTY, noncurr.AMOUNT_TOT, " +
						"other.QTY, other.AMOUNT_TOT,dead.DEAD_STOCK, dead.DEAD_STOCK_AMOUNT) " +
						"union all " +
						"(select master.KELBRG_ID_KELBRG,'NON CARBU', sum(fakturs.qty/12) as RATA_RATA_QTY, sum(fakturs.NET_PRICE/12) as rata_rata_amount, " +
						"kuda.QTY as QTY_ONH, kuda.AMOUNT_TOT, curr.QTY as CURRENT_QTY, curr.AMOUNT_TOT as CURRENT_AMOUNT, noncurr.QTY as NONCURRENT_QTY, " +
						"noncurr.AMOUNT_TOT as NONCURRENT_AMOUNT, other.QTY as OTHER_QTY, other.AMOUNT_TOT as OTHER_AMOUNT, " +
						"dead.DEAD_STOCK, dead.DEAD_STOCK_AMOUNT  from HONDA_H300_MASTER_PARTS master " +
						"left join HONDA_H300_DETAIL_FAKTURS fakturs " +
						"on master.PART_NO = fakturs.PART_PART_NO " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO not like '16100%') " +
						"	group by y.KELBRG_ID_KELBRG) kuda " +
						"on master.KELBRG_ID_KELBRG = kuda.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO not like '16100%') " +
						"	and y.VPARTSTAT = 'C' " +
						"	group by y.KELBRG_ID_KELBRG) curr " +
						"on master.KELBRG_ID_KELBRG = curr.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO not like '16100%') " +
						"	and y.VPARTSTAT = 'N' " +
						"	group by y.KELBRG_ID_KELBRG) noncurr " +
						"on master.KELBRG_ID_KELBRG = noncurr.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select y.KELBRG_ID_KELBRG ,SUM(x.QTY_ONH) as QTY, SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y " +
						"	on x.part_part_no = y.PART_no " +
						"	where x.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO not like '16100%') " +
						"	and y.VPARTSTAT <> 'C' " +
						"	and y.VPARTSTAT <> 'N' " +
						"	group by y.KELBRG_ID_KELBRG) other " +
						"on master.KELBRG_ID_KELBRG = other.KELBRG_ID_KELBRG " +
						"left join " +
						"	(select master.KELBRG_ID_KELBRG, SUM(lokases.QTY_ONH) as DEAD_STOCK, SUM(lokases.QTY_ONH * master.HET) as DEAD_STOCK_AMOUNT from HONDA_H300_MASTER_PARTS master left join HONDA_H300_PART_LOKASES lokases " +
						"	on master.PART_NO = lokases.PART_PART_NO " +
						"		left join ( " +
						"			select DISTINCT(PART_PART_NO) as PART_NO_KUDA from HONDA_H300_DETAIL_FAKTURS where to_char(CREA_DATE,'YYYYMMDD') >= to_char(add_months(sysdate,-24),'YYYYMMDD') " +
						"		)kuda_dalam " +
						"	on lokases.PART_PART_NO = kuda_dalam.PART_NO_KUDA " +
						"	where lokases.PART_PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO not like '16100%') " +
						"	and lokases.QTY_ONH > 0 " +
						"	and kuda_dalam.PART_NO_KUDA is null " +
						"	group by master.KELBRG_ID_KELBRG) dead " +
						"on master.KELBRG_ID_KELBRG = dead.KELBRG_ID_KELBRG " +
						"where master.PART_NO in (select PART_NO from HONDA_H300_MASTER_PARTS z where z.KELBRG_ID_KELBRG = 'AHM' and z.PART_NO not like '16100%') " +
						"and to_char(fakturs.CREA_DATE,'YYYYMMDD') >= to_char(add_months(sysdate,-12),'YYYYMMDD') " +
						"group by master.KELBRG_ID_KELBRG, kuda.QTY, kuda.AMOUNT_TOT, curr.QTY, curr.AMOUNT_TOT,noncurr.QTY, noncurr.AMOUNT_TOT, " +
						"other.QTY, other.AMOUNT_TOT,dead.DEAD_STOCK, dead.DEAD_STOCK_AMOUNT) ";

		PreparedStatement ps = connection.prepareStatement(sQuery);
		ResultSet rs = ps.executeQuery();

		ResultSetMetaData colInfo = rs.getMetaData();
		List<String> colNames = new ArrayList<String>();
		HSSFRow titleRow = xlsSheet.createRow(rowIndex++);


		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			xlsSheet.setColumnWidth((int) (i-1), (int) 4000);
		}

		// Save all the data from the database table rows
		while (rs.next()) {
			HSSFRow dataRow = xlsSheet.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				if(colIndex == 0 || colIndex == 1){
					dataRow.createCell(colIndex++).setCellValue(
							new HSSFRichTextString(rs.getString(colName)));
				}else{
					Cell cell = dataRow.createCell(colIndex++);
					if(rs.getDouble(colName) == 0){
						cell.setCellValue(0);
					}else{
						BigDecimal bd = rs.getBigDecimal(colName);
						bd.setScale(0, RoundingMode.HALF_UP);
						cell.setCellValue(bd.doubleValue());

						cell.setCellStyle(style);
					}
				}
			}
		}

		//TIRE
		HSSFSheet sheetTire = xlsWorkBook.createSheet("DETAIL_DEAD");
		rowIndex = 0;

		sQuery = 
				"( " +
						"   select " +
						"   master.KELBRG_ID_KELBRG, " +
						"   master.PART_NO , " +
						"   SUM(lokases.QTY_ONH) as DEAD_STOCK, " +
						"   SUM(lokases.QTY_ONH * master.HET) as DEAD_STOCK_AMOUNT " +
						"   from HONDA_H300_MASTER_PARTS master " +
						"   left join HONDA_H300_PART_LOKASES lokases on master.PART_NO = lokases.PART_PART_NO " +
						"   left join " +
						"   ( " +
						"      select " +
						"      DISTINCT(PART_PART_NO) as PART_NO_KUDA " +
						"      from HONDA_H300_DETAIL_FAKTURS " +
						"      where to_char " +
						"      ( " +
						"         CREA_DATE,'YYYYMMDD' " +
						"      ) " +
						"      >= to_char(add_months(sysdate,-24),'YYYYMMDD') " +
						"   ) " +
						"   kuda_dalam on lokases.PART_PART_NO = kuda_dalam.PART_NO_KUDA " +
						"   where master.KELBRG_ID_KELBRG in " +
						"   ( " +
						"      'IMPOR', " +
						"      'TAS', " +
						"      'PLAST', " +
						"      'STR', " +
						"      'RIMWH', " +
						"      'MUF', " +
						"      'BRAKE', " +
						"      'RBR', " +
						"      'VALVE', " +
						"      'BRNG', " +
						"      'SPOKE', " +
						"      'ELECT', " +
						"      'CHAIN', " +
						"      'CABLE', " +
						"      'SPLUG', " +
						"      'SPLUR', " +
						"      'SDN', " +
						"      'SHOCK', " +
						"      'TIRE', " +
						"      'TBHGP', " +
						"      'TBVL', " +
						"      'GST', " +
						"      'PSTN', " +
						"      'SDN', " +
						"      'DISK', " +
						"      'RPIST', " +
						"      'BATT', " +
						"      'OSEAL', " +
						"      'RPHVL', " +
						"      'DIHVL', " +
						"      'CDKIT', " +
						"      'RWHVL', " +
						"      'ACCEC', " +
						"      'PACC', " +
						"      'HELM', " +
						"      'CRKIT', " +
						"      'CCKIT', " +
						"      'EPHVL', " +
						"      'TL', " +
						"      'RSKIT', " +
						"      'CDKGP', " +
						"      'PSKIT', " +
						"      'COOL', " +
						"      'FLUID', " +
						"      'SPGUI', " +
						"      'BLDRV', " +
						"      'SAOIL', " +
						"      'LGS' " +
						"   ) " +
						"   and lokases.QTY_ONH > 0 " +
						"   and kuda_dalam.PART_NO_KUDA is null " +
						"   group by master.KELBRG_ID_KELBRG, master.part_NO " +
						") " +
						"union " +
						"ALL " +
						"( " +
						"   select " +
						"   y.KELBRG_ID_KELBRG , " +
						"   y.PART_NO, " +
						"   SUM(x.QTY_ONH) as QTY, " +
						"   SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT " +
						"   FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y on x.part_part_no = y.PART_no " +
						"   where x.PART_PART_NO in " +
						"   ( " +
						"      select " +
						"      PART_NO " +
						"      from HONDA_H300_MASTER_PARTS z " +
						"      where z.KELBRG_ID_KELBRG = 'AHM' " +
						"      and z.PART_NO like '16100%' " +
						"   ) " +
						"   group by y.KELBRG_ID_KELBRG,y.PART_NO " +
						") " +
						"union " +
						"all " +
						"( " +
						"   select " +
						"   y.KELBRG_ID_KELBRG , " +
						"   y.PART_NO, " +
						"   SUM(x.QTY_ONH) as QTY, " +
						"   SUM(x.QTY_ONH * y.HET) as AMOUNT_TOT " +
						"   FROM HONDA_H300_PART_LOKASES x join HONDA_H300_MASTER_PARTS y on x.part_part_no = y.PART_no " +
						"   where x.PART_PART_NO in " +
						"   ( " +
						"      select " +
						"      PART_NO " +
						"      from HONDA_H300_MASTER_PARTS z " +
						"      where z.KELBRG_ID_KELBRG = 'AHM' " +
						"      and z.PART_NO not like '16100%' " +
						"   ) " +
						"   group by y.KELBRG_ID_KELBRG,y.PART_NO " +
						") ";

		ps = connection.prepareStatement(sQuery);
		rs = ps.executeQuery();

		colInfo = rs.getMetaData();
		colNames = new ArrayList<String>();
		titleRow = sheetTire.createRow(rowIndex++);

		for (int i = 1; i <= colInfo.getColumnCount(); i++) {
			colNames.add(colInfo.getColumnName(i));
			titleRow.createCell((int) (i-1)).setCellValue(
					new HSSFRichTextString(colInfo.getColumnName(i)));
			sheetTire.setColumnWidth((int) (i-1), (int) 4000);
		}

		while (rs.next()) {
			HSSFRow dataRow = sheetTire.createRow(rowIndex++);
			int colIndex = 0;
			for (String colName : colNames) {
				dataRow.createCell(colIndex++).setCellValue(
						new HSSFRichTextString(rs.getString(colName)));
			}
		}

		FileOutputStream fOut = new FileOutputStream("reportAHMBaru.xls");
		xlsWorkBook.write(fOut);
		fOut.flush();
		fOut.close();
		connection.close();

		File file = new File("reportAHMBaru.xls");
		Filedownload.save(file, "XLS");

		System.out.println("done");


	}
	
	private String getTglCreateFile(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmss");
		return sdf.format(new Date());
	}

}

