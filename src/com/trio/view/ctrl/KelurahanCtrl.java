package com.trio.view.ctrl;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Messagebox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h100.TrioH100Kelurahans;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.KelurahanListRenderer;

public class KelurahanCtrl extends TrioBasePage {

	private static final long serialVersionUID = 20111130143824L;
	private AnnotateDataBinder binder;
	private Listbox kelurahanList;
	private Grid editKelurahanGrid;
	private Button createKelurahan;
	private Button updateKelurahan;
	private Button deleteKelurahan;
	private Button searchKelurahan;
	
	private TrioH100Kelurahans _kelurahan;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		binder = (AnnotateDataBinder) page.getAttribute("binder");
		_kelurahan = new TrioH100Kelurahans();
		kelurahanList.setModel(new ListModelList(getMasterFacade().getTrioH100KelurahansDao().findAll()));
		kelurahanList.setItemRenderer(new KelurahanListRenderer());
	}
	
	private ListModelList getModel() {
		return (ListModelList) kelurahanList.getModel();
	}
	
	public void onClick$resetKelurahan() {
		kelurahanList.clearSelection();
		kelurahanList.setModel(new ListModelList(getMasterFacade().getTrioH100KelurahansDao().findAll()));
		_kelurahan = new TrioH100Kelurahans();
		binder.loadComponent(editKelurahanGrid);
		createKelurahan.setDisabled(false);
		updateKelurahan.setDisabled(true);
		deleteKelurahan.setDisabled(true);
		//page.getFellow("contactDiv").setVisible(false);
	}
	
	//set selection to edit data
	public void onSelect$kelurahanList() {
		_kelurahan = (TrioH100Kelurahans) kelurahanList.getSelectedItem().getValue();
		binder.loadComponent(editKelurahanGrid);
		createKelurahan.setDisabled(true);
		updateKelurahan.setDisabled(false);
		deleteKelurahan.setDisabled(false);
		// used for Hibernate lazy-loading
		//_kelurahan = (Kelurahan) ServiceLocator.getHibernateSession().merge(_kelurahan);
		//Event event = new Event("onLoad", page.getFellow("contactDiv"), _kelurahan);
		//EventQueues.lookup("loadContact", EventQueues.DESKTOP, true).publish(event);
	}
	
	//register onClick event for creating new object into list model
	public void onClick$createKelurahan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		if (_kelurahan.getKdKec() == null && _kelurahan.getKelurahan() == null) {
			Messagebox.show("no new content to add");
		} else {
			getMasterFacade().getTrioH100KelurahansDao().saveOrUpdate(_kelurahan, getUserSession());
			getModel().add(_kelurahan);
		}
	}
	
	//register onClick event for updating edited data in list model
	public void onClick$updateKelurahan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		Listitem listItem = kelurahanList.getSelectedItem();
		getMasterFacade().getTrioH100KelurahansDao().saveOrUpdate(_kelurahan, getUserSession());
		listItem.setValue(_kelurahan);
		int index = kelurahanList.getSelectedIndex();
		getModel().set(index, _kelurahan);
	}
	
	//register onClick event for removing data in list model
	public void onClick$deleteKelurahan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		Messagebox.show("Are you sure to delete?", null, Messagebox.YES | Messagebox.NO, Messagebox.QUESTION, new EventListener() {
			public void onEvent(Event event) throws Exception {
				if(event.getName().equals("onYes")) {
					getModel().remove(_kelurahan);
					getMasterFacade().getTrioH100KelurahansDao().delete(_kelurahan);
				}
			}
		});
	}
	
	public void onClick$searchKelurahan() throws InterruptedException {
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		//kelurahanList.setModel(new ListModelList(getMasterFacade().getTrioH100KelurahansDao().getKelurahanByCriteria(_kelurahan)));  
		kelurahanList.setModel(new ListModelList(getMasterFacade().getTrioH100KelurahansDao().findByExample(_kelurahan)));
		 
	}
	
	public TrioH100Kelurahans getKelurahan() {
		return _kelurahan;
	}
	
	public void setKelurahan(TrioH100Kelurahans kelurahan) {
		_kelurahan = kelurahan;
	}
}
