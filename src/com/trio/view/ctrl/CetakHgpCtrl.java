package com.trio.view.ctrl;

import java.util.List;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.databind.AnnotateDataBinder;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.trio.base.TrioBasePage;
import com.trio.bean.h300.TrioH300DtlPrograms;
import com.trio.bean.h300.TrioH300HdrHadiahs;
import com.trio.bean.h300.TrioH300HdrPrograms;
import com.trio.component.TrioPopup;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.TrioProgramsListRenderer;

public class CetakHgpCtrl extends TrioBasePage{

	private Window win;

	private Grid myGrid;
	private Textbox tbProgram;
	private Textbox tbIdProgram;
	private Button btnProgramLov;
	private AnnotateDataBinder binder;

	private Listbox listBoxHadiah;
	private Listbox listBoxItem;

	private TrioH300HdrPrograms programs;

	private static final long serialVersionUID = 1L;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.setType(getConnectionSession()));
		binder = (AnnotateDataBinder) page.getAttribute("binder");
	}


	public void onClick$btnProgramLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		System.out.println("ini LOV");

		List<TrioH300HdrPrograms> lists = getMasterFacade().getTrioH300HdrProgramsDao().findAll();
		final Listbox lb = new Listbox();
		Listhead ld = new Listhead();

		final TrioPopup popUp= new TrioPopup(lists, new TrioProgramsListRenderer(), lb, "300px", "400px");

		Listheader lh = new Listheader("NAMA PROGRAM");
		Listheader lh2 = new Listheader("ID PROGRAM");

		lh.setParent(ld);
		lh2.setParent(ld);
		ld.setParent(lb);

		lb.addEventListener(Events.ON_SELECT, new EventListener() {
			@Override
			public void onEvent(Event arg0) throws Exception {
				TrioH300HdrPrograms programs = (TrioH300HdrPrograms) lb.getSelectedItem().getValue();
				setPrograms(programs);
				binder.loadComponent(myGrid);

				DatabaseContextHolder.setConnectionType(ConnectionType.PART);
				List<TrioH300HdrHadiahs> lists = getMasterFacade().getTrioH300HdrHadiahsDao()
						.findHadiahById(tbIdProgram.getValue());
				listBoxHadiah.setModel(new ListModelList(lists));
				listBoxHadiah.setItemRenderer(new ListitemRenderer() {

					@Override
					public void render(Listitem item, Object data) throws Exception {
						TrioH300HdrHadiahs hadiah = (TrioH300HdrHadiahs) data;
						item.setValue(hadiah);

						new Listcell(hadiah.getHadiah()).setParent(item);
						new Listcell(hadiah.getTrioH300HdrHadiahsPK().getPoint().toString()).setParent(item);
					}
				});

				DatabaseContextHolder.setConnectionType(ConnectionType.PART);
				List<TrioH300DtlPrograms> lists1 = getMasterFacade()
						.getTrioH300DtlProgramsDao().findItemById(tbIdProgram.getValue());
				listBoxItem.setModel(new ListModelList(lists1));
				listBoxItem.setItemRenderer(new ListitemRenderer() {
					
					@Override
					public void render(Listitem item, Object data) throws Exception {
						TrioH300DtlPrograms dtlPrograms= (TrioH300DtlPrograms) data;
						item.setValue(dtlPrograms);
						
						new Listcell(dtlPrograms.getTrioH300DtlProgramsPK().getPartNo()).setParent(item);
						new Listcell(dtlPrograms.getPoint().toString()).setParent(item);
					}
				});
				
				popUp.close();
			}
		});

		lb.setParent(popUp);
		popUp.setParent(win);
		popUp.open(self);
	}


	public TrioH300HdrPrograms getPrograms() {
		return programs;
	}

	public void setPrograms(TrioH300HdrPrograms programs) {
		this.programs = programs;
	}
}
