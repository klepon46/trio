package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h300.TrioH300HdrPrograms;

public class TrioProgramsListRenderer implements ListitemRenderer{

	@Override
	public void render(Listitem item, Object data) throws Exception {
		
		TrioH300HdrPrograms programs = (TrioH300HdrPrograms) data;
		item.setValue(programs);
		
		new Listcell(programs.getNamaProgram()).setParent(item);
		new Listcell(programs.getIdProgram()).setParent(item);
		
	}
}
