package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100Dlrpos;

/** @author Saifi Ahmada Jan 29, 2013 5:08:36 PM  **/

public class 
DealerPOSListRenderer implements ListitemRenderer{

	@Override
	public void render(Listitem item, Object data) throws Exception {
		TrioH100Dlrpos pos = (TrioH100Dlrpos) data;
		item.setValue(pos);
		
		new Listcell(pos.getKdDlrpos()).setParent(item);
		new Listcell(pos.getKdDlr()).setParent(item);
		new Listcell(pos.getNamaDlrpos()).setParent(item);
		
	}

}

