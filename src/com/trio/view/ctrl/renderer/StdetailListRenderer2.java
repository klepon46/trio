package com.trio.view.ctrl.renderer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.facade.MasterFacade;
import com.trio.util.TrioStringUtil;

public class StdetailListRenderer2 implements ListitemRenderer {
	
	MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");
	
	String nilaiPwConfig = mf.getHondaH000SetupsDao().findByValIdValTag("H1_CONFIG", "NOMINAL_PELANG_WIL").getValChar();

	public void render(Listitem item, Object data) throws Exception {
		
		String nilaiPwFormatted = TrioStringUtil.getPemisahRibuan(nilaiPwConfig);
		
		TrioH100Stdetails stdetail = (TrioH100Stdetails) data;
		item.setValue(stdetail);
		
		String nilaiPwDb = TrioStringUtil.getPemisahRibuan(stdetail.getNilaiPw().toString());
		
		new Listcell(stdetail.getNoMesin()).setParent(item);
		new Listcell(stdetail.getKdDlr()).setParent(item);
		new Listcell(stdetail.getNamaKonsumen()).setParent(item);
		new Listcell(stdetail.getCustomer().getAlamat1()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKota()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKecamatan()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKelurahan()).setParent(item);
		new Listcell(nilaiPwFormatted).setParent(item);
		new Listcell(stdetail.getNoApprovalPw()).setParent(item);
		new Listcell(stdetail.getPelangWils().getApprovedBy() == null ? "" : stdetail.getPelangWils().getApprovedBy()).setParent(item);
		new Listcell(getStringFromDate(stdetail.getPelangWils().getTglLunasPw())).setParent(item);
		new Listcell(getStringFromDate(stdetail.getPelangWils().getTglTagihPw())).setParent(item);
		new Listcell(nilaiPwDb).setParent(item);
	}
	
	public String getStringFromDate(Date tgl){	
		if (tgl == null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(tgl);
	}
}
