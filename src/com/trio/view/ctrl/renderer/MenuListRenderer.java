package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h000.TrioH000Mstmenu;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 31, 2012
 */
public class MenuListRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data) throws Exception {
		TrioH000Mstmenu menu = (TrioH000Mstmenu) data;
		item.setValue(menu);
		new Listcell(menu.getVmenuid()).setParent(item);
		new Listcell(menu.getVtitle()).setParent(item);
		new Listcell(menu.getVparent()).setParent(item);
		new Listcell(menu.getVlocation()).setParent(item);
		new Listcell(menu.getVimage()).setParent(item); 
		new Listcell(String.valueOf(menu.getNorderer())).setParent(item);
	}

}
