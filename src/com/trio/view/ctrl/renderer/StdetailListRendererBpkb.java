package com.trio.view.ctrl.renderer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.facade.MasterFacade;
import com.trio.util.TrioStringUtil;

/**
 * @author Gusti Arya 4:27:55 PM Jun 16, 2014
 */

public class StdetailListRendererBpkb implements ListitemRenderer{

	MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");

	String nilaiPwConfig = mf.getHondaH000SetupsDao().findByValIdValTag("H1_CONFIG", "NOMINAL_PELANG_WIL").getValChar();

	@Override
	public void render(Listitem item, Object data) throws Exception {
		String nilaiPwFormatted = TrioStringUtil.getPemisahRibuan(nilaiPwConfig);

		final TrioH100Stdetails stdetail = (TrioH100Stdetails) data;

		item.setValue(stdetail);
		new Listcell(stdetail.getNoMesin()).setParent(item);
		new Listcell(stdetail.getKdDlr()).setParent(item);
		new Listcell(stdetail.getNamaKonsumen()).setParent(item);
		new Listcell(stdetail.getCustomer().getAlamat1()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKota()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKecamatan()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKelurahan()).setParent(item);
		new Listcell(nilaiPwFormatted).setParent(item);
		new Listcell(stdetail.getNoApprovalPw() == null ? "" : stdetail.getNoApprovalPw()).setParent(item);
		new Listcell("").setParent(item);
		new Listcell("").setParent(item);
		new Listcell("").setParent(item);
		new Listcell(nilaiPwConfig).setParent(item);
//		Listcell lc = new Listcell();
//		Intbox ib = new Intbox(Integer.parseInt(nilaiPwConfig));
//		ib.setParent(lc);
//		lc.setParent(item);
		Listcell lc2 = new Listcell();
		Checkbox cb = new Checkbox();
		cb.setDisabled(true);
		cb.setParent(lc2);
		lc2.setParent(item);

	}
	
	public String getStringFromDate(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(tgl);
	}

}
