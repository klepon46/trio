package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100DtlAreaDealers;

public class DtlAreaDealersListRenderer implements ListitemRenderer {

	public void render(Listitem item, Object data) throws Exception {
		TrioH100DtlAreaDealers dealer = (TrioH100DtlAreaDealers) data;
		item.setValue(dealer);
		new Listcell(dealer.getTrioH100Kecamatans().getKota()).setParent(item);
		new Listcell(dealer.getKdDlr()).setParent(item);
		new Listcell(dealer.getKdDlrpos()).setParent(item);
		new Listcell(dealer.getKdKec()).setParent(item);
		new Listcell(dealer.getTrioH100Kecamatans().getKecamatan()).setParent(item);
		new Listcell(dealer.getStatus()).setParent(item);
	}
}
