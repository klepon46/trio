package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.util.TrioStringUtil;

public class KoreksiPelangWilRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object object) throws Exception {

		TrioH100Stdetails dt = (TrioH100Stdetails) object;
		item.setValue(dt);

		String nilaiPw = TrioStringUtil.getPemisahRibuan(dt.getNilaiPw());

		new Listcell(dt.getNoMesin()).setParent(item);
		new Listcell(dt.getDealer().getDlrNama()).setParent(item);
		new Listcell(dt.getNamaKonsumen()).setParent(item);
		new Listcell(dt.getCustomer().getAlamat1()).setParent(item);
		new Listcell(dt.getCustomer().getKota()).setParent(item);
		new Listcell(dt.getCustomer().getKecamatan()).setParent(item);
		new Listcell(dt.getCustomer().getKelurahan()).setParent(item);
//		new Listcell(dt.getKelurahan().getKota()).setParent(item);	
		new Listcell(nilaiPw).setParent(item);

		Listcell lc1 = new Listcell();
		Intbox in = new Intbox(dt.getNilaiPw());
		in.setParent(lc1);
		lc1.setParent(item);

		Listcell lc2 = new Listcell();
		Checkbox cb = new Checkbox();
		cb.setParent(lc2);
		lc2.setParent(item);

		if (cb.isChecked()){
			in.setDisabled(true);
		}

	}
}
