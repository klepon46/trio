package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h000.HondaH000Propins;

public class PropinsiRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data) throws Exception {
		HondaH000Propins prop = (HondaH000Propins) data;
		
		item.setValue(prop);
		new Listcell(prop.getVdeskripsi()).setParent(item);
	}

}
