package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

/**
 * @author glassfish | Saipi Ramli
 *
 * Nov 5, 2012
 */
public class DbaRoleRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data) throws Exception {
		
		String dba = (String) data;
		item.setValue(dba);
		new Listcell(dba).setParent(item);
	}
	

}

