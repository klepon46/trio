package com.trio.view.ctrl.renderer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

import com.trio.bean.h100.HondaH100Dtlpicklists;
import com.trio.bean.h100.HondaH100Stdetails;
import com.trio.facade.MasterFacade;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;

public class DtlpicklistRenderer implements ListitemRenderer {

	private ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

	MasterFacade mf = (MasterFacade) applicationContext.getBean("masterFacade");

	@Override
	public void render(Listitem item, Object data) throws Exception {

		HondaH100Dtlpicklists dtl = (HondaH100Dtlpicklists) data;

		item.setValue(data);

		new Listcell(dtl.getVnomesin()).setParent(item);
		new Listcell(dtl.getHondaH000Items().getKdItem()).setParent(item);
		new Listcell(dtl.getVsel()).setParent(item);
		new Listcell("2012-09-01").setParent(item);

		Listcell lc = new Listcell();
		final Textbox tb = new Textbox();
		final Textbox tb2 = new Textbox();
		tb.addEventListener(Events.ON_CHANGE, new InfoClickListener(tb, tb2, dtl.getHondaH000Items().getKdItem(),dtl.getVnomesin(), dtl.getVsel()));
		tb.setParent(lc);
		lc.setParent(item);

		Listcell lc2 = new Listcell();
		tb2.setParent(lc2);
		tb2.setDisabled(true);
		lc2.setParent(item);

		Listcell lc3 = new Listcell();
		final Checkbox cb = new Checkbox();
		cb.addEventListener(Events.ON_CHECK, new EventListener() {

			@Override
			public void onEvent(Event arg0) throws Exception {

				if (tb.getValue() == null || tb.getValue().equalsIgnoreCase("") ){
					Messagebox.show("No mesin baru tidak boleh kosong");
					cb.setChecked(false);
					return;
				}
				if (tb2.getValue() == null || tb2.getValue().equalsIgnoreCase("") ){
					System.out.println("masuk if null");
					Messagebox.show("Lokasi tidak boleh kosong");
					cb.setChecked(false);
					return;
				}
			}
		});
		cb.setParent(lc3);
		lc3.setParent(item);

	}

	private final class InfoClickListener implements EventListener {

		private Textbox tb, tb2;
		private String kdItem, noMesinAsal, vSel;

		public InfoClickListener() {

		}


		public InfoClickListener(Textbox tb, Textbox tb2, String kdItem,
				String noMesinAsal, String vSel) {
			super();
			this.tb = tb;
			this.tb2 = tb2;
			this.kdItem = kdItem;
			this.noMesinAsal = noMesinAsal;
			this.vSel = vSel;
		}


		@Override
		public void onEvent(Event event) throws Exception {
			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
			String noMesin = tb.getValue();
			HondaH100Stdetails std = mf.getHondaH100StdetailsDao().findByNomesin(noMesin);

			if (noMesinAsal.equalsIgnoreCase(noMesin)){ 
				Messagebox.show("No mesin asal tidak boleh sama dengan no mesin pengganti");
				tb.setValue(null);
				return;
			}

			if (std == null){
				Messagebox.show("No mesin [ "+noMesin+" ] tidak ditemukan");
				tb.setValue(null);
				return;
			}

			std = mf.getHondaH100StdetailsDao().getNomesinPengganti(noMesin, kdItem);
			if (std == null){
				Messagebox.show("No mesin [ "+noMesin+" ] dan Kode Item [ "+kdItem+" ] tidak ditemukan");
				tb.setValue(null);
				return;
			}
			
			String kdItemPengganti = mf.getHondaH100StdetailsDao().getKdItemByNoMesin(noMesin);
			String kdItemAwal = mf.getHondaH100StdetailsDao().getKdItemByNoMesin(noMesinAsal);
			if(!kdItemAwal.equalsIgnoreCase(kdItemPengganti)){
				Messagebox.show("No Mesin [ " + noMesin + "] Kode Item berbeda");
				tb.setValue(null);
				return;
			}

			String vSelPengganti = mf.getHondaH100StdetailsDao().getVselByNoMesin(noMesin);
			System.out.println("awal = " + vSel);
			System.out.println("vsel Pengganti = " + vSelPengganti);
			
			if(!vSel.substring(0,1).equalsIgnoreCase(vSelPengganti)){
				Messagebox.show("No Mesin [ " + noMesin + " ] beda gudang");
				tb2.setValue(null);
				return;
			}

			tb2.setValue(std.getVsel());
		}
	}
}
