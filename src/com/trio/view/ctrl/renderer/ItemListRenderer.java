package com.trio.view.ctrl.renderer;

import org.zkoss.zk.ui.event.Event;

import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h000.HondaH000Items;
import com.trio.facade.MasterFacade;

public class ItemListRenderer implements ListitemRenderer {
	
	/*private MasterFacade getMasterFacade() {
		MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");
		return mf;
	}*/

	public void render(final Listitem item, Object data) throws Exception { 
		
		final HondaH000Items objectItem = (HondaH000Items) data; 
		item.setValue(objectItem);
		new Listcell(objectItem.getKdItem()).setParent(item);
		new Listcell(objectItem.getKdTipe()).setParent(item);
		new Listcell(objectItem.getKet2()).setParent(item);
		new Listcell(objectItem.getStatus()).setParent(item);
		new Listcell(objectItem.getTrioStatus()).setParent(item);
		Listcell lc2 = new Listcell();
		Checkbox cb = new Checkbox();
		cb.setParent(lc2);
		lc2.setParent(item);
		/*cb.addEventListener(Events.ON_CHECK, new EventListener() {
			
			@Override
			public void onEvent(Event event) throws Exception {
				Checkbox checkbox = (Checkbox)event.getTarget();
				if (objectItem.getTrioStatus().equalsIgnoreCase("Y")) {
					checkbox.setChecked(true);
				} else {
					checkbox.setChecked(false);
				}
			}
		});
		*/
		
		
	}
}
