package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100Kecamatans;

public class KecamatanListRenderer implements ListitemRenderer {

	public void render(Listitem item, Object data) throws Exception {
		TrioH100Kecamatans kecamatan = (TrioH100Kecamatans) data;
		item.setValue(kecamatan);
		new Listcell(kecamatan.getKdKec()).setParent(item);
		new Listcell(kecamatan.getKecamatan()).setParent(item);
		new Listcell(kecamatan.getKota()).setParent(item);
		
		Checkbox cb = new Checkbox();
		Listcell lc = new Listcell();
		cb.setParent(lc);
		lc.setParent(item);
	}
}
