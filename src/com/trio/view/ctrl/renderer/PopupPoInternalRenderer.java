package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.HondaH100Mstpodlrs;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 15, 2012
 */
public class PopupPoInternalRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data) throws Exception {
		
		HondaH100Mstpodlrs podlr = (HondaH100Mstpodlrs) data;
		item.setValue(podlr);
		
		new Listcell(podlr.getNoPoint()).setParent(item); 
		new Listcell(podlr.getDealerKdDlr().getDlrNama()).setParent(item); 
		new Listcell(podlr.getVnama()).setParent(item);
	}

}
