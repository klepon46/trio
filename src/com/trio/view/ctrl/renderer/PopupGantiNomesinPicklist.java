package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.HondaH100Hdrpicklists;

public class PopupGantiNomesinPicklist implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data) throws Exception {
		
		HondaH100Hdrpicklists pl = (HondaH100Hdrpicklists) data;
		
		item.setValue(pl);
		
		new Listcell(pl.getVnopicklist()).setParent(item);
		new Listcell(pl.getHondaH100Fakdos().getNoDo()).setParent(item);
		new Listcell(pl.getHondaH100Fakdos().getHondaH000Dealers().getDlrNama()).setParent(item);
		
	}

}
