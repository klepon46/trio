package com.trio.view.ctrl.renderer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.zkoss.zkplus.spring.SpringUtil;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h100.TrioH100Stdetails;
import com.trio.facade.MasterFacade;
import com.trio.util.TrioStringUtil;

public class StdetailListRendererSpv implements ListitemRenderer {
	
	//private ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
	
	MasterFacade mf = (MasterFacade) SpringUtil.getBean("masterFacade");
	
	String nilaiPwConfig = mf.getHondaH000SetupsDao().findByValIdValTag("H1_CONFIG", "NOMINAL_PELANG_WIL").getValChar();
	
	public void render(Listitem item, Object data) throws Exception {
		
		String nilaiPwFormatted = TrioStringUtil.getPemisahRibuan(nilaiPwConfig);
		
		TrioH100Stdetails stdetail = (TrioH100Stdetails) data;
		item.setValue(stdetail);
		new Listcell(stdetail.getNoMesin()).setParent(item);
		new Listcell(stdetail.getKdDlr()).setParent(item);
		new Listcell(stdetail.getNamaKonsumen()).setParent(item);
		new Listcell(stdetail.getCustomer().getAlamat1()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKota()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKecamatan()).setParent(item);
		new Listcell(stdetail.getKelurahan().getKelurahan()).setParent(item);
		new Listcell(nilaiPwFormatted).setParent(item);
		new Listcell(stdetail.getNoApprovalPw() == null ? "" : stdetail.getNoApprovalPw()).setParent(item);
		new Listcell("").setParent(item);
		new Listcell("").setParent(item);
		new Listcell("").setParent(item);
		new Listcell(nilaiPwFormatted).setParent(item);
		Listcell lc2 = new Listcell();
		Checkbox cb = new Checkbox();
		cb.setParent(lc2);
		lc2.setParent(item);
	}
	
	public String getStringFromDate(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		return sdf.format(tgl);
	}
}
