package com.trio.view.ctrl.renderer;

import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;

import com.trio.bean.h300.HondaH300Salesman;

/**
 * @author glassfish | Saipi Ramli
 *
 * Oct 15, 2012
 */
public class SalesmanRenderer implements ListitemRenderer {

	@Override
	public void render(Listitem item, Object data) throws Exception {
		
		HondaH300Salesman sales = (HondaH300Salesman) data;
		item.setValue(sales);
		
		new Listcell(sales.getKodeSales()).setParent(item);
		new Listcell(sales.getNamaSales()).setParent(item);
		
	}

}
