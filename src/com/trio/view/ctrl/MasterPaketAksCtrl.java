package com.trio.view.ctrl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Button;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import com.trio.base.TrioBasePage;
import com.trio.bean.h000.HondaH000Items;
import com.trio.bean.h100.TrioH100DtlpktaksItem;
import com.trio.bean.h100.TrioH100DtlpktaksPart;
import com.trio.bean.h100.TrioH100Pktaks;
import com.trio.bean.h300.HondaH300MasterParts;
import com.trio.bean.helper.TrioBeanPartHelper;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.TrioDateConv;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;

/**
 * @author Gusti Arya 3:31:52 PM Dec 20, 2013
 */

public class MasterPaketAksCtrl extends TrioBasePage {

	private static final long serialVersionUID = 1L;

	private Window win;

	private Textbox tbNamaPaket;
	private Textbox tbKodePaket;
	private Checkbox cbCek;

	private Button btnLovPart;
	private Button btnLovItem;
	private Button btnAddPart;

	private Popup popUpPart;
	private Popup popUpItem;
	private Textbox tbPartSearch;
	private Textbox tbItemSearch;
	private Listbox lbLovPart;
	private Listbox lbLovItem;
	private Button btnPopPartSearch;

	private Listbox lbPart;
	private Listbox lbItem;

	private Textbox tbNoPart;
	private Textbox tbNamaPart;
	private Intbox tbQtyPart;

	private Button btnPopItemAdd;
	private Button btnSimpan;

	private HondaH300MasterParts parts; 

	private ListModelList lm;
	private List<TrioBeanPartHelper> listPartHelper;
	List<HondaH000Items> listItem;
	private Set<HondaH000Items> setItem;


	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		listPartHelper = new ArrayList<TrioBeanPartHelper>();
		listItem = new ArrayList<HondaH000Items>();

		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);

		String str = getMasterFacade().getHondaH000SetupsDao().findValCharByValIdValTag("BDLAKS", TrioDateConv.format(new Date(),"yyyy"));
		if(str == null){
			str = getMasterFacade().getHondaH000SetupsDao().getRunningNumber("BDLAKS", TrioDateConv.format(new Date(),"yyyy"), getUserSession()); 
		}
		int x = Integer.parseInt(str);
		x = x + 1 ;
		str = String.valueOf(x);

		String kdPktAks = TrioStringUtil.getFormattedRunno2("BDLAKS", Integer.valueOf(str));
		tbKodePaket.setValue(kdPktAks);
	}

	public void onCheck$cbCek(){
		if(cbCek.isChecked()){

			tbNoPart.setDisabled(true);
			tbQtyPart.setDisabled(true);
			btnLovPart.setDisabled(true);
			btnLovItem.setDisabled(false);
			btnAddPart.setDisabled(true);;
		}else{
			tbNamaPaket.setDisabled(false);
			tbNoPart.setDisabled(false);
			tbQtyPart.setDisabled(false);
			btnLovPart.setDisabled(false);
			btnLovItem.setDisabled(true);
			btnAddPart.setDisabled(false);
		}

	}

	public void onChange$tbNoPart() throws WrongValueException, InterruptedException{
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		try {
			parts = getMasterFacade().getHondaH300MasterPartsDao().findByPartNoManual(tbNoPart.getValue());
			tbNamaPart.setValue(parts.getPartName());
		} catch (NullPointerException e) {
			//			alert("Part iki ["+ tbNoPart.getValue() +"] Gak ono neng table HondaH300MasterParts le");
			Messagebox.show("Part iki ["+ tbNoPart.getValue() +"] Gak ono neng table HondaH300MasterParts le", "Error", Messagebox.OK, Messagebox.ERROR);
		}

		btnAddPart.setDisabled(false);
	}

	public void onClick$btnLovPart(){
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		List<HondaH300MasterParts> listPart = getMasterFacade().getHondaH300MasterPartsDao().findAll();
		lbLovPart.setModel(new ListModelList(listPart));
		lbLovPart.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH300MasterParts parts = (HondaH300MasterParts) data;
				item.setValue(parts);

				new Listcell(parts.getPartNo()).setParent(item);
				new Listcell(parts.getPartName()).setParent(item);
			}
		});

	}

	public void onSelect$lbLovPart(){
		parts = (HondaH300MasterParts) lbLovPart.getSelectedItem().getValue();
		tbNamaPart.setValue(parts.getPartName());
		tbNoPart.setValue(parts.getPartNo());

		tbNoPart.setDisabled(true);
		btnAddPart.setDisabled(false);
		popUpPart.close();

	}


	public void onClick$btnPartSearch(){
		String partNo = tbPartSearch.getValue();
		List<HondaH300MasterParts> list = getMasterFacade().getHondaH300MasterPartsDao().findByPartNo(partNo);
		lbLovPart.setModel(new ListModelList(list));
		lbLovPart.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH300MasterParts parts = (HondaH300MasterParts) data;
				item.setValue(parts);

				new Listcell(parts.getPartNo()).setParent(item);
				new Listcell(parts.getPartName()).setParent(item);
			}
		});

	}

	public void onClick$btnAddPart(){
		if(listPartHelper == null){
			listPartHelper = new ArrayList<TrioBeanPartHelper>();
		}


		for(TrioBeanPartHelper current : listPartHelper){
			if(current.getPartNo().equalsIgnoreCase(tbNoPart.getValue())){
				alert("No Part Tidak Boleh Sama");
				return;
			}
		}

		TrioBeanPartHelper helper = new TrioBeanPartHelper();
		helper.setPartName(parts.getPartName());
		helper.setPartNo(parts.getPartNo());

		if(tbQtyPart.getValue() == null || tbQtyPart.getValue() == 0){
			alert("Qty Tidak Boleh Null Atau Kosong");
			return;
		}

		helper.setQty(tbQtyPart.getValue());


		listPartHelper.add(helper);

		lbPart.setModel(new ListModelList(listPartHelper));
		lbPart.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				TrioBeanPartHelper parts = (TrioBeanPartHelper) data;
				item.setValue(parts);

				new Listcell(parts.getPartNo()).setParent(item);
				new Listcell(parts.getPartName()).setParent(item);
				new Listcell(parts.getQty().toString()).setParent(item);

				final Listcell lc = new Listcell();
				Checkbox cb = new Checkbox();
				cb.setParent(lc);
				lc.setParent(item);
			}
		});

		tbNoPart.setDisabled(false);
		tbNoPart.setValue(null);
		tbNamaPart.setValue(null);
		tbQtyPart.setValue(null);
		btnAddPart.setDisabled(true);
		btnLovItem.setDisabled(false);
	}


	@SuppressWarnings("unchecked")
	public void onClick$btnDelPart(){
		Listbox box = (Listbox) lbPart.getFellow("win").getFellow("lbPart");
		List<Listitem> item = box.getItems();
		Checkbox checkBox = new Checkbox();
		String marma="";

		System.out.println(item);

		for(Listitem currentItem : item){
			List<Component> com =  currentItem.getChildren();
			for(Component currentCom : com){
				Listcell list = (Listcell) currentCom;

				if(list.getFirstChild() instanceof Checkbox){
					Checkbox cb = (Checkbox) list.getFirstChild();
					checkBox = cb;

				}
			}

			if(checkBox.isChecked()){

				for(Component current : com){
					Listcell list = (Listcell) current;

					if(list.getColumnIndex()==0){
						marma = list.getLabel();
						System.out.println("MARMA!! = " + marma);
					}

				}
				com.clear();
				afterDeletePart(marma);
			}
		}

		if(listPartHelper.size() == 0 || listPartHelper == null){
			btnLovItem.setDisabled(true);
		}
	}



	private void afterDeletePart(String str){
		Iterator<TrioBeanPartHelper> iterator = listPartHelper.iterator();
		while (iterator.hasNext()) {
			TrioBeanPartHelper partHelper= (TrioBeanPartHelper) iterator
					.next();
			if(partHelper.getPartNo().equals(str)){
				iterator.remove();
			}
		}

	}


	public void onClick$btnLovItem(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		List<HondaH000Items> listItems = getMasterFacade().getHondaH000ItemsDao().getKdItemByValTag();
		lbLovItem.setModel(new ListModelList(listItems));
		lbLovItem.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH000Items hondaItem = (HondaH000Items) data;
				item.setValue(hondaItem);

				new Listcell().setParent(item);
				new Listcell(hondaItem.getKdItem()).setParent(item);
				new Listcell(hondaItem.getKet2()).setParent(item);
			}
		});
	}

	public void onClick$btnItemSearch(){
		List<HondaH000Items> list = getMasterFacade().getHondaH000ItemsDao().getKdItemByValTagAndKdTipe(tbItemSearch.getValue());
		lbLovItem.setModel(new ListModelList(list));
		lbLovItem.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH000Items hondaItem = (HondaH000Items) data;
				item.setValue(hondaItem);

				new Listcell().setParent(item);
				new Listcell(hondaItem.getKdItem()).setParent(item);
				new Listcell(hondaItem.getKet2()).setParent(item);
			}
		});

	}

	@SuppressWarnings("unchecked")
	public void onSelect$lbLovItem(){

		if(lm == null){
			lm = new ListModelList();
		}
		lm = (ListModelList) lbLovItem.getListModel();

		if(setItem == null){
			setItem = new HashSet<HondaH000Items>();
		}

		setItem = lm.getSelection();

	}

	public void onClick$btnPopItemAdd(){

		listItem = new ArrayList<HondaH000Items>(setItem);

		lbItem.setModel(new ListModelList(listItem));
		lbItem.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH000Items hondaItem = (HondaH000Items) data;
				item.setValue(hondaItem);

				new Listcell(hondaItem.getKdItem()).setParent(item);
				new Listcell(hondaItem.getKet2()).setParent(item);
				final Listcell lc = new Listcell();
				Checkbox cb = new Checkbox();
				cb.setParent(lc);
				lc.setParent(item);

			}
		});

		btnSimpan.setDisabled(false);
		popUpItem.close();
	}


	@SuppressWarnings("unchecked")
	public void onClick$btnSimpan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);

		if(tbNamaPaket.getValue() == null || tbNamaPaket.getValue().equalsIgnoreCase("")){
			alert("Nama Paket Bundling Harus Diisi");
			return;
		}

		String noPart = "";
		int qty = 0;

		TrioH100Pktaks pktAks = new TrioH100Pktaks();
		Set<TrioH100DtlpktaksPart> setDtlPart = new HashSet<TrioH100DtlpktaksPart>();
		TrioH100DtlpktaksPart dtlPart;

		Listbox box  = (Listbox) lbPart.getFellow("win").getFellow("lbPart");
		List<Listitem> lists = box.getItems();

		//jika checkbox di cek maka set nama part menjadi X
		if(cbCek.isChecked()){
			dtlPart = new TrioH100DtlpktaksPart();

			dtlPart.getTrioH100DtlpktaksPartPK().setKdPkt(tbKodePaket.getValue());
			dtlPart.getTrioH100DtlpktaksPartPK().setPartNo("X");
			dtlPart.setQty(0);
			dtlPart.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
			dtlPart.getUserTrailing().setVcreaby(getUserSession());
			setDtlPart.add(dtlPart);
		}else{
			for(Listitem currentList : lists){
				dtlPart = new TrioH100DtlpktaksPart();

				List<Component> com = currentList.getChildren();
				for(Component currentCom : com){
					Listcell lc = (Listcell) currentCom;

					if(lc.getColumnIndex()==0){
						noPart = lc.getLabel();
					}

					if(lc.getColumnIndex()==2){
						qty = Integer.parseInt(lc.getLabel());
						System.out.println("INI QTY = " + qty);
					}

					dtlPart.getTrioH100DtlpktaksPartPK().setKdPkt(tbKodePaket.getValue());
					dtlPart.getTrioH100DtlpktaksPartPK().setPartNo(noPart);
					dtlPart.setQty(qty);
					dtlPart.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
					dtlPart.getUserTrailing().setVcreaby(getUserSession());
					setDtlPart.add(dtlPart);
				}

			}
		}

		ListModelList lmItem = (ListModelList) lbItem.getListModel();
		List<HondaH000Items> listItem = lmItem.getInnerList();

		TrioH100DtlpktaksItem dtlItem;
		Set<TrioH100DtlpktaksItem> setDtlItem = new HashSet<TrioH100DtlpktaksItem>();
		for(HondaH000Items current : listItem){
			dtlItem = new TrioH100DtlpktaksItem();
			final String getKdItem = getMasterFacade().getHondaH000SetupsDao()
					.findValTagByValIdValChar("H1_VARIASI_UNIT", current.getKdItem().substring(0,3));
			
			dtlItem.setKdPkt(tbKodePaket.getValue());
			dtlItem.setKdItem(current.getKdItem());
			dtlItem.setKdItemBaru(getKdItem+current.getKdItem().substring(3, 6));
			dtlItem.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
			dtlItem.getUserTrailing().setVcreaby(getUserSession());
			setDtlItem.add(dtlItem);
		}

		pktAks.setKdPkt(tbKodePaket.getValue());
		pktAks.setNamaPkt(tbNamaPaket.getValue());
		pktAks.setTrioH100DtlpktaksPartsSet(setDtlPart);
		pktAks.setTrioH100DtlpktaksItemSet(setDtlItem);

		String kdPktAks = getMasterFacade().getTrioH100PktaksDao().savePaketAks(pktAks, getUserSession());
		alert("Berhasil Simpan Bundling Aksesoris Dengan Kode [" + kdPktAks +"]");
		disabledComponent();

	}


	public void disabledComponent(){
		tbNamaPaket.setDisabled(true);
		tbNoPart.setDisabled(true);
		btnLovPart.setDisabled(true);
		btnLovItem.setDisabled(true);
		btnAddPart.setDisabled(true);
		btnSimpan.setDisabled(true);

	}

	public void onClick$btnBaru(){
		String str = getMasterFacade().getHondaH000SetupsDao().findValCharByValIdValTag("BDLAKS", TrioDateConv.format(new Date(),"yyyy"));
		if(str == null){
			str = getMasterFacade().getHondaH000SetupsDao().getRunningNumber("BDLAKS", TrioDateConv.format(new Date(),"yyyy"), getUserSession()); 
		}
		int x = Integer.parseInt(str);
		x = x + 1 ;
		str = String.valueOf(x);

		String kdPktAks = TrioStringUtil.getFormattedRunno2("BDLAKS", Integer.valueOf(str));
		tbKodePaket.setValue(kdPktAks);

		listPartHelper = new ArrayList<TrioBeanPartHelper>();
		listItem = new ArrayList<HondaH000Items>();

		cbCek.setChecked(false);

		tbNamaPaket.setValue("");
		tbNoPart.setValue(null);
		tbNamaPart.setValue(null);

		tbNoPart.setDisabled(false);
		tbNamaPaket.setDisabled(false);
		btnLovPart.setDisabled(false);
		btnLovItem.setDisabled(true);
		btnAddPart.setDisabled(true);
		btnSimpan.setDisabled(false);

		lbPart.clearSelection();
		lbPart.setModel(new ListModelList());

		lbItem.clearSelection();
		lbItem.setModel(new ListModelList());
	}
}
