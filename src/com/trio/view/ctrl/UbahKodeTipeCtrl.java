package com.trio.view.ctrl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h100.HondaH100Stdetails;
import com.trio.bean.h100.TrioH100Bundlingaks;
import com.trio.bean.h100.TrioH100Dtlbundlingaks;
import com.trio.bean.h100.TrioH100DtlpktaksItem;
import com.trio.bean.h100.TrioH100DtlpktaksPart;
import com.trio.bean.h300.HondaH300DetInoutManuals;
import com.trio.bean.h300.HondaH300InoutManuals;
import com.trio.bean.h300.HondaH300PartLokases;
import com.trio.bean.helper.TrioBeanUbahTipeHelper;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.util.TrioDateConv;
import com.trio.util.TrioDateUtil;
import com.trio.util.TrioStringUtil;

/**
 * @author Gusti Arya 9:24:09 AM Dec 16, 2013
 */

public class UbahKodeTipeCtrl extends TrioBasePage{

	private static final long serialVersionUID = 1L;
	
	private static final Logger logger = Logger.getLogger(UbahKodeTipeCtrl.class);

	private Textbox tbNoBundling;
	private Textbox tbNoReference;
	private Textbox tbNoMesin;
	private Textbox tbNoRangka;
	private Textbox tbLokasi;
	private Textbox tbKdItemAsal;
	private Textbox tbKdPktAks;
	private Textbox tbNamaPktAks;
	private Textbox tbTotalUnit;
	private Datebox tbTanggal;

	private Listbox lbKdItem;
	private Listbox lbKdpktAks;
	private Listbox lbMain;

	private Button btnSimpan;

	private Popup popUpNoMesin;
	private Popup popUpKdPktAks;
	private HondaH100Stdetails details;
	Map<String, Integer> mapPart;
	Map<String, Integer> mapPartQty;
	Map<String, Integer> mapSeq;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		mapPart= new HashMap<String, Integer>();
		mapPartQty = new HashMap<String, Integer>();
		mapSeq = new HashMap<String, Integer>();
		String str = getMasterFacade().getHondaH000SetupsDao().findValCharByValIdValTag("BDL", TrioDateConv.format(new Date(),"yyyy"));
		if(str == null){
			str = getMasterFacade().getHondaH000SetupsDao().getRunningNumber("BDL", TrioDateConv.format(new Date(),"yyyy"), getUserSession()); 
		}
		int x = Integer.parseInt(str);
		x = x + 1 ;
		str = String.valueOf(x);
		String kdBundling = TrioStringUtil.getFormattedRunno4("BDL", Integer.valueOf(str));

		tbNoBundling.setValue(kdBundling);
		tbTanggal.setValue(new Date());
	}

	public void onChange$tbNoMesin(){
		try {
			details = getMasterFacade().getHondaH100StdetailsDao().findByNoMesinStatusA(tbNoMesin.getValue());
			tbNoRangka.setValue(details.getNoRangka());
			tbLokasi.setValue(details.getLokasi());
			tbKdItemAsal.setValue(details.getKdItem());
		} catch (NullPointerException e) {
			alert("No Mesin tidak Ditemukan");
		}

		tbKdPktAks.setValue(null);
		tbNamaPktAks.setValue(null);
	}

	public void onClick$btnLov(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		List<HondaH100Stdetails> list = getMasterFacade().getHondaH100StdetailsDao().findNoMesinStatusA();
		lbKdItem.setModel(new ListModelList(list));
		lbKdItem.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				HondaH100Stdetails details = (HondaH100Stdetails) data;
				item.setValue(details);

				new Listcell(details.getNoMesin()).setParent(item);
				new Listcell(details.getNoRangka()).setParent(item);
				new Listcell(details.getKdItem()).setParent(item);
				new Listcell(details.getLokasi()).setParent(item);
			}
		});

		tbKdPktAks.setValue(null);
		tbNamaPktAks.setValue(null);
	}

	public void onSelect$lbKdItem(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		details = (HondaH100Stdetails) lbKdItem.getSelectedItem().getValue();
		tbNoMesin.setValue(details.getNoMesin());
		tbNoRangka.setValue(details.getNoRangka());
		tbLokasi.setValue(details.getLokasi());
		tbKdItemAsal.setValue(details.getKdItem());

		popUpNoMesin.close();
	}

	public void onClick$btnLovKdPktAks(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);

		List<TrioH100DtlpktaksItem> listItem = getMasterFacade().getTrioH100DtlpktaksItemDao().findBykdItem(tbKdItemAsal.getValue());
		lbKdpktAks.setModel(new ListModelList(listItem));
		lbKdpktAks.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				TrioH100DtlpktaksItem itemAks = (TrioH100DtlpktaksItem) data;
				item.setValue(itemAks);

				new Listcell(itemAks.getKdPkt()).setParent(item);
				new Listcell(itemAks.getTrioH100Pktaks().getNamaPkt()).setParent(item);
			}
		});

	}

	public void onSelect$lbKdpktAks(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		TrioH100DtlpktaksItem item = (TrioH100DtlpktaksItem) lbKdpktAks.getSelectedItem().getValue();

		tbKdPktAks.setValue(item.getKdPkt());
		tbNamaPktAks.setValue(item.getTrioH100Pktaks().getNamaPkt());

		popUpKdPktAks.close();

	}

	@SuppressWarnings("unchecked")
	public void onClick$btnAdd(){
		List<TrioBeanUbahTipeHelper> lists = (List<TrioBeanUbahTipeHelper>) lbMain.getModel();
		if(lists == null){
			lists = new ArrayList<TrioBeanUbahTipeHelper>();
		}

		for(TrioBeanUbahTipeHelper current : lists){
			if(current.getNoMesin().equalsIgnoreCase(tbNoMesin.getValue())){
				alert("No Mesin tidak Boleh Sama");
				return;
			}
		}

		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);

		//mengecek apakah nomesin yang ingin di bundling sudah ada di pickinglist, jika sudah maka tolak
		List<String> listNosinX = getMasterFacade().getHondaH100DtlpicklistsDao().findNoMesinStatusX();
		if(listNosinX.contains(tbNoMesin.getValue().trim())){
			alert("No Mesin terdapat di Picking List");
			return;
		}

		String gudang = getMasterFacade().getHondaH000SetupsDao().getGudang();
		String rak = getMasterFacade().getHondaH000SetupsDao().getRak();
		System.out.println("INI GUDANG = " + gudang);
		System.out.println("INI RAK = " + rak);

		List<TrioH100DtlpktaksPart> listDetailPart = getMasterFacade().getTrioH100DtlpktaksPartDao().findByKdPkt(tbKdPktAks.getValue());

		for(TrioH100DtlpktaksPart part : listDetailPart){
			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
			String partNo = part.getTrioH100DtlpktaksPartPK().getPartNo();
			int qty = part.getQty();
			System.out.println("ini qty = " + qty);

			if(mapPart.containsKey(partNo)){
				int awal = mapPart.get(partNo);
				int input = mapPart.put(partNo, qty + awal);
				System.out.println("dari map if = " + input);
			}else{
				mapPart.put(partNo, qty);
				System.out.println("dari map di else = " + mapPart.get(partNo));
			}

			DatabaseContextHolder.setConnectionType(ConnectionType.PART);
			HondaH300PartLokases lokases = getMasterFacade().getHondaH300PartLokasesDao()
					.findByGudRakNoObj(gudang, rak, partNo);

			if(!partNo.equalsIgnoreCase("X")){
				if(lokases == null){
					alert("Stock Tidak Ditemukan di Tabel HONDA_H300_PARTLOKASES");
					return;
				}

				int x = mapPart.get(partNo);
				if(lokases.getQtyOnh() < x){
					alert("Stock QTY_ONH Tidak Mencukupi");
					return;
				}

			}

			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		}

		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		//		final String getKdItem = getMasterFacade().getHondaH000SetupsDao()
		//				.findValCharByValIdValTag("H1_VARIASI_UNIT", details.getKdItem().substring(0,3));


		//		final String getKdItem = getMasterFacade().getHondaH000SetupsDao()
		//				.findValTagByValIdValChar("H1_VARIASI_UNIT", details.getKdItem().substring(0,3));

		TrioH100DtlpktaksItem item = getMasterFacade().getTrioH100DtlpktaksItemDao()
				.getKdItemBaru(tbKdItemAsal.getValue(), tbKdPktAks.getValue());

		List<TrioH100DtlpktaksPart> listPart = getMasterFacade().getTrioH100DtlpktaksPartDao().findByKdPkt(tbKdPktAks.getValue());
		String discount = getMasterFacade().getHondaH000SetupsDao()
				.findValCharByValIdValTag("H1_CONFIG_VARIASI", "DISCOUNT");

		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		for(TrioH100DtlpktaksPart part : listPart){
			TrioBeanUbahTipeHelper helper= new TrioBeanUbahTipeHelper();

			BigDecimal het = new BigDecimal(0);

			if(!part.getTrioH100DtlpktaksPartPK().getPartNo().equalsIgnoreCase("X")){
				het = getMasterFacade().getHondaH300MasterPartsDao().getHetByPartNO(part.getTrioH100DtlpktaksPartPK().getPartNo());
			}

			helper.setNoMesin(tbNoMesin.getValue());
			helper.setNoRangka(tbNoRangka.getValue());
			helper.setLokasi(tbLokasi.getValue());
			helper.setKodePaket(tbKdPktAks.getValue());
			helper.setKodeItemAsal(tbKdItemAsal.getValue());
			helper.setKodeItemBaru(item.getKdItemBaru());
			helper.setNoPart(part.getTrioH100DtlpktaksPartPK().getPartNo());
			helper.setQty(part.getQty());
			helper.setHargaNett(het.subtract((het.multiply(new BigDecimal(discount)))));
			lists.add(helper);
		}
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);

		lbMain.setModel(new ListModelList(lists));
		lbMain.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object data) throws Exception {
				TrioBeanUbahTipeHelper helper = (TrioBeanUbahTipeHelper) data;
				item.setValue(helper);

				new Listcell(helper.getNoMesin()).setParent(item);
				new Listcell(helper.getNoRangka()).setParent(item);
				new Listcell(helper.getLokasi()).setParent(item);
				new Listcell(helper.getKodePaket()).setParent(item);
				new Listcell(helper.getKodeItemAsal()).setParent(item);
				new Listcell(helper.getKodeItemBaru()).setParent(item);
				new Listcell(helper.getNoPart()).setParent(item);
				new Listcell(helper.getQty().toString()).setParent(item);

				DecimalFormat df = new DecimalFormat("###,##0.00");
				String str = df.format(helper.getHargaNett());
				new Listcell(str).setParent(item);
			}
		});

		ListModelList lm = (ListModelList) lbMain.getListModel();
		List<TrioBeanUbahTipeHelper> list = lm.getInnerList();

		Set<TrioBeanUbahTipeHelper> setNew = new HashSet<TrioBeanUbahTipeHelper>(list);
		tbTotalUnit.setValue(String.valueOf(setNew.size()));
	}


	@SuppressWarnings("unchecked")
	public void onClick$btnSimpan(){
		//ambil nilai gudang dan rak
		logger.info("btnSimpan diklik");
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String gudang = getMasterFacade().getHondaH000SetupsDao().getGudang();
		logger.info("gudang = " + gudang);
		String rak = getMasterFacade().getHondaH000SetupsDao().getRak();
		logger.info("rak = " + rak);
		//ambil running number untuk TRANS_NO di tabel HONDA_H300_INOUT_MANUALS 
		DatabaseContextHolder.setConnectionType(ConnectionType.PART);
		String str = getMasterFacade().getHondaH000SetupsDao().getRunningNumber("H3_NO_OMOT", TrioDateConv.format(new Date(),"yyyy"), getUserSession());
		String partInOut = TrioStringUtil.getFormattedRunno3("OM-OT", Integer.valueOf(str));

		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		TrioH100Bundlingaks bundling = new TrioH100Bundlingaks();
		HondaH300InoutManuals inOut = new HondaH300InoutManuals();

		//mengambil isi dari ListBox kemudian dimasukkan ke dalam variable list
		ListModelList lm = (ListModelList) lbMain.getListModel();
		List<TrioBeanUbahTipeHelper> list = lm.getInnerList();

		//membuat object dtlBundling dan dtlInout dan object Set
		TrioH100Dtlbundlingaks dtlBundling;
		HondaH300DetInoutManuals dtlInOut;
		Set<TrioH100Dtlbundlingaks> setDtlBundling = new HashSet<TrioH100Dtlbundlingaks>();
		Set<HondaH300DetInoutManuals> setDtlinOut = new HashSet<HondaH300DetInoutManuals>();

		//integer X untuk input field seq di table HONDA_H300_DET_INOUTMANUALS
		int x = 1;

		//mentotal jumlah quantity tiap part yang ada di listbox
		for(TrioBeanUbahTipeHelper current : list){
			if(mapPartQty.containsKey(current.getNoPart())){
				int awal = mapPartQty.get(current.getNoPart());
				mapPartQty.put(current.getNoPart(), current.getQty() + awal);
			}else{
				System.out.println("MASUK KE DALAM ELSE CONTAINS");
				mapPartQty.put(current.getNoPart(), current.getQty());
			}
		}

		//Iterasi object list
		for(TrioBeanUbahTipeHelper current : list){
			dtlBundling = new TrioH100Dtlbundlingaks();
			dtlBundling.getTrioH100DtlbundlingaksPK().setNoBundling(tbNoBundling.getValue());
			dtlBundling.getTrioH100DtlbundlingaksPK().setNoMesin(current.getNoMesin());
			dtlBundling.setKdPkt(current.getKodePaket());
			dtlBundling.setKdItemAsal(current.getKodeItemAsal());
			dtlBundling.setNoRangka(current.getNoRangka());
			dtlBundling.setLokasi(current.getLokasi());
			dtlBundling.setKdItemBaru(current.getKodeItemBaru());
			dtlBundling.getTrioH100DtlbundlingaksPK().setPartNo(current.getNoPart());
			dtlBundling.setQty(current.getQty());
			dtlBundling.setHargaNett(current.getHargaNett());
			dtlBundling.getUserTrailing().setDcrea(TrioDateUtil.getLongSysDateOracle());
			dtlBundling.getUserTrailing().setVcreaby(getUserSession());
			setDtlBundling.add(dtlBundling);

			dtlInOut = new HondaH300DetInoutManuals();
			DatabaseContextHolder.setConnectionType(ConnectionType.PART);
			BigDecimal hargaPokok = getMasterFacade().getHondaH300MasterPartsDao().getHargaPokokByPartNo(current.getNoPart());
			//set hargaPokok di object dtlInout
			dtlInOut.getHondaH300DetInoutManualsPK().setInoutTransNo(partInOut);
			//set gudang di object dltInOUt
			dtlInOut.setRakLokLokasiKodeLokasi(gudang);
			//set rak di object dtlInOut
			dtlInOut.setRakLokRakNoRak(rak);
			//set nomor part di object dtlInOut
			dtlInOut.setPartNo(current.getNoPart());
			dtlInOut.setRemark(tbNoBundling.getValue());

			//jika dalam list terdapat nomor part yang sama, ambil qty di urutan pertama masukkan ke object awal
			//object awal tadi kemudian ditambah dengan jumlah qty di nomor part berikutnya di dalam list
			if(mapPartQty.containsKey(current.getNoPart())){
				dtlInOut.setQty(mapPartQty.get(current.getNoPart()));
			}

			//i don't know why i retrieve value from mapPart instead mapPartQty LOL!!
			//			int y = mapPart.get(current.getNoPart());
			//			System.out.print("ini nomor part nya = " + current.getNoPart());
			//			System.out.println(" ini quantity y diluar if = " + y);

			//set Quantity Value
			//			dtlInOut.setQty(y);

			//set HP value
			dtlInOut.setHp(hargaPokok);
			dtlInOut.getUserTrailing().setVcreaby(getUserSession());

			if(mapSeq.containsKey(current.getNoPart())){

			}else{
				mapSeq.put(current.getNoPart(), x++);
			}

			dtlInOut.getHondaH300DetInoutManualsPK().setSeq(mapSeq.get(current.getNoPart()));
			setDtlinOut.add(dtlInOut);
		}

		bundling.setNoBundling(tbNoBundling.getValue());
		bundling.setReference(tbNoReference.getValue());
		bundling.setTglBundling(tbTanggal.getValue());
		bundling.setTrioH100DtlbundlingaksSet(setDtlBundling);

		inOut.setTransNo(partInOut);
		inOut.setTransDate(tbTanggal.getValue());
		inOut.setTransType("O");
		inOut.setCustCode("W8");
		inOut.setTransCode("OT");
		inOut.setRemark(tbNoBundling.getValue());
		inOut.setHondaH300DetInoutManualsSet(setDtlinOut);

		try {
			DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
			String kdBundling = getMasterFacade().getTrioGlobalTransactionDao().save(bundling, inOut, gudang, rak, getUserSession());
			alert("Berhasil Simpan dengan Kode Bundling" + "["+kdBundling+"]");
			btnSimpan.setDisabled(true);
		} catch (Exception e) {
			btnSimpan.setDisabled(true);
			alert("Error Saat Simpan ke Database");
			e.printStackTrace();
		}
		
		btnSimpan.setDisabled(true);

	}

	public void onClick$btnBersihkan(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		btnSimpan.setDisabled(false);
		tbNoMesin.setValue(null);
		tbNoRangka.setValue(null);
		tbLokasi.setValue(null);
		tbKdItemAsal.setValue(null);
		tbKdPktAks.setValue(null);
		tbNamaPktAks.setValue(null);
		tbTotalUnit.setValue(null);
		mapPart = new HashMap<String, Integer>();
		mapPartQty = new HashMap<String, Integer>();
		mapSeq = new HashMap<String, Integer>();
		lbMain.setModel(new ListModelList());
		lbMain.clearSelection();
	}
}
