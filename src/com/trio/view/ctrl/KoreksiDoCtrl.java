package com.trio.view.ctrl;

import java.math.BigDecimal;
import java.util.List;

import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Button;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.ListitemRenderer;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;

import com.trio.base.TrioBasePage;
import com.trio.bean.h100.HondaH100Dtlfakdos;
import com.trio.bean.h100.HondaH100Fakdos;
import com.trio.spring.util.ConnectionType;
import com.trio.spring.util.DatabaseContextHolder;
import com.trio.view.ctrl.renderer.KoreksiDoListRenderer;

public class KoreksiDoCtrl extends TrioBasePage{

	private static final long serialVersionUID = 1L;

	private Textbox tbNoDo, tbNoFaktur, tbNamaDealer;
	private Button btLovDO;

	private Popup popNoDo;
	private Listbox lbPopNoDo;

	private Textbox tbSearchLovDO;
	private Button btSearchLov;

	private Button btTampil;
	private Button btSimpan;
	private Button btRefresh;

	private Listbox lbKoreksiList;

	@Override
	public void doAfterCompose(Component comp) throws Exception {
		super.doAfterCompose(comp);
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		//set disable textbox pada saat pertama kali menjalankan form
		tbNoDo.setDisabled(false);
		tbNoFaktur.setDisabled(true);
		tbNamaDealer.setDisabled(true);
		btSimpan.setDisabled(true);

	}

	//Method yang dijalakan ketika me-klik tombol LOV
	public void onClick$btLovDO(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		List<HondaH100Fakdos> lists = getMasterFacade().getHondaH100FakdosDao().getNoDoByStatus();
		lbPopNoDo.setModel(new ListModelList(lists));
		lbPopNoDo.setItemRenderer(new ListitemRenderer() {

			@Override
			public void render(Listitem item, Object object) throws Exception {
				HondaH100Fakdos fakdos = (HondaH100Fakdos) object;
				item.setValue(fakdos);

				new Listcell(fakdos.getNoDo()).setParent(item);
				new Listcell(fakdos.getNoFaktur()).setParent(item);
				new Listcell(fakdos.getNamaTagihan()).setParent(item);
			}
		});
	}

	//Method yang di jalankan saat button search di LOV di klik
	public void onClick$btSearchLov(){

		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH100Fakdos fak = new HondaH100Fakdos();

		//set value ke object fak yang digunakan sebagai restriksi pada method findByCriteria
		fak.setNoDo(tbSearchLovDO.getValue().trim());
		fak.setStatus("X");
		fak.setDoTahun("2012");

		//panggil method findByCriteria mempass object fak
		List<HondaH100Fakdos> lists = getMasterFacade().getHondaH100FakdosDao().findByCriteria(fak);
		lbPopNoDo.setModel(new ListModelList(lists));

	}


	//methode yang di jalan ketika listbox pada LOV di klik
	public void onSelect$lbPopNoDo(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		HondaH100Fakdos fak = (HondaH100Fakdos) lbPopNoDo.getSelectedItem().getValue();

		//set value textbox berdasarkan listbox yang telah di klik
		tbNoDo.setValue(fak.getNoDo());
		tbNoFaktur.setValue(fak.getNoFaktur());
		tbNamaDealer.setValue(fak.getNamaTagihan());
		popNoDo.close();

	}

	//method yang dijalankan ketika tombol tampil di klik
	public void onClick$btTampil(){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		//memanggil method findBynodo untuk menjalankan query, mempass noDo yang di ambil dari textBox noDo
		List<HondaH100Dtlfakdos>lists = getMasterFacade().getHondaH100DtlfakdosDao()
				.findBynoDo(tbNoDo.getValue().toUpperCase().trim());
		lbKoreksiList.setModel(new ListModelList(lists));

		//panggil kelas KoreksiDoListRender, untuk merender tampilan pada listbox
		lbKoreksiList.setItemRenderer(new KoreksiDoListRenderer());

		btSimpan.setDisabled(false);

	}

	//Method yang di jalankan ketika tombol refresh di klik
	public void onClick$btRefresh(){
		
		//merefresh semua listbox dan textbox kembali seperti pada saat form dijalankan
		tbNoDo.setValue(null);
		tbNoFaktur.setValue(null);
		tbNamaDealer.setValue(null);
		lbKoreksiList.setModel(new ListModelList());
		lbKoreksiList.clearSelection();
		btSimpan.setDisabled(true);
	}

	//Method yang dijalankan ketika tombol simpan di klik
	public void onClick$btSimpan() throws InterruptedException{
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String kdItem = "";
		String hargaStd= "";
		String disc = "";
		String discPersen = "";
		BigDecimal hargaNett = null;


		HondaH100Dtlfakdos dtlFak = new HondaH100Dtlfakdos();
		Listbox box = (Listbox) lbKoreksiList.getFellow("win").getFellow("lbKoreksiList");
		List<Listitem> lists = box.getItems();

		//for untuk membaca per row yang ada di listbox
		for(Listitem currentLists : lists){
			dtlFak = (HondaH100Dtlfakdos) currentLists.getValue();
			List<Component> com = currentLists.getChildren();

			//for untuk membaca komponen disetiap row
			for(Component currentCom : com){
				Listcell lc = (Listcell) currentCom;

				//baris2 if dibawah untuk mengambil value yang ada di setiap column
				if(lc.getColumnIndex()==0){
					kdItem = lc.getLabel();
				}
				if(lc.getColumnIndex()==2){
					hargaStd = lc.getLabel();
					System.out.println("harga std 1 = " + hargaStd);

					if(hargaStd.contains(".")){
						hargaStd = hargaStd.replaceAll("\\.", "");
						System.out.println("harga std 2 " + hargaStd);
					}
				}

				if(lc.getColumnIndex()==3){
					Intbox ib = (Intbox)lc.getFirstChild();
					disc = ib.getValue().toString();
				}

				if(lc.getColumnIndex()==4){
					Decimalbox db = (Decimalbox) lc.getFirstChild();
					discPersen = db.getValue().toString();
				}
				if(lc.getColumnIndex()==5){
					Decimalbox db = (Decimalbox) lc.getFirstChild();
					hargaNett = db.getValue();
				}
				//Inner For
			}
			//Outer For

			//set value pada object
			dtlFak.setFakdoNoDo(tbNoDo.getValue());
			dtlFak.setKdItem(kdItem);
			System.out.println("ini harganett = " + hargaNett);
			dtlFak.setHargaKosong(hargaNett);
			dtlFak.setHargaStd(new BigDecimal(hargaStd));
			dtlFak.setDiscount(new BigDecimal(disc));
			dtlFak.setDiscountPersen(new BigDecimal(discPersen));
			dtlFak.setHargaNetto(hargaNett);

			//update hasil
			getMasterFacade().getHondaH100DtlfakdosDao().updateTransaction(dtlFak, getUserSession());

		}

		//update hasil
		savePpnDppAr(tbNoDo.getValue());
		Messagebox.show("Berhasil Simpan");
	}


	public void savePpnDppAr(String noDo){
		DatabaseContextHolder.setConnectionType(ConnectionType.UNIT);
		String strPpn = getMasterFacade().getHondaH000SetupsDao().findValCharByValIdValTag("H1_CONFIG", "PPN");

		//perhitungan ppn, dpp, dan AR
		BigDecimal ppn = new BigDecimal(strPpn);
		BigDecimal nilaiDpp = getMasterFacade().getHondaH100DtlfakdosDao().getNilaiDppByNoDo(noDo);
		BigDecimal nilaiPpn = (ppn.divide(new BigDecimal(100)).multiply(nilaiDpp));
		BigDecimal nilaiAr = nilaiDpp.add(nilaiPpn);

		//update harga, amount pokok,ppn, dan sisa utang, kedalam tabel yang ditentukan
		getMasterFacade().getHondaH100FakdosDao().updateHargaByNoDo(nilaiAr, noDo);
		getMasterFacade().getHondaH100ArsDao().updateAmtPkokAndAmtPpn(nilaiAr, nilaiPpn, noDo);
		getMasterFacade().getTrioH100ArsDao().updateSisaUtang(nilaiAr, noDo);

	}
}
